#!/bin/bash

echo "
### <=========================================================================================> ###
### ---> Replacing Domain Mailwizz ATMautoresponder.com = turbo-trocadominio.sh ========> ###
### <=========================================================================================> ###
"

Data=`date +%d/%m/%Y-%T`
DominioAntigo=`hostname | cut -d. -f2-6`
ReversoAntigo=`cat /root/scripts-2015/reverso.info`
UsuarioEnvio=`cat /root/scripts-2015/usuarioenvio.info`
SenhaUsuarioEnvio=`cat /root/scripts-2015/senhausuarioenvio.info`
SenhaUsuarioEnvioMysql=`echo -ne $SenhaUsuarioEnvio | base64`
sqlpass=`cat /root/scripts-2015/sqlpass.info`

echo "
This server is configured with the following domain : $DominioAntigo ...
What domain name would you like to replace $DominioAntigo ? "
read DominioNovo

echo "
This server is configured with the following reverseDNS: $ReversoAntigo ...
If you would like to change your current reverseDNS $ReversoAntigo please type it in "
read ReversoNovo

echo "
This script can completely reset the server installation settings in this case
the database and Mailwizz will be completely replaced , you will lose the current data from the server ,
Are you sure you want to delete Mailwizz settings and database ? (yes | no) "
read RedefineMW

echo "
~> Fields of change in $Data : 
~> Domain $DominioAntigo changed too $DominioNovo
~> ReverseDNS $ReversoAntigo changed too $ReversoNovo
### <=========================================================================================> ###
" >> /root/scripts-2015/Readme-2015.info

echo "
### ~> Setting please wait ... 
### <=========================================================================================> ###
"

if [ $RedefineMW = yes ]
then
	rm -rf /var/www/mw/apps/common/config/main-custom.php 
	rm -rf /var/www/mw/*
	unzip -q /root/scripts-2015/backup-local/.Originais/mailwizz.zip -d /var/www/mw/
	rm -rf main-custom.php /var/www/mw/apps/common/config/main-custom.php
	mysql -uroot -p$sqlpass -e "drop database mailwizz;"
	mysql -uroot -p$sqlpass -e "create database mailwizz;"
	mysql -uroot -p$sqlpass mailwizz < /root/scripts-2015/backup-local/.Originais/mailwizz.sql
	mysql -uroot -p$sqlpass mailwizz < /root/scripts-2015/backup-local/.Originais/mw_email_blacklist.sql
chmod 777 /var/www/mw/apps/common/config
chmod 777 /var/www/mw/apps/common/runtime
chmod 777 /var/www/mw/backend/assets/cache
chmod 777 /var/www/mw/customer/assets/cache
chmod 777 /var/www/mw/frontend/assets/cache
chmod 777 /var/www/mw/frontend/assets/files
chmod 777 /var/www/mw/frontend/assets/gallery
chmod 777 /var/www/mw/apps/extensions

echo "<?php defined('MW_PATH') || exit('No direct script access allowed');

    
return array(

    // application components
    'components' => array(
        'db' => array(
            'connectionString'  => 'mysql:host=localhost;dbname=mailwizz',
            'username'          => 'root',
            'password'          => '$sqlpass',
            'tablePrefix'       => 'mw_',
        ),
    ),
);
" > /var/www/mw/apps/common/config/main-custom.php

fi

### ~> INFO

sed -i "s/$ReversoAntigo/$ReversoNovo/g" /root/scripts-2015/reverso.info
sed -i "s/$DominioAntigo/$DominioNovo/g" /root/scripts-2015/dominio.info
sed -i "s/$ReversoAntigo/$ReversoNovo/g" /root/scripts-2015/Readme-2015.info
sed -i "s/$DominioAntigo/$DominioNovo/g" /root/scripts-2015/Readme-2015.info

### ~> DNS

mv /var/named/chroot/var/named/$DominioAntigo.db /var/named/chroot/var/named/$DominioNovo.db
sed -i "s/$ReversoAntigo/$ReversoNovo/g" /var/named/chroot/var/named/$DominioNovo.db
sed -i "s/$DominioAntigo/$DominioNovo/g" /var/named/chroot/var/named/$DominioNovo.db
sed -i "s/$DominioAntigo/$DominioNovo/g" /var/named/chroot/etc/named.rfc1912.zones
sed -i '/DKIM/d' /var/named/chroot/var/named/$DominioNovo.db

/usr/sbin/opendkim-genkey -d $DominioNovo
cat default.txt >> /var/named/chroot/var/named/$DominioNovo.db
echo "" >> /var/named/chroot/var/named/$DominioNovo.db
rm -rf default.txt

service named restart

### ~> MYSQL

mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_bounce_server SET hostname = '$Dominio';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_bounce_server SET email = 'return@$Dominio';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET hostname = '$Dominio';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET username = '$UsuarioEnvio';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET password = '$SenhaUsuarioEnvio';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET from_email = '$UsuarioEnvio@$Dominio';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET from_name = '$UsuarioEnvio';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET reply_to_email = '$UsuarioMonitoramento';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET port = '2525';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_delivery_server SET status = 'active';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_customer SET first_name = '$firstname';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_customer SET last_name = '$lastname';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_customer SET email = '$adminemail';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_user SET first_name = '$firstname';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_user SET last_name = '$lastname';"
mysql -uroot -p$sqlpass mailwizz -e "UPDATE mw_user SET email = '$adminemail';"

service mysqld restart

### ~> APACHE

mv /etc/httpd/conf.d/$DominioAntigo.conf /etc/httpd/conf.d/$DominioNovo.conf
sed -i "s/$DominioAntigo/$DominioNovo/g" /var/www/index.html
sed -i "s/$DominioAntigo/$DominioNovo/g" /home/$UsuarioEnvio/websites/index.html
sed -i "s/$DominioAntigo/$DominioNovo/g" /var/www/turbo/admin/includes/config.php
sed -i "s/$DominioAntigo/$DominioNovo/g" /etc/httpd/conf.d/$DominioNovo.conf
sed -i "s/$DominioAntigo/$DominioNovo/g" /etc/httpd/conf/httpd.conf
sed -i "s/$DominioAntigo/$DominioNovo/g" /etc/squirrelmail/config.php
rm -rf /var/log/httpd/$DominioAntigo*

service httpd restart

### ~> DOVECOT

sed -i "s/$DominioAntigo/$DominioNovo/g" /etc/dovecot.conf

service dovecot restart

### ~> POSTFIX

sed -i "s/$DominioAntigo/$DominioNovo/g" /etc/postfix/main.cf

service postfix restart

### ~> PMTA

sed -i "s/$ReversoAntigo/$ReversoNovo/g" /etc/pmta/config
sed -i "s/$DominioAntigo/$DominioNovo/g" /etc/pmta/config
mv default.private /etc/pmta/$DominioNovo-dkim.key
chown pmta:pmta /etc/pmta/ -R

service pmta restart

### ~> BACKUP

ls /root/scripts-2015/hostftp.info

if [ $? = 0 ]
then

HostFtp=`cat /root/scripts-2015/hostftp.info`
UsuarioFtp=`cat /root/scripts-2015/usuarioftp.info`
SenhaUsuarioFtp=`cat /root/scripts-2015/senhausuarioftp.info`

/usr/bin/ftp -in << EOF
open $HostFtp
user $UsuarioFtp $SenhaUsuarioFtp
bin
mkdir backup-$Dominio
bye
EOF

fi

echo "
### ---> ...THIS IS YOUR NEW DNS INFORMATION PLEASE REPLACE YOUR OLD DNS INFORMATION IN THE README FILE WITH THIS...
### <=========================================================================================> ###
"
cat /var/named/chroot/var/named/$DominioNovo.db

echo "
### ---> Synchronizing data , wiping installation, wait ...
### <=========================================================================================> ###
" 
/usr/sbin/ntpdate -u pool.ntp.br >> /dev/null 2>&1 || /usr/bin/rdate -s rdate.cpanel.net >> /dev/null 2>&1
echo server.$DominioNovo > /proc/sys/kernel/hostname
hostname server.$DominioNovo
updatedb

echo "
### ---> Restarting Server ...
### <=========================================================================================> ###
" 

shutdown -r now






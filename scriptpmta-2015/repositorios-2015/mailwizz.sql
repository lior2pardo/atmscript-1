-- phpMyAdmin SQL Dump
-- version 4.2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 22, 2016 at 12:44 AM
-- Server version: 5.1.73
-- PHP Version: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mailwizz`
--

-- --------------------------------------------------------

--
-- Table structure for table `mw_article`
--

CREATE TABLE IF NOT EXISTS `mw_article` (
`article_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `status` char(15) NOT NULL DEFAULT 'published',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_article_category`
--

CREATE TABLE IF NOT EXISTS `mw_article_category` (
`category_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `description` text,
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_article_to_category`
--

CREATE TABLE IF NOT EXISTS `mw_article_to_category` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mw_bounce_server`
--

CREATE TABLE IF NOT EXISTS `mw_bounce_server` (
`server_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `hostname` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `service` enum('imap','pop3') NOT NULL DEFAULT 'imap',
  `port` int(5) NOT NULL DEFAULT '143',
  `protocol` enum('ssl','tls','notls') NOT NULL DEFAULT 'notls',
  `validate_ssl` enum('yes','no') NOT NULL DEFAULT 'no',
  `locked` enum('yes','no') NOT NULL DEFAULT 'no',
  `disable_authenticator` varchar(50) DEFAULT NULL,
  `search_charset` varchar(50) NOT NULL DEFAULT 'UTF-8',
  `delete_all_messages` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_bounce_server`
--

INSERT INTO `mw_bounce_server` (`server_id`, `customer_id`, `hostname`, `username`, `password`, `email`, `service`, `port`, `protocol`, `validate_ssl`, `locked`, `disable_authenticator`, `search_charset`, `delete_all_messages`, `status`, `date_added`, `last_updated`) VALUES
(1, NULL, 'a-ddiction.press', 'admin', 'admin*p5347*vip', 'return@a-ddiction.press', 'imap', 143, 'tls', 'no', 'no', '', 'UTF-8', 'yes', 'active', '2016-03-17 20:34:10', '2016-12-12 10:54:57');

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign`
--

CREATE TABLE IF NOT EXISTS `mw_campaign` (
`campaign_id` int(11) NOT NULL,
  `campaign_uid` char(13) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `type` char(15) NOT NULL DEFAULT 'regular',
  `name` varchar(255) NOT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_email` varchar(100) NOT NULL,
  `to_name` varchar(255) NOT NULL DEFAULT '[EMAIL]',
  `reply_to` varchar(100) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `send_at` datetime DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `delivery_logs_archived` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` char(15) NOT NULL DEFAULT 'draft',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `mw_campaign`
--

INSERT INTO `mw_campaign` (`campaign_id`, `campaign_uid`, `customer_id`, `list_id`, `segment_id`, `group_id`, `type`, `name`, `from_name`, `from_email`, `to_name`, `reply_to`, `subject`, `send_at`, `started_at`, `finished_at`, `delivery_logs_archived`, `status`, `date_added`, `last_updated`) VALUES
(2, 'cv8021p9ldc09', 1, 2, NULL, NULL, 'autoresponder', 'MainList #1(Deleted)', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-11-09 08:26:59', '2016-12-18 10:16:05', NULL, 'no', 'pending-delete', '2016-11-09 08:26:59', '2016-12-22 10:50:52'),
(3, 'gl340xvlsp16e', 1, 2, NULL, NULL, 'autoresponder', 'MainList #2', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-11-09 09:24:48', '2016-12-22 10:59:00', NULL, 'no', 'sending', '2016-11-09 09:24:41', '2016-12-22 11:44:01'),
(4, 'zq062ertb4a34', 1, 2, NULL, NULL, 'autoresponder', 'MainList #3', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-11-09 09:49:01', '2016-12-22 10:59:56', NULL, 'no', 'sending', '2016-11-09 09:48:53', '2016-12-22 11:44:01'),
(5, 'zm959raj0g771', 1, 2, NULL, NULL, 'autoresponder', 'MainList #4', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-11-09 09:51:12', '2016-12-22 11:00:37', NULL, 'no', 'sending', '2016-11-09 09:51:05', '2016-12-22 11:44:01'),
(7, 'qx8982pt0m9a2', 1, 3, NULL, NULL, 'autoresponder', 'Day #0', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'This is the email address you registered correct? [EMAIL] ', '2016-11-09 10:08:27', '2016-12-22 10:55:19', NULL, 'no', 'sending', '2016-11-09 10:08:27', '2016-12-22 11:44:02'),
(8, 'qj304oezt0fb5', 1, 3, NULL, NULL, 'autoresponder', 'Day #1', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], Can you [RANDOM_CONTENT:call |reply |answer ]me', '2016-11-09 10:11:19', '2016-12-22 11:02:04', NULL, 'no', 'sending', '2016-11-09 10:11:19', '2016-12-22 11:44:02'),
(9, 'rf501s8zrl6d8', 1, 3, NULL, NULL, 'autoresponder', 'Day #2', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], I had some [RANDOM_CONTENT:questions|answers|tips] for you .', '2016-11-09 10:12:19', '2016-12-22 11:03:24', NULL, 'no', 'sending', '2016-11-09 10:12:19', '2016-12-22 11:44:02'),
(10, 'hd835zc2tkac3', 1, 3, NULL, NULL, 'autoresponder', 'Day #3', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[RANDOM_CONTENT:Follow up|email|followup] messages go to [EMAIL]  , yes?', '2016-11-09 10:13:19', '2016-12-22 11:04:36', NULL, 'no', 'sending', '2016-11-09 10:13:19', '2016-12-22 11:44:02'),
(11, 'lc187v3zn5491', 1, 3, NULL, NULL, 'autoresponder', 'Day #4', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'Do you [RANDOM_CONTENT:need|want|check] the URL?', '2016-11-09 10:14:21', '2016-12-22 11:05:34', NULL, 'no', 'sending', '2016-11-09 10:14:21', '2016-12-22 11:44:02'),
(12, 'mw9784gvqh798', 1, 3, NULL, NULL, 'autoresponder', 'Day #5', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'Can we [RANDOM_CONTENT:setup|make|create] the [RANDOM_CONTENT:automation process|automation|system] for [EMAIL] ?', '2016-11-09 10:15:22', '2016-12-22 11:07:21', NULL, 'no', 'sending', '2016-11-09 10:15:22', '2016-12-22 11:44:02'),
(13, 'vg459vnaho97d', 1, 3, NULL, NULL, 'autoresponder', 'Day #6', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[RANDOM_CONTENT:Should|May|Can]  we send new information here [EMAIL] ?', '2016-11-09 10:16:22', '2016-12-22 11:08:34', NULL, 'no', 'sending', '2016-11-09 10:16:22', '2016-12-22 11:44:02'),
(14, 'fm29786va1762', 1, 3, NULL, NULL, 'autoresponder', 'Day #7', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'Does this [RANDOM_CONTENT:address|email|email address] work? [EMAIL]', '2016-11-09 10:17:23', '2016-12-22 11:09:39', NULL, 'no', 'sending', '2016-11-09 10:17:23', '2016-12-22 11:44:02'),
(15, 'ka53270kog640', 1, 3, NULL, NULL, 'autoresponder', 'Day #8', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'Can you send me a [RANDOM_CONTENT:quick|fast|immediate] [RANDOM_CONTENT:reply|email|message] ?', '2016-11-09 10:18:23', '2016-12-22 11:11:26', NULL, 'no', 'sending', '2016-11-09 10:18:23', '2016-12-22 11:44:02'),
(16, 'bk669vpkxec26', 1, 3, NULL, NULL, 'autoresponder', 'Day #9', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:This is|This really is|That is] [RANDOM_CONTENT:only for|just for|limited to] the [RANDOM_CONTENT:serious|smart ones]', '2016-11-09 10:19:24', '2016-12-22 11:12:11', NULL, 'no', 'sending', '2016-11-09 10:19:24', '2016-12-22 11:44:02'),
(17, 'rk610ey8bc7e7', 1, 3, NULL, NULL, 'autoresponder', 'Day #10', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Can we|Should we|Could we] [RANDOM_CONTENT:set up|install|setup|create] the automation [RANDOM_CONTENT:process|method] for [EMAIL] ?', '2016-11-09 10:20:26', '2016-12-22 11:13:05', NULL, 'no', 'sending', '2016-11-09 10:20:26', '2016-12-22 11:44:02'),
(18, 'te387cf24nf94', 1, 3, NULL, NULL, 'autoresponder', 'Day #11', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME],  What are you [RANDOM_CONTENT:looking|searching|waiting] for?', '2016-11-09 10:21:28', '2016-12-22 11:14:30', NULL, 'no', 'sending', '2016-11-09 10:21:28', '2016-12-22 11:44:02'),
(19, 'wl334crvsf2a1', 1, 3, NULL, NULL, 'autoresponder', 'Day #12', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], Can you copy rinse repeat?', '2016-11-09 10:22:29', '2016-12-22 11:15:14', NULL, 'no', 'sending', '2016-11-09 10:22:29', '2016-12-22 11:44:02'),
(20, 'kv276smrhk03b', 1, 3, NULL, NULL, 'autoresponder', 'Day #13', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[EMAIL] registered successfully', '2016-11-09 10:23:29', '2016-12-22 11:17:07', NULL, 'no', 'sending', '2016-11-09 10:23:29', '2016-12-22 11:44:02'),
(21, 'ec778pd4pgf8d', 1, 3, NULL, NULL, 'autoresponder', 'Day #14', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[RANDOM_CONTENT:We''re|We are] launching [RANDOM_CONTENT:a new|a brand new|a fresh] [RANDOM_CONTENT:site|website]', '2016-11-09 10:24:30', '2016-12-22 11:17:55', NULL, 'no', 'sending', '2016-11-09 10:24:30', '2016-12-22 11:44:02'),
(22, 'dy266583s3c82', 1, 3, NULL, NULL, 'autoresponder', 'Day #15', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], You should do it!', '2016-11-09 10:25:30', '2016-12-22 11:18:44', NULL, 'no', 'sending', '2016-11-09 10:25:30', '2016-12-22 11:44:02'),
(23, 'go009omy3af45', 1, 3, NULL, NULL, 'autoresponder', 'Day #16', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[RANDOM_CONTENT:3|4|5|6|2|7|8] counter intuitive [RANDOM_CONTENT:secrets|strategies|techniques] about [RANDOM_CONTENT:earning|making|getting] more', '2016-11-09 10:26:31', '2016-12-22 11:24:26', NULL, 'no', 'sending', '2016-11-09 10:26:31', '2016-12-22 11:44:02'),
(24, 'll243q8w1p1ab', 1, 3, NULL, NULL, 'autoresponder', 'Day #17', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'Messages for registered address [EMAIL] ', '2016-11-09 10:27:33', '2016-12-22 11:25:10', NULL, 'no', 'sending', '2016-11-09 10:27:33', '2016-12-22 11:44:02'),
(25, 'se01987r64abf', 1, 3, NULL, NULL, 'autoresponder', 'Day #18', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME] can you [RANDOM_CONTENT:make|do] [RANDOM_CONTENT:it|this|that] ? ', '2016-11-09 10:28:34', '2016-12-22 11:25:48', NULL, 'no', 'sending', '2016-11-09 10:28:34', '2016-12-22 11:44:02'),
(26, 'nx7612nlkn744', 1, 3, NULL, NULL, 'autoresponder', 'Day #19', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-11-09 10:29:35', '2016-12-22 11:26:30', NULL, 'no', 'sending', '2016-11-09 10:29:35', '2016-12-22 11:44:02'),
(27, 'lb509985hff4f', 1, 3, NULL, NULL, 'autoresponder', 'Day #20', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], What''s the [RANDOM_CONTENT:delay|hold off|postpone|wait|hold up|hesitate|holdup]? ', '2016-11-09 10:30:35', '2016-12-22 11:27:15', NULL, 'no', 'sending', '2016-11-09 10:30:35', '2016-12-22 11:44:02'),
(28, 'mp870809b142a', 1, 3, NULL, NULL, 'autoresponder', 'Day #21', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'Re: [RANDOM_CONTENT:Give|Provide] me [RANDOM_CONTENT:more information|extra information|more details|more info|additional information]', '2016-11-09 10:31:36', '2016-12-22 11:28:07', NULL, 'no', 'sending', '2016-11-09 10:31:36', '2016-12-22 11:44:02'),
(29, 'bf522e77nha8d', 1, 3, NULL, NULL, 'autoresponder', 'Day #22', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[RANDOM_CONTENT:Stop|End] [RANDOM_CONTENT:wasting|squandering] time on [RANDOM_CONTENT:minor|small|slight|modest] [RANDOM_CONTENT:life|living] [RANDOM_CONTENT:decisions|choices|conclusions]', '2016-11-09 10:32:37', '2016-12-22 11:28:48', NULL, 'no', 'sending', '2016-11-09 10:32:37', '2016-12-22 11:44:02'),
(30, 'sf858kby3o982', 1, 3, NULL, NULL, 'autoresponder', 'Day #23', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:You should|You need to|You ought to|You must] [RANDOM_CONTENT:do it|get it done|take action]', '2016-11-09 10:33:38', '2016-12-22 11:29:31', NULL, 'no', 'sending', '2016-11-09 10:33:38', '2016-12-22 11:44:02'),
(31, 'pq6095y092dbd', 1, 3, NULL, NULL, 'autoresponder', 'Day #24', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[RANDOM_CONTENT:Yeah|Yes|Yep|Yup] [RANDOM_CONTENT:here''s|here is|this is|the following is|and here is] [RANDOM_CONTENT:how it works|how it operates|the ins and outs|the way it operates|the way it works]', '2016-11-09 10:34:38', '2016-12-22 11:30:22', NULL, 'no', 'sending', '2016-11-09 10:34:38', '2016-12-22 11:44:02'),
(32, 'jw580gkyd5238', 1, 3, NULL, NULL, 'autoresponder', 'Day #25', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Nothing like|Nothing beats|Nothing can beat] [RANDOM_CONTENT:it|this|that]', '2016-11-09 10:35:39', '2016-12-22 11:31:48', NULL, 'no', 'sending', '2016-11-09 10:35:39', '2016-12-22 11:44:02'),
(33, 'yz925p80fj39d', 1, 3, NULL, NULL, 'autoresponder', 'Day #26', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', 'Do you [RANDOM_CONTENT:need|wait|want] the [RANDOM_CONTENT:URL|Link|email] ? ', '2016-11-09 10:36:41', '2016-12-22 11:32:39', NULL, 'no', 'sending', '2016-11-09 10:36:41', '2016-12-22 11:44:02'),
(34, 'my236qzq9g2a4', 1, 3, NULL, NULL, 'autoresponder', 'Day #27', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[RANDOM_CONTENT:How to work|Working] [RANDOM_CONTENT:smarter|wiser|better|cleverer|smart], [RANDOM_CONTENT:not|not really|not necessarily|certainly not|definitely not] [RANDOM_CONTENT:harder|tougher|more difficult|more challenging]', '2016-11-09 10:37:41', '2016-12-22 11:33:23', NULL, 'no', 'sending', '2016-11-09 10:37:41', '2016-12-22 11:44:02'),
(35, 'oy826sj0h052a', 1, 3, NULL, NULL, 'autoresponder', 'Day #28', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Should|Could|Can] we send new [RANDOM_CONTENT:information|info|tips] here [EMAIL] ', '2016-11-09 10:38:41', '2016-12-22 11:33:58', NULL, 'no', 'sending', '2016-11-09 10:38:41', '2016-12-22 11:44:02'),
(36, 'dl670aehf460c', 1, 3, NULL, NULL, 'autoresponder', 'Day #29', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-11-09 10:39:41', '2016-12-22 11:34:38', NULL, 'no', 'sending', '2016-11-09 10:39:41', '2016-12-22 11:44:02'),
(37, 'jw0152p16tb0d', 1, 3, NULL, NULL, 'autoresponder', 'Day #30', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Can|May] [RANDOM_CONTENT:you send|you return] me [RANDOM_CONTENT:a quick|a fast|an instant] [RANDOM_CONTENT:reply|response|answer]', '2016-11-09 10:40:42', '2016-12-22 11:35:12', NULL, 'no', 'sending', '2016-11-09 10:40:42', '2016-12-22 11:44:02'),
(47, 'lw045hcbos942', 1, 3, NULL, NULL, 'autoresponder', 'Day #31(Deleted)', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Can|May] [RANDOM_CONTENT:you send|you return] me [RANDOM_CONTENT:a quick|a fast|an instant] [RANDOM_CONTENT:reply|response|answer]', '2016-12-18 13:03:52', NULL, NULL, 'no', 'pending-delete', '2016-12-18 13:03:48', '2016-12-20 08:59:09'),
(48, 'aw755vy27e74a', 1, 2, NULL, NULL, 'autoresponder', 'MainList #2(Deleted)', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-12-18 13:48:39', NULL, NULL, 'no', 'pending-delete', '2016-12-18 13:48:08', '2016-12-20 08:59:07'),
(49, 'ct843qkxer0f3', 1, 2, NULL, NULL, 'autoresponder', 'MainList #1', 'Miki Miles', 'admin@a-ddiction.press', '[EMAIL]', 'reply@a-ddiction.press', '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', '2016-12-22 10:45:53', '2016-12-22 10:47:12', NULL, 'no', 'sending', '2016-12-22 10:45:44', '2016-12-22 11:44:02');

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_abuse_report`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_abuse_report` (
`report_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `list_id` int(11) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `customer_info` varchar(255) NOT NULL,
  `campaign_info` varchar(255) NOT NULL,
  `list_info` varchar(255) NOT NULL,
  `subscriber_info` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `log` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_attachment`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_attachment` (
`attachment_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `extension` char(10) NOT NULL,
  `mime_type` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_bounce_log`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_bounce_log` (
`log_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `message` text,
  `bounce_type` enum('hard','soft','internal') NOT NULL DEFAULT 'hard',
  `processed` enum('yes','no') NOT NULL DEFAULT 'no',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_delivery_log`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_delivery_log` (
`log_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `server_id` int(11) DEFAULT NULL,
  `message` text,
  `processed` enum('yes','no') NOT NULL DEFAULT 'no',
  `retries` int(1) NOT NULL DEFAULT '0',
  `max_retries` int(1) NOT NULL DEFAULT '3',
  `email_message_id` varchar(255) DEFAULT NULL,
  `delivery_confirmed` enum('yes','no') NOT NULL DEFAULT 'yes',
  `status` char(15) NOT NULL DEFAULT 'success',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_delivery_log_archive`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_delivery_log_archive` (
`log_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `server_id` int(11) DEFAULT NULL,
  `message` text,
  `processed` enum('yes','no') NOT NULL DEFAULT 'no',
  `retries` int(1) NOT NULL DEFAULT '0',
  `max_retries` int(1) NOT NULL DEFAULT '3',
  `email_message_id` varchar(255) DEFAULT NULL,
  `delivery_confirmed` enum('yes','no') NOT NULL DEFAULT 'yes',
  `status` char(15) NOT NULL DEFAULT 'success',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_forward_friend`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_forward_friend` (
`forward_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `to_email` varchar(150) NOT NULL,
  `to_name` varchar(150) NOT NULL,
  `from_email` varchar(150) NOT NULL,
  `from_name` varchar(150) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `ip_address` char(15) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_group`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_group` (
`group_id` int(11) NOT NULL,
  `group_uid` char(13) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_open_action_list_field`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_open_action_list_field` (
`action_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_open_action_subscriber`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_open_action_subscriber` (
`action_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `action` char(5) NOT NULL DEFAULT 'copy',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `mw_campaign_open_action_subscriber`
--

INSERT INTO `mw_campaign_open_action_subscriber` (`action_id`, `campaign_id`, `list_id`, `action`, `date_added`, `last_updated`) VALUES
(24, 48, 3, 'move', '2016-12-18 13:48:39', '2016-12-18 13:48:39'),
(25, 2, 3, 'move', '2016-12-20 09:00:41', '2016-12-20 09:00:41'),
(27, 49, 3, 'move', '2016-12-22 10:46:20', '2016-12-22 10:46:20'),
(28, 3, 3, 'move', '2016-12-22 10:58:26', '2016-12-22 10:58:26'),
(29, 4, 3, 'move', '2016-12-22 10:59:22', '2016-12-22 10:59:22'),
(30, 5, 3, 'move', '2016-12-22 11:00:16', '2016-12-22 11:00:16');

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_option`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_option` (
  `campaign_id` int(11) NOT NULL,
  `open_tracking` enum('yes','no') NOT NULL DEFAULT 'yes',
  `url_tracking` enum('yes','no') NOT NULL DEFAULT 'no',
  `json_feed` enum('yes','no') NOT NULL DEFAULT 'no',
  `xml_feed` enum('yes','no') NOT NULL DEFAULT 'no',
  `embed_images` enum('yes','no') NOT NULL DEFAULT 'no',
  `plain_text_email` enum('yes','no') NOT NULL DEFAULT 'yes',
  `autoresponder_event` char(20) NOT NULL DEFAULT 'AFTER-SUBSCRIBE',
  `autoresponder_time_unit` varchar(6) NOT NULL DEFAULT 'day',
  `autoresponder_time_value` int(11) NOT NULL DEFAULT '0',
  `autoresponder_open_campaign_id` int(11) DEFAULT NULL,
  `autoresponder_include_imported` enum('yes','no') NOT NULL DEFAULT 'no',
  `email_stats` varchar(255) NOT NULL,
  `regular_open_unopen_action` char(10) DEFAULT NULL,
  `regular_open_unopen_campaign_id` int(11) DEFAULT NULL,
  `cronjob` varchar(255) DEFAULT NULL,
  `cronjob_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `blocked_reason` varchar(255) DEFAULT NULL,
  `giveup_counter` int(11) NOT NULL DEFAULT '0',
  `max_send_count` int(11) NOT NULL DEFAULT '0',
  `max_send_count_random` enum('yes','no') NOT NULL DEFAULT 'no',
  `tracking_domain_id` int(11) DEFAULT NULL,
  `preheader` varchar(255) DEFAULT NULL,
  `share_reports_enabled` enum('yes','no') NOT NULL DEFAULT 'no',
  `share_reports_password` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mw_campaign_option`
--

INSERT INTO `mw_campaign_option` (`campaign_id`, `open_tracking`, `url_tracking`, `json_feed`, `xml_feed`, `embed_images`, `plain_text_email`, `autoresponder_event`, `autoresponder_time_unit`, `autoresponder_time_value`, `autoresponder_open_campaign_id`, `autoresponder_include_imported`, `email_stats`, `regular_open_unopen_action`, `regular_open_unopen_campaign_id`, `cronjob`, `cronjob_enabled`, `blocked_reason`, `giveup_counter`, `max_send_count`, `max_send_count_random`, `tracking_domain_id`, `preheader`, `share_reports_enabled`, `share_reports_password`) VALUES
(2, 'yes', 'yes', 'no', 'yes', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 0, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '', 'no', NULL),
(3, 'yes', 'yes', 'no', 'yes', 'no', 'yes', 'AFTER-SUBSCRIBE', 'hour', 16, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', 'no', NULL),
(4, 'yes', 'yes', 'no', 'yes', 'no', 'yes', 'AFTER-SUBSCRIBE', 'hour', 33, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', 'no', NULL),
(5, 'yes', 'yes', 'no', 'yes', 'no', 'yes', 'AFTER-SUBSCRIBE', 'hour', 50, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', 'no', NULL),
(7, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 0, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'This is the email address you registered correct? [EMAIL] ', 'no', NULL),
(8, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 1, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], Can you [RANDOM_CONTENT:call |reply |answer ]me', 'no', NULL),
(9, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 2, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], I had some [RANDOM_CONTENT:questions|answers|tips] for you .', 'no', NULL),
(10, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 3, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[RANDOM_CONTENT:Follow up|email|followup] messages go to [EMAIL]  , yes?', 'no', NULL),
(11, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 4, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'Do you [RANDOM_CONTENT:need|want|check] the URL?', 'no', NULL),
(12, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 5, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'Can we [RANDOM_CONTENT:setup|make|create] the [RANDOM_CONTENT:automation process|automation|system] for [EMAIL] ?', 'no', NULL),
(13, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 6, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[RANDOM_CONTENT:Should|May|Can]  we send new information here [EMAIL] ?', 'no', NULL),
(14, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 7, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'Does this [RANDOM_CONTENT:address|email|email address] work? [EMAIL]', 'no', NULL),
(15, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 8, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'Can you send me a [RANDOM_CONTENT:quick|fast|immediate] [RANDOM_CONTENT:reply|email|message] ?', 'no', NULL),
(16, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 9, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:This is|This really is|That is] [RANDOM_CONTENT:only for|just for|limited to] the [RANDOM_CONTENT:serious|smart ones]', 'no', NULL),
(17, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 10, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Can we|Should we|Could we] [RANDOM_CONTENT:set up|install|setup|create] the automation [RANDOM_CONTENT:process|method] for [EMAIL] ?', 'no', NULL),
(18, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 11, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME],  What are you [RANDOM_CONTENT:looking|searching|waiting] for?', 'no', NULL),
(19, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 12, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], Can you copy rinse repeat?', 'no', NULL),
(20, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 13, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[EMAIL] registered successfully', 'no', NULL),
(21, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 14, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[RANDOM_CONTENT:We''re|We are] launching [RANDOM_CONTENT:a new|a brand new|a fresh] [RANDOM_CONTENT:site|website]', 'no', NULL),
(22, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 15, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], You should do it!', 'no', NULL),
(23, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 16, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[RANDOM_CONTENT:3|4|5|6|2|7|8] counter intuitive [RANDOM_CONTENT:secrets|strategies|techniques] about [RANDOM_CONTENT:earning|making|getting] more', 'no', NULL),
(24, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 17, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'Messages for registered address [EMAIL] ', 'no', NULL),
(25, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 18, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME] can you [RANDOM_CONTENT:make|do] [RANDOM_CONTENT:it|this|that] ? ', 'no', NULL),
(26, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 19, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', 'no', NULL),
(27, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 20, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], What''s the [RANDOM_CONTENT:delay|hold off|postpone|wait|hold up|hesitate|holdup]? ', 'no', NULL),
(28, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 21, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'Re: [RANDOM_CONTENT:Give|Provide] me [RANDOM_CONTENT:more information|extra information|more details|more info|additional information]', 'no', NULL),
(29, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 22, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[RANDOM_CONTENT:Stop|End] [RANDOM_CONTENT:wasting|squandering] time on [RANDOM_CONTENT:minor|small|slight|modest] [RANDOM_CONTENT:life|living] [RANDOM_CONTENT:decisions|choices|conclusions]', 'no', NULL),
(30, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 23, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:You should|You need to|You ought to|You must] [RANDOM_CONTENT:do it|get it done|take action]', 'no', NULL),
(31, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 24, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[RANDOM_CONTENT:Yeah|Yes|Yep|Yup] [RANDOM_CONTENT:here''s|here is|this is|the following is|and here is] [RANDOM_CONTENT:how it works|how it operates|the ins and outs|the way it operates|the way it works]', 'no', NULL),
(32, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 25, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Nothing like|Nothing beats|Nothing can beat] [RANDOM_CONTENT:it|this|that]', 'no', NULL),
(33, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 26, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, 'Do you [RANDOM_CONTENT:need|wait|want] the [RANDOM_CONTENT:URL|Link|email] ? ', 'no', NULL),
(34, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 27, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[RANDOM_CONTENT:How to work|Working] [RANDOM_CONTENT:smarter|wiser|better|cleverer|smart], [RANDOM_CONTENT:not|not really|not necessarily|certainly not|definitely not] [RANDOM_CONTENT:harder|tougher|more difficult|more challenging]', 'no', NULL),
(35, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 28, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Should|Could|Can] we send new [RANDOM_CONTENT:information|info|tips] here [EMAIL] ', 'no', NULL),
(36, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 29, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', 'no', NULL),
(37, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 30, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Can|May] [RANDOM_CONTENT:you send|you return] me [RANDOM_CONTENT:a quick|a fast|an instant] [RANDOM_CONTENT:reply|response|answer]', 'no', NULL),
(47, 'yes', 'yes', 'no', 'no', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 30, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '', 'no', NULL),
(48, 'yes', 'yes', 'no', 'yes', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 0, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '', 'no', NULL),
(49, 'yes', 'yes', 'no', 'yes', 'no', 'yes', 'AFTER-SUBSCRIBE', 'day', 0, NULL, 'yes', '', NULL, NULL, NULL, 0, NULL, 0, 0, 'no', NULL, '[FNAME], [RANDOM_CONTENT:Waiting for|Awaiting|Looking forward to|Expecting] [RANDOM_CONTENT:your response|your reply] ', 'no', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_template`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_template` (
`template_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `customer_template_id` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  `inline_css` enum('yes','no') NOT NULL DEFAULT 'no',
  `minify` enum('yes','no') NOT NULL DEFAULT 'no',
  `plain_text` text,
  `only_plain_text` enum('yes','no') NOT NULL DEFAULT 'no',
  `auto_plain_text` enum('yes','no') NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `mw_campaign_template`
--

INSERT INTO `mw_campaign_template` (`template_id`, `campaign_id`, `customer_template_id`, `content`, `inline_css`, `minify`, `plain_text`, `only_plain_text`, `auto_plain_text`) VALUES
(2, 2, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0e5ee549-1f00-99a4-c714-b72644b3b06c">It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our last [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone call]<br />\r\nYou are still using this [RANDOM_CONTENT:email|e-mail|e mail|mail|email address] [EMAIL] ?<br />\r\n<br />\r\nThis [RANDOM_CONTENT:month|week|year] we are planning to share some [RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very good|amazing|awesome] [RANDOM_CONTENT:information|info|details|facts|information and facts|material|tips|advices|knowledge].<br />\r\nshowing people how they can start [RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] and [RANDOM_CONTENT:generate|make] a part time or full time income.<br />\r\n<br />\r\nBut if you are interested in learning about Online businesses<br />\r\nthat are available for you please click reply and say "[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I like|I''d like to see|We need] [RANDOM_CONTENT:more information|more info|more details|details|much more information|additional information|more info|addiitional information|more info .|additional info]".<br />\r\n<br />\r\nIf you no longer are interested in getting information about<br />\r\n[RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] please use the unsubscribe link below.<br />\r\n<br />\r\n<br />\r\n\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\nhttp://a-ddiction.press/<br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a>', 'no', 'no', ' It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our\r\nlast\r\n[RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone\r\ncall]\r\n You are still using this [RANDOM_CONTENT:email|e-mail|e\r\nmail|mail|email address] [EMAIL] ?\r\n\r\n This [RANDOM_CONTENT:month|week|year] we are planning to share some\r\n[RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very\r\ngood|amazing|awesome]\r\n[RANDOM_CONTENT:information|info|details|facts|information and\r\nfacts|material|tips|advices|knowledge].\r\n showing people how they can start [RANDOM_CONTENT:work from home|work\r\nat home|home based business|online business|do business from\r\nhome|internet business] and [RANDOM_CONTENT:generate|make] a part time\r\nor full time income.\r\n\r\n But if you are interested in learning about Online businesses\r\n that are available for you please click reply and say\r\n"[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I\r\nlike|I''d like to see|We need] [RANDOM_CONTENT:more information|more\r\ninfo|more details|details|much more information|additional\r\ninformation|more info|addiitional information|more info .|additional\r\ninfo]".\r\n\r\n If you no longer are interested in getting information about\r\n [RANDOM_CONTENT:work from home|work at home|home based\r\nbusiness|online business|do business from home|internet business]\r\nplease use the unsubscribe link below.\r\n\r\n Talk Soon,\r\n Miki Miles\r\n\r\n [UNSUBSCRIBE_URL]', 'no', 'yes'),
(3, 3, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0e5ee549-1f00-99a4-c714-b72644b3b06c">It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our last [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone call]<br />\r\nYou are still using this [RANDOM_CONTENT:email|e-mail|e mail|mail|email address] [EMAIL] ?<br />\r\n<br />\r\nThis [RANDOM_CONTENT:month|week|year] we are planning to share some [RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very good|amazing|awesome] [RANDOM_CONTENT:information|info|details|facts|information and facts|material|tips|advices|knowledge].<br />\r\nshowing people how they can start [RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] and [RANDOM_CONTENT:generate|make] a part time or full time income.<br />\r\n<br />\r\nBut if you are interested in learning about Online businesses<br />\r\nthat are available for you please click reply and say "[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I like|I''d like to see|We need] [RANDOM_CONTENT:more information|more info|more details|details|much more information|additional information|more info|addiitional information|more info .|additional info]".<br />\r\n<br />\r\nIf you no longer are interested in getting information about<br />\r\n[RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] please use the unsubscribe link below.<br />\r\n<br />\r\n<br />\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our\r\nlast [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone\r\ncall]\r\n You are still using this [RANDOM_CONTENT:email|e-mail|e\r\nmail|mail|email address] [EMAIL] ?\r\n\r\n This [RANDOM_CONTENT:month|week|year] we are planning to share some\r\n[RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very\r\ngood|amazing|awesome]\r\n[RANDOM_CONTENT:information|info|details|facts|information and\r\nfacts|material|tips|advices|knowledge].\r\n showing people how they can start [RANDOM_CONTENT:work from home|work\r\nat home|home based business|online business|do business from\r\nhome|internet business] and [RANDOM_CONTENT:generate|make] a part time\r\nor full time income.\r\n\r\n But if you are interested in learning about Online businesses\r\n that are available for you please click reply and say\r\n"[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I\r\nlike|I''d like to see|We need] [RANDOM_CONTENT:more information|more\r\ninfo|more details|details|much more information|additional\r\ninformation|more info|addiitional information|more info .|additional\r\ninfo]".\r\n\r\n If you no longer are interested in getting information about\r\n [RANDOM_CONTENT:work from home|work at home|home based\r\nbusiness|online business|do business from home|internet business]\r\nplease use the unsubscribe link below.\r\n\r\n Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(4, 4, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0e5ee549-1f00-99a4-c714-b72644b3b06c">It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our last [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone call]<br />\r\nYou are still using this [RANDOM_CONTENT:email|e-mail|e mail|mail|email address] [EMAIL] ?<br />\r\n<br />\r\nThis [RANDOM_CONTENT:month|week|year] we are planning to share some [RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very good|amazing|awesome] [RANDOM_CONTENT:information|info|details|facts|information and facts|material|tips|advices|knowledge].<br />\r\nshowing people how they can start [RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] and [RANDOM_CONTENT:generate|make] a part time or full time income.<br />\r\n<br />\r\nBut if you are interested in learning about Online businesses<br />\r\nthat are available for you please click reply and say "[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I like|I''d like to see|We need] [RANDOM_CONTENT:more information|more info|more details|details|much more information|additional information|more info|addiitional information|more info .|additional info]".<br />\r\n<br />\r\nIf you no longer are interested in getting information about<br />\r\n[RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] please use the unsubscribe link below.<br />\r\n<br />\r\n<br />\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our\r\nlast [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone\r\ncall]\r\n You are still using this [RANDOM_CONTENT:email|e-mail|e\r\nmail|mail|email address] [EMAIL] ?\r\n\r\n This [RANDOM_CONTENT:month|week|year] we are planning to share some\r\n[RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very\r\ngood|amazing|awesome]\r\n[RANDOM_CONTENT:information|info|details|facts|information and\r\nfacts|material|tips|advices|knowledge].\r\n showing people how they can start [RANDOM_CONTENT:work from home|work\r\nat home|home based business|online business|do business from\r\nhome|internet business] and [RANDOM_CONTENT:generate|make] a part time\r\nor full time income.\r\n\r\n But if you are interested in learning about Online businesses\r\n that are available for you please click reply and say\r\n"[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I\r\nlike|I''d like to see|We need] [RANDOM_CONTENT:more information|more\r\ninfo|more details|details|much more information|additional\r\ninformation|more info|addiitional information|more info .|additional\r\ninfo]".\r\n\r\n If you no longer are interested in getting information about\r\n [RANDOM_CONTENT:work from home|work at home|home based\r\nbusiness|online business|do business from home|internet business]\r\nplease use the unsubscribe link below.\r\n\r\n Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(5, 5, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0e5ee549-1f00-99a4-c714-b72644b3b06c">It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our last [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone call]<br />\r\nYou are still using this [RANDOM_CONTENT:email|e-mail|e mail|mail|email address] [EMAIL] ?<br />\r\n<br />\r\nThis [RANDOM_CONTENT:month|week|year] we are planning to share some [RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very good|amazing|awesome] [RANDOM_CONTENT:information|info|details|facts|information and facts|material|tips|advices|knowledge].<br />\r\nshowing people how they can start [RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] and [RANDOM_CONTENT:generate|make] a part time or full time income.<br />\r\n<br />\r\nBut if you are interested in learning about Online businesses<br />\r\nthat are available for you please click reply and say "[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I like|I''d like to see|We need] [RANDOM_CONTENT:more information|more info|more details|details|much more information|additional information|more info|addiitional information|more info .|additional info]".<br />\r\n<br />\r\nIf you no longer are interested in getting information about<br />\r\n[RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] please use the unsubscribe link below.<br />\r\n<br />\r\n<br />\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our\r\nlast [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone\r\ncall]\r\n You are still using this [RANDOM_CONTENT:email|e-mail|e\r\nmail|mail|email address] [EMAIL] ?\r\n\r\n This [RANDOM_CONTENT:month|week|year] we are planning to share some\r\n[RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very\r\ngood|amazing|awesome]\r\n[RANDOM_CONTENT:information|info|details|facts|information and\r\nfacts|material|tips|advices|knowledge].\r\n showing people how they can start [RANDOM_CONTENT:work from home|work\r\nat home|home based business|online business|do business from\r\nhome|internet business] and [RANDOM_CONTENT:generate|make] a part time\r\nor full time income.\r\n\r\n But if you are interested in learning about Online businesses\r\n that are available for you please click reply and say\r\n"[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I\r\nlike|I''d like to see|We need] [RANDOM_CONTENT:more information|more\r\ninfo|more details|details|much more information|additional\r\ninformation|more info|addiitional information|more info .|additional\r\ninfo]".\r\n\r\n If you no longer are interested in getting information about\r\n [RANDOM_CONTENT:work from home|work at home|home based\r\nbusiness|online business|do business from home|internet business]\r\nplease use the unsubscribe link below.\r\n\r\n Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(6, 7, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="38f508ca-8d44-5fa9-3e59-7f15a37b8a6f">This month we are planning on sending out some great information showing \r\n<div>people how they can work from home and generate a part time or full time</div>\r\n\r\n<div>income. </div>\r\n\r\n<div> </div>\r\n\r\n<div>We have registered on file this address for you [EMAIL] </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you no longer are interested in getting information about </div>\r\n\r\n<div>work at home jobs & businesses please use the unsubscribe </div>\r\n\r\n<div>link below. </div>\r\n\r\n<div> </div>\r\n\r\n<div>But if you are interested in learning about positions and businesses </div>\r\n\r\n<div>that are available for you please click reply and say "I want more information".</div>\r\n\r\n<div> </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' This month we are planning on sending out some great information\r\nshowing  \r\npeople how they can work from home and generate a part time or full\r\ntime \r\nincome.  \r\n  \r\nWe have registered on file this address for you [EMAIL]  \r\n  \r\nIf you no longer are interested in getting information about  \r\nwork at home jobs & businesses please use the unsubscribe  \r\nlink below.  \r\n  \r\nBut if you are interested in learning about positions and businesses \r\n\r\nthat are available for you please click reply and say "I want more\r\ninformation". \r\n  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(7, 8, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="b06a06b4-7508-d9ba-1fcb-666327df55cc">\r\n<div>Let us know if you are still interested in learning how to work online </div>\r\n\r\n<div> </div>\r\n\r\n<div>or if you aren''t interested and just want to be unsubscribed. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Reply "Yes I want more info"</div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' \r\nLet us know if you are still interested in learning how to work\r\nonline  \r\n  \r\nor if you aren''t interested and just want to be unsubscribed.  \r\n  \r\nReply "Yes I want more info" \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(8, 9, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="e2f9d5e8-c5c5-dd1a-1717-c95f4094fd7b">Can you send us an email by clicking the reply button?\r\n<div> </div>\r\nLet us know the answer to these questions\r\n\r\n<div> </div>\r\n\r\n<div>. Have you worked at home or online before? </div>\r\n\r\n<div>2. What was your success rate? </div>\r\n\r\n<div>3. If you could work at home what is the ideal style job you would have? </div>\r\n\r\n<div>4. Do you like to communicate via phone, email, skype or instant message? </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Can you send us an email by clicking the reply button? \r\n  Let us know the answer to these questions \r\n  \r\n. Have you worked at home or online before?  \r\n2. What was your success rate?  \r\n3. If you could work at home what is the ideal style job you would\r\nhave?  \r\n4. Do you like to communicate via phone, email, skype or instant\r\nmessage?  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(9, 10, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="a5f9fa6f-841c-6a8b-a78d-b8bd83086ec6">Our system indicates that approximately [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days ago you registered \r\n<div>this email address ( [EMAIL]  ) on one of our websites. </div>\r\n\r\n<div> </div>\r\n\r\n<div>We provide information about available jobs, positions, and businesses </div>\r\n\r\n<div>that can help you to earn a part time or full time income working from home. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you are still looking and want more information click the reply button above. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you no longer wish to receive information in this field simply go to the bottom of </div>\r\n\r\n<div>this email and click the unsubscribe button. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Our system indicates that approximately\r\n[RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days ago you registered  \r\nthis email address ( [EMAIL]  ) on one of our websites.  \r\n  \r\nWe provide information about available jobs, positions, and\r\nbusinesses  \r\nthat can help you to earn a part time or full time income working from\r\nhome.  \r\n  \r\nIf you are still looking and want more information click the reply\r\nbutton above.  \r\n  \r\nIf you no longer wish to receive information in this field simply go\r\nto the bottom of  \r\nthis email and click the unsubscribe button.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(10, 11, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="307333a7-9ccf-0e1c-7a9b-4b29e934f3b5">This new business has testimonial after testimonial of normal \r\n<div>average working people making over $5k monthly. Do you want </div>\r\n\r\n<div>the URL to check out the information about the business? </div>\r\n\r\n<div> </div>\r\n\r\n<div>Let us know by simply clicking the reply button above. </div>\r\n\r\n<div> </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' This new business has testimonial after testimonial of normal  \r\naverage working people making over $5k monthly. Do you want  \r\nthe URL to check out the information about the business?  \r\n  \r\nLet us know by simply clicking the reply button above.  \r\n  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(11, 12, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="16e60e37-ac08-733d-075a-a5b4596dfb96">Many people have a hard time working online. \r\n<div> </div>\r\n\r\n<div>After years of experience </div>\r\n\r\n<div>what I''ve discovered is people just have no clue how to set up an automation </div>\r\n\r\n<div>process to actually help you run the business. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Let''s say a company could help you nearly automate your entire business. </div>\r\n\r\n<div>Is it something you want to know more about? </div>\r\n\r\n<div> </div>\r\n\r\n<div>Reply and let me know : Sure I''d hear more.  </div>\r\n\r\n<div> </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Many people have a hard time working online.  \r\n  \r\nAfter years of experience  \r\nwhat I''ve discovered is people just have no clue how to set up an\r\nautomation  \r\nprocess to actually help you run the business.  \r\n  \r\nLet''s say a company could help you nearly automate your entire\r\nbusiness.  \r\nIs it something you want to know more about?  \r\n  \r\nReply and let me know : Sure I''d hear more.   \r\n  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(12, 13, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="3c9eadc3-3c14-7832-aca5-af00ba02072e">We have business associates that have clients earning between \r\n<div>$5000 - $20,000 per-month within their businesses. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Should we send the information to the email address ( [EMAIL]  ) we have on file? </div>\r\n\r\n<div> </div>\r\n\r\n<div>Please let us know by clicking the reply button above. </div>\r\n\r\n<div> </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' We have business associates that have clients earning between  \r\n$5000 - $20,000 per-month within their businesses.  \r\n  \r\nShould we send the information to the email address ( [EMAIL]  ) we\r\nhave on file?  \r\n  \r\nPlease let us know by clicking the reply button above.  \r\n  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(13, 14, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="806cbe8c-a02c-87e4-ec59-5dee8e1c586f">So I just want to make sure that this email address still works ( [EMAIL]  ) \r\n<div>and that you are getting our emails. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you see this could you help me out </div>\r\n\r\n<div>and simply respond saying, "Yes I see the emails". </div>\r\n\r\n<div> </div>\r\n\r\n<div> </div>\r\n\r\n<div>Thanks so much. </div>\r\n\r\n<div>Not sure if this is a technical issue or if things are working. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' So I just want to make sure that this email address still works (\r\n[EMAIL]  )  \r\nand that you are getting our emails.  \r\n  \r\nIf you see this could you help me out  \r\nand simply respond saying, "Yes I see the emails".  \r\n  \r\n  \r\nThanks so much.  \r\nNot sure if this is a technical issue or if things are working.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(14, 15, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="976c24bc-afa2-c132-0103-dfda5b4bd6cb">Could you shoot me a quick reply back just saying \r\n<div>"Hey I see this still"</div>\r\n\r\n<div> </div>\r\n\r\n<div>Recently I was playing with </div>\r\n\r\n<div>my settings and made some changes. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Email can get confusing sometimes. </div>\r\n\r\n<div>Not sure if I did something to completely </div>\r\n\r\n<div>disconfigure my mailbox.</div>\r\n\r\n<div> </div>\r\n\r\n<div>I appreciate the help. </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Could you shoot me a quick reply back just saying  \r\n"Hey I see this still" \r\n  \r\nRecently I was playing with  \r\nmy settings and made some changes.  \r\n  \r\nEmail can get confusing sometimes.  \r\nNot sure if I did something to completely  \r\ndisconfigure my mailbox. \r\n  \r\nI appreciate the help.  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(15, 16, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="4fb8dde5-f4c5-9754-632b-7573050ae637">When you start a business that actually has a real \r\n<div>price tag attached, its for only serious entrepreneurs. </div>\r\n\r\n<div> </div>\r\n\r\n<div>We have many members earning </div>\r\n\r\n<div>$1000 </div>\r\n\r\n<div>$2500</div>\r\n\r\n<div>$5000</div>\r\n\r\n<div>$10000</div>\r\n\r\n<div>$25,000+ every single month. </div>\r\n\r\n<div> </div>\r\n\r\n<div>The best part is they don''t have to bug friends and family.</div>\r\n\r\n<div>They only speak to people that want more information and </div>\r\n\r\n<div>thinking about joining.  </div>\r\n\r\n<div> </div>\r\n\r\n<div>This is "big ticket" business. If you are serious and want to </div>\r\n\r\n<div>take a closer look reach out to myself or team. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Reply saying "More Info"</div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' When you start a business that actually has a real  \r\nprice tag attached, its for only serious entrepreneurs.  \r\n  \r\nWe have many members earning  \r\n$1000  \r\n$2500 \r\n$5000 \r\n$10000 \r\n$25,000+ every single month.  \r\n  \r\nThe best part is they don''t have to bug friends and family. \r\nThey only speak to people that want more information and  \r\nthinking about joining.   \r\n  \r\nThis is "big ticket" business. If you are serious and want to  \r\ntake a closer look reach out to myself or team.  \r\n  \r\nReply saying "More Info" Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes');
INSERT INTO `mw_campaign_template` (`template_id`, `campaign_id`, `customer_template_id`, `content`, `inline_css`, `minify`, `plain_text`, `only_plain_text`, `auto_plain_text`) VALUES
(16, 17, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="a49cfa9c-760d-14dc-a4ee-5553d0693044">Understanding what a "Big Ticket Sale" can easily \r\n<div> </div>\r\n\r\n<div>change your financial life. When you start to make </div>\r\n\r\n<div> </div>\r\n\r\n<div>say, $2,500.00 from one sale as a commission the </div>\r\n\r\n<div> </div>\r\n\r\n<div>idea of making a serious full time income from home </div>\r\n\r\n<div> </div>\r\n\r\n<div>comes to life. </div>\r\n\r\n<div> </div>\r\n\r\n<div>See often we think about working from home or online </div>\r\n\r\n<div>as a long, hard tedious process. But what if you could make </div>\r\n\r\n<div>$2,500 to $25,000.00 from (1) sale?  </div>\r\n\r\n<div> </div>\r\n\r\n<div>We have a guy willing to teach you and give you information </div>\r\n\r\n<div>showing you how he makes over $40,000.00 every month </div>\r\n\r\n<div>following a Copy - Rinse - Repeat plan.  </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press">Check it out a-ddiction.press</a></div>\r\n\r\n<div>You can also Message me back - Click The Reply Button Above. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Understanding what a "Big Ticket Sale" can easily  \r\n  \r\nchange your financial life. When you start to make  \r\n  \r\nsay, $2,500.00 from one sale as a commission the  \r\n  \r\nidea of making a serious full time income from home  \r\n  \r\ncomes to life.  \r\n  \r\nSee often we think about working from home or online  \r\nas a long, hard tedious process. But what if you could make  \r\n$2,500 to $25,000.00 from (1) sale?   \r\n  \r\nWe have a guy willing to teach you and give you information  \r\nshowing you how he makes over $40,000.00 every month  \r\nfollowing a Copy - Rinse - Repeat plan.   \r\n  \r\nCheck it out a-ddiction.press http://a-ddiction.press \r\nYou can also Message me back - Click The Reply Button Above.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(17, 18, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="3ac75d72-dc3d-84d1-f35e-dd4ff787c280">Most people want to have a "real work at home" business, but they \r\n<div>don''t want to treat it as such. I''m not talking about working 14 hours </div>\r\n\r\n<div>a day and making a wimpy $1000 a month. </div>\r\n\r\n<div> </div>\r\n\r\n<div>I''m talking about having the potential to make 1 or 2 sales a month</div>\r\n\r\n<div>and paying off your mortgage. High ticket business is the real deal. </div>\r\n\r\n<div>Anything else you are simply risking your money for something silly </div>\r\n\r\n<div>and unrealistic. </div>\r\n\r\n<div> </div>\r\n\r\n<div>So if you are looking to earn $2500 to $20000 a month then </div>\r\n\r\n<div>send me a reply back or visit <a href="http://a-ddiction.press/">http://a-ddiction.press/ </a></div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Most people want to have a "real work at home" business, but they  \r\ndon''t want to treat it as such. I''m not talking about working 14\r\nhours  \r\na day and making a wimpy $1000 a month.  \r\n  \r\nI''m talking about having the potential to make 1 or 2 sales a month \r\nand paying off your mortgage. High ticket business is the real deal. \r\n\r\nAnything else you are simply risking your money for something silly  \r\nand unrealistic.  \r\n  \r\nSo if you are looking to earn $2500 to $20000 a month then  \r\nsend me a reply back or visit http://a-ddiction.press/ \r\nhttp://a-ddiction.press/ \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(18, 19, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="6f13fa56-b019-16d9-009c-66a1fe1dce5f">Our high ticket business is designed to get people making at least \r\n<div>$5000.00 every single month as quickly as possible. We do this by </div>\r\n\r\n<div>showing people (no matter what experience level) how to simply </div>\r\n\r\n<div><a href="http://a-ddiction.press/"><strong>copy - rinse - repeat.  </strong></a></div>\r\n\r\n<div> </div>\r\n\r\n<div>If you can follow directions there is absolutely no reason you </div>\r\n\r\n<div>shouldn''t be making $5000.00 or more with an online business. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Message me back</div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Our high ticket business is designed to get people making at least  \r\n$5000.00 every single month as quickly as possible. We do this by  \r\nshowing people (no matter what experience level) how to simply  \r\nCOPY - RINSE - REPEAT.   http://a-ddiction.press/ \r\n  \r\nIf you can follow directions there is absolutely no reason you  \r\nshouldn''t be making $5000.00 or more with an online business.  \r\n  \r\nMessage me back \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(19, 20, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0b6c5ae3-2b52-0a19-af0f-86383ecc960f">We have on file that you registered this address [EMAIL]  to \r\n<div>learn more about high ticket business opportunities. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If this is correct please reply and let us know. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Our job is to provide you with the best and most recent </div>\r\n\r\n<div>information showing you what companies have the most </div>\r\n\r\n<div>successful people making over $5000 per-month with a </div>\r\n\r\n<div>high ticket business. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you no longer wish to receive this type of information </div>\r\n\r\n<div>just click the unsubscribe button below. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Or </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press/">If you would like more info click the reply button. </a></div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' We have on file that you registered this address [EMAIL]  to  \r\nlearn more about high ticket business opportunities.  \r\n  \r\nIf this is correct please reply and let us know.  \r\n  \r\nOur job is to provide you with the best and most recent  \r\ninformation showing you what companies have the most  \r\nsuccessful people making over $5000 per-month with a  \r\nhigh ticket business.  \r\n  \r\nIf you no longer wish to receive this type of information  \r\njust click the unsubscribe button below.  \r\n  \r\nOr  \r\n  \r\nIf you would like more info click the reply button. \r\nhttp://a-ddiction.press/ \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(20, 21, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="a14757d1-6986-49ba-fd6b-950d271a68d7">I can promise you when you earn a $5000 commission \r\n<div>or a $20000 payment at one time the only thing that will </div>\r\n\r\n<div>go through your head is ....  Beach Mode.   lol  ; )  </div>\r\n\r\n<div> </div>\r\n\r\n<div>Most people can''t imagine being able to get </div>\r\n\r\n<div>paid $5k or $20k from one single sale but its </div>\r\n\r\n<div><a href="http://a-ddiction.press/">very possible. </a></div>\r\n\r\n<div> </div>\r\n\r\n<div>Also, its no harder than making a $100 sale.  </div>\r\n\r\n<div> </div>\r\n\r\n<div>Well if you are ready to start saying Beach Mode</div>\r\n\r\n<div>then shoot me a message back and I''ll let you know </div>\r\n\r\n<div>how this works.<br />\r\n </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' I can promise you when you earn a $5000 commission  \r\nor a $20000 payment at one time the only thing that will  \r\ngo through your head is ....  Beach Mode.   lol  ; )   \r\n  \r\nMost people can''t imagine being able to get  \r\npaid $5k or $20k from one single sale but its  \r\nvery possible.  http://a-ddiction.press/ \r\n  \r\nAlso, its no harder than making a $100 sale.   \r\n  \r\nWell if you are ready to start saying Beach Mode \r\nthen shoot me a message back and I''ll let you know  \r\nhow this works.\r\n   Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(21, 22, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="da95300b-86f8-3845-0949-5817c6e1008b">If you actually want to make an income online or to be able to work \r\n<div>from home then you should take a serious look at what is called a </div>\r\n\r\n<div>''High Ticket Business''.  </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press/">High ticket allows you to earn a lot of money with one sale versus</a> </div>\r\n\r\n<div>making a little money & trying to get a ton of sales. </div>\r\n\r\n<div> </div>\r\n\r\n<div>We have a real team and real families that are earning some </div>\r\n\r\n<div>serious money. You can meet everyone in our Facebook group</div>\r\n\r\n<div>so you can watch what is happening and when you are ready to </div>\r\n\r\n<div>jump in then you do so.  </div>\r\n\r\n<div> </div>\r\n\r\n<div>Need more info send me a message back</div>\r\n\r\n<div> </div>\r\n\r\n<div>See You There...</div>\r\n\r\n<div><br />\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></div>\r\n</body>\r\n</html>\r\n', 'no', 'no', ' If you actually want to make an income online or to be able to work \r\n\r\nfrom home then you should take a serious look at what is called a  \r\n''High Ticket Business''.   \r\n  \r\nHigh ticket allows you to earn a lot of money with one sale versus\r\nhttp://a-ddiction.press/  \r\nmaking a little money & trying to get a ton of sales.  \r\n  \r\nWe have a real team and real families that are earning some  \r\nserious money. You can meet everyone in our Facebook group \r\nso you can watch what is happening and when you are ready to  \r\njump in then you do so.   \r\n  \r\nNeed more info send me a message back \r\n  \r\nSee You There... \r\n\r\n Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(22, 23, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="be535d1d-e671-3d08-02ac-c659bd3d7ca0">I know that sounds very harsh. Actually quite rude and mean. \r\n<div>But honestly . . . </div>\r\n\r\n<div> </div>\r\n\r\n<div>It''s the truth. </div>\r\n\r\n<div> </div>\r\n\r\n<div>See we are programmed to go to school from an early age. </div>\r\n\r\n<div>Wake up a 7 then truck to school for eight hours. Sometimes </div>\r\n\r\n<div>even staying 10 to 12 hours if we played sports. </div>\r\n\r\n<div> </div>\r\n\r\n<div>After school we head off to college and repeat this process.</div>\r\n\r\n<div>After college we go out to find a "job" so we can then take </div>\r\n\r\n<div>our training wake up in the morning and go to work for 8 hours. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Don''t get me wrong there is nothing wrong with hard work. </div>\r\n\r\n<div> </div>\r\n\r\n<div>But what I don''t like is that the majority of people are programmed </div>\r\n\r\n<div>and brainwashed to be in the middle class. What happened to being </div>\r\n\r\n<div>an entrepreneur. </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press/">Can you even fathom having a $50,000.00 month? </a></div>\r\n\r\n<div> </div>\r\n\r\n<div>I can. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Why? </div>\r\n\r\n<div> </div>\r\n\r\n<div>Because it''s happened multiple times in my career. </div>\r\n\r\n<div>Now I haven''t been able to keep that consistent but the </div>\r\n\r\n<div>fact that I could earn $50,000 in a single month shows </div>\r\n\r\n<div>me I have the chance to be part of the super rich. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Don''t you want that lifestyle. Or are you happy with a job</div>\r\n\r\n<div>making a mediocre pay. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Something to think about. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' I know that sounds very harsh. Actually quite rude and mean.  \r\nBut honestly . . .  \r\n  \r\nIt''s the truth.  \r\n  \r\nSee we are programmed to go to school from an early age.  \r\nWake up a 7 then truck to school for eight hours. Sometimes  \r\neven staying 10 to 12 hours if we played sports.  \r\n  \r\nAfter school we head off to college and repeat this process. \r\nAfter college we go out to find a "job" so we can then take  \r\nour training wake up in the morning and go to work for 8 hours.  \r\n  \r\nDon''t get me wrong there is nothing wrong with hard work.  \r\n  \r\nBut what I don''t like is that the majority of people are programmed  \r\nand brainwashed to be in the middle class. What happened to being  \r\nan entrepreneur.  \r\n  \r\nCan you even fathom having a $50,000.00 month? \r\nhttp://a-ddiction.press/ \r\n  \r\nI can.  \r\n  \r\nWhy?  \r\n  \r\nBecause it''s happened multiple times in my career.  \r\nNow I haven''t been able to keep that consistent but the  \r\nfact that I could earn $50,000 in a single month shows  \r\nme I have the chance to be part of the super rich.  \r\n  \r\nDon''t you want that lifestyle. Or are you happy with a job \r\nmaking a mediocre pay.  \r\n  \r\nSomething to think about.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(23, 24, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="51fa924d-2c3e-8239-74f8-bf77817ec762">It was approximately 60 days ago that you registered on one of \r\n<div>our high ticket business information websites requesting more </div>\r\n\r\n<div>information on ways to earn $5000 to $250000 per month. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you would like to learn more about available jobs, open positions and </div>\r\n\r\n<div>available business opportunities click the Reply button above. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' It was approximately 60 days ago that you registered on one of  \r\nour high ticket business information websites requesting more  \r\ninformation on ways to earn $5000 to $250000 per month.  \r\n  \r\nIf you would like to learn more about available jobs, open positions\r\nand  \r\navailable business opportunities click the Reply button above.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(24, 25, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0152c892-1538-c59a-a186-dc7ea60d0010">We have live calls with some of our top performing \r\n<div>entrepreneurs in the business normally every week. </div>\r\n\r\n<div>They share some of their journeys but more importantly </div>\r\n\r\n<div>what you need to do to mimic the success. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Want to start earning $2500 to $25000 per month? </div>\r\n\r\n<div> </div>\r\n\r\n<div>This is for serious people only that want a real </div>\r\n\r\n<div>work at home style business.  </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press/">Message back for more details</a></div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' We have live calls with some of our top performing  \r\nentrepreneurs in the business normally every week.  \r\nThey share some of their journeys but more importantly  \r\nwhat you need to do to mimic the success.  \r\n  \r\nWant to start earning $2500 to $25000 per month?  \r\n  \r\nThis is for serious people only that want a real  \r\nwork at home style business.   \r\n  \r\nMessage back for more details http://a-ddiction.press/ \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(25, 26, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="ab42105c-e2df-3be1-493a-00e5bc48e4fc">Let me know if this works. We have a way for you to get into a business \r\n<div>that has the potential for you to earn between $1,000 to $25,000+ a month. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Understand that this is a business NOT a job. </div>\r\n\r\n<div> </div>\r\n\r\n<div>The business is set up to automate most of the work for you so that </div>\r\n\r\n<div>when you get customers they go through a sales process to learn about </div>\r\n\r\n<div>the company, product and benefits. </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press/">This eliminates you spending countless hours doing tedious tasks.</a> </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you need help making money maybe this business is for you if </div>\r\n\r\n<div>not we might have something else. Shoot us a reply back and let </div>\r\n\r\n<div>us know what specifically you are looking for and we can make it </div>\r\n\r\n<div>work. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Let me know if this works. We have a way for you to get into a\r\nbusiness  \r\nthat has the potential for you to earn between $1,000 to $25,000+ a\r\nmonth.  \r\n  \r\nUnderstand that this is a business NOT a job.  \r\n  \r\nThe business is set up to automate most of the work for you so that  \r\nwhen you get customers they go through a sales process to learn\r\nabout  \r\nthe company, product and benefits.  \r\n  \r\nThis eliminates you spending countless hours doing tedious tasks.\r\nhttp://a-ddiction.press/  \r\n  \r\nIf you need help making money maybe this business is for you if  \r\nnot we might have something else. Shoot us a reply back and let  \r\nus know what specifically you are looking for and we can make it  \r\nwork.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(26, 27, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="7b56f550-28d9-87b4-4a79-6f191850e417">What''s the [RANDOM_CONTENT:delay|hold off|postpone|wait|hold up|hesitate|holdup]? \r\n<div> </div>\r\n\r\n<div>Here''s some of the most common road blocks by other members. </div>\r\n\r\n<div> </div>\r\n\r\n<div>* They don''t know if they want a ''Job" or a "Work at home business". </div>\r\n\r\n<div> </div>\r\n\r\n<div>* Not sure if they trust working online. </div>\r\n\r\n<div> </div>\r\n\r\n<div>* Unsure of how to get started. </div>\r\n\r\n<div> </div>\r\n\r\n<div>* Don''t know if they will succeed or fail.  </div>\r\n\r\n<div> </div>\r\n\r\n<div>* Would need step by step guidance. </div>\r\n\r\n<div> </div>\r\n\r\n<div>* Time may be an issue for a work at home side-job. </div>\r\n\r\n<div> </div>\r\n\r\n<div>* Would the money earned be worth the effort. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If any of the above are possible questions that you have about starting a </div>\r\n\r\n<div>work at home job or business it''s perfectly normal. & More importantly we </div>\r\n\r\n<div>are used to answer these questions. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Send us an email let us know where you stand. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' What''s the [RANDOM_CONTENT:delay|hold off|postpone|wait|hold\r\nup|hesitate|holdup]?  \r\n  \r\nHere''s some of the most common road blocks by other members.  \r\n  \r\n* They don''t know if they want a ''Job" or a "Work at home business". \r\n\r\n  \r\n* Not sure if they trust working online.  \r\n  \r\n* Unsure of how to get started.  \r\n  \r\n* Don''t know if they will succeed or fail.   \r\n  \r\n* Would need step by step guidance.  \r\n  \r\n* Time may be an issue for a work at home side-job.  \r\n  \r\n* Would the money earned be worth the effort.  \r\n  \r\nIf any of the above are possible questions that you have about\r\nstarting a  \r\nwork at home job or business it''s perfectly normal. & More importantly\r\nwe  \r\nare used to answer these questions.  \r\n  \r\nSend us an email let us know where you stand.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(27, 28, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="47e48b9b-3e59-5056-d00f-bb84b1a3080c"><a href="http://a-ddiction.press/">Step by step hand holding guidance to help you start making</a> \r\n<div> </div>\r\n\r\n<div>over $2,000.00 a month minimum. We understand that sometimes </div>\r\n\r\n<div> </div>\r\n\r\n<div>it''s difficult to get the ball rolling. Our company provides what you need </div>\r\n\r\n<div> </div>\r\n\r\n<div>to get moving and to start making money fast. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Here''s what you need to do: </div>\r\n\r\n<div> </div>\r\n\r\n<div>1. Reply back to this email saying, "Give me more information". </div>\r\n\r\n<div> </div>\r\n\r\n<div>2. Be serious about a work at home business. </div>\r\n\r\n<div> </div>\r\n\r\n<div>This is not a hobby. </div>\r\n\r\n<div>This is not some make money rich thing. </div>\r\n\r\n<div> </div>\r\n\r\n<div>This is a real way to generate some money by doing some actual work. </div>\r\n\r\n<div>The company is willing to hold your hand and get you moving but you need </div>\r\n\r\n<div>to be willing to put in some effort. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you are still serious about working from home after this email. </div>\r\n\r\n<div>Simply reply. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Step by step hand holding guidance to help you start making\r\nhttp://a-ddiction.press/  \r\n  \r\nover $2,000.00 a month minimum. We understand that sometimes  \r\n  \r\nit''s difficult to get the ball rolling. Our company provides what you\r\nneed  \r\n  \r\nto get moving and to start making money fast.  \r\n  \r\nHere''s what you need to do:  \r\n  \r\n1. Reply back to this email saying, "Give me more information".  \r\n  \r\n2. Be serious about a work at home business.  \r\n  \r\nThis is not a hobby.  \r\nThis is not some make money rich thing.  \r\n  \r\nThis is a real way to generate some money by doing some actual work. \r\n\r\nThe company is willing to hold your hand and get you moving but you\r\nneed  \r\nto be willing to put in some effort.  \r\n  \r\nIf you are still serious about working from home after this email.  \r\nSimply reply.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(28, 29, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="4374e688-80b2-ebbc-9bab-fedf30cfe4bf">Sometimes we get into a position that we need to start \r\n<div>making an income fast. We don''t have time to go on job </div>\r\n\r\n<div>interviews or to take some min. wage position waiting </div>\r\n\r\n<div>weeks on end for a weak paycheck. </div>\r\n\r\n<div> </div>\r\n\r\n<div>We have a solid business that allows you to get in </div>\r\n\r\n<div>and start making some serious money fast. </div>\r\n\r\n<div> </div>\r\n\r\n<div><strong><a href="http://a-ddiction.press/">Need the information? </a></strong></div>\r\n\r\n<div> </div>\r\n\r\n<div>Message back saying "send the info please". </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Sometimes we get into a position that we need to start  \r\nmaking an income fast. We don''t have time to go on job  \r\ninterviews or to take some min. wage position waiting  \r\nweeks on end for a weak paycheck.  \r\n  \r\nWe have a solid business that allows you to get in  \r\nand start making some serious money fast.  \r\n  \r\nNEED THE INFORMATION?  HTTP://A-DDICTION.PRESS/ \r\n  \r\nMessage back saying "send the info please".  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(29, 30, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="604ce906-2755-6cc0-d738-561fa8ea538d">Primary question we get from someone wanting to \r\n<div>work online / from home is . . . whats the next step? </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press/"><strong>It''s quite easy. </strong></a></div>\r\n\r\n<div> </div>\r\n\r\n<div>We need to know if you are serious so please </div>\r\n\r\n<div>click the Reply Button sending us an email saying </div>\r\n\r\n<div>"I''m interested in working from home whats the next step". </div>\r\n\r\n<div> </div>\r\n\r\n<div>We will have one of our team or coaches contact you to </div>\r\n\r\n<div>give you step by step instructions so you can decide if working </div>\r\n\r\n<div>from home is for you or not. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Primary question we get from someone wanting to  \r\nwork online / from home is . . . whats the next step?  \r\n  \r\nIT''S QUITE EASY.  http://a-ddiction.press/ \r\n  \r\nWe need to know if you are serious so please  \r\nclick the Reply Button sending us an email saying  \r\n"I''m interested in working from home whats the next step".  \r\n  \r\nWe will have one of our team or coaches contact you to  \r\ngive you step by step instructions so you can decide if working  \r\nfrom home is for you or not.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes');
INSERT INTO `mw_campaign_template` (`template_id`, `campaign_id`, `customer_template_id`, `content`, `inline_css`, `minify`, `plain_text`, `only_plain_text`, `auto_plain_text`) VALUES
(30, 31, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="2244b01e-65b0-a436-d565-1c33520669b7">Here''s how it works. \r\n<div> </div>\r\n\r\n<div>1. The company eliminates 85% of the hard work for it''s employees. </div>\r\n\r\n<div> </div>\r\n\r\n<div>2. They provide affiliate sales pages and lead capture pages. </div>\r\n\r\n<div> </div>\r\n\r\n<div>3. The company provides what to say and how to say it, whether that </div>\r\n\r\n<div>is on the phone, email or private message. </div>\r\n\r\n<div> </div>\r\n\r\n<div>4. <a href="http://a-ddiction.press/"><strong>Step by step training</strong></a>.  </div>\r\n\r\n<div> </div>\r\n\r\n<div>When you have a company holding you by the hand to help you </div>\r\n\r\n<div>succeed earning $2,500 to $25,000+ every single month is actually </div>\r\n\r\n<div>quite easy.   </div>\r\n\r\n<div> </div>\r\n\r\n<div>Reply if you want more information. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Here''s how it works.  \r\n  \r\n1. The company eliminates 85% of the hard work for it''s employees.  \r\n  \r\n2. They provide affiliate sales pages and lead capture pages.  \r\n  \r\n3. The company provides what to say and how to say it, whether that  \r\nis on the phone, email or private message.  \r\n  \r\n4. STEP BY STEP TRAINING http://a-ddiction.press/.   \r\n  \r\nWhen you have a company holding you by the hand to help you  \r\nsucceed earning $2,500 to $25,000+ every single month is actually  \r\nquite easy.    \r\n  \r\nReply if you want more information.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(31, 32, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="190fa26e-8e06-438e-d023-d578d99a7bf7">There''s nothing like getting email notifications that you have \r\n<div>a payment received. I mean absolutely nothing like it. </div>\r\n\r\n<div> </div>\r\n\r\n<div>and no. . . it doesn''t get old no matter what success level you are on. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Now if you haven''t earned a commissions or payment online before, </div>\r\n\r\n<div>well that''s actually not very hard. See there is a simple process to </div>\r\n\r\n<div>follow. </div>\r\n\r\n<div> </div>\r\n\r\n<div>I wish someone would have told me about it when I first started online. </div>\r\n\r\n<div>But I had to figure it out on my own.</div>\r\n\r\n<div> </div>\r\n\r\n<div>Anyways.... </div>\r\n\r\n<div> </div>\r\n\r\n<div>The process goes like this and if you follow it you can easily make </div>\r\n\r\n<div>$5,000 or $15,000 or $25,000+ a month working from home. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Step 1 - Need a <strong><a href="http://a-ddiction.press/">good product or company</a></strong> that sells something people want. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Step 2 - Need a way to collect names & emails of your potential customers so you </div>\r\n\r\n<div>can follow up with them. This is necessary for any small or big business. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Step 3 - Sales funnel is absolutely needed. A way to present the product so that a </div>\r\n\r\n<div>potential customer gets value and purchases a product or multiple products. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Step 4 - A great sales process all together. Getting customers is one thing but selling </div>\r\n\r\n<div>something that has the customer returning to continue to buy is the key.  </div>\r\n\r\n<div> </div>\r\n\r\n<div> </div>\r\n\r\n<div>Okay so how do you do this? </div>\r\n\r\n<div> </div>\r\n\r\n<div>Well I''ll tell you more just shoot me an email saying, "Can you tell me more about the sales process". </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' There''s nothing like getting email notifications that you have  \r\na payment received. I mean absolutely nothing like it.  \r\n  \r\nand no. . . it doesn''t get old no matter what success level you are\r\non.  \r\n  \r\nNow if you haven''t earned a commissions or payment online before,  \r\nwell that''s actually not very hard. See there is a simple process to \r\n\r\nfollow.  \r\n  \r\nI wish someone would have told me about it when I first started\r\nonline.  \r\nBut I had to figure it out on my own. \r\n  \r\nAnyways....  \r\n  \r\nThe process goes like this and if you follow it you can easily make  \r\n$5,000 or $15,000 or $25,000+ a month working from home.  \r\n  \r\nStep 1 - Need a GOOD PRODUCT OR COMPANY HTTP://A-DDICTION.PRESS/ that\r\nsells something people want.  \r\n  \r\nStep 2 - Need a way to collect names & emails of your potential\r\ncustomers so you  \r\ncan follow up with them. This is necessary for any small or big\r\nbusiness.  \r\n  \r\nStep 3 - Sales funnel is absolutely needed. A way to present the\r\nproduct so that a  \r\npotential customer gets value and purchases a product or multiple\r\nproducts.  \r\n  \r\nStep 4 - A great sales process all together. Getting customers is one\r\nthing but selling  \r\nsomething that has the customer returning to continue to buy is the\r\nkey.   \r\n  \r\n  \r\nOkay so how do you do this?  \r\n  \r\nWell I''ll tell you more just shoot me an email saying, "Can you tell\r\nme more about the sales process".  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(32, 33, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="7a2cc067-8585-902b-6c87-356d7a105c5b">This new business has testimonial after testimonial of normal \r\n<div>average working people making over $5k monthly. Do you want </div>\r\n\r\n<div>the URL to <a href="http://a-ddiction.press/"><strong>check out the information</strong></a> about the business? </div>\r\n\r\n<div> </div>\r\n\r\n<div>Let us know by simply clicking the reply button above. </div>\r\n\r\n<div><br />\r\n </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' This new business has testimonial after testimonial of normal  \r\naverage working people making over $5k monthly. Do you want  \r\nthe URL to CHECK OUT THE INFORMATION http://a-ddiction.press/ about\r\nthe business?  \r\n  \r\nLet us know by simply clicking the reply button above.  \r\n\r\n   Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(33, 34, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="bcf83c6f-107d-cf33-d5a9-3eebaaea6e6f">Many people have a hard time working online. \r\n<div> </div>\r\n\r\n<div>After years of experience </div>\r\n\r\n<div>what I''ve discovered is people just have no clue how to set up an automation </div>\r\n\r\n<div>process to actually help you run the business. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Let''s say a company could help you nearly automate your entire business. </div>\r\n\r\n<div><a href="http://a-ddiction.press/"><strong>Is it something you want to know more about?</strong></a> </div>\r\n\r\n<div> </div>\r\n\r\n<div>Reply and let me know : Sure I''d hear more.  </div>\r\n\r\n<div> </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Many people have a hard time working online.  \r\n  \r\nAfter years of experience  \r\nwhat I''ve discovered is people just have no clue how to set up an\r\nautomation  \r\nprocess to actually help you run the business.  \r\n  \r\nLet''s say a company could help you nearly automate your entire\r\nbusiness.  \r\nIS IT SOMETHING YOU WANT TO KNOW MORE ABOUT?\r\nhttp://a-ddiction.press/  \r\n  \r\nReply and let me know : Sure I''d hear more.   \r\n  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(34, 35, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="7c9b2dd4-6682-b5b8-e99f-1f7ecf94ad02">We have business associates that have clients earning between \r\n<div>$5000 - $20,000 per-month within their businesses. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Should we </div>\r\n\r\n<div>send the information to the email address ( [EMAIL]  ) we have on file? </div>\r\n\r\n<div> </div>\r\n\r\n<div>Please let us know by clicking the reply button above. <br />\r\n </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' We have business associates that have clients earning between  \r\n$5000 - $20,000 per-month within their businesses.  \r\n  \r\nShould we  \r\nsend the information to the email address ( [EMAIL]  ) we have on\r\nfile?  \r\n  \r\nPlease let us know by clicking the reply button above. \r\n   Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(35, 36, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="80a0df61-670a-d1da-36d3-a82850ca5b86">So I just want to make sure that this email address still works ( [EMAIL] ) \r\n<div>and that you are getting our emails. </div>\r\n\r\n<div> </div>\r\n\r\n<div>If you see this could you help me out </div>\r\n\r\n<div>and simply respond saying, "Yes I see the emails". </div>\r\n\r\n<div> </div>\r\n\r\n<div><a href="http://a-ddiction.press/"><strong>Thanks so much. </strong></a></div>\r\n\r\n<div>Not sure if this is a technical issue or if things are working. </div>\r\n\r\n<div> </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' So I just want to make sure that this email address still works (\r\n[EMAIL] )  \r\nand that you are getting our emails.  \r\n  \r\nIf you see this could you help me out  \r\nand simply respond saying, "Yes I see the emails".  \r\n  \r\nTHANKS SO MUCH.  http://a-ddiction.press/ \r\nNot sure if this is a technical issue or if things are working.  \r\n  Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(36, 37, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="7bd1c4dc-d747-25d6-fd92-1178b5bb81ac">Could you shoot me a quick reply back just saying "Hey I see this still"\r\n<div> </div>\r\n\r\n<div>Recently I was playing with </div>\r\n\r\n<div>my settings and made some changes. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Email can get confusing sometimes. </div>\r\n\r\n<div>Not sure if I did something to completely </div>\r\n\r\n<div>dis configure my mailbox.</div>\r\n\r\n<div> </div>\r\n\r\n<div>I appreciate the help. <br />\r\n </div>\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\n<a href="http://a-ddiction.press/">http://a-ddiction.press/</a><br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a></body>\r\n</html>\r\n', 'no', 'no', ' Could you shoot me a quick reply back just saying "Hey I see this\r\nstill" \r\n  \r\nRecently I was playing with  \r\nmy settings and made some changes.  \r\n  \r\nEmail can get confusing sometimes.  \r\nNot sure if I did something to completely  \r\ndis configure my mailbox. \r\n  \r\nI appreciate the help. \r\n   Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/ http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] ', 'no', 'yes'),
(40, 47, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="7bd1c4dc-d747-25d6-fd92-1178b5bb81ac">Could you shoot me a quick reply back just saying "Hey I see this still"\r\n<div> </div>\r\n\r\n<div>Recently I was playing with </div>\r\n\r\n<div>my settings and made some changes. </div>\r\n\r\n<div> </div>\r\n\r\n<div>Email can get confusing sometimes. </div>\r\n\r\n<div>Not sure if I did something to completely </div>\r\n\r\n<div>dis configure my mailbox.</div>\r\n\r\n<div> </div>\r\n\r\n<div>I appreciate the help. </div>\r\n\r\n\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\nhttp://a-ddiction.press/<br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a>', 'no', 'no', ' Could you shoot me a quick reply back just saying \r\n"Hey I see this still" Or visit http://a-ddiction.press/ \r\n\r\nRecently I was playing with \r\nmy settings and made some changes. \r\n\r\nEmail can get confusing sometimes. \r\nNot sure if I did something to completely \r\ndis configure my mailbox. \r\n\r\nI appreciate the help. \rJohn Miles \r\nhttp://a-ddiction.press \r\n\r\n[UNSUBSCRIBE_URL]', 'no', 'yes'),
(41, 48, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0e5ee549-1f00-99a4-c714-b72644b3b06c">It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our last [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone call]<br />\r\nYou are still using this [RANDOM_CONTENT:email|e-mail|e mail|mail|email address] [EMAIL] ?<br />\r\n<br />\r\nThis [RANDOM_CONTENT:month|week|year] we are planning to share some [RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very good|amazing|awesome] [RANDOM_CONTENT:information|info|details|facts|information and facts|material|tips|advices|knowledge].<br />\r\nshowing people how they can start [RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] and [RANDOM_CONTENT:generate|make] a part time or full time income.<br />\r\n<br />\r\nBut if you are interested in learning about Online businesses<br />\r\nthat are available for you please click reply and say "[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I like|I''d like to see|We need] [RANDOM_CONTENT:more information|more info|more details|details|much more information|additional information|more info|addiitional information|more info .|additional info]".<br />\r\n<br />\r\nIf you no longer are interested in getting information about<br />\r\n[RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] please use the unsubscribe link below.<br />\r\n<br />\r\n<br />\r\n\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\nhttp://a-ddiction.press/<br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a>', 'no', 'no', ' It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our\r\nlast\r\n[RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone\r\ncall]\r\n You are still using this [RANDOM_CONTENT:email|e-mail|e\r\nmail|mail|email address] [EMAIL] ?\r\n\r\n This [RANDOM_CONTENT:month|week|year] we are planning to share some\r\n[RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very\r\ngood|amazing|awesome]\r\n[RANDOM_CONTENT:information|info|details|facts|information and\r\nfacts|material|tips|advices|knowledge].\r\n showing people how they can start [RANDOM_CONTENT:work from home|work\r\nat home|home based business|online business|do business from\r\nhome|internet business] and [RANDOM_CONTENT:generate|make] a part time\r\nor full time income.\r\n\r\n But if you are interested in learning about Online businesses\r\n that are available for you please click reply and say\r\n"[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I\r\nlike|I''d like to see|We need] [RANDOM_CONTENT:more information|more\r\ninfo|more details|details|much more information|additional\r\ninformation|more info|addiitional information|more info .|additional\r\ninfo]".\r\n\r\n If you no longer are interested in getting information about\r\n [RANDOM_CONTENT:work from home|work at home|home based\r\nbusiness|online business|do business from home|internet business]\r\nplease use the unsubscribe link below.\r\n\r\n Talk Soon,\r\n Miki Miles\r\n\r\n [UNSUBSCRIBE_URL]', 'no', 'yes'),
(42, 49, NULL, '<!DOCTYPE html>\r\n<html>\r\n<head><meta name="charset" content="utf-8">\r\n	<title></title>\r\n</head>\r\n<body data-gramm="true" data-gramm_editor="true" data-gramm_id="0e5ee549-1f00-99a4-c714-b72644b3b06c">It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our last [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone call]<br />\r\nYou are still using this [RANDOM_CONTENT:email|e-mail|e mail|mail|email address] [EMAIL] ?<br />\r\n<br />\r\nThis [RANDOM_CONTENT:month|week|year] we are planning to share some [RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very good|amazing|awesome] [RANDOM_CONTENT:information|info|details|facts|information and facts|material|tips|advices|knowledge].<br />\r\nshowing people how they can start [RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] and [RANDOM_CONTENT:generate|make] a part time or full time income.<br />\r\n<br />\r\nBut if you are interested in learning about Online businesses<br />\r\nthat are available for you please click reply and say "[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I like|I''d like to see|We need] [RANDOM_CONTENT:more information|more info|more details|details|much more information|additional information|more info|addiitional information|more info .|additional info]".<br />\r\n<br />\r\nIf you no longer are interested in getting information about<br />\r\n[RANDOM_CONTENT:work from home|work at home|home based business|online business|do business from home|internet business] please use the unsubscribe link below.<br />\r\n<br />\r\n<br />\r\nTalk Tomorrow,<br />\r\n<span style="font-family:''lucida sans unicode'', ''lucida grande'', sans-serif;">Miki Miles</span><br />\r\nIncome Boost Inc.<br />\r\nhttp://a-ddiction.press/<br />\r\n<br />\r\nMy story....<br />\r\nI become successful at the age of 21, I was thinking that the whole world was mine.<br />\r\n2 years later I was addicted and my family didn''t even recognize me.<br />\r\nI know some of you are in the same boat now, so I ask you, please do yourself a favor, and get some help.<br />\r\nIt wasn''t until I called these guys up - <a href="tel:855-6263-725">(855) 6263-725</a>, and actually went and did the whole recovery process.<br />\r\nI''m balanced and relaxed now, I grow my business, and I am happy. Call them, and get help, you know you need it.<br />\r\n<br />\r\n<br />\r\n<a href="[UNSUBSCRIBE_URL]">Report this email</a>  - <a href="[UNSUBSCRIBE_URL]">Unsubscribe this email</a> <br />\r\n<br />\r\n </body>\r\n</html>\r\n', 'no', 'no', ' It''s been [RANDOM_CONTENT:24|26|32|48|60|43|13|52|55] days since our\r\nlast [RANDOM_CONTENT:conversation|discussion|chat|talk|connection|phone\r\ncall]\r\n You are still using this [RANDOM_CONTENT:email|e-mail|e\r\nmail|mail|email address] [EMAIL] ?\r\n\r\n This [RANDOM_CONTENT:month|week|year] we are planning to share some\r\n[RANDOM_CONTENT:great|excellent|fantastic|wonderful|good|terrific|superb|very\r\ngood|amazing|awesome]\r\n[RANDOM_CONTENT:information|info|details|facts|information and\r\nfacts|material|tips|advices|knowledge].\r\n showing people how they can start [RANDOM_CONTENT:work from home|work\r\nat home|home based business|online business|do business from\r\nhome|internet business] and [RANDOM_CONTENT:generate|make] a part time\r\nor full time income.\r\n\r\n But if you are interested in learning about Online businesses\r\n that are available for you please click reply and say\r\n"[RANDOM_CONTENT:I want|I''d like|I would like|I need|I''d prefer|I\r\nlike|I''d like to see|We need] [RANDOM_CONTENT:more information|more\r\ninfo|more details|details|much more information|additional\r\ninformation|more info|addiitional information|more info .|additional\r\ninfo]".\r\n\r\n If you no longer are interested in getting information about\r\n [RANDOM_CONTENT:work from home|work at home|home based\r\nbusiness|online business|do business from home|internet business]\r\nplease use the unsubscribe link below.\r\n\r\n Talk Soon,\r\n Miki Miles\r\n Prove My Profits Inc.\r\n http://a-ddiction.press/\r\n\r\n My story....\r\n I become successful at the age of 21, I was thinking that the whole\r\nworld was mine.\r\n 2 years later I was addicted and my family didn''t even recognize me.\r\n I know some of you are in the same boat now, so I ask you, please do\r\nyourself a favor, and get some help.\r\n It wasn''t until I called these guys up - (855) 6263-725\r\ntel:855-6263-725, and actually went and did the whole recovery\r\nprocess.\r\n I''m balanced and relaxed now, I grow my business, and I am happy.\r\nCall them, and get help, you know you need it.\r\n\r\n Report this email [UNSUBSCRIBE_URL]  - Unsubscribe this email\r\n[UNSUBSCRIBE_URL] \r\n\r\n   ', 'no', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_template_url_action_list_field`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_template_url_action_list_field` (
`url_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_template_url_action_subscriber`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_template_url_action_subscriber` (
`url_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `action` char(5) NOT NULL DEFAULT 'copy',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_temporary_source`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_temporary_source` (
`source_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `segment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_to_delivery_server`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_to_delivery_server` (
  `campaign_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_track_open`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_track_open` (
`id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_track_unsubscribe`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_track_unsubscribe` (
`id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_track_url`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_track_url` (
`id` bigint(20) NOT NULL,
  `url_id` bigint(20) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_campaign_url`
--

CREATE TABLE IF NOT EXISTS `mw_campaign_url` (
`url_id` bigint(20) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `hash` char(40) NOT NULL,
  `destination` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `mw_campaign_url`
--

INSERT INTO `mw_campaign_url` (`url_id`, `campaign_id`, `hash`, `destination`, `date_added`) VALUES
(10, 7, '8927757fe92ed2899c695120602ddb8609a6f6d1', '[UNSUBSCRIBE_URL]', '2016-12-18 10:18:03'),
(11, 2, '31842f9dba2990b7d0e72a733a9b9092d1eca424', '[UNSUBSCRIBE_URL]', '2016-12-22 10:38:04'),
(12, 49, '93be120a99395a513d0b04205bf79c804c58c3d0', '[UNSUBSCRIBE_URL]', '2016-12-22 10:48:04'),
(13, 7, 'ddca04d38561a3bb2f7f9e143c5f4b0fe50eb103', 'http://a-ddiction.press/', '2016-12-22 10:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `mw_company_type`
--

CREATE TABLE IF NOT EXISTS `mw_company_type` (
`type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `mw_company_type`
--

INSERT INTO `mw_company_type` (`type_id`, `name`, `date_added`, `last_updated`) VALUES
(1, 'Agriculture and Food Services', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(2, 'Architecture and Construction', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(3, 'Arts and Artists', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(4, 'Beauty and Personal Care', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(5, 'Business and Finance', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(6, 'Computers and Electronics', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(7, 'Construction ', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(8, 'Consulting', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(9, 'Creative Services/Agency', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(10, 'Daily Deals/E-Coupons', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(11, 'eCommerce', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(12, 'Education and Training', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(13, 'Entertainment and Events', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(14, 'Gambling', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(15, 'Games', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(16, 'Government', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(17, 'Health and Fitness', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(18, 'Hobbies', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(19, 'Home and Garden', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(20, 'Insurance', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(21, 'Legal', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(22, 'Manufacturing', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(23, 'Marketing and Advertising', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(24, 'Media and Publishing', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(25, 'Medical, Dental, and Healthcare', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(26, 'Mobile', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(27, 'Music and Musicians', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(28, 'Non-Profit', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(29, 'Pharmaceuticals', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(30, 'Photo and Video', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(31, 'Politics', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(32, 'Professional Services', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(33, 'Public Relations', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(34, 'Real Estate', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(35, 'Recruitment and Staffing', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(36, 'Religion', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(37, 'Restaurant and Venue', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(38, 'Retail', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(39, 'Social Networks and Online Communities', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(40, 'Software and Web App', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(41, 'Sports', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(42, 'Telecommunications', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(43, 'Travel and Transportation', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(44, 'Vitamin supplements', '2014-08-31 09:45:05', '2014-08-31 09:45:05'),
(45, 'Other', '2014-08-31 09:45:05', '2014-08-31 09:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `mw_country`
--

CREATE TABLE IF NOT EXISTS `mw_country` (
`country_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` char(3) NOT NULL,
  `status` char(10) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `mw_country`
--

INSERT INTO `mw_country` (`country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(1, 'Afghanistan', 'AF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:25'),
(2, 'Albania', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3, 'Algeria', 'DZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(4, 'American Samoa', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(5, 'Andorra', 'AD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(6, 'Angola', 'AO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(7, 'Anguilla', 'AI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(8, 'Antarctica', 'AQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(9, 'Antigua and Barbuda', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(10, 'Argentina', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(11, 'Armenia', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(12, 'Aruba', 'AW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(13, 'Australia', 'AU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(14, 'Austria', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(15, 'Azerbaijan', 'AZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(16, 'Bahamas', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(17, 'Bahrain', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(18, 'Bangladesh', 'BD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(19, 'Barbados', 'BB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(20, 'Belarus', 'BY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(21, 'Belgium', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(22, 'Belize', 'BZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(23, 'Benin', 'BJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(24, 'Bermuda', 'BM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(25, 'Bhutan', 'BT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(26, 'Bolivia', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(27, 'Bosnia and Herzegowina', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(28, 'Botswana', 'BW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(29, 'Bouvet Island', 'BV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(30, 'Brazil', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(31, 'British Indian Ocean Territory', 'IO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(32, 'Brunei Darussalam', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(33, 'Bulgaria', 'BG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(34, 'Burkina Faso', 'BF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(35, 'Burundi', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(36, 'Cambodia', 'KH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(37, 'Cameroon', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(38, 'Canada', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(39, 'Cape Verde', 'CV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(40, 'Cayman Islands', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(41, 'Central African Republic', 'CF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(42, 'Chad', 'TD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(43, 'Chile', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(44, 'China', 'CN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(45, 'Christmas Island', 'CX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(46, 'Cocos (Keeling) Islands', 'CC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(47, 'Colombia', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(48, 'Comoros', 'KM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(49, 'Congo', 'CG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(50, 'Cook Islands', 'CK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(51, 'Costa Rica', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(52, 'Cote D''Ivoire', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(53, 'Croatia', 'HR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(54, 'Cuba', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(55, 'Cyprus', 'CY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(56, 'Czech Republic', 'CZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(57, 'Denmark', 'DK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(58, 'Djibouti', 'DJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(59, 'Dominica', 'DM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(60, 'Dominican Republic', 'DO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(61, 'East Timor', 'TP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(62, 'Ecuador', 'EC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(63, 'Egypt', 'EG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(64, 'El Salvador', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(65, 'Equatorial Guinea', 'GQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(66, 'Eritrea', 'ER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(67, 'Estonia', 'EE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(68, 'Ethiopia', 'ET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(69, 'Falkland Islands (Malvinas)', 'FK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(70, 'Faroe Islands', 'FO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(71, 'Fiji', 'FJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(72, 'Finland', 'FI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(73, 'France', 'FR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(74, 'France, Metropolitan', 'FX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(75, 'French Guiana', 'GF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(76, 'French Polynesia', 'PF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(77, 'French Southern Territories', 'TF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(78, 'Gabon', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(79, 'Gambia', 'GM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(80, 'Georgia', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(81, 'Germany', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(82, 'Ghana', 'GH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(83, 'Gibraltar', 'GI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(84, 'Greece', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(85, 'Greenland', 'GL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(86, 'Grenada', 'GD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(87, 'Guadeloupe', 'GP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(88, 'Guam', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(89, 'Guatemala', 'GT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(90, 'Guinea', 'GN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(91, 'Guinea-bissau', 'GW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(92, 'Guyana', 'GY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(93, 'Haiti', 'HT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(94, 'Heard and Mc Donald Islands', 'HM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(95, 'Honduras', 'HN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(96, 'Hong Kong', 'HK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(97, 'Hungary', 'HU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(98, 'Iceland', 'IS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(99, 'India', 'IN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(100, 'Indonesia', 'ID', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(101, 'Iran (Islamic Republic of)', 'IR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(102, 'Iraq', 'IQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(103, 'Ireland', 'IE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(104, 'Israel', 'IL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(105, 'Italy', 'IT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(106, 'Jamaica', 'JM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(107, 'Japan', 'JP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(108, 'Jordan', 'JO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(109, 'Kazakhstan', 'KZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(110, 'Kenya', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(111, 'Kiribati', 'KI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(112, 'North Korea', 'KP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(113, 'Korea, Republic of', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(114, 'Kuwait', 'KW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(115, 'Kyrgyzstan', 'KG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(116, 'Lao People''s Democratic Republic', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(117, 'Latvia', 'LV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(118, 'Lebanon', 'LB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(119, 'Lesotho', 'LS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(120, 'Liberia', 'LR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(121, 'Libyan Arab Jamahiriya', 'LY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(122, 'Liechtenstein', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(123, 'Lithuania', 'LT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(124, 'Luxembourg', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(125, 'Macau', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(126, 'FYROM', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(127, 'Madagascar', 'MG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(128, 'Malawi', 'MW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(129, 'Malaysia', 'MY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(130, 'Maldives', 'MV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(131, 'Mali', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(132, 'Malta', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(133, 'Marshall Islands', 'MH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(134, 'Martinique', 'MQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(135, 'Mauritania', 'MR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(136, 'Mauritius', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(137, 'Mayotte', 'YT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(138, 'Mexico', 'MX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(139, 'Micronesia, Federated States of', 'FM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(140, 'Moldova, Republic of', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(141, 'Monaco', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(142, 'Mongolia', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(143, 'Montserrat', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(144, 'Morocco', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(145, 'Mozambique', 'MZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(146, 'Myanmar', 'MM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(147, 'Namibia', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(148, 'Nauru', 'NR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(149, 'Nepal', 'NP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(150, 'Netherlands', 'NL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(151, 'Netherlands Antilles', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(152, 'New Caledonia', 'NC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(153, 'New Zealand', 'NZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(154, 'Nicaragua', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(155, 'Niger', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(156, 'Nigeria', 'NG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(157, 'Niue', 'NU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(158, 'Norfolk Island', 'NF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(159, 'Northern Mariana Islands', 'MP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(160, 'Norway', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(161, 'Oman', 'OM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(162, 'Pakistan', 'PK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(163, 'Palau', 'PW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(164, 'Panama', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(165, 'Papua New Guinea', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(166, 'Paraguay', 'PY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(167, 'Peru', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(168, 'Philippines', 'PH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(169, 'Pitcairn', 'PN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(170, 'Poland', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(171, 'Portugal', 'PT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(172, 'Puerto Rico', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(173, 'Qatar', 'QA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(174, 'Reunion', 'RE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(175, 'Romania', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(176, 'Russian Federation', 'RU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(177, 'Rwanda', 'RW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(178, 'Saint Kitts and Nevis', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(179, 'Saint Lucia', 'LC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(180, 'Saint Vincent and the Grenadines', 'VC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(181, 'Samoa', 'WS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(182, 'San Marino', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(183, 'Sao Tome and Principe', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(184, 'Saudi Arabia', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(185, 'Senegal', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(186, 'Seychelles', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(187, 'Sierra Leone', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(188, 'Singapore', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(189, 'Slovak Republic', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(190, 'Slovenia', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(191, 'Solomon Islands', 'SB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(192, 'Somalia', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(193, 'South Africa', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(195, 'Spain', 'ES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(196, 'Sri Lanka', 'LK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(197, 'St. Helena', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(198, 'St. Pierre and Miquelon', 'PM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(199, 'Sudan', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(200, 'Suriname', 'SR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(202, 'Swaziland', 'SZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(203, 'Sweden', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(204, 'Switzerland', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(205, 'Syrian Arab Republic', 'SY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(206, 'Taiwan', 'TW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(207, 'Tajikistan', 'TJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(208, 'Tanzania, United Republic of', 'TZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(209, 'Thailand', 'TH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(210, 'Togo', 'TG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(211, 'Tokelau', 'TK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(212, 'Tonga', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(213, 'Trinidad and Tobago', 'TT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(214, 'Tunisia', 'TN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(215, 'Turkey', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(216, 'Turkmenistan', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(217, 'Turks and Caicos Islands', 'TC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(218, 'Tuvalu', 'TV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(219, 'Uganda', 'UG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(220, 'Ukraine', 'UA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(221, 'United Arab Emirates', 'AE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(222, 'United Kingdom', 'GB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(223, 'United States', 'US', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(224, 'United States Minor Outlying Islands', 'UM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(225, 'Uruguay', 'UY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(226, 'Uzbekistan', 'UZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(227, 'Vanuatu', 'VU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(228, 'Vatican City State (Holy See)', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(229, 'Venezuela', 'VE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(230, 'Viet Nam', 'VN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(231, 'Virgin Islands (British)', 'VG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(232, 'Virgin Islands (U.S.)', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(233, 'Wallis and Futuna Islands', 'WF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(234, 'Western Sahara', 'EH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(235, 'Yemen', 'YE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(236, 'Yugoslavia', 'YU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(237, 'Democratic Republic of Congo', 'CD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(238, 'Zambia', 'ZM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(239, 'Zimbabwe', 'ZW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `mw_currency`
--

CREATE TABLE IF NOT EXISTS `mw_currency` (
`currency_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` char(3) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `is_default` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_currency`
--

INSERT INTO `mw_currency` (`currency_id`, `name`, `code`, `value`, `is_default`, `status`, `date_added`, `last_updated`) VALUES
(1, 'US Dollar', 'USD', '1.00000000', 'yes', 'active', '2014-05-17 00:00:00', '2014-05-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer`
--

CREATE TABLE IF NOT EXISTS `mw_customer` (
`customer_id` int(11) NOT NULL,
  `customer_uid` char(13) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` char(34) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `hourly_quota` int(11) NOT NULL DEFAULT '0',
  `removable` enum('yes','no') NOT NULL DEFAULT 'yes',
  `confirmation_key` char(40) DEFAULT NULL,
  `oauth_uid` bigint(20) DEFAULT NULL,
  `oauth_provider` char(10) DEFAULT NULL,
  `status` char(15) NOT NULL DEFAULT 'inactive',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_customer`
--

INSERT INTO `mw_customer` (`customer_id`, `customer_uid`, `group_id`, `language_id`, `first_name`, `last_name`, `email`, `password`, `timezone`, `avatar`, `hourly_quota`, `removable`, `confirmation_key`, `oauth_uid`, `oauth_provider`, `status`, `date_added`, `last_updated`) VALUES
(1, 'cx075oc6gz028', NULL, NULL, 'Miki', 'Miles', 'admin@a-ddiction.press', '$P$GgkE0BEj5UHVr9HregwJ3Are.rjiLc0', 'Asia/Singapore', NULL, 0, 'yes', '4c95e45a7e5be0a9328f346dd4a7139e4238d2dc', NULL, NULL, 'active', '2016-03-09 19:26:55', '2016-11-08 18:18:38');

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_action_log`
--

CREATE TABLE IF NOT EXISTS `mw_customer_action_log` (
`log_id` bigint(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL DEFAULT 'info',
  `reference_id` int(11) NOT NULL DEFAULT '0',
  `reference_relation_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=264 ;

--
-- Dumping data for table `mw_customer_action_log`
--

INSERT INTO `mw_customer_action_log` (`log_id`, `customer_id`, `category`, `reference_id`, `reference_relation_id`, `message`, `date_added`) VALUES
(155, 1, 'lists.deleted', 0, 0, 'The list "bounce list #1" has been successfully deleted!', '2016-12-15 15:21:12'),
(156, 1, 'lists.deleted', 0, 0, 'The list "test" has been successfully deleted!', '2016-12-15 15:21:15'),
(157, 1, 'lists.deleted', 0, 0, 'The list "bounce list" has been successfully deleted!', '2016-12-15 15:21:18'),
(158, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "jesco_corp@yahoo.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-15 15:21:29'),
(163, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "hacvan.a@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-18 08:57:49'),
(164, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "hacvana@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-18 08:57:59'),
(168, 1, 'lists.campaigns.deleted', 44, 2, 'The campaign(autoresponder) called "MainList #2(Deleted)" has been successfully deleted!', '2016-12-18 10:14:57'),
(170, 1, 'lists.campaigns.scheduled', 3, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/gl340xvlsp16e/overview">MainList #2</a>" has been scheduled for sending!', '2016-12-18 10:16:30'),
(171, 1, 'lists.campaigns.scheduled', 4, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/zq062ertb4a34/overview">MainList #3</a>" has been scheduled for sending!', '2016-12-18 10:16:49'),
(172, 1, 'lists.campaigns.scheduled', 5, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/zm959raj0g771/overview">MainList #4</a>" has been scheduled for sending!', '2016-12-18 10:17:16'),
(173, 1, 'lists.campaigns.scheduled', 7, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/qx8982pt0m9a2/overview">Day #0</a>" has been scheduled for sending!', '2016-12-18 10:17:53'),
(174, 1, 'lists.campaigns.scheduled', 8, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/qj304oezt0fb5/overview">Day #1</a>" has been scheduled for sending!', '2016-12-18 10:18:11'),
(175, 1, 'lists.campaigns.scheduled', 9, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/rf501s8zrl6d8/overview">Day #2</a>" has been scheduled for sending!', '2016-12-18 10:18:27'),
(176, 1, 'lists.campaigns.scheduled', 10, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/hd835zc2tkac3/overview">Day #3</a>" has been scheduled for sending!', '2016-12-18 10:18:47'),
(177, 1, 'lists.campaigns.scheduled', 11, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/lc187v3zn5491/overview">Day #4</a>" has been scheduled for sending!', '2016-12-18 10:19:03'),
(178, 1, 'lists.campaigns.scheduled', 12, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/mw9784gvqh798/overview">Day #5</a>" has been scheduled for sending!', '2016-12-18 10:19:19'),
(179, 1, 'lists.campaigns.scheduled', 13, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/vg459vnaho97d/overview">Day #6</a>" has been scheduled for sending!', '2016-12-18 10:19:33'),
(180, 1, 'lists.campaigns.scheduled', 14, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/fm29786va1762/overview">Day #7</a>" has been scheduled for sending!', '2016-12-18 10:19:51'),
(181, 1, 'lists.campaigns.scheduled', 15, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ka53270kog640/overview">Day #8</a>" has been scheduled for sending!', '2016-12-18 10:20:07'),
(182, 1, 'lists.campaigns.scheduled', 16, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/bk669vpkxec26/overview">Day #9</a>" has been scheduled for sending!', '2016-12-18 10:20:26'),
(183, 1, 'lists.campaigns.scheduled', 17, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/rk610ey8bc7e7/overview">Day #10</a>" has been scheduled for sending!', '2016-12-18 10:21:02'),
(184, 1, 'lists.campaigns.scheduled', 18, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/te387cf24nf94/overview">Day #11</a>" has been scheduled for sending!', '2016-12-18 10:21:17'),
(185, 1, 'lists.campaigns.scheduled', 19, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/wl334crvsf2a1/overview">Day #12</a>" has been scheduled for sending!', '2016-12-18 10:21:34'),
(186, 1, 'lists.campaigns.scheduled', 20, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/kv276smrhk03b/overview">Day #13</a>" has been scheduled for sending!', '2016-12-18 10:21:57'),
(187, 1, 'lists.campaigns.scheduled', 21, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ec778pd4pgf8d/overview">Day #14</a>" has been scheduled for sending!', '2016-12-18 10:24:33'),
(188, 1, 'lists.campaigns.scheduled', 22, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/dy266583s3c82/overview">Day #15</a>" has been scheduled for sending!', '2016-12-18 10:25:04'),
(189, 1, 'lists.campaigns.scheduled', 23, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/go009omy3af45/overview">Day #16</a>" has been scheduled for sending!', '2016-12-18 10:25:55'),
(190, 1, 'lists.campaigns.scheduled', 24, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ll243q8w1p1ab/overview">Day #17</a>" has been scheduled for sending!', '2016-12-18 10:26:09'),
(191, 1, 'lists.campaigns.scheduled', 25, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/se01987r64abf/overview">Day #18</a>" has been scheduled for sending!', '2016-12-18 10:26:21'),
(192, 1, 'lists.campaigns.scheduled', 26, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/nx7612nlkn744/overview">Day #19</a>" has been scheduled for sending!', '2016-12-18 10:26:36'),
(193, 1, 'lists.campaigns.scheduled', 27, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/lb509985hff4f/overview">Day #20</a>" has been scheduled for sending!', '2016-12-18 10:26:51'),
(194, 1, 'lists.campaigns.scheduled', 28, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/mp870809b142a/overview">Day #21</a>" has been scheduled for sending!', '2016-12-18 10:27:28'),
(195, 1, 'lists.campaigns.scheduled', 29, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/bf522e77nha8d/overview">Day #22</a>" has been scheduled for sending!', '2016-12-18 10:27:44'),
(196, 1, 'lists.campaigns.scheduled', 30, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/sf858kby3o982/overview">Day #23</a>" has been scheduled for sending!', '2016-12-18 10:27:57'),
(197, 1, 'lists.campaigns.scheduled', 31, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/pq6095y092dbd/overview">Day #24</a>" has been scheduled for sending!', '2016-12-18 10:28:11'),
(198, 1, 'lists.campaigns.scheduled', 32, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/jw580gkyd5238/overview">Day #25</a>" has been scheduled for sending!', '2016-12-18 10:28:27'),
(199, 1, 'lists.campaigns.scheduled', 33, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/yz925p80fj39d/overview">Day #26</a>" has been scheduled for sending!', '2016-12-18 10:28:40'),
(200, 1, 'lists.campaigns.scheduled', 34, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/my236qzq9g2a4/overview">Day #27</a>" has been scheduled for sending!', '2016-12-18 10:28:52'),
(201, 1, 'lists.campaigns.scheduled', 35, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/oy826sj0h052a/overview">Day #28</a>" has been scheduled for sending!', '2016-12-18 10:29:05'),
(202, 1, 'lists.campaigns.scheduled', 36, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/dl670aehf460c/overview">Day #29</a>" has been scheduled for sending!', '2016-12-18 10:29:16'),
(203, 1, 'lists.campaigns.scheduled', 37, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/jw0152p16tb0d/overview">Day #30</a>" has been scheduled for sending!', '2016-12-18 10:29:28'),
(209, 1, 'lists.campaigns.deleted', 46, 3, 'The campaign(regular) called "Day #32(Deleted)" has been successfully deleted!', '2016-12-18 12:46:28'),
(210, 1, 'lists.campaigns.deleted', 45, 3, 'The campaign(regular) called "Day #31(Deleted)" has been successfully deleted!', '2016-12-18 12:46:28'),
(211, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "hacvana@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-18 12:47:15'),
(212, 1, 'lists.subscribers.deleted', 0, 3, 'The subscriber having the email address "hacvan.a@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/rz817fpg3ad61/overview">Openers List</a>" list!', '2016-12-18 12:47:30'),
(213, 1, 'lists.subscribers.deleted', 0, 3, 'The subscriber having the email address "hacvana@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/rz817fpg3ad61/overview">Openers List</a>" list!', '2016-12-18 12:47:34'),
(215, 1, 'lists.campaigns.deleted', 48, 2, 'The campaign(autoresponder) called "MainList #2(Deleted)" has been successfully deleted!', '2016-12-20 08:59:07'),
(216, 1, 'lists.campaigns.deleted', 47, 3, 'The campaign(autoresponder) called "Day #31(Deleted)" has been successfully deleted!', '2016-12-20 08:59:09'),
(217, 1, 'lists.campaigns.updated', 49, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ct843qkxer0f3/overview">MainList #1</a>" has been updated!', '2016-12-22 10:45:53'),
(218, 1, 'lists.campaigns.scheduled', 49, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ct843qkxer0f3/overview">MainList #1</a>" has been scheduled for sending!', '2016-12-22 10:47:12'),
(219, 1, 'lists.subscribers.deleted', 0, 3, 'The subscriber having the email address "hacvan.a@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/rz817fpg3ad61/overview">Openers List</a>" list!', '2016-12-22 10:50:18'),
(220, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "hacvan.a@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-22 10:50:23'),
(221, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "polomutagim@yahoo.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-22 10:50:23'),
(222, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "hacvana@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-22 10:50:25'),
(223, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "web-r2cho@mail-tester.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-22 10:50:27'),
(224, 1, 'lists.campaigns.deleted', 2, 2, 'The campaign(autoresponder) called "MainList #1(Deleted)" has been successfully deleted!', '2016-12-22 10:50:52'),
(225, 1, 'lists.campaigns.scheduled', 7, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/qx8982pt0m9a2/overview">Day #0</a>" has been scheduled for sending!', '2016-12-22 10:52:40'),
(226, 1, 'lists.campaigns.scheduled', 7, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/qx8982pt0m9a2/overview">Day #0</a>" has been scheduled for sending!', '2016-12-22 10:55:19'),
(227, 1, 'lists.subscribers.deleted', 0, 3, 'The subscriber having the email address "hacvan.a@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/rz817fpg3ad61/overview">Openers List</a>" list!', '2016-12-22 10:57:46'),
(228, 1, 'lists.subscribers.deleted', 0, 3, 'The subscriber having the email address "hacvana@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/rz817fpg3ad61/overview">Openers List</a>" list!', '2016-12-22 10:57:48'),
(229, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "hacvan.a@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-22 10:57:50'),
(230, 1, 'lists.subscribers.deleted', 0, 2, 'The subscriber having the email address "hacvana@gmail.com" has been successfully removed from the "<a href="http://a-ddiction.press/mw/customer/index.php/lists/pn053lxhfa6a2/overview">Main List</a>" list!', '2016-12-22 10:57:52'),
(231, 1, 'lists.campaigns.scheduled', 3, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/gl340xvlsp16e/overview">MainList #2</a>" has been scheduled for sending!', '2016-12-22 10:59:00'),
(232, 1, 'lists.campaigns.scheduled', 4, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/zq062ertb4a34/overview">MainList #3</a>" has been scheduled for sending!', '2016-12-22 10:59:56'),
(233, 1, 'lists.campaigns.scheduled', 5, 2, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/zm959raj0g771/overview">MainList #4</a>" has been scheduled for sending!', '2016-12-22 11:00:37'),
(234, 1, 'lists.campaigns.scheduled', 8, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/qj304oezt0fb5/overview">Day #1</a>" has been scheduled for sending!', '2016-12-22 11:02:04'),
(235, 1, 'lists.campaigns.scheduled', 9, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/rf501s8zrl6d8/overview">Day #2</a>" has been scheduled for sending!', '2016-12-22 11:03:24'),
(236, 1, 'lists.campaigns.scheduled', 10, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/hd835zc2tkac3/overview">Day #3</a>" has been scheduled for sending!', '2016-12-22 11:04:36'),
(237, 1, 'lists.campaigns.scheduled', 11, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/lc187v3zn5491/overview">Day #4</a>" has been scheduled for sending!', '2016-12-22 11:05:34'),
(238, 1, 'lists.campaigns.scheduled', 12, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/mw9784gvqh798/overview">Day #5</a>" has been scheduled for sending!', '2016-12-22 11:07:21'),
(239, 1, 'lists.campaigns.scheduled', 13, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/vg459vnaho97d/overview">Day #6</a>" has been scheduled for sending!', '2016-12-22 11:08:34'),
(240, 1, 'lists.campaigns.scheduled', 14, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/fm29786va1762/overview">Day #7</a>" has been scheduled for sending!', '2016-12-22 11:09:39'),
(241, 1, 'lists.campaigns.scheduled', 15, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ka53270kog640/overview">Day #8</a>" has been scheduled for sending!', '2016-12-22 11:11:26'),
(242, 1, 'lists.campaigns.scheduled', 16, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/bk669vpkxec26/overview">Day #9</a>" has been scheduled for sending!', '2016-12-22 11:12:11'),
(243, 1, 'lists.campaigns.scheduled', 17, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/rk610ey8bc7e7/overview">Day #10</a>" has been scheduled for sending!', '2016-12-22 11:13:05'),
(244, 1, 'lists.campaigns.scheduled', 18, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/te387cf24nf94/overview">Day #11</a>" has been scheduled for sending!', '2016-12-22 11:14:30'),
(245, 1, 'lists.campaigns.scheduled', 19, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/wl334crvsf2a1/overview">Day #12</a>" has been scheduled for sending!', '2016-12-22 11:15:14'),
(246, 1, 'lists.campaigns.scheduled', 20, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/kv276smrhk03b/overview">Day #13</a>" has been scheduled for sending!', '2016-12-22 11:17:07'),
(247, 1, 'lists.campaigns.scheduled', 21, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ec778pd4pgf8d/overview">Day #14</a>" has been scheduled for sending!', '2016-12-22 11:17:55'),
(248, 1, 'lists.campaigns.scheduled', 22, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/dy266583s3c82/overview">Day #15</a>" has been scheduled for sending!', '2016-12-22 11:18:44'),
(249, 1, 'lists.campaigns.scheduled', 23, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/go009omy3af45/overview">Day #16</a>" has been scheduled for sending!', '2016-12-22 11:24:26'),
(250, 1, 'lists.campaigns.scheduled', 24, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/ll243q8w1p1ab/overview">Day #17</a>" has been scheduled for sending!', '2016-12-22 11:25:10'),
(251, 1, 'lists.campaigns.scheduled', 25, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/se01987r64abf/overview">Day #18</a>" has been scheduled for sending!', '2016-12-22 11:25:48'),
(252, 1, 'lists.campaigns.scheduled', 26, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/nx7612nlkn744/overview">Day #19</a>" has been scheduled for sending!', '2016-12-22 11:26:30'),
(253, 1, 'lists.campaigns.scheduled', 27, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/lb509985hff4f/overview">Day #20</a>" has been scheduled for sending!', '2016-12-22 11:27:15'),
(254, 1, 'lists.campaigns.scheduled', 28, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/mp870809b142a/overview">Day #21</a>" has been scheduled for sending!', '2016-12-22 11:28:07'),
(255, 1, 'lists.campaigns.scheduled', 29, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/bf522e77nha8d/overview">Day #22</a>" has been scheduled for sending!', '2016-12-22 11:28:48'),
(256, 1, 'lists.campaigns.scheduled', 30, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/sf858kby3o982/overview">Day #23</a>" has been scheduled for sending!', '2016-12-22 11:29:31'),
(257, 1, 'lists.campaigns.scheduled', 31, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/pq6095y092dbd/overview">Day #24</a>" has been scheduled for sending!', '2016-12-22 11:30:22'),
(258, 1, 'lists.campaigns.scheduled', 32, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/jw580gkyd5238/overview">Day #25</a>" has been scheduled for sending!', '2016-12-22 11:31:48'),
(259, 1, 'lists.campaigns.scheduled', 33, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/yz925p80fj39d/overview">Day #26</a>" has been scheduled for sending!', '2016-12-22 11:32:39'),
(260, 1, 'lists.campaigns.scheduled', 34, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/my236qzq9g2a4/overview">Day #27</a>" has been scheduled for sending!', '2016-12-22 11:33:23'),
(261, 1, 'lists.campaigns.scheduled', 35, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/oy826sj0h052a/overview">Day #28</a>" has been scheduled for sending!', '2016-12-22 11:33:58'),
(262, 1, 'lists.campaigns.scheduled', 36, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/dl670aehf460c/overview">Day #29</a>" has been scheduled for sending!', '2016-12-22 11:34:38'),
(263, 1, 'lists.campaigns.scheduled', 37, 3, 'The campaign(autoresponder) called "<a href="http://a-ddiction.press/mw/customer/index.php/campaigns/jw0152p16tb0d/overview">Day #30</a>" has been scheduled for sending!', '2016-12-22 11:35:12');

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_api_key`
--

CREATE TABLE IF NOT EXISTS `mw_customer_api_key` (
`key_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `public` char(40) NOT NULL,
  `private` char(40) NOT NULL,
  `ip_whitelist` varchar(255) DEFAULT NULL,
  `ip_blacklist` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_customer_api_key`
--

INSERT INTO `mw_customer_api_key` (`key_id`, `customer_id`, `public`, `private`, `ip_whitelist`, `ip_blacklist`, `date_added`) VALUES
(1, 1, '559cbb15b7af342f40b5f4ce995608aebb8b2d89', 'f429575aa17fc445b99e055beab7798e391d8207', NULL, NULL, '2016-03-09 19:26:56');

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_auto_login_token`
--

CREATE TABLE IF NOT EXISTS `mw_customer_auto_login_token` (
`token_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `token` char(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `mw_customer_auto_login_token`
--

INSERT INTO `mw_customer_auto_login_token` (`token_id`, `customer_id`, `token`) VALUES
(40, 1, '57050de0dd7bd60c251ebec7293e81002196055b');

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_campaign_tag`
--

CREATE TABLE IF NOT EXISTS `mw_customer_campaign_tag` (
`tag_id` int(11) NOT NULL,
  `tag_uid` char(13) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `random` enum('yes','no') NOT NULL DEFAULT 'no',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_company`
--

CREATE TABLE IF NOT EXISTS `mw_customer_company` (
`company_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `zone_name` varchar(150) DEFAULT NULL,
  `city` varchar(150) NOT NULL,
  `zip_code` char(10) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `fax` varchar(32) DEFAULT NULL,
  `vat_number` varchar(100) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_email_blacklist`
--

CREATE TABLE IF NOT EXISTS `mw_customer_email_blacklist` (
`email_id` int(11) NOT NULL,
  `email_uid` char(13) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_email_template`
--

CREATE TABLE IF NOT EXISTS `mw_customer_email_template` (
`template_id` int(11) NOT NULL,
  `template_uid` char(13) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `content_hash` char(40) NOT NULL,
  `create_screenshot` enum('yes','no') NOT NULL DEFAULT 'yes',
  `screenshot` varchar(255) DEFAULT NULL,
  `inline_css` enum('yes','no') NOT NULL DEFAULT 'no',
  `minify` enum('yes','no') NOT NULL DEFAULT 'no',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_group`
--

CREATE TABLE IF NOT EXISTS `mw_customer_group` (
`group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_default` enum('yes','no') NOT NULL DEFAULT 'no',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_group_option`
--

CREATE TABLE IF NOT EXISTS `mw_customer_group_option` (
`option_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL DEFAULT '0',
  `value` longblob,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_login_log`
--

CREATE TABLE IF NOT EXISTS `mw_customer_login_log` (
`log_id` bigint(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `ip_address` char(15) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `mw_customer_login_log`
--

INSERT INTO `mw_customer_login_log` (`log_id`, `customer_id`, `location_id`, `ip_address`, `user_agent`, `date_added`) VALUES
(1, 1, NULL, '188.24.30.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36 OPR/41.0.2353.69', '2016-12-10 08:20:15'),
(2, 1, NULL, '188.24.30.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36 OPR/41.0.2353.69', '2016-12-10 09:19:46'),
(3, 1, NULL, '188.24.30.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36', '2016-12-11 17:37:54'),
(4, 1, NULL, '188.24.30.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36 OPR/41.0.2353.69', '2016-12-12 10:46:11'),
(5, 1, NULL, '199.209.144.224', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36', '2016-12-13 19:51:10'),
(6, 1, NULL, '173.16.55.201', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36', '2016-12-14 02:14:32'),
(7, 1, NULL, '79.180.121.93', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36', '2016-12-14 06:22:33'),
(8, 1, NULL, '199.209.144.224', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36', '2016-12-14 15:46:07'),
(9, 1, NULL, '188.24.30.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36 OPR/41.0.2353.69', '2016-12-15 15:21:00'),
(10, 1, NULL, '79.181.188.247', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36', '2016-12-17 18:48:51'),
(11, 1, NULL, '188.24.30.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36 OPR/41.0.2353.69', '2016-12-18 08:10:58'),
(12, 1, NULL, '213.136.90.252', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-20 08:58:59'),
(13, 1, NULL, '79.181.188.247', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36', '2016-12-22 10:36:44');

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_message`
--

CREATE TABLE IF NOT EXISTS `mw_customer_message` (
`message_id` int(11) NOT NULL,
  `message_uid` char(13) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text NOT NULL,
  `title_translation_params` blob,
  `message_translation_params` blob,
  `status` char(15) NOT NULL DEFAULT 'unseen',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_password_reset`
--

CREATE TABLE IF NOT EXISTS `mw_customer_password_reset` (
`request_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `reset_key` char(40) NOT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_customer_quota_mark`
--

CREATE TABLE IF NOT EXISTS `mw_customer_quota_mark` (
`mark_id` bigint(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_customer_quota_mark`
--

INSERT INTO `mw_customer_quota_mark` (`mark_id`, `customer_id`, `date_added`) VALUES
(1, 1, '2016-03-18 14:31:10');

-- --------------------------------------------------------

--
-- Table structure for table `mw_delivery_server`
--

CREATE TABLE IF NOT EXISTS `mw_delivery_server` (
`server_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `bounce_server_id` int(11) DEFAULT NULL,
  `tracking_domain_id` int(11) DEFAULT NULL,
  `type` char(20) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `hostname` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `port` int(5) DEFAULT '25',
  `protocol` char(10) DEFAULT NULL,
  `timeout` int(3) DEFAULT '30',
  `from_email` varchar(150) NOT NULL,
  `from_name` varchar(150) DEFAULT NULL,
  `reply_to_email` varchar(150) DEFAULT NULL,
  `probability` int(3) NOT NULL DEFAULT '100',
  `hourly_quota` int(11) NOT NULL DEFAULT '0',
  `monthly_quota` int(11) NOT NULL DEFAULT '0',
  `meta_data` blob,
  `confirmation_key` char(40) DEFAULT NULL,
  `locked` enum('yes','no') NOT NULL DEFAULT 'no',
  `use_for` char(15) NOT NULL DEFAULT 'all',
  `use_queue` enum('yes','no') NOT NULL DEFAULT 'no',
  `signing_enabled` enum('yes','no') NOT NULL DEFAULT 'yes',
  `force_from` varchar(50) NOT NULL DEFAULT 'never',
  `force_reply_to` varchar(50) NOT NULL DEFAULT 'never',
  `force_sender` enum('yes','no') NOT NULL DEFAULT 'no',
  `must_confirm_delivery` enum('yes','no') NOT NULL DEFAULT 'no',
  `max_connection_messages` int(11) NOT NULL DEFAULT '1',
  `status` char(15) NOT NULL DEFAULT 'inactive',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_delivery_server`
--

INSERT INTO `mw_delivery_server` (`server_id`, `customer_id`, `bounce_server_id`, `tracking_domain_id`, `type`, `name`, `hostname`, `username`, `password`, `port`, `protocol`, `timeout`, `from_email`, `from_name`, `reply_to_email`, `probability`, `hourly_quota`, `monthly_quota`, `meta_data`, `confirmation_key`, `locked`, `use_for`, `use_queue`, `signing_enabled`, `force_from`, `force_reply_to`, `force_sender`, `must_confirm_delivery`, `max_connection_messages`, `status`, `date_added`, `last_updated`) VALUES
(1, NULL, 1, NULL, 'smtp', 'Server 1', 'a-ddiction.press', 'admin', 'vipb41d10c91aa7', 2525, 'starttls', 30, 'admin@a-ddiction.press', 'admin', 'admin@a-ddiction.press', 100, 0, 0, 0x613a313a7b733a31383a226164646974696f6e616c5f68656164657273223b613a303a7b7d7d, NULL, 'no', 'all', 'no', 'yes', 'never', 'never', 'no', 'no', 1, 'active', '2016-03-17 20:37:13', '2016-12-12 10:55:39');

-- --------------------------------------------------------

--
-- Table structure for table `mw_delivery_server_domain_policy`
--

CREATE TABLE IF NOT EXISTS `mw_delivery_server_domain_policy` (
`domain_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `domain` varchar(64) NOT NULL,
  `policy` char(15) NOT NULL DEFAULT 'allow',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_delivery_server_to_customer_group`
--

CREATE TABLE IF NOT EXISTS `mw_delivery_server_to_customer_group` (
  `server_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mw_delivery_server_usage_log`
--

CREATE TABLE IF NOT EXISTS `mw_delivery_server_usage_log` (
`log_id` bigint(20) NOT NULL,
  `server_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `delivery_for` char(15) NOT NULL DEFAULT 'system',
  `customer_countable` enum('yes','no') NOT NULL DEFAULT 'yes',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2246 ;

--
-- Dumping data for table `mw_delivery_server_usage_log`
--

INSERT INTO `mw_delivery_server_usage_log` (`log_id`, `server_id`, `customer_id`, `delivery_for`, `customer_countable`, `date_added`) VALUES
(1, 1, 1, 'campaign', 'yes', '2016-12-11 17:37:03'),
(2, 1, 1, 'campaign', 'yes', '2016-12-11 17:37:03'),
(3, 1, 1, 'campaign', 'yes', '2016-12-11 17:37:03'),
(4, 1, 1, 'campaign', 'yes', '2016-12-12 10:01:03'),
(5, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:04'),
(6, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:04'),
(7, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:04'),
(8, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:04'),
(9, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:04'),
(10, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:04'),
(11, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(12, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(13, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(14, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(15, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(16, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(17, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(18, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(19, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(20, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(21, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(22, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(23, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(24, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(25, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(26, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(27, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(28, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(29, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(30, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(31, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:05'),
(32, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(33, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(34, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(35, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(36, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(37, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(38, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(39, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(40, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(41, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(42, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(43, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(44, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(45, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(46, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(47, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(48, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(49, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(50, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(51, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(52, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:06'),
(53, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(54, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(55, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(56, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(57, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(58, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(59, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(60, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(61, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(62, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(63, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(64, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(65, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(66, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(67, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(68, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(69, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(70, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(71, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(72, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(73, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:07'),
(74, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(75, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(76, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(77, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(78, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(79, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(80, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(81, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(82, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(83, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(84, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(85, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(86, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(87, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(88, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(89, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(90, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(91, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(92, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(93, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(94, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:08'),
(95, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(96, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(97, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(98, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(99, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(100, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(101, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(102, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(103, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(104, 1, 1, 'campaign', 'yes', '2016-12-12 10:14:09'),
(105, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(106, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(107, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(108, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(109, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(110, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(111, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(112, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(113, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(114, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(115, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(116, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(117, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(118, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(119, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(120, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(121, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(122, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:04'),
(123, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(124, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(125, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(126, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(127, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(128, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(129, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(130, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(131, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(132, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(133, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(134, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(135, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(136, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(137, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(138, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(139, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(140, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(141, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(142, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(143, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(144, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:05'),
(145, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(146, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(147, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(148, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(149, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(150, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(151, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(152, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(153, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(154, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(155, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(156, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(157, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(158, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(159, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(160, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(161, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(162, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(163, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(164, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(165, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:06'),
(166, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(167, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(168, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(169, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(170, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(171, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(172, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(173, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(174, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(175, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(176, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(177, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(178, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(179, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(180, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(181, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(182, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(183, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(184, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(185, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(186, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:07'),
(187, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(188, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(189, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(190, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(191, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(192, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(193, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(194, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(195, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(196, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(197, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(198, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(199, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(200, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(201, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(202, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(203, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(204, 1, 1, 'campaign', 'yes', '2016-12-12 10:15:08'),
(205, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(206, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(207, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(208, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(209, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(210, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(211, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(212, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(213, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(214, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(215, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(216, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(217, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(218, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(219, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(220, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(221, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(222, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:04'),
(223, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(224, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(225, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(226, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(227, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(228, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(229, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(230, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(231, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(232, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(233, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(234, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(235, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(236, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(237, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(238, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(239, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(240, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(241, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(242, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(243, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:05'),
(244, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(245, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(246, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(247, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(248, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(249, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(250, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(251, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(252, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(253, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(254, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(255, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(256, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(257, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(258, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(259, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(260, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(261, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(262, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(263, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(264, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(265, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:06'),
(266, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(267, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(268, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(269, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(270, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(271, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(272, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(273, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(274, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(275, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(276, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(277, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(278, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(279, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(280, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(281, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(282, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(283, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(284, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(285, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(286, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:07'),
(287, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(288, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(289, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(290, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(291, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(292, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(293, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(294, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(295, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(296, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(297, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(298, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(299, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(300, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(301, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(302, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(303, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(304, 1, 1, 'campaign', 'yes', '2016-12-12 10:16:08'),
(305, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:04'),
(306, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:04'),
(307, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:04'),
(308, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:04'),
(309, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:04'),
(310, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:04'),
(311, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:04'),
(312, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(313, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(314, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(315, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(316, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(317, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(318, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(319, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(320, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(321, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(322, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(323, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(324, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(325, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(326, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(327, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(328, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(329, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(330, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(331, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(332, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(333, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:05'),
(334, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(335, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(336, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(337, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(338, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(339, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(340, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(341, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(342, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(343, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(344, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(345, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(346, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(347, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(348, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(349, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(350, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(351, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(352, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(353, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(354, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:06'),
(355, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(356, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(357, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(358, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(359, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(360, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(361, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(362, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(363, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(364, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(365, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(366, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(367, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(368, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(369, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(370, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(371, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(372, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(373, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(374, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(375, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:07'),
(376, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(377, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(378, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(379, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(380, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(381, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(382, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(383, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(384, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(385, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(386, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(387, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(388, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(389, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(390, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(391, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(392, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(393, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(394, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(395, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(396, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:08'),
(397, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(398, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(399, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(400, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(401, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(402, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(403, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(404, 1, 1, 'campaign', 'yes', '2016-12-12 10:17:09'),
(405, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:03'),
(406, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(407, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(408, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(409, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(410, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(411, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(412, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(413, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(414, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(415, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(416, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(417, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(418, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(419, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(420, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(421, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(422, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(423, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(424, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(425, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(426, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:04'),
(427, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(428, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(429, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(430, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(431, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(432, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(433, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(434, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(435, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(436, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(437, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(438, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(439, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(440, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(441, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(442, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(443, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(444, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(445, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(446, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(447, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:05'),
(448, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:06'),
(449, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:06'),
(450, 1, 1, 'campaign', 'yes', '2016-12-12 10:18:06'),
(451, 1, 1, 'campaign', 'yes', '2016-12-12 10:59:04'),
(452, 1, 1, 'campaign', 'yes', '2016-12-12 13:00:03'),
(453, 1, 1, 'campaign', 'yes', '2016-12-12 13:00:03'),
(454, 1, 1, 'campaign', 'yes', '2016-12-12 13:00:03'),
(455, 1, 1, 'campaign', 'yes', '2016-12-12 16:17:01'),
(456, 1, 1, 'campaign', 'yes', '2016-12-13 08:17:01'),
(457, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:02'),
(458, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:02'),
(459, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:02'),
(460, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(461, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(462, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(463, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(464, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(465, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(466, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(467, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(468, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(469, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(470, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(471, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(472, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(473, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(474, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(475, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(476, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(477, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(478, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(479, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(480, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:03'),
(481, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(482, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(483, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(484, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(485, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(486, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(487, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(488, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(489, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(490, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(491, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(492, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(493, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(494, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(495, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(496, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(497, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(498, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(499, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(500, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(501, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:04'),
(502, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(503, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(504, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(505, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(506, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(507, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(508, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(509, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(510, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(511, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(512, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(513, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(514, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(515, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(516, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(517, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(518, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(519, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(520, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(521, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(522, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:05'),
(523, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(524, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(525, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(526, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(527, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(528, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(529, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(530, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(531, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(532, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(533, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(534, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(535, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(536, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(537, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(538, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(539, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(540, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(541, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(542, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:06'),
(543, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(544, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(545, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(546, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(547, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(548, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(549, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(550, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(551, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(552, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(553, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(554, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(555, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(556, 1, 1, 'campaign', 'yes', '2016-12-13 10:13:07'),
(557, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(558, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(559, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(560, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(561, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(562, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(563, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(564, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(565, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(566, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(567, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(568, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(569, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(570, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(571, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(572, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(573, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(574, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:03'),
(575, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(576, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(577, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(578, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(579, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(580, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(581, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(582, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(583, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(584, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(585, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(586, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(587, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(588, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(589, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(590, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(591, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(592, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(593, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(594, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(595, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:04'),
(596, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(597, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(598, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(599, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(600, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(601, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(602, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(603, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(604, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(605, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(606, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(607, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(608, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(609, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(610, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(611, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(612, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(613, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(614, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(615, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(616, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:05'),
(617, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(618, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(619, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(620, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(621, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(622, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(623, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(624, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(625, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(626, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(627, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(628, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(629, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(630, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(631, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(632, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(633, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(634, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(635, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(636, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(637, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:06'),
(638, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(639, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(640, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(641, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(642, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(643, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(644, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(645, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(646, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(647, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(648, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(649, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(650, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(651, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(652, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(653, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(654, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(655, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(656, 1, 1, 'campaign', 'yes', '2016-12-13 10:14:07'),
(657, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(658, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(659, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(660, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(661, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(662, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(663, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(664, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(665, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(666, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(667, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(668, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(669, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(670, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(671, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:05'),
(672, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(673, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(674, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(675, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(676, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(677, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(678, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(679, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(680, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(681, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(682, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(683, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(684, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(685, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(686, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(687, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(688, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(689, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(690, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(691, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(692, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:06'),
(693, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(694, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(695, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(696, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(697, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(698, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(699, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(700, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(701, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(702, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(703, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(704, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(705, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(706, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(707, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(708, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(709, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(710, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(711, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(712, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(713, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(714, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:07'),
(715, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(716, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(717, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(718, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(719, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(720, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(721, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(722, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(723, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(724, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(725, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(726, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(727, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(728, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(729, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(730, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(731, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(732, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(733, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(734, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(735, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:08'),
(736, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(737, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(738, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(739, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(740, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(741, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(742, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(743, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(744, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(745, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(746, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(747, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(748, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(749, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(750, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(751, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(752, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(753, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(754, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(755, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(756, 1, 1, 'campaign', 'yes', '2016-12-13 10:15:09'),
(757, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:03'),
(758, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:03'),
(759, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(760, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(761, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(762, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(763, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(764, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(765, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(766, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(767, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(768, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(769, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(770, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(771, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(772, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(773, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(774, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(775, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(776, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(777, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(778, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(779, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:04'),
(780, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(781, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(782, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(783, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(784, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(785, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(786, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(787, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(788, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(789, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(790, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(791, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(792, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(793, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(794, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(795, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(796, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(797, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(798, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(799, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(800, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:05'),
(801, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(802, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(803, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(804, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(805, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(806, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(807, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(808, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(809, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(810, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(811, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(812, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(813, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(814, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(815, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(816, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(817, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(818, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(819, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(820, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(821, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:06'),
(822, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(823, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(824, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(825, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(826, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(827, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(828, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(829, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(830, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(831, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(832, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(833, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(834, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(835, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(836, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(837, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(838, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(839, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(840, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(841, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(842, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:07'),
(843, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(844, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(845, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(846, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(847, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(848, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(849, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(850, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(851, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(852, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(853, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(854, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(855, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(856, 1, 1, 'campaign', 'yes', '2016-12-13 10:16:08'),
(857, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(858, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(859, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(860, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(861, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(862, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(863, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(864, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(865, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(866, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(867, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(868, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(869, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(870, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(871, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(872, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(873, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(874, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:03'),
(875, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(876, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(877, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(878, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(879, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(880, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(881, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(882, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(883, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(884, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(885, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(886, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(887, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(888, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(889, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(890, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(891, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(892, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(893, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(894, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(895, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:04'),
(896, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(897, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(898, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(899, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(900, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(901, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(902, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(903, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(904, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(905, 1, 1, 'campaign', 'yes', '2016-12-13 10:17:05'),
(906, 1, 1, 'campaign', 'yes', '2016-12-14 01:17:01'),
(907, 1, 1, 'campaign', 'yes', '2016-12-14 06:25:04'),
(908, 1, 1, 'campaign', 'yes', '2016-12-14 06:30:03'),
(909, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:03'),
(910, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:03'),
(911, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(912, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(913, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(914, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(915, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(916, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(917, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(918, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(919, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(920, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(921, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(922, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(923, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(924, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(925, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(926, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(927, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(928, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(929, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(930, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(931, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:04'),
(932, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(933, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(934, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(935, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(936, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(937, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(938, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(939, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(940, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(941, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(942, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05');
INSERT INTO `mw_delivery_server_usage_log` (`log_id`, `server_id`, `customer_id`, `delivery_for`, `customer_countable`, `date_added`) VALUES
(943, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(944, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(945, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(946, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(947, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(948, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(949, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(950, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(951, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(952, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:05'),
(953, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(954, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(955, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(956, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(957, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(958, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(959, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(960, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(961, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(962, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(963, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(964, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(965, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(966, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(967, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(968, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(969, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(970, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(971, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(972, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(973, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:06'),
(974, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(975, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(976, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(977, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(978, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(979, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(980, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(981, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(982, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(983, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(984, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(985, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(986, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(987, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(988, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(989, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(990, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(991, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(992, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(993, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(994, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:07'),
(995, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(996, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(997, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(998, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(999, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1000, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1001, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1002, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1003, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1004, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1005, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1006, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1007, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1008, 1, 1, 'campaign', 'yes', '2016-12-14 15:47:08'),
(1009, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:04'),
(1010, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:04'),
(1011, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:04'),
(1012, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:04'),
(1013, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1014, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1015, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1016, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1017, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1018, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1019, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1020, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1021, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1022, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1023, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1024, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1025, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1026, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1027, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1028, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1029, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1030, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1031, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1032, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1033, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:05'),
(1034, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1035, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1036, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1037, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1038, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1039, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1040, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1041, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1042, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1043, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1044, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1045, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1046, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1047, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1048, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1049, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1050, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1051, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1052, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1053, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1054, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:06'),
(1055, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1056, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1057, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1058, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1059, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1060, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1061, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1062, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1063, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1064, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1065, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1066, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1067, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1068, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1069, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1070, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1071, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1072, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1073, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1074, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1075, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:07'),
(1076, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1077, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1078, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1079, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1080, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1081, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1082, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1083, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1084, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1085, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1086, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1087, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1088, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1089, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1090, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1091, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1092, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1093, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1094, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1095, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1096, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:08'),
(1097, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1098, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1099, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1100, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1101, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1102, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1103, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1104, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1105, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1106, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1107, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1108, 1, 1, 'campaign', 'yes', '2016-12-14 15:48:09'),
(1109, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1110, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1111, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1112, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1113, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1114, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1115, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1116, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1117, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1118, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1119, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1120, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1121, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1122, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1123, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:04'),
(1124, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1125, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1126, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1127, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1128, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1129, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1130, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1131, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1132, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1133, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1134, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1135, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1136, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1137, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1138, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1139, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1140, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1141, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1142, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1143, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1144, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:05'),
(1145, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1146, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1147, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1148, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1149, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1150, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1151, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1152, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1153, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1154, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1155, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1156, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1157, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1158, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1159, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1160, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1161, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1162, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1163, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1164, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1165, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:06'),
(1166, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1167, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1168, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1169, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1170, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1171, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1172, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1173, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1174, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1175, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1176, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1177, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1178, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1179, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1180, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1181, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1182, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1183, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1184, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1185, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1186, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:07'),
(1187, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1188, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1189, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1190, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1191, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1192, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1193, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1194, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1195, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1196, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1197, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1198, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1199, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1200, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1201, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1202, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1203, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1204, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1205, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1206, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1207, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:08'),
(1208, 1, 1, 'campaign', 'yes', '2016-12-14 15:49:09'),
(1209, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1210, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1211, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1212, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1213, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1214, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1215, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1216, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1217, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1218, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1219, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1220, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1221, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1222, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1223, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:03'),
(1224, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1225, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1226, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1227, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1228, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1229, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1230, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1231, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1232, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1233, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1234, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1235, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1236, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1237, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1238, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1239, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1240, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1241, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1242, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1243, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1244, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:04'),
(1245, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1246, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1247, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1248, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1249, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1250, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1251, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1252, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1253, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1254, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1255, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1256, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1257, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1258, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1259, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1260, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1261, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1262, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1263, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1264, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:05'),
(1265, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1266, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1267, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1268, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1269, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1270, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1271, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1272, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1273, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1274, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1275, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1276, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1277, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1278, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1279, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1280, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1281, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1282, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1283, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1284, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1285, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:06'),
(1286, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1287, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1288, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1289, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1290, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1291, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1292, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1293, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1294, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1295, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1296, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1297, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1298, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1299, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1300, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1301, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1302, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1303, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1304, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1305, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1306, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:07'),
(1307, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:08'),
(1308, 1, 1, 'campaign', 'yes', '2016-12-14 15:50:08'),
(1309, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:03'),
(1310, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:03'),
(1311, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1312, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1313, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1314, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1315, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1316, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1317, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1318, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1319, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1320, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1321, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1322, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1323, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1324, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1325, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1326, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1327, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1328, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1329, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1330, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1331, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:04'),
(1332, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1333, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1334, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1335, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1336, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1337, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1338, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1339, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1340, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1341, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1342, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1343, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1344, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1345, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1346, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1347, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1348, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1349, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1350, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1351, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1352, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:05'),
(1353, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:06'),
(1354, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:06'),
(1355, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:06'),
(1356, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:06'),
(1357, 1, 1, 'campaign', 'yes', '2016-12-14 15:51:06'),
(1358, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:03'),
(1359, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:03'),
(1360, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1361, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1362, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1363, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1364, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1365, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1366, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1367, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1368, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1369, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1370, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1371, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1372, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1373, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1374, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1375, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1376, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1377, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1378, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1379, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1380, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:04'),
(1381, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1382, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1383, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1384, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1385, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1386, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1387, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1388, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1389, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1390, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1391, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1392, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1393, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1394, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1395, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1396, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1397, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1398, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1399, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1400, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1401, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:05'),
(1402, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1403, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1404, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1405, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1406, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1407, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1408, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1409, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1410, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1411, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1412, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1413, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1414, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1415, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1416, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1417, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1418, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1419, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1420, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1421, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1422, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1423, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:06'),
(1424, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1425, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1426, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1427, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1428, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1429, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1430, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1431, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1432, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1433, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1434, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1435, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1436, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1437, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1438, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1439, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1440, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1441, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1442, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1443, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1444, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:07'),
(1445, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1446, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1447, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1448, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1449, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1450, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1451, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1452, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1453, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1454, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1455, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1456, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1457, 1, 1, 'campaign', 'yes', '2016-12-14 15:54:08'),
(1458, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1459, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1460, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1461, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1462, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1463, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1464, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1465, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1466, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1467, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1468, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1469, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1470, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1471, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1472, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1473, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1474, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1475, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:03'),
(1476, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1477, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1478, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1479, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1480, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1481, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1482, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1483, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1484, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1485, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1486, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1487, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1488, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1489, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1490, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1491, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1492, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1493, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1494, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1495, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:04'),
(1496, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1497, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1498, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1499, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1500, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1501, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1502, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1503, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1504, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1505, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1506, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1507, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1508, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1509, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1510, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1511, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1512, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1513, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1514, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1515, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1516, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:05'),
(1517, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1518, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1519, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1520, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1521, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1522, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1523, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1524, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1525, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1526, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1527, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1528, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1529, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1530, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1531, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1532, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1533, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1534, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1535, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1536, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1537, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:06'),
(1538, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1539, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1540, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1541, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1542, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1543, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1544, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1545, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1546, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1547, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1548, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1549, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1550, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1551, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1552, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1553, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1554, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1555, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1556, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1557, 1, 1, 'campaign', 'yes', '2016-12-14 15:55:07'),
(1558, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1559, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1560, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1561, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1562, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1563, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1564, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1565, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1566, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1567, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1568, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1569, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:03'),
(1570, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1571, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1572, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1573, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1574, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1575, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1576, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1577, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1578, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1579, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1580, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1581, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1582, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1583, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1584, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1585, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1586, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1587, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1588, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1589, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1590, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:04'),
(1591, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1592, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1593, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1594, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1595, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1596, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1597, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1598, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1599, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1600, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1601, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1602, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1603, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1604, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1605, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1606, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1607, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1608, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1609, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1610, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1611, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:05'),
(1612, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1613, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1614, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1615, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1616, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1617, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1618, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1619, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1620, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1621, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1622, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1623, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1624, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1625, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1626, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1627, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1628, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1629, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1630, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1631, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1632, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:06'),
(1633, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1634, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1635, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1636, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1637, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1638, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1639, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1640, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1641, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1642, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1643, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1644, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1645, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1646, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1647, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1648, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1649, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1650, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1651, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1652, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1653, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:07'),
(1654, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:08'),
(1655, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:08'),
(1656, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:08'),
(1657, 1, 1, 'campaign', 'yes', '2016-12-14 15:56:08'),
(1658, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1659, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1660, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1661, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1662, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1663, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1664, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1665, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1666, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1667, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1668, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1669, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1670, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1671, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1672, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1673, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1674, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:04'),
(1675, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1676, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1677, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1678, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1679, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1680, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1681, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1682, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1683, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1684, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1685, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1686, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1687, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1688, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1689, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1690, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1691, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1692, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1693, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1694, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1695, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1696, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:05'),
(1697, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1698, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1699, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1700, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1701, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1702, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1703, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1704, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1705, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1706, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1707, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1708, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1709, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1710, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1711, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1712, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1713, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1714, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1715, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1716, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1717, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:06'),
(1718, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1719, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1720, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1721, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1722, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1723, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1724, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1725, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1726, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1727, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1728, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1729, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1730, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1731, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1732, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1733, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1734, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1735, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1736, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1737, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1738, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:07'),
(1739, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1740, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1741, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1742, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1743, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1744, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1745, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1746, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1747, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1748, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1749, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1750, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1751, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1752, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1753, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1754, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1755, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1756, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1757, 1, 1, 'campaign', 'yes', '2016-12-14 15:57:08'),
(1758, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:03'),
(1759, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:03'),
(1760, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1761, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1762, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1763, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1764, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1765, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1766, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1767, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1768, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1769, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1770, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1771, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1772, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1773, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1774, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1775, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1776, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1777, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1778, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1779, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1780, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:04'),
(1781, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1782, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1783, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1784, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1785, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1786, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1787, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1788, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1789, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1790, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1791, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1792, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1793, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1794, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1795, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1796, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1797, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1798, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1799, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1800, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1801, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:05'),
(1802, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:06'),
(1803, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:06'),
(1804, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:06'),
(1805, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:06'),
(1806, 1, 1, 'campaign', 'yes', '2016-12-14 15:58:06'),
(1807, 1, 1, 'campaign', 'yes', '2016-12-14 18:17:01'),
(1808, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1809, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1810, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1811, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1812, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1813, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1814, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1815, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1816, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1817, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1818, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1819, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1820, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1821, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1822, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1823, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1824, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:03'),
(1825, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1826, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1827, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1828, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1829, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1830, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1831, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1832, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1833, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1834, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1835, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1836, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1837, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1838, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1839, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1840, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1841, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1842, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1843, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1844, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1845, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:04'),
(1846, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1847, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1848, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1849, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1850, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1851, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1852, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1853, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1854, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1855, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1856, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1857, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1858, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1859, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1860, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1861, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1862, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1863, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1864, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1865, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05'),
(1866, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:05');
INSERT INTO `mw_delivery_server_usage_log` (`log_id`, `server_id`, `customer_id`, `delivery_for`, `customer_countable`, `date_added`) VALUES
(1867, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1868, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1869, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1870, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1871, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1872, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1873, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1874, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1875, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1876, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1877, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1878, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1879, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1880, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1881, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1882, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1883, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1884, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1885, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1886, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1887, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:06'),
(1888, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1889, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1890, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1891, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1892, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1893, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1894, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1895, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1896, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1897, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1898, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1899, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1900, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1901, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1902, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1903, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1904, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1905, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1906, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1907, 1, 1, 'campaign', 'yes', '2016-12-15 07:42:07'),
(1908, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:04'),
(1909, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1910, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1911, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1912, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1913, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1914, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1915, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1916, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1917, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1918, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1919, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1920, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1921, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1922, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1923, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1924, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1925, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1926, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1927, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1928, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1929, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1930, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:05'),
(1931, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1932, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1933, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1934, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1935, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1936, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1937, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1938, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1939, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1940, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1941, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1942, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1943, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1944, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1945, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1946, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1947, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1948, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1949, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1950, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1951, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:06'),
(1952, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1953, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1954, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1955, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1956, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1957, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1958, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1959, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1960, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1961, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1962, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1963, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1964, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1965, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1966, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1967, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1968, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1969, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1970, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1971, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1972, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:07'),
(1973, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1974, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1975, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1976, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1977, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1978, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1979, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1980, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1981, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1982, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1983, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1984, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1985, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1986, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1987, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1988, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1989, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1990, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1991, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1992, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1993, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1994, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:08'),
(1995, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(1996, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(1997, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(1998, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(1999, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2000, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2001, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2002, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2003, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2004, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2005, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2006, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2007, 1, 1, 'campaign', 'yes', '2016-12-15 07:43:09'),
(2008, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2009, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2010, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2011, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2012, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2013, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2014, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2015, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2016, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2017, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2018, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2019, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2020, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2021, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2022, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2023, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2024, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:03'),
(2025, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2026, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2027, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2028, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2029, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2030, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2031, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2032, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2033, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2034, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2035, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2036, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2037, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2038, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2039, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2040, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2041, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2042, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2043, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2044, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2045, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:04'),
(2046, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2047, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2048, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2049, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2050, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2051, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2052, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2053, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2054, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2055, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2056, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2057, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2058, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2059, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2060, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2061, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2062, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2063, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2064, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2065, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2066, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:05'),
(2067, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2068, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2069, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2070, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2071, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2072, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2073, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2074, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2075, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2076, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2077, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2078, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2079, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2080, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2081, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2082, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2083, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2084, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2085, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2086, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2087, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:06'),
(2088, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2089, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2090, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2091, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2092, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2093, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2094, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2095, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2096, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2097, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2098, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2099, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2100, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2101, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2102, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2103, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2104, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2105, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2106, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2107, 1, 1, 'campaign', 'yes', '2016-12-15 07:44:07'),
(2108, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2109, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2110, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2111, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2112, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2113, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2114, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2115, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2116, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2117, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:03'),
(2118, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2119, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2120, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2121, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2122, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2123, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2124, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2125, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2126, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2127, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2128, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2129, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2130, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2131, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2132, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2133, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2134, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2135, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2136, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2137, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2138, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:04'),
(2139, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2140, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2141, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2142, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2143, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2144, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2145, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2146, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2147, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2148, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2149, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2150, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2151, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2152, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2153, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2154, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2155, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2156, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2157, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2158, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2159, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:05'),
(2160, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2161, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2162, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2163, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2164, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2165, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2166, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2167, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2168, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2169, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2170, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2171, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2172, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2173, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2174, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2175, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2176, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2177, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2178, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2179, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2180, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:06'),
(2181, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2182, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2183, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2184, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2185, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2186, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2187, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2188, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2189, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2190, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2191, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2192, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2193, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2194, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2195, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2196, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2197, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2198, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2199, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2200, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2201, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:07'),
(2202, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:08'),
(2203, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:08'),
(2204, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:08'),
(2205, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:08'),
(2206, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:08'),
(2207, 1, 1, 'campaign', 'yes', '2016-12-15 07:45:08'),
(2208, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2209, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2210, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2211, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2212, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2213, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2214, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2215, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2216, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2217, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2218, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2219, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:04'),
(2220, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:05'),
(2221, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:05'),
(2222, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:05'),
(2223, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:05'),
(2224, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:05'),
(2225, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:05'),
(2226, 1, 1, 'campaign', 'yes', '2016-12-15 07:46:05'),
(2227, 1, 1, 'campaign-test', 'yes', '2016-12-18 08:05:51'),
(2228, 1, 1, 'campaign', 'yes', '2016-12-18 08:08:04'),
(2229, 1, 1, 'campaign', 'yes', '2016-12-18 08:27:04'),
(2230, 1, 1, 'campaign', 'yes', '2016-12-18 08:59:04'),
(2231, 1, 1, 'campaign', 'yes', '2016-12-18 09:28:05'),
(2232, 1, 1, 'campaign', 'yes', '2016-12-18 09:50:05'),
(2233, 1, 1, 'campaign', 'yes', '2016-12-18 10:18:03'),
(2234, 1, 1, 'campaign', 'yes', '2016-12-18 10:18:04'),
(2235, 1, 1, 'campaign', 'yes', '2016-12-18 12:43:04'),
(2236, 1, 1, 'campaign', 'yes', '2016-12-18 12:43:04'),
(2237, 1, 1, 'campaign', 'yes', '2016-12-22 10:38:04'),
(2238, 1, 1, 'campaign', 'yes', '2016-12-22 10:38:04'),
(2239, 1, 1, 'campaign', 'yes', '2016-12-22 10:38:04'),
(2240, 1, 1, 'campaign', 'yes', '2016-12-22 10:48:04'),
(2241, 1, 1, 'campaign', 'yes', '2016-12-22 10:49:03'),
(2242, 1, 1, 'campaign', 'yes', '2016-12-22 10:53:03'),
(2243, 1, 1, 'campaign', 'yes', '2016-12-22 10:54:03'),
(2244, 1, 1, 'campaign', 'yes', '2016-12-22 10:56:02'),
(2245, 1, 1, 'campaign', 'yes', '2016-12-22 10:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `mw_email_blacklist`
--

CREATE TABLE IF NOT EXISTS `mw_email_blacklist` (
`email_id` bigint(20) NOT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `reason` text NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_email_blacklist_monitor`
--

CREATE TABLE IF NOT EXISTS `mw_email_blacklist_monitor` (
`monitor_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email_condition` char(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `reason_condition` char(15) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `condition_operator` enum('and','or') NOT NULL DEFAULT 'and',
  `notifications_to` varchar(255) DEFAULT NULL,
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_email_blacklist_suggest`
--

CREATE TABLE IF NOT EXISTS `mw_email_blacklist_suggest` (
`email_id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_feedback_loop_server`
--

CREATE TABLE IF NOT EXISTS `mw_feedback_loop_server` (
`server_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `hostname` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `service` enum('imap','pop3') NOT NULL DEFAULT 'imap',
  `port` int(5) NOT NULL DEFAULT '143',
  `protocol` enum('ssl','tls','notls') NOT NULL DEFAULT 'notls',
  `validate_ssl` enum('yes','no') NOT NULL DEFAULT 'no',
  `locked` enum('yes','no') NOT NULL DEFAULT 'no',
  `disable_authenticator` varchar(50) DEFAULT NULL,
  `search_charset` varchar(50) NOT NULL DEFAULT 'UTF-8',
  `delete_all_messages` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_feedback_loop_server`
--

INSERT INTO `mw_feedback_loop_server` (`server_id`, `customer_id`, `hostname`, `username`, `password`, `email`, `service`, `port`, `protocol`, `validate_ssl`, `locked`, `disable_authenticator`, `search_charset`, `delete_all_messages`, `status`, `date_added`, `last_updated`) VALUES
(1, NULL, 'localhost', 'abuse', 'abuse*p5348*vip', '', 'imap', 143, 'tls', 'no', 'no', '', 'UTF-8', 'no', 'active', '2016-03-17 21:36:37', '2016-03-17 21:36:52');

-- --------------------------------------------------------

--
-- Table structure for table `mw_guest_fail_attempt`
--

CREATE TABLE IF NOT EXISTS `mw_guest_fail_attempt` (
`attempt_id` bigint(20) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `ip_address_hash` char(32) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `place` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_ip_location`
--

CREATE TABLE IF NOT EXISTS `mw_ip_location` (
`location_id` bigint(20) NOT NULL,
  `ip_address` char(15) NOT NULL,
  `country_code` char(3) NOT NULL,
  `country_name` varchar(150) NOT NULL,
  `zone_name` varchar(150) DEFAULT NULL,
  `city_name` varchar(150) DEFAULT NULL,
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_language`
--

CREATE TABLE IF NOT EXISTS `mw_language` (
`language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `language_code` char(2) NOT NULL,
  `region_code` char(2) DEFAULT NULL,
  `is_default` enum('yes','no') NOT NULL DEFAULT 'no',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list`
--

CREATE TABLE IF NOT EXISTS `mw_list` (
`list_id` int(11) NOT NULL,
  `list_uid` char(13) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `visibility` char(15) NOT NULL DEFAULT 'public',
  `opt_in` enum('double','single') NOT NULL DEFAULT 'double',
  `opt_out` enum('double','single') NOT NULL DEFAULT 'single',
  `merged` enum('yes','no') NOT NULL DEFAULT 'no',
  `welcome_email` enum('yes','no') NOT NULL DEFAULT 'no',
  `removable` enum('yes','no') NOT NULL DEFAULT 'yes',
  `subscriber_require_approval` enum('yes','no') NOT NULL DEFAULT 'no',
  `subscriber_404_redirect` varchar(255) DEFAULT NULL,
  `subscriber_exists_redirect` varchar(255) DEFAULT NULL,
  `meta_data` blob,
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `mw_list`
--

INSERT INTO `mw_list` (`list_id`, `list_uid`, `customer_id`, `name`, `display_name`, `description`, `visibility`, `opt_in`, `opt_out`, `merged`, `welcome_email`, `removable`, `subscriber_require_approval`, `subscriber_404_redirect`, `subscriber_exists_redirect`, `meta_data`, `status`, `date_added`, `last_updated`) VALUES
(2, 'pn053lxhfa6a2', 1, 'Main List', 'Main List', 'Main List', 'public', 'single', 'single', 'no', 'no', 'yes', 'no', 'http://a-ddiction.press/', '', 0x613a303a7b7d, 'active', '2016-11-09 08:24:13', '2016-11-09 08:24:13'),
(3, 'rz817fpg3ad61', 1, 'Openers List', 'Openers List', 'Openers List', 'public', 'single', 'single', 'no', 'no', 'yes', 'no', 'http://a-ddiction.press/', '', 0x613a303a7b7d, 'active', '2016-11-09 08:24:24', '2016-11-09 08:24:43');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_company`
--

CREATE TABLE IF NOT EXISTS `mw_list_company` (
  `list_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `zone_name` varchar(150) DEFAULT NULL,
  `city` varchar(150) NOT NULL,
  `zip_code` char(10) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `address_format` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mw_list_company`
--

INSERT INTO `mw_list_company` (`list_id`, `type_id`, `country_id`, `zone_id`, `name`, `website`, `address_1`, `address_2`, `zone_name`, `city`, `zip_code`, `phone`, `address_format`) VALUES
(2, NULL, 223, 3661, 'Miki Miles', '', '3105 O. B. Riley Rd', '', '', 'Bend', '97703', '', '[COMPANY_NAME]\r\n[COMPANY_ADDRESS_1] [COMPANY_ADDRESS_2]\r\n[COMPANY_CITY] [COMPANY_ZONE] [COMPANY_ZIP]\r\n[COMPANY_COUNTRY]\r\n[COMPANY_WEBSITE]'),
(3, NULL, 223, 3661, 'Miki Miles', '', '3105 O. B. Riley Rd', '', '', 'Bend', '97703', '', '[COMPANY_NAME]\r\n[COMPANY_ADDRESS_1] [COMPANY_ADDRESS_2]\r\n[COMPANY_CITY] [COMPANY_ZONE] [COMPANY_ZIP]\r\n[COMPANY_COUNTRY]\r\n[COMPANY_WEBSITE]');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_customer_notification`
--

CREATE TABLE IF NOT EXISTS `mw_list_customer_notification` (
  `list_id` int(11) NOT NULL,
  `daily` enum('yes','no') NOT NULL DEFAULT 'no',
  `subscribe` enum('yes','no') NOT NULL DEFAULT 'no',
  `unsubscribe` enum('yes','no') NOT NULL DEFAULT 'no',
  `daily_to` varchar(255) DEFAULT NULL,
  `subscribe_to` varchar(255) DEFAULT NULL,
  `unsubscribe_to` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mw_list_customer_notification`
--

INSERT INTO `mw_list_customer_notification` (`list_id`, `daily`, `subscribe`, `unsubscribe`, `daily_to`, `subscribe_to`, `unsubscribe_to`) VALUES
(2, 'no', 'no', 'no', NULL, '', ''),
(3, 'no', 'no', 'no', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_default`
--

CREATE TABLE IF NOT EXISTS `mw_list_default` (
  `list_id` int(11) NOT NULL,
  `from_name` varchar(100) NOT NULL,
  `from_email` varchar(100) NOT NULL,
  `reply_to` varchar(100) NOT NULL,
  `subject` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mw_list_default`
--

INSERT INTO `mw_list_default` (`list_id`, `from_name`, `from_email`, `reply_to`, `subject`) VALUES
(2, 'Miki Miles', 'admin@a-ddiction.press', 'reply@a-ddiction.press', ''),
(3, 'Miki Miles', 'admin@a-ddiction.press', 'reply@a-ddiction.press', '');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_field`
--

CREATE TABLE IF NOT EXISTS `mw_list_field` (
`field_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `help_text` varchar(255) DEFAULT NULL,
  `required` enum('yes','no') NOT NULL DEFAULT 'no',
  `visibility` enum('visible','hidden') NOT NULL DEFAULT 'visible',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mw_list_field`
--

INSERT INTO `mw_list_field` (`field_id`, `type_id`, `list_id`, `label`, `tag`, `default_value`, `help_text`, `required`, `visibility`, `sort_order`, `date_added`, `last_updated`) VALUES
(4, 1, 2, 'Email', 'EMAIL', NULL, NULL, 'yes', 'visible', 0, '2016-11-09 08:24:13', '2016-11-09 08:24:13'),
(5, 1, 2, 'First name', 'FNAME', NULL, NULL, 'no', 'visible', 1, '2016-11-09 08:24:13', '2016-11-09 08:24:13'),
(6, 1, 2, 'Last name', 'LNAME', NULL, NULL, 'no', 'visible', 2, '2016-11-09 08:24:13', '2016-11-09 08:24:13'),
(7, 1, 3, 'Email', 'EMAIL', NULL, NULL, 'yes', 'visible', 0, '2016-11-09 08:24:24', '2016-11-09 08:24:24'),
(8, 1, 3, 'First name', 'FNAME', NULL, NULL, 'no', 'visible', 1, '2016-11-09 08:24:24', '2016-11-09 08:24:24'),
(9, 1, 3, 'Last name', 'LNAME', NULL, NULL, 'no', 'visible', 2, '2016-11-09 08:24:24', '2016-11-09 08:24:24'),
(10, 1, 2, 'Name', 'NAME', NULL, NULL, 'no', 'visible', 0, '2016-11-11 19:42:46', '2016-11-11 19:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_field_option`
--

CREATE TABLE IF NOT EXISTS `mw_list_field_option` (
`option_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  `is_default` enum('yes','no') NOT NULL DEFAULT 'no',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_field_type`
--

CREATE TABLE IF NOT EXISTS `mw_list_field_type` (
`type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `class_alias` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `mw_list_field_type`
--

INSERT INTO `mw_list_field_type` (`type_id`, `name`, `identifier`, `class_alias`, `description`, `date_added`, `last_updated`) VALUES
(1, 'Text', 'text', 'customer.components.field-builder.text.FieldBuilderTypeText', 'Text', '2013-09-01 14:26:26', '2013-09-01 14:26:29'),
(2, 'Dropdown', 'dropdown', 'customer.components.field-builder.dropdown.FieldBuilderTypeDropdown', 'Dropdown', '2013-09-01 14:26:26', '2013-09-01 14:26:29'),
(3, 'Multiselect', 'multiselect', 'customer.components.field-builder.multiselect.FieldBuilderTypeMultiselect', 'Multiselect', '2014-05-27 14:26:26', '2014-05-27 00:00:00'),
(4, 'Date', 'date', 'customer.components.field-builder.date.FieldBuilderTypeDate', 'Date', '2014-05-27 14:26:26', '2014-05-27 00:00:00'),
(5, 'Datetime', 'datetime', 'customer.components.field-builder.datetime.FieldBuilderTypeDatetime', 'Datetime', '2014-05-27 14:26:26', '2014-05-27 00:00:00'),
(6, 'Textarea', 'textarea', 'customer.components.field-builder.textarea.FieldBuilderTypeTextarea', 'Textarea', '2014-05-27 14:26:26', '2014-05-27 00:00:00'),
(7, 'Country', 'country', 'customer.components.field-builder.country.FieldBuilderTypeCountry', 'Country', '2016-11-04 14:26:26', '2016-11-04 00:00:00'),
(8, 'State', 'state', 'customer.components.field-builder.state.FieldBuilderTypeState', 'State', '2016-11-04 14:26:26', '2016-11-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_field_value`
--

CREATE TABLE IF NOT EXISTS `mw_list_field_value` (
`value_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_page`
--

CREATE TABLE IF NOT EXISTS `mw_list_page` (
  `list_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `meta_data` longblob,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_page_type`
--

CREATE TABLE IF NOT EXISTS `mw_list_page_type` (
`type_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `content` longtext NOT NULL,
  `full_html` enum('yes','no') NOT NULL DEFAULT 'no',
  `meta_data` longblob,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `mw_list_page_type`
--

INSERT INTO `mw_list_page_type` (`type_id`, `name`, `slug`, `description`, `content`, `full_html`, `meta_data`, `date_added`, `last_updated`) VALUES
(1, 'Subscribe form', 'subscribe-form', 'When the user will reach the subscription form, he will see this page .', '<div class="box box-primary borderless">\n<div class="box-header">\n<h3 class="box-title">[LIST_NAME]</h3>\n</div>\n\n<div class="box-body">\n<div class="callout callout-info">We''re happy you decided to subscribe to our email list.<br />\nPlease take a few seconds and fill in the list details in order to subscribe to our list.<br />\nYou will receive an email to confirm your subscription, just to be sure this is your email address.</div>\n[LIST_FIELDS]</div>\n\n<div class="box-footer">\n<div class="pull-right">[SUBMIT_BUTTON]</div>\n\n<div class="clearfix">Â </div>\n</div>\n</div>\n', 'no', 0x613a303a7b7d, '2013-09-02 21:47:32', '2014-03-15 14:54:24'),
(2, 'Pending subscribe', 'subscribe-pending', 'After the user will submit the subscription form, he will see this page.', '<div class="box box-primary borderless">\n<div class="box-header">\n<h3 class="box-title">[LIST_NAME]</h3>\n</div>\n\n<div class="box-body">\n<div class="callout callout-info">Please check your email address in order to confirm your subscription.<br />\nThanks.</div>\n</div>\n</div>\n', 'no', 0x613a303a7b7d, '2013-09-02 21:47:56', '2014-03-15 14:54:24'),
(3, 'Subscription confirmed', 'subscribe-confirm', 'After the user will click the confirmation link from within the email, he will see this page.', '<div class="box box-primary borderless">\n<div class="box-header">\n<h3 class="box-title">[LIST_NAME]</h3>\n</div>\n\n<div class="box-body">\n<div class="callout callout-info">Congratulations, your subscription is now complete.<br />\nYou can always update your profile by visiting the following url:<br /><a href="[UPDATE_PROFILE_URL]">Update profile</a></div>\n</div>\n</div>\n', 'no', 0x613a303a7b7d, '2013-09-02 21:48:48', '2014-03-15 14:54:24'),
(4, 'Update Profile', 'update-profile', 'This page will contain all the elements the subscription form contains, the only difference is the heading message.', '<div class="box box-primary borderless">\n<div class="box-header">\n<h3 class="box-title">[LIST_NAME]</h3>\n</div>\n\n<div class="box-body">\n<div class="callout callout-info">Use this form to update your profile information.</div>\n[LIST_FIELDS]</div>\n\n<div class="box-footer">\n<div class="pull-right">[SUBMIT_BUTTON]</div>\n\n<div class="clearfix">Â </div>\n</div>\n</div>\n', 'no', 0x613a303a7b7d, '2013-09-02 21:49:40', '2014-03-15 14:54:24'),
(5, 'Unsubscribe form', 'unsubscribe-form', 'This is the form the user will see when following the unsubscribe link.', '<div class="box box-primary borderless">\n<div class="box-header">\n<h3 class="box-title">[LIST_NAME]</h3>\n</div>\n\n<div class="box-body">\n<div class="callout callout-info">We''re sorry to see you go, but hey, no hard feelings, hopefully we will see you back one day.<br />\nPlease fill in your email address in order to unsubscribe from the list.<br />\nYou will receive an email to confirm your unsubscription, just to make sure this is not an accident or somebody else tries to unsubscribe you.</div>\n[UNSUBSCRIBE_EMAIL_FIELD]<br />\n[UNSUBSCRIBE_REASON_FIELD]</div>\n\n<div class="box-footer">\n<div class="pull-right">[SUBMIT_BUTTON]</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n', 'no', 0x613a303a7b7d, '2013-09-03 11:18:44', '2014-03-15 14:54:24'),
(6, 'Unsubscribe confirmation', 'unsubscribe-confirm', 'When the user clicks on the unsubscribe link from within the email, he will see this page.', '<div class="box box-primary borderless">\n<div class="box-header">\n<h3 class="box-title">[LIST_NAME]</h3>\n</div>\n\n<div class="box-body">\n<div class="callout callout-info">You were successfully removed from the [LIST_NAME] list.<br />\nHopefully you will come back one day.<br /><br />\nHaving doubts?<br />\nPlease click <a href="[SUBSCRIBE_URL]">here</a> in order to subscribe again to the list.</div>\n</div>\n</div>\n', 'no', 0x613a303a7b7d, '2013-09-03 12:14:37', '2014-03-15 14:54:24'),
(7, 'Subscribe confirm email', 'subscribe-confirm-email', 'The email the user receives with the confirmation link', '<!DOCTYPE html>\n<html><head><title>[LIST_NAME]</title><meta content="utf-8" name="charset"><style type="text/css">\n\n	\n	\n	#outlook a{padding:0;}\n	body {width:100% !important; -webkit-text-size-adjust:none; margin:0; padding:0; font-family:  sans-serif; background: #f5f5f5; font-size:12px;}\n	img {border:0;height:auto;line-height:100%;outline:none;text-decoration:none;}\n	table td{border-collapse:collapse;}\n	a {color: #008ca9;text-decoration:none}\n	a:hover {color: #008ca9;text-decoration:none;}\n	#wrap {background:#f5f5f5; padding:10px;}\n	table#main-table {-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px; border:1px solid #008ca9; overflow:hidden; background: #FFFFFF; width: 600px}\n	h1{padding:0; margin:0; font-family: sans-serif;font-size:25px;font-style:italic;color:#FFFFFF; font-weight:bold;}\n	h1 small{font-size:13px;font-weight:normal; font-family:  sans-serif; font-style:italic;}\n	h6{font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal}\n	.darkbg {background: #008ca9}\n	input{outline:none}\n</style>\n</head><body dir="undefined" style="width:100%;-webkit-text-size-adjust:none;margin:0;padding:0;font-family:sans-serif;background:#f5f5f5;font-size:12px">\n                \n            <div id="wrap" style="background:#f5f5f5;padding:10px">\n<table align="center" border="0" cellpadding="0" cellspacing="0" id="main-table" style="-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;border:1px solid #008ca9;overflow:hidden;background:#FFFFFF;width:600px"><tbody><tr><td class="darkbg" style="border-collapse:collapse;background:#008ca9">\n			<table border="0" cellpadding="0" cellspacing="20" width="100%"><tbody><tr><td style="border-collapse:collapse">\n						<h1 style="padding:0;margin:0;font-family:sans-serif;font-size:25px;font-style:italic;color:#FFFFFF;font-weight:bold">[LIST_NAME] <small style="font-size:13px;font-weight:normal;font-family:sans-serif;font-style:italic">[COMPANY_NAME]</small></h1>\n						</td>\n					</tr></tbody></table></td>\n		</tr><tr><td style="border-collapse:collapse">\n			<table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr><td style="border-collapse:collapse">&nbsp;</td>\n					</tr><tr><td style="border-collapse:collapse">Please click <a href="[SUBSCRIBE_URL]" style="color:#008ca9;text-decoration:none">here</a> in order to complete your subscription.<br>\n						If for any reason you cannot access the link, please copy the following url into your browser address bar:<br>\n						[SUBSCRIBE_URL]</td>\n					</tr><tr><td style="border-collapse:collapse">&nbsp;</td>\n					</tr></tbody></table></td>\n		</tr><tr><td class="darkbg" style="padding:10px;border-collapse:collapse;background:#008ca9">\n			<h6 style="font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal">&copy; [CURRENT_YEAR] [COMPANY_NAME]. All rights reserved</h6>\n			</td>\n		</tr></tbody></table></div></body></html>\n', 'yes', 0x613a303a7b7d, '2013-09-05 13:39:56', '2014-03-15 14:54:24'),
(8, 'Unsubscribe confirm email', 'unsubscribe-confirm-email', 'The email the user receives with the confirmation link to unsubscribe', '<!DOCTYPE html>\n<html><head><title>[LIST_NAME]</title><meta content="utf-8" name="charset"><style type="text/css">\n\n	\n	\n	#outlook a{padding:0;}\n	body {width:100% !important; -webkit-text-size-adjust:none; margin:0; padding:0; font-family:  sans-serif; background: #f5f5f5; font-size:12px;}\n	img {border:0;height:auto;line-height:100%;outline:none;text-decoration:none;}\n	table td{border-collapse:collapse;}\n	a {color: #008ca9;text-decoration:none}\n	a:hover {color: #008ca9;text-decoration:none;}\n	#wrap {background:#f5f5f5; padding:10px;}\n	table#main-table {-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px; border:1px solid #008ca9; overflow:hidden; background: #FFFFFF; width: 600px}\n	h1{padding:0; margin:0; font-family: sans-serif;font-size:25px;font-style:italic;color:#FFFFFF; font-weight:bold;}\n	h1 small{font-size:13px;font-weight:normal; font-family:  sans-serif; font-style:italic;}\n	h6{font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal}\n	.darkbg {background: #008ca9}\n	input{outline:none}\n</style>\n</head><body dir="undefined" style="width:100%;-webkit-text-size-adjust:none;margin:0;padding:0;font-family:sans-serif;background:#f5f5f5;font-size:12px">\n                \n            <div id="wrap" style="background:#f5f5f5;padding:10px">\n<table align="center" border="0" cellpadding="0" cellspacing="0" id="main-table" style="-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;border:1px solid #008ca9;overflow:hidden;background:#FFFFFF;width:600px"><tbody><tr><td class="darkbg" style="border-collapse:collapse;background:#008ca9">\n			<table border="0" cellpadding="0" cellspacing="20" width="100%"><tbody><tr><td style="border-collapse:collapse">\n						<h1 style="padding:0;margin:0;font-family:sans-serif;font-size:25px;font-style:italic;color:#FFFFFF;font-weight:bold">[LIST_NAME] <small style="font-size:13px;font-weight:normal;font-family:sans-serif;font-style:italic">[COMPANY_NAME]</small></h1>\n						</td>\n					</tr></tbody></table></td>\n		</tr><tr><td style="border-collapse:collapse">\n			<table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr><td style="border-collapse:collapse">&nbsp;</td>\n					</tr><tr><td style="border-collapse:collapse">Please click <a href="[UNSUBSCRIBE_URL]" style="color:#008ca9;text-decoration:none">here</a> in order to unsubscribe.<br>\n						If for any reason you cannot access the link, please copy the following url into your browser address bar:<br>\n						[UNSUBSCRIBE_URL]</td>\n					</tr><tr><td style="border-collapse:collapse">&nbsp;</td>\n					</tr></tbody></table></td>\n		</tr><tr><td class="darkbg" style="padding:10px;border-collapse:collapse;background:#008ca9">\n			<h6 style="font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal">&copy; [CURRENT_YEAR] [COMPANY_NAME]. All rights reserved</h6>\n			</td>\n		</tr></tbody></table></div></body></html>\n', 'yes', 0x613a303a7b7d, '2013-09-05 13:39:56', '2014-03-15 14:54:24'),
(9, 'Welcome email', 'welcome-email', 'The email the user receives after he successfully subscribes into the list', '<!DOCTYPE html>\n<html><head><title>[LIST_NAME]</title><meta content="utf-8" name="charset">\n<style type="text/css">\n#outlook a{padding:0;}\n	body {width:100% !important; -webkit-text-size-adjust:none; margin:0; padding:0; font-family:  sans-serif; background: #f5f5f5; font-size:12px;}\n	img {border:0;height:auto;line-height:100%;outline:none;text-decoration:none;}\n	table td{border-collapse:collapse;}\n	a {color: #008ca9;text-decoration:none}\n	a:hover {color: #008ca9;text-decoration:none;}\n	#wrap {background:#f5f5f5; padding:10px;}\n	table#main-table {-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px; border:1px solid #008ca9; overflow:hidden; background: #FFFFFF; width: 600px}\n	h1{padding:0; margin:0; font-family: sans-serif;font-size:25px;font-style:italic;color:#FFFFFF; font-weight:bold;}\n	h1 small{font-size:13px;font-weight:normal; font-family:  sans-serif; font-style:italic;}\n	h6{font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal}\n	.darkbg {background: #008ca9}\n	input{outline:none}\n</style>\n</head><body style="width:100%;-webkit-text-size-adjust:none;margin:0;padding:0;font-family:sans-serif;background:#f5f5f5;font-size:12px">\n                \n            <div id="wrap" style="background:#f5f5f5;padding:10px">\n<table align="center" border="0" cellpadding="0" cellspacing="0" id="main-table" style="-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;border:1px solid #008ca9;overflow:hidden;background:#FFFFFF;width:600px"><tbody><tr><td class="darkbg" style="border-collapse:collapse;background:#008ca9">\n			<table border="0" cellpadding="0" cellspacing="20" width="100%"><tbody><tr><td style="border-collapse:collapse">\n						<h1 style="padding:0;margin:0;font-family:sans-serif;font-size:25px;font-style:italic;color:#FFFFFF;font-weight:bold">[LIST_NAME] <small style="font-size:13px;font-weight:normal;font-family:sans-serif;font-style:italic">[COMPANY_NAME]</small></h1>\n						</td>\n					</tr></tbody></table></td>\n		</tr><tr><td style="border-collapse:collapse">\n			<table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr><td style="border-collapse:collapse">&nbsp;</td>\n					</tr><tr><td style="border-collapse:collapse">Thank you for subscribing into [LIST_NAME] email list.<br>\n						You can update your information at any time by clicking <a href="[UPDATE_PROFILE_URL]" style="color:#008ca9;text-decoration:none">here</a>.<br>\n						Thank you.</td>\n					</tr><tr><td style="border-collapse:collapse">&nbsp;</td>\n					</tr></tbody></table></td>\n		</tr><tr><td class="darkbg" style="padding:10px;border-collapse:collapse;background:#008ca9">\n			<h6 style="font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal">&copy; [CURRENT_YEAR] [COMPANY_NAME]. All rights reserved</h6>\n			</td>\n		</tr></tbody></table></div></body></html>\n', 'yes', 0x613a303a7b7d, '2013-09-05 13:39:56', '2015-03-19 11:13:09'),
(10, 'Subscription confirmed approval', 'subscribe-confirm-approval', 'After the user will click the confirmation link from within the email, if the list requires confirm approval, he will see this page.', '<div class="box box-primary borderless"> <div class="box-header"> <h3 class="box-title">[LIST_NAME]</h3> </div> <div class="box-body"> <div class="callout callout-info">Congratulations, your subscription is now complete and awaiting approval.<br /> Once the approval process is done, you will get a confirmation email with further instructions.<br />Thanks.</div> </div> </div> ', 'no', 0x613a303a7b7d, '2016-04-04 21:48:48', '2016-04-04 14:54:24'),
(11, 'Subscription confirmed approval email', 'subscribe-confirm-approval-email', 'The email the user receives after his subscription is approved.', '<!DOCTYPE html>\r\n<html><head><title>[LIST_NAME]</title><meta content="utf-8" name="charset">\r\n<style type="text/css">\r\n#outlook a{padding:0;}\r\n	body {width:100% !important; -webkit-text-size-adjust:none; margin:0; padding:0; font-family:  sans-serif; background: #f5f5f5; font-size:12px;}\r\n	img {border:0;height:auto;line-height:100%;outline:none;text-decoration:none;}\r\n	table td{border-collapse:collapse;}\r\n	a {color: #008ca9;text-decoration:none}\r\n	a:hover {color: #008ca9;text-decoration:none;}\r\n	#wrap {background:#f5f5f5; padding:10px;}\r\n	table#main-table {-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px; border:1px solid #008ca9; overflow:hidden; background: #FFFFFF; width: 600px}\r\n	h1{padding:0; margin:0; font-family: sans-serif;font-size:25px;font-style:italic;color:#FFFFFF; font-weight:bold;}\r\n	h1 small{font-size:13px;font-weight:normal; font-family:  sans-serif; font-style:italic;}\r\n	h6{font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal}\r\n	.darkbg {background: #008ca9}\r\n	input{outline:none}\r\n</style>\r\n</head><body style="width:100%;-webkit-text-size-adjust:none;margin:0;padding:0;font-family:sans-serif;background:#f5f5f5;font-size:12px">\r\n                \r\n            <div id="wrap" style="background:#f5f5f5;padding:10px">\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" id="main-table" style="-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;border:1px solid #008ca9;overflow:hidden;background:#FFFFFF;width:600px"><tbody><tr><td class="darkbg" style="border-collapse:collapse;background:#008ca9">\r\n			<table border="0" cellpadding="0" cellspacing="20" width="100%"><tbody><tr><td style="border-collapse:collapse">\r\n						<h1 style="padding:0;margin:0;font-family:sans-serif;font-size:25px;font-style:italic;color:#FFFFFF;font-weight:bold">[LIST_NAME] <small style="font-size:13px;font-weight:normal;font-family:sans-serif;font-style:italic">[COMPANY_NAME]</small></h1>\r\n						</td>\r\n					</tr></tbody></table></td>\r\n		</tr><tr><td style="border-collapse:collapse">\r\n			<table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr><td style="border-collapse:collapse">&nbsp;</td>\r\n					</tr><tr><td style="border-collapse:collapse">Congratulations, <br />Your subscription into [LIST_NAME] email list is now approved.<br>\r\n						You can update your information at any time by clicking <a href="[UPDATE_PROFILE_URL]" style="color:#008ca9;text-decoration:none">here</a>.<br>\r\n						Thank you.</td>\r\n					</tr><tr><td style="border-collapse:collapse">&nbsp;</td>\r\n					</tr></tbody></table></td>\r\n		</tr><tr><td class="darkbg" style="padding:10px;border-collapse:collapse;background:#008ca9">\r\n			<h6 style="font-size:10px;color:#FFFFFF;margin:0;padding:0;font-weight:normal">&copy; [CURRENT_YEAR] [COMPANY_NAME]. All rights reserved</h6>\r\n			</td>\r\n		</tr></tbody></table></div></body></html>\r\n', 'yes', 0x613a303a7b7d, '2013-09-05 13:39:56', '2015-03-19 11:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_segment`
--

CREATE TABLE IF NOT EXISTS `mw_list_segment` (
`segment_id` int(11) NOT NULL,
  `segment_uid` char(13) NOT NULL,
  `list_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `operator_match` enum('any','all') NOT NULL DEFAULT 'any',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_segment_condition`
--

CREATE TABLE IF NOT EXISTS `mw_list_segment_condition` (
`condition_id` int(11) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `operator_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_segment_operator`
--

CREATE TABLE IF NOT EXISTS `mw_list_segment_operator` (
`operator_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mw_list_segment_operator`
--

INSERT INTO `mw_list_segment_operator` (`operator_id`, `name`, `slug`, `date_added`, `last_updated`) VALUES
(1, 'is', 'is', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(2, 'is not', 'is-not', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(3, 'contains', 'contains', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(4, 'not contains', 'not-contains', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(5, 'starts with', 'starts', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(6, 'ends with', 'ends', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(7, 'is greater than', 'greater', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(8, 'is less than', 'less', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(9, 'not starts with', 'not-starts', '2013-09-06 16:06:01', '2013-09-06 16:06:01'),
(10, 'not ends with', 'not-ends', '2013-09-06 16:06:01', '2013-09-06 16:06:01');

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_subscriber`
--

CREATE TABLE IF NOT EXISTS `mw_list_subscriber` (
`subscriber_id` int(11) NOT NULL,
  `subscriber_uid` char(13) NOT NULL,
  `list_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `source` enum('web','api','import') NOT NULL DEFAULT 'web',
  `status` char(15) NOT NULL DEFAULT 'unconfirmed',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_subscriber_action`
--

CREATE TABLE IF NOT EXISTS `mw_list_subscriber_action` (
`action_id` int(11) NOT NULL,
  `source_list_id` int(11) NOT NULL,
  `source_action` char(15) NOT NULL DEFAULT 'subscribe',
  `target_list_id` int(11) NOT NULL,
  `target_action` char(15) NOT NULL DEFAULT 'unsubscribe'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_subscriber_field_cache`
--

CREATE TABLE IF NOT EXISTS `mw_list_subscriber_field_cache` (
  `subscriber_id` int(11) NOT NULL,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mw_list_subscriber_list_move`
--

CREATE TABLE IF NOT EXISTS `mw_list_subscriber_list_move` (
`id` int(11) NOT NULL,
  `source_subscriber_id` int(11) NOT NULL,
  `source_list_id` int(11) NOT NULL,
  `destination_subscriber_id` int(11) NOT NULL,
  `destination_list_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_option`
--

CREATE TABLE IF NOT EXISTS `mw_option` (
  `category` varchar(150) NOT NULL,
  `key` varchar(150) NOT NULL,
  `value` longblob NOT NULL,
  `is_serialized` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mw_option`
--

INSERT INTO `mw_option` (`category`, `key`, `value`, `is_serialized`, `date_added`, `last_updated`) VALUES
('system', 'common', 0x6170705f6e616d65, 0, '2013-10-29 23:31:29', '2013-10-29 23:31:29'),
('system.campaign.attachments', 'allowed_extensions', 0x613a393a7b693a303b733a333a22706466223b693a313b733a333a22646f63223b693a323b733a343a22646f6378223b693a333b733a333a22786c73223b693a343b733a343a22786c7378223b693a353b733a333a22707074223b693a363b733a343a2270707478223b693a373b733a333a227a6970223b693a383b733a333a22726172223b7d, 1, '2014-01-14 09:57:08', '2014-01-18 11:09:50'),
('system.campaign.attachments', 'allowed_files_count', 0x35, 0, '2014-01-14 10:07:14', '2014-01-14 10:07:14'),
('system.campaign.attachments', 'allowed_file_size', 0x31303438353736, 0, '2014-01-14 10:07:14', '2014-01-14 10:12:53'),
('system.campaign.attachments', 'allowed_mime_types', 0x613a34313a7b693a303b733a31353a226170706c69636174696f6e2f706466223b693a313b733a31373a226170706c69636174696f6e2f782d706466223b693a323b733a31393a226170706c69636174696f6e2f6163726f626174223b693a333b733a32303a226170706c69636174696f6e732f766e642e706466223b693a343b733a383a22746578742f706466223b693a353b733a31303a22746578742f782d706466223b693a363b733a31383a226170706c69636174696f6e2f6d73776f7264223b693a373b733a31353a226170706c69636174696f6e2f646f63223b693a383b733a393a226170706c2f74657874223b693a393b733a32323a226170706c69636174696f6e2f766e642e6d73776f7264223b693a31303b733a32333a226170706c69636174696f6e2f766e642e6d732d776f7264223b693a31313b733a31393a226170706c69636174696f6e2f77696e776f7264223b693a31323b733a31363a226170706c69636174696f6e2f776f7264223b693a31333b733a31383a226170706c69636174696f6e2f782d6d737736223b693a31343b733a32303a226170706c69636174696f6e2f782d6d73776f7264223b693a31353b733a37313a226170706c69636174696f6e2f766e642e6f70656e786d6c666f726d6174732d6f6666696365646f63756d656e742e776f726470726f63657373696e676d6c2e646f63756d656e74223b693a31363b733a32343a226170706c69636174696f6e2f766e642e6d732d657863656c223b693a31373b733a31393a226170706c69636174696f6e2f6d73657863656c223b693a31383b733a32313a226170706c69636174696f6e2f782d6d73657863656c223b693a31393b733a32323a226170706c69636174696f6e2f782d6d732d657863656c223b693a32303b733a31393a226170706c69636174696f6e2f782d657863656c223b693a32313b733a32363a226170706c69636174696f6e2f782d646f735f6d735f657863656c223b693a32323b733a31353a226170706c69636174696f6e2f786c73223b693a32333b733a36353a226170706c69636174696f6e2f766e642e6f70656e786d6c666f726d6174732d6f6666696365646f63756d656e742e73707265616473686565746d6c2e7368656574223b693a32343b733a32393a226170706c69636174696f6e2f766e642e6d732d706f776572706f696e74223b693a32353b733a32343a226170706c69636174696f6e2f6d73706f776572706f696e74223b693a32363b733a32353a226170706c69636174696f6e2f6d732d706f776572706f696e74223b693a32373b733a32323a226170706c69636174696f6e2f6d73706f776572706e74223b693a32383b733a32383a226170706c69636174696f6e2f766e642d6d73706f776572706f696e74223b693a32393b733a32323a226170706c69636174696f6e2f706f776572706f696e74223b693a33303b733a32343a226170706c69636174696f6e2f782d706f776572706f696e74223b693a33313b733a31353a226170706c69636174696f6e2f782d6d223b693a33323b733a37333a226170706c69636174696f6e2f766e642e6f70656e786d6c666f726d6174732d6f6666696365646f63756d656e742e70726573656e746174696f6e6d6c2e70726573656e746174696f6e223b693a33333b733a31353a226170706c69636174696f6e2f7a6970223b693a33343b733a31373a226170706c69636174696f6e2f782d7a6970223b693a33353b733a32383a226170706c69636174696f6e2f782d7a69702d636f6d70726573736564223b693a33363b733a32343a226170706c69636174696f6e2f6f637465742d73747265616d223b693a33373b733a32323a226170706c69636174696f6e2f782d636f6d7072657373223b693a33383b733a32343a226170706c69636174696f6e2f782d636f6d70726573736564223b693a33393b733a31353a226d756c7469706172742f782d7a6970223b693a34303b733a32383a226170706c69636174696f6e2f782d7261722d636f6d70726573736564223b7d, 1, '2014-01-14 09:57:08', '2014-01-18 11:09:50'),
('system.campaign.attachments', 'enabled', 0x6e6f, 0, '2014-01-14 09:57:08', '2014-01-14 10:09:42'),
('system.campaign.template_tags', 'template_tags', 0x613a34323a7b693a303b613a323a7b733a333a22746167223b733a31373a225b554e5355425343524942455f55524c5d223b733a383a227265717569726564223b623a313b7d693a313b613a323a7b733a333a22746167223b733a32323a225b434f4d50414e595f46554c4c5f414444524553535d223b733a383a227265717569726564223b623a303b7d693a323b613a323a7b733a333a22746167223b733a32303a225b5550444154455f50524f46494c455f55524c5d223b733a383a227265717569726564223b623a303b7d693a333b613a323a7b733a333a22746167223b733a31373a225b5745425f56455253494f4e5f55524c5d223b733a383a227265717569726564223b623a303b7d693a343b613a323a7b733a333a22746167223b733a31343a225b43414d504149474e5f55524c5d223b733a383a227265717569726564223b623a303b7d693a353b613a323a7b733a333a22746167223b733a32303a225b464f52574152445f465249454e445f55524c5d223b733a383a227265717569726564223b623a303b7d693a363b613a323a7b733a333a22746167223b733a32343a225b4449524543545f554e5355425343524942455f55524c5d223b733a383a227265717569726564223b623a303b7d693a373b613a323a7b733a333a22746167223b733a31313a225b4c4953545f4e414d455d223b733a383a227265717569726564223b623a303b7d693a383b613a323a7b733a333a22746167223b733a31343a225b4c4953545f5355424a4543545d223b733a383a227265717569726564223b623a303b7d693a393b613a323a7b733a333a22746167223b733a31383a225b4c4953545f4445534352495054494f4e5d223b733a383a227265717569726564223b623a303b7d693a31303b613a323a7b733a333a22746167223b733a31363a225b4c4953545f46524f4d5f4e414d455d223b733a383a227265717569726564223b623a303b7d693a31313b613a323a7b733a333a22746167223b733a31373a225b4c4953545f46524f4d5f454d41494c5d223b733a383a227265717569726564223b623a303b7d693a31323b613a323a7b733a333a22746167223b733a31343a225b43555252454e545f594541525d223b733a383a227265717569726564223b623a303b7d693a31333b613a323a7b733a333a22746167223b733a31353a225b43555252454e545f4d4f4e54485d223b733a383a227265717569726564223b623a303b7d693a31343b613a323a7b733a333a22746167223b733a31333a225b43555252454e545f4441595d223b733a383a227265717569726564223b623a303b7d693a31353b613a323a7b733a333a22746167223b733a31343a225b43555252454e545f444154455d223b733a383a227265717569726564223b623a303b7d693a31363b613a323a7b733a333a22746167223b733a31343a225b434f4d50414e595f4e414d455d223b733a383a227265717569726564223b623a303b7d693a31373b613a323a7b733a333a22746167223b733a31373a225b434f4d50414e595f574542534954455d223b733a383a227265717569726564223b623a303b7d693a31383b613a323a7b733a333a22746167223b733a31393a225b434f4d50414e595f414444524553535f315d223b733a383a227265717569726564223b623a303b7d693a31393b613a323a7b733a333a22746167223b733a31393a225b434f4d50414e595f414444524553535f325d223b733a383a227265717569726564223b623a303b7d693a32303b613a323a7b733a333a22746167223b733a31343a225b434f4d50414e595f434954595d223b733a383a227265717569726564223b623a303b7d693a32313b613a323a7b733a333a22746167223b733a31343a225b434f4d50414e595f5a4f4e455d223b733a383a227265717569726564223b623a303b7d693a32323b613a323a7b733a333a22746167223b733a31333a225b434f4d50414e595f5a49505d223b733a383a227265717569726564223b623a303b7d693a32333b613a323a7b733a333a22746167223b733a31373a225b434f4d50414e595f434f554e5452595d223b733a383a227265717569726564223b623a303b7d693a32343b613a323a7b733a333a22746167223b733a31353a225b434f4d50414e595f50484f4e455d223b733a383a227265717569726564223b623a303b7d693a32353b613a323a7b733a333a22746167223b733a31353a225b43414d504149474e5f4e414d455d223b733a383a227265717569726564223b623a303b7d693a32363b613a323a7b733a333a22746167223b733a31383a225b43414d504149474e5f5355424a4543545d223b733a383a227265717569726564223b623a303b7d693a32373b613a323a7b733a333a22746167223b733a31383a225b43414d504149474e5f544f5f4e414d455d223b733a383a227265717569726564223b623a303b7d693a32383b613a323a7b733a333a22746167223b733a32303a225b43414d504149474e5f46524f4d5f4e414d455d223b733a383a227265717569726564223b623a303b7d693a32393b613a323a7b733a333a22746167223b733a32313a225b43414d504149474e5f46524f4d5f454d41494c5d223b733a383a227265717569726564223b623a303b7d693a33303b613a323a7b733a333a22746167223b733a31393a225b43414d504149474e5f5245504c595f544f5d223b733a383a227265717569726564223b623a303b7d693a33313b613a323a7b733a333a22746167223b733a31343a225b43414d504149474e5f5549445d223b733a383a227265717569726564223b623a303b7d693a33323b613a323a7b733a333a22746167223b733a31363a225b535542534352494245525f5549445d223b733a383a227265717569726564223b623a303b7d693a33333b613a323a7b733a333a22746167223b733a31353a225b535542534352494245525f49505d223b733a383a227265717569726564223b623a303b7d693a33343b613a323a7b733a333a22746167223b733a32333a225b535542534352494245525f444154455f41444445445d223b733a383a227265717569726564223b623a303b7d693a33353b613a323a7b733a333a22746167223b733a33333a225b535542534352494245525f444154455f41444445445f4c4f43414c495a45445d223b733a383a227265717569726564223b623a303b7d693a33363b613a323a7b733a333a22746167223b733a363a225b444154455d223b733a383a227265717569726564223b623a303b7d693a33373b613a323a7b733a333a22746167223b733a31303a225b4441544554494d455d223b733a383a227265717569726564223b623a303b7d693a33383b613a323a7b733a333a22746167223b733a32323a225b52414e444f4d5f434f4e54454e543a617c627c635d223b733a383a227265717569726564223b623a303b7d693a33393b613a323a7b733a333a22746167223b733a32373a225b43414d504149474e5f5245504f52545f41425553455f55524c5d223b733a383a227265717569726564223b623a303b7d693a34303b613a323a7b733a333a22746167223b733a32303a225b43555252454e545f444f4d41494e5f55524c5d223b733a383a227265717569726564223b623a303b7d693a34313b613a323a7b733a333a22746167223b733a31363a225b43555252454e545f444f4d41494e5d223b733a383a227265717569726564223b623a303b7d7d, 1, '2016-06-06 00:54:11', '2016-06-06 00:54:11'),
('system.common', 'api_status', 0x6f6e6c696e65, 0, '2013-10-16 11:37:11', '2013-10-16 11:37:20'),
('system.common', 'app_name', 0x4d61696c57697a7a20454d53, 0, '2013-10-29 23:33:18', '2013-10-29 23:33:18'),
('system.common', 'app_version', 0x312e30, 0, '2013-10-29 23:33:18', '2013-10-29 23:33:18'),
('system.common', 'backend_page_size', 0x3130, 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'check_version_update', 0x6e6f, 0, '2016-03-25 09:15:43', '2016-12-22 10:39:57'),
('system.common', 'clean_urls', 0x30, 0, '2013-09-04 09:43:07', '2016-03-25 09:17:17'),
('system.common', 'company_info', '', 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'customer_page_size', 0x3130, 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'default_mailer', 0x5048504d61696c6572, 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'ga_tracking_code_id', '', 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'show_backend_timeinfo', 0x6e6f, 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'show_customer_timeinfo', 0x6e6f, 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'site_description', 0x4175746f6d6174656420456d61696c206d61726b6574696e6720536574757020546f6f6c, 0, '2013-09-04 08:47:17', '2016-03-25 09:15:43'),
('system.common', 'site_keywords', 0x656d61696c2c206d61726b6574696e672c20656d61696c206d61726b6574696e672c20656d61696c2064656c69766572792c2064656c69766572792c20696e626f782064656c6976657279, 0, '2013-09-04 08:47:17', '2013-10-26 20:06:12'),
('system.common', 'site_name', 0x41544d204175746f726573706f6e646572, 0, '2013-09-04 08:39:08', '2016-12-22 10:39:57'),
('system.common', 'site_offline_message', 0x4170706c69636174696f6e2063757272656e746c79206f66666c696e652e2054727920616761696e206c6174657221, 0, '2013-09-22 23:26:00', '2013-09-22 23:26:40'),
('system.common', 'site_status', 0x6f6e6c696e65, 0, '2013-09-22 22:47:38', '2016-12-08 19:40:42'),
('system.common', 'site_tagline', 0x4175746f6d6174656420456d61696c206d61726b6574696e67, 0, '2013-09-04 08:39:08', '2016-03-25 09:15:43'),
('system.common', 'support_forum_url', 0x68747470733a2f2f666f72756d2e6d61696c77697a7a2e636f6d2f, 0, '2016-03-25 09:15:43', '2016-03-25 09:15:43'),
('system.common', 'support_url', 0x68747470733a2f2f6b622e6d61696c77697a7a2e636f6d2f, 0, '2016-12-22 10:39:57', '2016-12-22 10:39:57'),
('system.common', 'use_tidy', 0x796573, 0, '2016-12-22 10:39:57', '2016-12-22 10:39:57'),
('system.common', 'version', 0x312e332e382e30, 0, '2016-03-09 19:24:56', '2016-12-08 19:40:42'),
('system.common.version_update', 'current_version', 0x312e332e382e33, 0, '2016-06-06 00:52:16', '2016-12-18 08:04:28'),
('system.common.version_update', 'last_check', 0x693a313438323134343134323b, 1, '2016-03-17 20:25:15', '2016-12-19 10:42:22'),
('system.cron.process_bounce_servers', 'emails_at_once', 0x313030, 0, '2016-11-06 07:24:11', '2016-11-06 07:25:18'),
('system.cron.process_bounce_servers', 'memory_limit', '', 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_bounce_servers', 'pause', 0x35, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_bounce_servers', 'servers_at_once', 0x3130, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_delivery_bounce', 'max_fatal_errors', 0x31, 0, '2013-10-07 11:36:23', '2013-10-22 20:32:22'),
('system.cron.process_delivery_bounce', 'max_hard_bounce', 0x31, 0, '2013-10-07 11:36:23', '2013-10-22 20:32:22'),
('system.cron.process_delivery_bounce', 'max_soft_bounce', 0x31, 0, '2013-10-07 11:36:23', '2016-11-06 07:26:47'),
('system.cron.process_delivery_bounce', 'max_soft_errors', 0x31, 0, '2013-10-07 11:36:23', '2016-11-06 07:26:47'),
('system.cron.process_delivery_bounce', 'memory_limit', '', 0, '2013-10-07 11:36:23', '2013-11-01 13:42:47'),
('system.cron.process_delivery_bounce', 'process_at_once', 0x3530, 0, '2013-10-07 11:36:23', '2016-11-06 07:26:47'),
('system.cron.process_feedback_loop_servers', 'emails_at_once', 0x313030, 0, '2016-11-06 07:24:11', '2016-11-06 07:26:26'),
('system.cron.process_feedback_loop_servers', 'memory_limit', '', 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_feedback_loop_servers', 'pause', 0x35, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_feedback_loop_servers', 'servers_at_once', 0x3130, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_feedback_loop_servers', 'subscriber_action', 0x756e737562736372696265, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_subscribers', 'blacklisted_days', 0x30, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_subscribers', 'memory_limit', '', 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_subscribers', 'unconfirm_days', 0x33, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.process_subscribers', 'unsubscribe_days', 0x30, 0, '2016-11-06 07:24:11', '2016-11-06 07:27:58'),
('system.cron.send_campaigns', 'campaigns_at_once', 0x3530, 0, '2013-10-07 11:12:18', '2016-11-06 07:24:11'),
('system.cron.send_campaigns', 'campaigns_in_parallel', 0x3530, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:43'),
('system.cron.send_campaigns', 'change_server_at', 0x323030, 0, '2013-10-07 13:08:56', '2013-10-16 23:56:27'),
('system.cron.send_campaigns', 'emails_per_minute', 0x313030, 0, '2013-10-07 11:12:18', '2013-10-07 11:12:18'),
('system.cron.send_campaigns', 'max_bounce_rate', 0x3130, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:30'),
('system.cron.send_campaigns', 'memory_limit', '', 0, '2013-10-07 11:12:18', '2013-11-01 13:42:47'),
('system.cron.send_campaigns', 'parallel_processes_per_campaign', 0x33, 0, '2014-02-16 22:39:58', '2014-02-16 22:39:58'),
('system.cron.send_campaigns', 'pause', 0x3132, 0, '2013-10-07 11:12:18', '2016-11-06 07:24:11'),
('system.cron.send_campaigns', 'send_at_once', 0x3230, 0, '2013-10-07 11:12:18', '2016-11-06 07:24:11'),
('system.cron.send_campaigns', 'subscribers_at_once', 0x313030, 0, '2013-10-07 11:12:18', '2013-11-01 13:41:55'),
('system.cron.send_campaigns', 'subscriber_batches_in_parallel', 0x35, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.send_campaigns', 'use_pcntl', 0x6e6f, 0, '2016-11-06 07:24:11', '2016-11-06 07:24:11'),
('system.cron.transactional_emails', 'offset', 0x693a303b, 1, '2016-03-09 19:46:01', '2016-03-17 20:48:02'),
('system.customer_campaigns', 'can_delete_own_campaigns', 0x796573, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_campaigns', 'can_export_stats', 0x796573, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_campaigns', 'change_server_at', 0x323030, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_campaigns', 'emails_per_minute', 0x313030, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_campaigns', 'email_footer', '', 0, '2016-11-06 07:30:47', '2016-11-06 07:30:47'),
('system.customer_campaigns', 'max_bounce_rate', 0x3130, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_campaigns', 'max_campaigns', 0x2d31, 0, '2016-11-06 07:30:47', '2016-11-06 07:30:47'),
('system.customer_campaigns', 'must_verify_sending_domain', 0x6e6f, 0, '2016-11-06 07:30:47', '2016-11-06 07:30:47'),
('system.customer_campaigns', 'pause', 0x3132, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_campaigns', 'send_at_once', 0x3230, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_campaigns', 'send_to_multiple_lists', 0x796573, 0, '2016-11-06 07:30:47', '2016-11-06 07:30:47'),
('system.customer_campaigns', 'subscribers_at_once', 0x313030, 0, '2016-11-06 07:30:48', '2016-11-06 07:30:48'),
('system.customer_lists', 'can_copy_subscribers', 0x796573, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'can_delete_own_lists', 0x796573, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'can_delete_own_subscribers', 0x796573, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'can_export_subscribers', 0x796573, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'can_import_subscribers', 0x796573, 0, '2016-11-06 07:53:40', '2016-11-06 07:53:40'),
('system.customer_lists', 'can_mark_blacklisted_as_confirmed', 0x6e6f, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'can_segment_lists', 0x796573, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'copy_subscribers_at_once', 0x313030, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'copy_subscribers_memory_limit', '', 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'max_lists', 0x2d31, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'max_segment_conditions', 0x3130, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'max_segment_wait_timeout', 0x35, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'max_subscribers', 0x2d31, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_lists', 'max_subscribers_per_list', 0x2d31, 0, '2016-11-06 07:53:41', '2016-11-06 07:53:41'),
('system.customer_sending', 'quota_notify_email_content', 0x48656c6c6f205b46554c4c5f4e414d455d2c20c2a03c6272202f3e3c6272202f3e0a596f7572206d6178696d756d20616c6c6f7765642073656e64696e672071756f74612069732073657420746f205b51554f54415f544f54414c5d20656d61696c7320616e6420796f752063757272656e746c7920686176652073656e74205b51554f54415f55534147455d20656d61696c732c207768696368206d65616e7320796f7520686176652075736564205b51554f54415f55534147455f50455243454e545d206f6620796f757220616c6c6f7765642073656e64696e672071756f7461213c6272202f3e0a4f6e636520796f75722073656e64696e672071756f7461206973206f7665722c20796f752077696c6c206e6f742062652061626c6520746f2073656e6420616e7920656d61696c73213c6272202f3e3c6272202f3e0a506c65617365206d616b65207375726520796f752072656e657720796f75722073656e64696e672071756f74612e3c6272202f3e0a5468616e6b20796f7521, 0, '2016-10-31 17:55:19', '2016-10-31 18:43:04'),
('system.customer_servers', 'allowed_server_types', 0x613a303a7b7d, 1, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customer_servers', 'can_select_delivery_servers_for_campaign', 0x6e6f, 0, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customer_servers', 'can_send_from_system_servers', 0x796573, 0, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customer_servers', 'custom_headers', '', 0, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customer_servers', 'max_bounce_servers', 0x30, 0, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customer_servers', 'max_delivery_servers', 0x30, 0, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customer_servers', 'max_fbl_servers', 0x30, 0, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customer_servers', 'must_add_bounce_server', 0x796573, 0, '2016-12-18 10:34:41', '2016-12-18 10:34:41'),
('system.customization', 'backend_logo', '', 0, '2016-12-22 10:41:40', '2016-12-22 10:41:40'),
('system.customization', 'backend_logo_text', 0x41544d204175746f726573706f6e646572, 0, '2016-03-25 09:18:26', '2016-12-22 10:41:40'),
('system.customization', 'backend_skin', 0x736b696e2d6461726b2d626c7565, 0, '2016-03-25 09:18:26', '2016-03-25 09:18:34'),
('system.customization', 'customer_logo', '', 0, '2016-12-22 10:41:40', '2016-12-22 10:41:40'),
('system.customization', 'customer_logo_text', 0x41544d204175746f726573706f6e646572, 0, '2016-03-25 09:18:26', '2016-12-22 10:41:40'),
('system.customization', 'customer_skin', 0x736b696e2d6461726b2d626c7565, 0, '2016-03-25 09:18:26', '2016-03-25 09:18:47'),
('system.customization', 'frontend_logo', '', 0, '2016-12-22 10:41:40', '2016-12-22 10:41:40'),
('system.customization', 'frontend_logo_text', 0x41544d204175746f726573706f6e646572, 0, '2016-03-25 09:18:26', '2016-12-22 10:41:40'),
('system.customization', 'frontend_skin', 0x736b696e2d6461726b2d626c7565, 0, '2016-03-25 09:18:26', '2016-03-25 09:18:47'),
('system.email_blacklist', 'local_check', 0x796573, 0, '2014-01-09 14:48:41', '2014-01-09 15:18:22'),
('system.email_blacklist', 'regular_expressions', 0x2f6a756e6b206d61696c2f207370616d2d72656c617465640d0a2f626c61636b6c6973742f207370616d2d72656c617465640d0a2f626c6f636b65642f207370616d2d72656c617465640d0a2f5c62555c2e3f435c2e3f455c2e3f5c622f207370616d2d72656c617465640d0a202f5c62416476286572746973656d656e74733f293f5c622f207370616d2d72656c61746564202f756e736f6c6963697465642f207370616d2d72656c617465640d0a2f5c62286f70656e293f52424c5c622f207370616d2d72656c617465640d0a2f7265616c74696d6520626c61636b686f6c652f207370616d2d72656c617465640d0a2f687474703a5c2f5c2f62617369632e776972656875622e6e6c5c2f626c61636b686f6c65732e68746d6c2f20202020202020207370616d2d72656c617465640d0a2f5c6276697275735c622f2076697275732d72656c617465640d0a2f6d657373616765202b636f6e74656e742f20636f6e74656e742d72656c617465640d0a202f636f6e74656e74202b72656a65637465642f20636f6e74656e742d72656c617465640d0a2f71756f74612f2071756f74612d6973737565730d0a2f6c696d69742065786365656465642f2071756f74612d6973737565730d0a2f6d61696c626f78202b286973202b293f66756c6c2f2071756f74612d6973737565730d0a2f5c6273746f726167655c622f2071756f74612d6973737565730d0a2f28757365727c6d61696c626f787c726563697069656e747c726370747c6c6f63616c20706172747c616464726573737c6163636f756e747c6d61696c2064726f707c616428643f297265737365652920286861737c686173206265656e7c6973293f202a2863757272656e746c797c74656d706f726172696c79202b293f2864697361626c65647c0d0a657870697265647c696e6163746976657c6e6f7420616374697661746564292f20696e6163746976652d6d61696c626f780d0a2f28636f6e74617c7573752e72696f2920696e6174697628617c6f292f20696e6163746976652d6d61696c626f780d0a2f546f6f206d616e7920286261647c696e76616c69647c756e6b6e6f776e7c696c6c6567616c7c756e617661696c61626c65292028757365727c6d61696c626f787c726563697069656e747c726370747c6c6f63616c0d0a706172747c616464726573737c6163636f756e747c6d61696c2064726f707c616428643f29726573736565292f206f74686572202f284e6f20737563687c6261647c696e76616c69647c756e6b6e6f776e7c696c6c6567616c7c20756e617661696c61626c652920286c6f63616c202b293f28757365727c6d61696c626f787c726563697069656e747c726370747c206c6f63616c20706172747c616464726573737c6163636f756e747c6d61696c2064726f707c616428643f29726573736565292f206261642d6d61696c626f780d0a2f28757365727c6d61696c626f787c726563697069656e747c726370747c6c6f63616c20706172747c20616464726573737c6163636f756e747c6d61696c2064726f707c616428643f2972657373656529202b285c532b405c532b202b293f286e6f74202861202b293f76616c69647c6e6f74206b6e6f776e7c6e6f7420686572657c6e6f7420666f756e647c646f6573206e6f742065786973747c6261647c696e76616c69647c756e6b6e6f776e7c696c6c6567616c7c756e617661696c61626c65292f206261642d6d61696c626f780d0a2f5c532b405c532b202b286973202b293f286e6f74202861202b293f76616c69647c6e6f74206b6e6f776e7c6e6f7420686572657c6e6f7420666f756e647c646f6573206e6f742065786973747c6261647c696e76616c69647c756e6b6e6f776e7c20696c6c6567616c7c756e617661696c61626c65292f206261642d6d61696c626f780d0a2f6e6f206d61696c626f7820686572652062792074686174206e616d652f206261642d6d61696c626f780d0a2f6d792062616472637074746f206c6973742f206261642d6d61696c626f780d0a2f6e6f74206f757220637573746f6d65722f206261642d6d61696c626f780d0a2f6e6f206c6f6e676572202876616c69647c617661696c61626c65292f206261642d6d61696c626f780d0a2f686176652061205c532b206163636f756e742f206261642d6d61696c626f780d0a2f5c6272656c617928696e67293f2f2072656c6179696e672d6973737565730d0a2f646f6d61696e2028726574697265647c6261647c696e76616c69647c756e6b6e6f776e7c696c6c6567616c7c20756e617661696c61626c65292f206261642d646f6d61696e0d0a2f646f6d61696e206e6f206c6f6e67657220696e207573652f206261642d646f6d61696e0d0a2f646f6d61696e20285c532b202b293f286973202b293f6f62736f6c6574652f206261642d646f6d61696e0d0a2f64656e6965642f20706f6c6963792d72656c617465640d0a2f70726f68696269742f20706f6c6963792d72656c617465640d0a2f72656a65637465642f20706f6c6963792d72656c617465640d0a2f726566757365642f20706f6c6963792d72656c617465640d0a2f616c6c6f7765642f20706f6c6963792d72656c617465640d0a2f62616e6e65642f20706f6c6963792d72656c617465640d0a2f706f6c6963792f20706f6c6963792d72656c617465640d0a2f737573706963696f75732061637469766974792f20706f6c6963792d72656c617465640d0a2f6261642073657175656e63652f2070726f746f636f6c2d6572726f72730d0a2f73796e746178206572726f722f2070726f746f636f6c2d6572726f72730d0a2f5c62726f7574655c622f20726f7574696e672d6572726f72730d0a2f5c62756e726f757461626c655c622f20726f7574696e672d6572726f72730d0a2f5c62756e726f75746561626c655c622f20726f7574696e672d6572726f72730d0a2f5e322e5c642e5c642f20737563636573730d0a2f5e5b34355d5c2e315c2e312f206261642d6d61696c626f780d0a2f5e5b34355d5c2e315c2e322f206261642d646f6d61696e0d0a2f5e5b34355d5c2e335c2e352f206261642d636f6e66696775726174696f6e0d0a2f5e5b34355d5c2e345c2e312f206e6f2d616e737765722d66726f6d2d686f73740d0a2f5e5b34355d5c2e345c2e322f206261642d636f6e6e656374696f6e0d0a2f5e5b34355d5c2e345c2e342f20726f7574696e672d6572726f72730d0a2f5e5b34355d5c2e345c2e362f20726f7574696e672d6572726f72730d0a2f5e5b34355d5c2e345c2e372f206d6573736167652d657870697265640d0a2f5e5b34355d5c2e375c2e312f20706f6c6963792d72656c617465640d0a2f2f206f7468657220232063617463682d616c6c0a, 0, '2016-11-06 07:51:06', '2016-12-08 19:40:42'),
('system.email_blacklist', 'remote_check', 0x6e6f, 0, '2014-01-09 14:48:41', '2014-01-09 21:45:18'),
('system.email_blacklist', 'remote_dnsbls', 0x613a323a7b693a303b733a31343a22626c2e7370616d636f702e6e6574223b693a313b733a31363a227a656e2e7370616d686175732e6f7267223b7d, 1, '2014-01-09 14:48:41', '2014-01-09 16:21:11'),
('system.email_templates', 'common', 0x3c21444f43545950452068746d6c3e0a3c68746d6c3e3c686561643e3c7469746c653e4d61696c57697a7a3c2f7469746c653e3c6d65746120636f6e74656e743d227574662d3822206e616d653d2263686172736574223e0a3c7374796c6520747970653d22746578742f637373223e0a236f75746c6f6f6b20617b70616464696e673a303b7d0a09626f6479207b77696474683a313030252021696d706f7274616e743b202d7765626b69742d746578742d73697a652d61646a7573743a6e6f6e653b206d617267696e3a303b2070616464696e673a303b20666f6e742d66616d696c793a20274f70656e2053616e73272c2073616e732d73657269663b206261636b67726f756e643a20236635663566353b20666f6e742d73697a653a313270783b7d0a09696d67207b626f726465723a303b6865696768743a6175746f3b6c696e652d6865696768743a313030253b6f75746c696e653a6e6f6e653b746578742d6465636f726174696f6e3a6e6f6e653b7d0a097461626c652074647b626f726465722d636f6c6c617073653a636f6c6c617073653b7d0a0961207b636f6c6f723a20233336376661393b746578742d6465636f726174696f6e3a6e6f6e657d0a09613a686f766572207b636f6c6f723a20233336376661393b746578742d6465636f726174696f6e3a6e6f6e653b7d0a3c2f7374796c653e0a3c2f686561643e3c626f6479206267636f6c6f723d222366356635663522206469723d22756e646566696e656422207374796c653d2277696474683a313030253b2d7765626b69742d746578742d73697a652d61646a7573743a6e6f6e653b6d617267696e3a303b70616464696e673a303b666f6e742d66616d696c793a26616d703b233033393b4f70656e2053616e7326616d703b233033393b2c73616e732d73657269663b6261636b67726f756e643a236635663566353b666f6e742d73697a653a31327078223e0a202020202020202020202020202020200a2020202020202020202020203c646976207374796c653d226261636b67726f756e643a236635663566353b70616464696e673a313070783b223e0a3c7461626c6520616c69676e3d2263656e74657222206267636f6c6f723d22234646464646462220626f726465723d2230222063656c6c70616464696e673d2230222063656c6c73706163696e673d223022207374796c653d222d7765626b69742d626f726465722d7261646975733a3570783b2d6d6f7a2d626f726465722d7261646975733a3570783b626f726465722d7261646975733a3570783b626f726465723a31707820736f6c696420233336376661393b6f766572666c6f773a68696464656e3b222077696474683d22363030223e3c74626f64793e3c74723e3c7464206267636f6c6f723d222333363766613922207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e0a0909093c7461626c6520626f726465723d2230222063656c6c70616464696e673d2230222063656c6c73706163696e673d223230222077696474683d2231303025223e3c74626f64793e3c74723e3c7464207374796c653d22666f6e742d73697a653a323570783b666f6e742d7374796c653a6974616c69633b626f726465722d636f6c6c617073653a636f6c6c617073653b223e3c666f6e7420636f6c6f723d2223464646464646223e3c7370616e207374796c653d22666f6e742d66616d696c793a274e6f746f2053616e73272c2073616e732d73657269663b223e3c7374726f6e673e4d61696c57697a7a3c2f7374726f6e673e3c2f7370616e3e203c7370616e207374796c653d22666f6e742d73697a653a3530253b666f6e742d66616d696c793a274f70656e2053616e73272c2073616e732d73657269663b223e456d61696c206d61726b6574696e67206170706c69636174696f6e3c2f7370616e3e3c2f666f6e743e3c2f74643e0a09090909093c2f74723e3c2f74626f64793e3c2f7461626c653e3c2f74643e0a09093c2f74723e3c74723e3c7464207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e0a0909093c7461626c6520626f726465723d2230222063656c6c70616464696e673d223230222063656c6c73706163696e673d2230222077696474683d2231303025223e3c74626f64793e3c74723e3c7464207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e266e6273703b3c2f74643e0a09090909093c2f74723e3c74723e3c7464207374796c653d22666f6e742d66616d696c793a274f70656e2053616e73272c2073616e732d73657269663b666f6e742d73697a653a313270783b626f726465722d636f6c6c617073653a636f6c6c617073653b223e5b434f4e54454e545d3c2f74643e0a09090909093c2f74723e3c74723e3c7464207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e266e6273703b3c2f74643e0a09090909093c2f74723e3c2f74626f64793e3c2f7461626c653e3c2f74643e0a09093c2f74723e3c74723e3c7464206267636f6c6f723d222333363766613922207374796c653d2270616464696e673a313070783b626f726465722d636f6c6c617073653a636f6c6c617073653b223e3c666f6e7420636f6c6f723d2223464646464646223e3c7370616e207374796c653d22666f6e742d73697a653a313070783b666f6e742d66616d696c793a274f70656e2053616e73272c2073616e732d73657269663b223e26636f70793b2032303133204d61696c57697a7a2e20416c6c207269676874732072657365727665643c2f7370616e3e3c2f666f6e743e3c2f74643e0a09093c2f74723e3c2f74626f64793e3c2f7461626c653e3c2f6469763e3c2f626f64793e3c2f68746d6c3e0a, 0, '2013-10-14 10:18:10', '2014-03-15 14:39:58'),
('system.exporter', 'enabled', 0x796573, 0, '2016-06-06 00:54:33', '2016-06-06 00:54:33'),
('system.exporter', 'memory_limit', '', 0, '2013-09-29 22:23:10', '2013-11-01 13:42:33'),
('system.exporter', 'pause', 0x31, 0, '2013-09-29 22:23:10', '2013-09-29 22:24:41'),
('system.exporter', 'process_at_once', 0x3530, 0, '2013-09-29 22:23:10', '2013-09-29 22:24:41'),
('system.exporter', 'records_per_file', 0x3530303030, 0, '2013-09-29 22:23:10', '2016-11-10 12:57:44'),
('system.extension.ckeditor', 'status', 0x656e61626c6564, 0, '2013-11-07 10:33:38', '2013-11-07 10:33:38'),
('system.extension.ckeditor', 'version', 0x312e322e35, 0, '2016-03-25 09:09:54', '2016-12-08 19:40:42'),
('system.extension.list-unsubscribe', 'status', 0x656e61626c6564, 0, '2016-12-10 08:21:21', '2016-12-10 08:21:21'),
('system.extension.list-unsubscribe', 'version', 0x312e30, 0, '2016-12-10 08:21:21', '2016-12-10 08:21:21'),
('system.extension.list-unsubscribe.data', 'email_address_format', 0x5b535542534352494245525f5549445d2e5b4c4953545f5549445d2e5b43414d504149474e5f5549445d4066626c2d756e7375622e612d6464696374696f6e2e7072657373, 0, '2016-12-10 08:21:40', '2016-12-12 09:56:59'),
('system.extension.list-unsubscribe.data', 'enabled', 0x796573, 0, '2016-12-10 08:21:40', '2016-12-10 08:21:40'),
('system.extension.tour', 'status', 0x656e61626c6564, 0, '2016-12-08 19:40:42', '2016-12-08 19:40:42'),
('system.extension.tour', 'version', 0x312e30, 0, '2016-12-08 19:40:42', '2016-12-08 19:40:42'),
('system.extension.tour.data', 'enabled', 0x796573, 0, '2016-12-08 19:40:42', '2016-12-08 19:40:42'),
('system.extension.tour.data.views.backend.2', 'viewed', 0x31, 0, '2016-12-09 12:10:13', '2016-12-09 12:10:13'),
('system.extension.tour.data.views.customer.1', 'viewed', 0x32, 0, '2016-12-11 17:34:29', '2016-12-11 17:34:29'),
('system.importer', 'check_mime_type', 0x796573, 0, '2016-06-06 00:54:32', '2016-06-06 00:54:32'),
('system.importer', 'cli_enabled', 0x6e6f, 0, '2016-06-06 00:54:32', '2016-06-06 00:54:32'),
('system.importer', 'enabled', 0x796573, 0, '2016-06-06 00:54:32', '2016-06-06 00:54:32'),
('system.importer', 'file_size_limit', 0x313034383537363030, 0, '2013-09-28 11:51:35', '2016-06-06 00:54:32'),
('system.importer', 'import_at_once', 0x313030, 0, '2013-09-28 11:51:35', '2013-11-01 13:42:33'),
('system.importer', 'memory_limit', '', 0, '2013-09-28 11:51:35', '2013-11-01 13:42:33'),
('system.importer', 'pause', 0x31, 0, '2013-09-28 11:51:35', '2013-11-01 13:42:33'),
('system.importer', 'web_enabled', 0x796573, 0, '2016-06-06 00:54:32', '2016-06-06 00:54:32'),
('system.installer', 'freshinstallextensionscheck', 0x693a313b, 1, '2016-12-08 19:40:42', '2016-12-08 19:40:42'),
('system.license', 'email', 0x6d61696c77697a7a406e756c6c6564627966697265323030302e636f6d, 0, '2016-03-09 19:24:56', '2016-03-09 19:24:56'),
('system.license', 'first_name', 0x4e554c4c4544, 0, '2016-03-09 19:24:56', '2016-03-09 19:24:56'),
('system.license', 'last_name', 0x4e554c4c4544, 0, '2016-03-09 19:24:56', '2016-03-09 19:24:56'),
('system.license', 'market_place', 0x656e7661746f, 0, '2016-03-09 19:24:56', '2016-03-09 19:24:56'),
('system.license', 'purchase_code', 0x4e554c4c4544, 0, '2016-03-09 19:24:56', '2016-03-09 19:24:56'),
('system.urls', 'api_absolute_url', 0x687474703a2f2f612d6464696374696f6e2e70726573732f6d772f6170692f696e6465782e7068702f, 0, '2016-03-13 17:18:11', '2016-12-18 08:07:13'),
('system.urls', 'backend_absolute_url', 0x687474703a2f2f612d6464696374696f6e2e70726573732f6d772f6261636b656e642f696e6465782e7068702f, 0, '2016-03-13 17:18:11', '2016-12-18 08:07:13'),
('system.urls', 'customer_absolute_url', 0x687474703a2f2f612d6464696374696f6e2e70726573732f6d772f637573746f6d65722f696e6465782e7068702f, 0, '2016-03-13 17:18:11', '2016-12-18 08:07:13'),
('system.urls', 'frontend_absolute_url', 0x687474703a2f2f612d6464696374696f6e2e70726573732f6d772f696e6465782e7068702f, 0, '2016-03-13 17:18:11', '2016-12-18 08:07:13'),
('system.urls', 'hash', 0x37303664356334643962306237386566313863613333373133303036663730376431346465383565, 0, '2016-03-13 17:18:11', '2016-03-27 18:53:51'),
('system.urls', 'scheme', 0x68747470, 0, '2016-12-18 08:07:13', '2016-12-18 08:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `mw_price_plan`
--

CREATE TABLE IF NOT EXISTS `mw_price_plan` (
`plan_id` int(11) NOT NULL,
  `plan_uid` char(13) NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `description` text NOT NULL,
  `recommended` enum('yes','no') NOT NULL DEFAULT 'no',
  `visible` enum('yes','no') NOT NULL DEFAULT 'yes',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_price_plan_order`
--

CREATE TABLE IF NOT EXISTS `mw_price_plan_order` (
`order_id` int(11) NOT NULL,
  `order_uid` char(13) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `promo_code_id` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `currency_id` int(11) NOT NULL,
  `subtotal` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_percent` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax_value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `discount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `status` char(15) NOT NULL DEFAULT 'incomplete',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_price_plan_order_note`
--

CREATE TABLE IF NOT EXISTS `mw_price_plan_order_note` (
`note_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `note` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_price_plan_order_transaction`
--

CREATE TABLE IF NOT EXISTS `mw_price_plan_order_transaction` (
`transaction_id` int(11) NOT NULL,
  `transaction_uid` char(13) NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_gateway_name` varchar(50) NOT NULL,
  `payment_gateway_transaction_id` varchar(100) NOT NULL,
  `payment_gateway_response` text NOT NULL,
  `status` char(15) NOT NULL DEFAULT 'failed',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_price_plan_promo_code`
--

CREATE TABLE IF NOT EXISTS `mw_price_plan_promo_code` (
`promo_code_id` int(11) NOT NULL,
  `code` char(15) NOT NULL,
  `type` enum('percentage','fixed amount') NOT NULL DEFAULT 'fixed amount',
  `discount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total_amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total_usage` tinyint(4) NOT NULL DEFAULT '0',
  `customer_usage` tinyint(4) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_sending_domain`
--

CREATE TABLE IF NOT EXISTS `mw_sending_domain` (
`domain_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `dkim_private_key` text NOT NULL,
  `dkim_public_key` text NOT NULL,
  `locked` enum('yes','no') NOT NULL DEFAULT 'no',
  `verified` enum('yes','no') NOT NULL DEFAULT 'no',
  `signing_enabled` enum('yes','no') NOT NULL DEFAULT 'yes',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_session`
--

CREATE TABLE IF NOT EXISTS `mw_session` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mw_session`
--

INSERT INTO `mw_session` (`id`, `expire`, `data`) VALUES
('bf2ripaq8s3r6rfg2i6sg76e14', 1482414245, 0x66313134303263363136386239313933313032306364353939666462346238655f5f69647c693a323b66313134303263363136386239313933313032306364353939666462346238655f5f6e616d657c733a32323a2261646d696e40612d6464696374696f6e2e7072657373223b66313134303263363136386239313933313032306364353939666462346238655f5f757365725f6175746f5f6c6f67696e5f746f6b656e7c733a34303a2231626236623430346134316361356264646265396264333366326135353735346635326430653061223b66313134303263363136386239313933313032306364353939666462346238655f5f7374617465737c613a313a7b733a32333a225f5f757365725f6175746f5f6c6f67696e5f746f6b656e223b623a313b7d66313134303263363136386239313933313032306364353939666462346238655f5f74696d656f75747c693a313438323431343234353b66666564646265636663353465313766323462663833363261613061336263365f5f72657475726e55726c7c733a33383a222f6d772f637573746f6d65722f696e6465782e7068702f63616d706169676e732f696e646578223b66666564646265636663353465313766323462663833363261613061336263365f5f69647c693a313b66666564646265636663353465313766323462663833363261613061336263365f5f6e616d657c733a32323a2261646d696e40612d6464696374696f6e2e7072657373223b66666564646265636663353465313766323462663833363261613061336263365f5f637573746f6d65725f6175746f5f6c6f67696e5f746f6b656e7c733a34303a2235373035306465306464376264363063323531656265633732393365383130303231393630353562223b66666564646265636663353465313766323462663833363261613061336263365f5f7374617465737c613a313a7b733a32373a225f5f637573746f6d65725f6175746f5f6c6f67696e5f746f6b656e223b623a313b7d66666564646265636663353465313766323462663833363261613061336263365f5f74696d656f75747c693a313438323431343234353b706167655f73697a657c693a36303b66313134303263363136386239313933313032306364353939666462346238655f5f72657475726e55726c7c613a313a7b693a303b733a31353a2264617368626f6172642f696e646578223b7d);

-- --------------------------------------------------------

--
-- Table structure for table `mw_tag_registry`
--

CREATE TABLE IF NOT EXISTS `mw_tag_registry` (
`tag_id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `mw_tag_registry`
--

INSERT INTO `mw_tag_registry` (`tag_id`, `tag`, `description`, `date_added`, `last_updated`) VALUES
(45, '[LIST_NAME]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(46, '[LIST_FIELDS]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(47, '[UNSUBSCRIBE_EMAIL_FIELD]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(48, '[UPDATE_PROFILE_URL]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(49, '[CURRENT_YEAR]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(50, '[SUBSCRIBE_URL]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(51, '[CHARSET]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(52, '[UNSUBSCRIBE_URL]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(53, '[SUBMIT_BUTTON]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(54, '[COMPANY_NAME]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(55, '[COMPANY_COUNTRY]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(56, '[COMPANY_ZONE]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(57, '[COMPANY_CITY]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(58, '[COMPANY_ADDRESS_1]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(59, '[COMPANY_PHONE]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(60, '[COMPANY_ADDRESS_2]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(61, '[CURRENT_MONTH]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(62, '[CURRENT_DAY]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(63, '[CURRENT_DATE]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(64, '[WEB_VERSION_URL]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(65, '[LIST_DESCRIPTION]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(66, '[LIST_FROM_NAME]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(67, '[CAMPAIGN_SUBJECT]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(68, '[CAMPAIGN_FROM_NAME]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(69, '[CAMPAIGN_REPLY_TO]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(70, '[COMPANY_FULL_ADDRESS]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(71, '[COMPANY_ZIP]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(72, '[CAMPAIGN_TO_NAME]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(73, '[LIST_SUBJECT]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(74, '[CAMPAIGN_URL]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(75, '[CAMPAIGN_UID]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(76, '[SUBSCRIBER_UID]', NULL, '2013-10-25 03:08:28', '2013-10-25 03:08:28'),
(77, '[XML_FEED_BEGIN]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(78, '[XML_FEED_ITEM_LINK]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(79, '[XML_FEED_ITEM_IMAGE]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(80, '[XML_FEED_ITEM_TITLE]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(81, '[XML_FEED_ITEM_DESCRIPTION]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(82, '[XML_FEED_END]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(83, '[XML_FEED_ITEM_PUBDATE]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(84, '[XML_FEED_ITEM_GUID]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(85, '[JSON_FEED_BEGIN]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(86, '[JSON_FEED_ITEM_LINK]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(87, '[JSON_FEED_ITEM_IMAGE]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(88, '[JSON_FEED_ITEM_TITLE]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(89, '[JSON_FEED_ITEM_DESCRIPTION]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(90, '[JSON_FEED_END]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(91, '[JSON_FEED_ITEM_PUBDATE]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(92, '[JSON_FEED_ITEM_GUID]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(93, '[XML_FEED_ITEM_CONTENT]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(94, '[JSON_FEED_ITEM_CONTENT]', NULL, '2013-12-09 00:00:00', '2013-12-09 00:00:00'),
(95, '[CAMPAIGN_FROM_EMAIL]', NULL, '2014-02-02 00:00:00', '2014-02-02 00:00:00'),
(96, '[LIST_FROM_EMAIL]', NULL, '2014-02-02 00:00:00', '2014-02-02 00:00:00'),
(97, '[SUBSCRIBER_DATE_ADDED]', NULL, '2014-06-23 00:00:00', '2014-06-23 00:00:00'),
(98, '[SUBSCRIBER_DATE_ADDED_LOCALIZED]', NULL, '2014-06-23 00:00:00', '2014-06-23 00:00:00'),
(99, '[DATE]', NULL, '2014-06-23 00:00:00', '2014-06-23 00:00:00'),
(100, '[DATETIME]', NULL, '2014-06-23 00:00:00', '2014-06-23 00:00:00'),
(101, '[FORWARD_FRIEND_URL]', NULL, '2014-08-31 00:00:00', '2014-08-31 00:00:00'),
(102, '[CAMPAIGN_NAME]', NULL, '2014-08-31 00:00:00', '2014-08-31 00:00:00'),
(103, '[DIRECT_UNSUBSCRIBE_URL]', NULL, '2014-08-31 00:00:00', '2014-08-31 00:00:00'),
(104, '[RANDOM_CONTENT]', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00'),
(105, '[CAMPAIGN_REPORT_ABUSE_URL]', NULL, '2014-11-18 00:00:00', '2014-11-18 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mw_tax`
--

CREATE TABLE IF NOT EXISTS `mw_tax` (
`tax_id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `percent` decimal(4,2) NOT NULL DEFAULT '0.00',
  `is_global` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` char(15) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_tour_slideshow`
--

CREATE TABLE IF NOT EXISTS `mw_tour_slideshow` (
`slideshow_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `application` varchar(45) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mw_tour_slideshow`
--

INSERT INTO `mw_tour_slideshow` (`slideshow_id`, `name`, `application`, `status`, `date_added`, `last_updated`) VALUES
(1, 'Backend', 'backend', 'active', '2016-05-27 06:53:43', '2016-05-27 07:43:18'),
(2, 'Customer', 'customer', 'active', '2016-05-26 09:51:36', '2016-05-26 09:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `mw_tour_slideshow_slide`
--

CREATE TABLE IF NOT EXISTS `mw_tour_slideshow_slide` (
`slide_id` int(11) NOT NULL,
  `slideshow_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `mw_tour_slideshow_slide`
--

INSERT INTO `mw_tour_slideshow_slide` (`slide_id`, `slideshow_id`, `title`, `content`, `image`, `sort_order`, `status`, `date_added`, `last_updated`) VALUES
(1, 1, 'Thank you for choosing [APP_NAME]!', 'Hello [FULL_NAME], thank you for choosing [APP_NAME]!<br />\nThis small tour will guide you through some of the [APP_NAME]  features and will help you get started using the application.<br />\nIf you ever need support, please use our dedicated <a href="[SUPPORT_URL]" target="_blank">support channel</a>.<br />\nLet''s get started!', '', 0, 'active', '2016-05-27 06:55:02', '2016-05-27 06:55:02'),
(2, 1, 'Understanding the application structure', 'In order to make things easier to manage, [APP_NAME] is divided into several small sub-apps, like the <a href="[BACKEND_URL]" target="_blank">backend</a>, <a href="[CUSTOMER_URL]" target="_blank">customer</a>, <a href="[API_URL]" target="_blank">api</a>, console and <a href="[FRONTEND_URL]" target="_blank">frontend</a> app.<br /><br />\nThe <a href="[BACKEND_URL]" target="_blank">backend</a> app is used for administrative tasks and here only the system users have access.<br />\nThe <a href="[CUSTOMER_URL]" target="_blank">customer</a> app is used to create manage emails lists, subscribers and campaigns.<br />\nThe console app is the heavy worker, it contains all the cron jobs commands that are executed on various intervals to run maintenance, send campaigns, process bounces and so on.<br />\nThe <a href="[FRONTEND_URL]" target="_blank">frontend</a> app is used for public facing actions, like showing the subscription forms.<br />\nThe <a href="[API_URL]" target="_blank">api</a> app is used to allow custom integrations from various other apps with your own app, like customers sending subscribers from external systems to their lists. You can <a href="[BACKEND_URL]/settings/index" target="_blank">disable</a> it any any time.', '', 1, 'active', '2016-05-27 07:08:23', '2016-05-27 07:38:16'),
(3, 1, 'Cron jobs setup', 'Maybe the most important thing that you have to do after you install the application is to make sure all the cron jobs are set properly. This is very important since without the cron jobs, the application will not be able to send any email at all, or to do maintenance cleanup and a lot other tasks. <br /><br />\nYou can find a list with all the cron jobs that you have to add <a href="[BACKEND_URL]/misc/cron-jobs-list" target="_blank">here</a> and if you have any issue setting them up, you can reach out for help <a href="https://forum.mailwizz.com/threads/what-are-the-cron-jobs-that-i-have-to-add.12/" target="_blank">here</a>.  If you are not sure what cron jobs are, we have you covered, read more about them <a href="https://forum.mailwizz.com/threads/what-cron-jobs-are-why-do-i-need-to-add-them-and-how.3/" target="_blank">here</a>.', '', 2, 'active', '2016-05-27 07:22:47', '2016-05-27 07:40:02'),
(4, 1, 'Your customer account', 'When you first installed the application, you were asked to create a customer account.<br />\nIn case you haven''t done so, please go ahead and <a href="[BACKEND_URL]/customers/create" target="_blank">create one</a>.<br />\nWhen using [APP_NAME], the customers are the ones that manage the email lists, campaigns, templates and so on.', '', 3, 'active', '2016-05-27 07:25:02', '2016-05-27 07:40:17'),
(5, 1, 'Delivery servers', '<a href="[BACKEND_URL]/delivery-servers" target="_blank">Delivery servers</a> are needed in order to send out all the emails from the application.<br />\nEven if it''s about sending a confirmation email, an email campaign, a test email,<br />\nyou need a delivery server to actually make the delivery.<br /><br />\n[APP_NAME] comes with support for any SMTP server out there, PHP''s built-in mail function and sendmail. Also it integrates with services like Sparkpost, MailGun, Amazon SES, SendGrid, ElasticEmail and much more in order to make sure you are not limited into using just a single service but you have a wide array of options.<br />\nYou can use either of them, or all of them together.<br />\n[APP_NAME] gives you total flexibility for this.', '', 4, 'active', '2016-05-27 07:28:05', '2016-05-27 11:28:32'),
(6, 1, 'Bounce servers', 'You like it or not, when you start sending emails, not all your addresses will be valid and when sending emails to invalid email addresses, these emails will return to you in order to notify you that the given email address is not valid anymore. In order for [APP_NAME] to be able to catch these emails and take proper actions against them, you need to use a feature called <a href="[BACKEND_URL]/bounce-servers" target="_blank">bounce servers</a>.<br /><br />\nBounce servers are actually regular email boxes that are used by [APP_NAME] in order to catch the returning emails and take proper actions. After creating a bounce server, make sure you associate it with a delivery server, otherwise bounce processing will not work.<br /><br />\nPlease note that <a href="[BACKEND_URL]/delivery-servers" target="_blank">delivery servers </a>of type web api do not need bounce servers. The bounces are processed automatically through webhooks.', '', 5, 'active', '2016-05-27 07:28:58', '2016-05-27 11:29:12'),
(7, 1, 'Feedback loop servers', 'Everytime when the Spam button is pressed by one of your subscribers, the email provider the subscriber belongs to, will send a notification related to that event.  In order to receive such notifications, you should subscribe for such notifications at the email provider.<br /><a href="https://www.port25.com/list-of-current-feedback-loops-offered-at-isps/" target="_blank">Here</a> you can find a list with email providers and their feedback loop subscribe process. <br /><br />\nHowever, most email providers will also send the notifications at one of these 3 addresses: abuse@domain.com, postmaster@domain.com and fbl@domain.com. <br />\nIn order for [APP_NAME] to process these notifications, you need to add <a href="[BACKEND_URL]/feedback-loop-servers" target="_blank">feedback loop </a>servers for each email address, either for the one you subscribed at the email provider, or for the 3 listed above.<br /><br />\nPlease note that when sending only via <a href="[BACKEND_URL]/delivery-servers" target="_blank">delivery servers</a> of type web api you don''t need to setup <a href="[BACKEND_URL]/feedback-loop-servers" target="_blank">feedback loop servers</a> since these are automatically processed via webhooks.', '', 6, 'active', '2016-05-27 07:29:49', '2016-05-27 11:31:17'),
(8, 1, 'Delivery settings', 'By default, [APP_NAME] uses <a href="[BACKEND_URL]/settings/cron" target="_blank">delivery settings</a> that will be fine for most of people. However, if you need, you can properly adjust them in order to get a higher delivery speed. Please be aware that wrong delivery settings can cause a lot of issues, including the fact that some ISP''s will blacklist your sending ip addresses if you are sending too fast.<br /><br />\nYou can read more about how delivery settings work <a href="https://forum.mailwizz.com/threads/understanding-email-delivery-settings-and-how-they-impact-performance.1595/" target="_blank">here</a>.', '', 7, 'active', '2016-05-27 07:30:49', '2016-05-27 07:41:15'),
(9, 1, 'Monetization', 'If you decide to use [APP_NAME] to provide email services to your own customers, then you have to <a href="[BACKEND_URL]/settings/monetization" target="_blank">enable the monetization module</a>. Once the module is enabled, you will be able to create <a href="[BACKEND_URL]/customers/groups/index" target="_blank">customer groups</a>, assign them to <a href="[BACKEND_URL]/price-plans/index" target="_blank">price plans</a>, <a href="[BACKEND_URL]/payment-gateways/index" target="_blank">enable and setup payment extensions</a>, define <a href="[BACKEND_URL]/currencies/index" target="_blank">currencies</a>, <a href="[BACKEND_URL]/promo-codes/index" target="_blank">promo codes</a> and <a href="[BACKEND_URL]/taxes/index" target="_blank">taxes</a>.<br /><br />\nPlease note that when you create customer groups, the default settings will be inherit from <a href="[BACKEND_URL]/settings/customers/common" target="_blank">customer settings</a>. This allows you to set some base settings for all customers, then in customer groups you can simply adjust them depending on the group permissions.', '', 8, 'active', '2016-05-27 07:32:04', '2016-05-27 07:41:26'),
(10, 1, 'Extend [APP_NAME]', '[APP_NAME] is highly <a href="https://forum.mailwizz.com/categories/extend.15/" target="_blank">extensible</a> and flexible. You can extend it using a <a href="http://codecanyon.net/user/twisted1919/portfolio?ref=twisted1919" target="_blank">high number of extensions</a> for payments, backup, email validation and so on, you can alter the look of it by either using <a href="[BACKEND_URL]/settings/customization" target="_blank">customizations</a> or <a href="https://forum.mailwizz.com/forums/themes/" target="_blank">custom themes</a> and you can <a href="https://forum.mailwizz.com/forums/translations/" target="_blank">translate it</a> in your language.', '', 9, 'active', '2016-05-27 07:33:24', '2016-05-27 07:41:36'),
(11, 1, 'Logs', 'While [APP_NAME] is running, it saves a lot of logs in order for you to see what is happening under the hood, which means in case something goes wrong, these logs are also extremely useful for debugging purposes.<br /><br />You can view the <a href="[BACKEND_URL]/misc/campaigns-delivery-logs" target="_blank">campaigns delivery logs</a> which record every emails sent out by any campaign, the <a href="[BACKEND_URL]/misc/campaigns-bounce-logs" target="_blank">campaigns bounce logs</a> which stores data related to bounces, <br />the <a href="[BACKEND_URL]/campaign-abuse-reports" target="_blank">campaigns abuse reports</a> that store information when a subscriber fills in an abuse form,<br />the <a href="[BACKEND_URL]/transactional-emails/index" target="_blank">transactional emails logs</a> which hold information about the transactional emails,<br /><a href="[BACKEND_URL]/misc/delivery-servers-usage-logs" target="_blank">delivery servers usage logs</a> which record absolutely all the emails sent out by the <a href="[BACKEND_URL]/delivery-servers/index" target="_blank">delivery servers</a>,<br />the <a href="[BACKEND_URL]/misc/application-log" target="_blank">application log</a> which stores various events that happen when the application runs,<br />and also a detailed history with all <a href="[BACKEND_URL]/misc/guest-fail-attempts" target="_blank">failed login attempts</a>.', '', 10, 'active', '2016-05-27 07:33:24', '2016-05-27 07:41:36'),
(12, 2, '[FULL_NAME], welcome!', 'Using [APP_NAME], you will be able to create <a href="[CUSTOMER_URL]/lists/index" target="_blank">email lists</a>, customize them entirely, add subscribers to them, <a href="[CUSTOMER_URL]/campaigns/index" target="_blank">create and send campaigns</a> and enjoy a very detailed stats area for each of your campaigns.<br />\nLet''s get started!', '', 0, 'active', '2016-05-26 09:56:50', '2016-05-27 11:32:13'),
(13, 2, 'Email lists', 'In order to start sending email campaign, you should start by creating your <a target="_blank" href="[CUSTOMER_URL]/lists/index">first email list</a>. Your email list identifies you as a sender, it contains your information and allows you to take various decisions related to your subscribers.<br />\nEach list is totally separate, which means you can customize each list as you wish, including subscribers, custom forms/pages and custom fields.', '', 1, 'active', '2016-05-26 10:02:30', '2016-05-27 07:51:11'),
(14, 2, 'Subscribers', 'Once your email list is created, next step is to start importing your subscribers into the list.<br />\nIn order to do so, just click on your list title from <a href="[CUSTOMER_URL]/lists/index" target="_blank">lists area</a>,<br />\nthen click on the Tools box, followed by the Import box.<br />\nYou will be allowed to import from a csv file, a text file or from another database.<br />\nIt is that simple!', '', 2, 'active', '2016-05-26 10:04:21', '2016-05-27 11:32:55'),
(15, 2, 'Campaigns', 'Finally, after your list is created and you have added your subscribers into it,<br />\nit''s time to create <a href="[CUSTOMER_URL]/campaigns/create" target="_blank">your first campaign</a>.<br />\nYou will be able to select the <a href="[CUSTOMER_URL]/lists/index" target="_blank">list</a> you want to use for sending and optionally, a segment from the list.<br />\nYou can add a new template just for the campaign itself or use an <a href="[CUSTOMER_URL]/templates/index" target="_blank">existing one</a>.<br />\nMake sure you test all campaigns before actually sending them, this is crucial. If you want to test your campaign spam score, feel free to use this <a href="http://www.mail-tester.com/" target="_blank">tool</a>.<br />\nOnce you see everything looks good, send your campaign and enjoy very detailed, real time statistics.', '', 3, 'active', '2016-05-26 10:09:59', '2016-05-27 11:33:22'),
(16, 2, 'Email templates', 'When creating new campaigns, you will want to use nice looking email templates. <br />\nYou can use the <a href="[CUSTOMER_URL]/templates/index" target="_blank">templates</a> area to create and/or import your existing templates. If you don''t have any email template to import or you are not sure how to create one, you can also import one from the <a href="[CUSTOMER_URL]/templates/gallery" target="_blank">templates gallery</a>. You can later use any of these templates for your <a href="[CUSTOMER_URL]/campaigns/index" target="_blank">campaigns</a> and you can even create new templates from the campaigns area for any specific campaign.', '', 4, 'active', '2016-05-26 10:13:20', '2016-05-27 07:52:22');

-- --------------------------------------------------------

--
-- Table structure for table `mw_tracking_domain`
--

CREATE TABLE IF NOT EXISTS `mw_tracking_domain` (
`domain_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_transactional_email`
--

CREATE TABLE IF NOT EXISTS `mw_transactional_email` (
`email_id` bigint(20) NOT NULL,
  `email_uid` char(13) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `to_email` varchar(150) NOT NULL,
  `to_name` varchar(150) NOT NULL,
  `from_email` varchar(150) NOT NULL,
  `from_name` varchar(150) NOT NULL,
  `reply_to_email` varchar(150) DEFAULT NULL,
  `reply_to_name` varchar(150) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `body` longblob NOT NULL,
  `plain_text` longblob NOT NULL,
  `priority` tinyint(1) NOT NULL DEFAULT '5',
  `retries` tinyint(1) NOT NULL DEFAULT '0',
  `max_retries` tinyint(1) NOT NULL DEFAULT '3',
  `send_at` datetime NOT NULL,
  `status` char(15) NOT NULL DEFAULT 'unsent',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_transactional_email`
--

INSERT INTO `mw_transactional_email` (`email_id`, `email_uid`, `customer_id`, `to_email`, `to_name`, `from_email`, `from_name`, `reply_to_email`, `reply_to_name`, `subject`, `body`, `plain_text`, `priority`, `retries`, `max_retries`, `send_at`, `status`, `date_added`, `last_updated`) VALUES
(1, 'my629ktnto8e3', NULL, 'cgjackson83@outlook.com', 'Christopher Johnson', '', 'MailWizz', NULL, NULL, 'Password reset request!', 0x3c21444f43545950452068746d6c3e0a3c68746d6c3e3c686561643e3c7469746c653e4d61696c57697a7a3c2f7469746c653e3c6d65746120636f6e74656e743d227574662d3822206e616d653d2263686172736574223e0a3c7374796c6520747970653d22746578742f637373223e0a236f75746c6f6f6b20617b70616464696e673a303b7d0a09626f6479207b77696474683a313030252021696d706f7274616e743b202d7765626b69742d746578742d73697a652d61646a7573743a6e6f6e653b206d617267696e3a303b2070616464696e673a303b20666f6e742d66616d696c793a20274f70656e2053616e73272c2073616e732d73657269663b206261636b67726f756e643a20236635663566353b20666f6e742d73697a653a313270783b7d0a09696d67207b626f726465723a303b6865696768743a6175746f3b6c696e652d6865696768743a313030253b6f75746c696e653a6e6f6e653b746578742d6465636f726174696f6e3a6e6f6e653b7d0a097461626c652074647b626f726465722d636f6c6c617073653a636f6c6c617073653b7d0a0961207b636f6c6f723a20233336376661393b746578742d6465636f726174696f6e3a6e6f6e657d0a09613a686f766572207b636f6c6f723a20233336376661393b746578742d6465636f726174696f6e3a6e6f6e653b7d0a3c2f7374796c653e0a3c2f686561643e3c626f6479206267636f6c6f723d222366356635663522206469723d22756e646566696e656422207374796c653d2277696474683a313030253b2d7765626b69742d746578742d73697a652d61646a7573743a6e6f6e653b6d617267696e3a303b70616464696e673a303b666f6e742d66616d696c793a26616d703b233033393b4f70656e2053616e7326616d703b233033393b2c73616e732d73657269663b6261636b67726f756e643a236635663566353b666f6e742d73697a653a31327078223e0a202020202020202020202020202020200a2020202020202020202020203c646976207374796c653d226261636b67726f756e643a236635663566353b70616464696e673a313070783b223e0a3c7461626c6520616c69676e3d2263656e74657222206267636f6c6f723d22234646464646462220626f726465723d2230222063656c6c70616464696e673d2230222063656c6c73706163696e673d223022207374796c653d222d7765626b69742d626f726465722d7261646975733a3570783b2d6d6f7a2d626f726465722d7261646975733a3570783b626f726465722d7261646975733a3570783b626f726465723a31707820736f6c696420233336376661393b6f766572666c6f773a68696464656e3b222077696474683d22363030223e3c74626f64793e3c74723e3c7464206267636f6c6f723d222333363766613922207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e0a0909093c7461626c6520626f726465723d2230222063656c6c70616464696e673d2230222063656c6c73706163696e673d223230222077696474683d2231303025223e3c74626f64793e3c74723e3c7464207374796c653d22666f6e742d73697a653a323570783b666f6e742d7374796c653a6974616c69633b626f726465722d636f6c6c617073653a636f6c6c617073653b223e3c666f6e7420636f6c6f723d2223464646464646223e3c7370616e207374796c653d22666f6e742d66616d696c793a274e6f746f2053616e73272c2073616e732d73657269663b223e3c7374726f6e673e4d61696c57697a7a3c2f7374726f6e673e3c2f7370616e3e203c7370616e207374796c653d22666f6e742d73697a653a3530253b666f6e742d66616d696c793a274f70656e2053616e73272c2073616e732d73657269663b223e456d61696c206d61726b6574696e67206170706c69636174696f6e3c2f7370616e3e3c2f666f6e743e3c2f74643e0a09090909093c2f74723e3c2f74626f64793e3c2f7461626c653e3c2f74643e0a09093c2f74723e3c74723e3c7464207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e0a0909093c7461626c6520626f726465723d2230222063656c6c70616464696e673d223230222063656c6c73706163696e673d2230222077696474683d2231303025223e3c74626f64793e3c74723e3c7464207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e266e6273703b3c2f74643e0a09090909093c2f74723e3c74723e3c7464207374796c653d22666f6e742d66616d696c793a274f70656e2053616e73272c2073616e732d73657269663b666f6e742d73697a653a313270783b626f726465722d636f6c6c617073653a636f6c6c617073653b223e0d0a3c212d2d20535441525420434f4e54454e54202d2d3e0d0a506c6561736520666f6c6c6f772074686520666f6c6c6f77696e672075726c20696e206f7264657220746f20726573657420796f75722070617373776f72643a3c6272202f3e0d0a3c6120687265663d22687474703a2f2f6333666f6f6c70726f6f666c6973742e636f6d2f6d61696c77697a7a2f6261636b656e642f696e6465782e7068702f67756573742f72657365742d70617373776f72642f34316536306164326165356131336165396130663064316362383736336632613361316337666139223e687474703a2f2f6333666f6f6c70726f6f666c6973742e636f6d2f6d61696c77697a7a2f6261636b656e642f696e6465782e7068702f67756573742f72657365742d70617373776f72642f343165363061643261653561313361653961306630643163623837363366326133613163376661393c2f613e3c6272202f3e3c6272202f3e0d0a496620666f7220736f6d6520726561736f6e20796f752063616e6e6f7420636c69636b207468652061626f76652075726c2c20706c656173652070617374652074686973206f6e6520696e746f20796f75722062726f777365722061646472657373206261723a3c6272202f3e0d0a687474703a2f2f6333666f6f6c70726f6f666c6973742e636f6d2f6d61696c77697a7a2f6261636b656e642f696e6465782e7068702f67756573742f72657365742d70617373776f72642f343165363061643261653561313361653961306630643163623837363366326133613163376661393c212d2d20454e4420434f4e54454e542d2d3e3c2f74643e0a09090909093c2f74723e3c74723e3c7464207374796c653d22626f726465722d636f6c6c617073653a636f6c6c617073653b223e266e6273703b3c2f74643e0a09090909093c2f74723e3c2f74626f64793e3c2f7461626c653e3c2f74643e0a09093c2f74723e3c74723e3c7464206267636f6c6f723d222333363766613922207374796c653d2270616464696e673a313070783b626f726465722d636f6c6c617073653a636f6c6c617073653b223e3c666f6e7420636f6c6f723d2223464646464646223e3c7370616e207374796c653d22666f6e742d73697a653a313070783b666f6e742d66616d696c793a274f70656e2053616e73272c2073616e732d73657269663b223e26636f70793b2032303133204d61696c57697a7a2e20416c6c207269676874732072657365727665643c2f7370616e3e3c2f666f6e743e3c2f74643e0a09093c2f74723e3c2f74626f64793e3c2f7461626c653e3c2f6469763e3c2f626f64793e3c2f68746d6c3e0a, 0x200a0a09094d41494c57495a5a20456d61696c206d61726b6574696e67206170706c69636174696f6e0a0a20506c6561736520666f6c6c6f772074686520666f6c6c6f77696e672075726c20696e206f7264657220746f20726573657420796f75722070617373776f72643a0a20687474703a2f2f6333666f6f6c70726f6f666c6973742e636f6d2f6d61696c77697a7a2f6261636b656e642f696e6465782e7068702f67756573742f72657365742d70617373776f72642f343165363061643261653561313361653961306630643163623837363366326133613163376661390a687474703a2f2f6333666f6f6c70726f6f666c6973742e636f6d2f6d61696c77697a7a2f6261636b656e642f696e6465782e7068702f67756573742f72657365742d70617373776f72642f343165363061643261653561313361653961306630643163623837363366326133613163376661390a0a20496620666f7220736f6d6520726561736f6e20796f752063616e6e6f7420636c69636b207468652061626f76652075726c2c20706c6561736520706173746520746869730a6f6e6520696e746f20796f75722062726f777365722061646472657373206261723a0a20687474703a2f2f6333666f6f6c70726f6f666c6973742e636f6d2f6d61696c77697a7a2f6261636b656e642f696e6465782e7068702f67756573742f72657365742d70617373776f72642f343165363061643261653561313361653961306630643163623837363366326133613163376661390a0a0a09092863292032303133204d61696c57697a7a2e20416c6c207269676874732072657365727665640a0a, 5, 0, 3, '2016-03-17 20:21:47', 'sent', '2016-03-17 20:21:47', '2016-03-17 20:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `mw_transactional_email_log`
--

CREATE TABLE IF NOT EXISTS `mw_transactional_email_log` (
`log_id` bigint(20) NOT NULL,
  `email_id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_transactional_email_log`
--

INSERT INTO `mw_transactional_email_log` (`log_id`, `email_id`, `message`, `date_added`) VALUES
(1, 1, 'OK', '2016-03-17 20:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `mw_user`
--

CREATE TABLE IF NOT EXISTS `mw_user` (
`user_id` int(11) NOT NULL,
  `user_uid` char(13) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(34) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `removable` enum('yes','no') NOT NULL DEFAULT 'yes',
  `status` char(15) NOT NULL DEFAULT 'inactive',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mw_user`
--

INSERT INTO `mw_user` (`user_id`, `user_uid`, `group_id`, `language_id`, `first_name`, `last_name`, `email`, `password`, `timezone`, `avatar`, `removable`, `status`, `date_added`, `last_updated`) VALUES
(2, 'dh114maoor3a6', NULL, NULL, 'Miki', 'Miles', 'admin@a-ddiction.press', '$P$GgkE0BEj5UHVr9HregwJ3Are.rjiLc0', 'Pacific/Midway', NULL, 'no', 'active', '2016-03-09 19:26:55', '2016-06-06 00:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `mw_user_auto_login_token`
--

CREATE TABLE IF NOT EXISTS `mw_user_auto_login_token` (
`token_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` char(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `mw_user_auto_login_token`
--

INSERT INTO `mw_user_auto_login_token` (`token_id`, `user_id`, `token`) VALUES
(26, 2, '1bb6b404a41ca5bddbe9bd33f2a55754f52d0e0a');

-- --------------------------------------------------------

--
-- Table structure for table `mw_user_group`
--

CREATE TABLE IF NOT EXISTS `mw_user_group` (
`group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_user_group_route_access`
--

CREATE TABLE IF NOT EXISTS `mw_user_group_route_access` (
`route_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL,
  `access` enum('allow','deny') NOT NULL DEFAULT 'allow',
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_user_message`
--

CREATE TABLE IF NOT EXISTS `mw_user_message` (
`message_id` int(11) NOT NULL,
  `message_uid` char(13) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text NOT NULL,
  `title_translation_params` blob,
  `message_translation_params` blob,
  `status` char(15) NOT NULL DEFAULT 'unseen',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mw_user_password_reset`
--

CREATE TABLE IF NOT EXISTS `mw_user_password_reset` (
`request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reset_key` char(40) NOT NULL,
  `ip_address` char(15) DEFAULT NULL,
  `status` char(15) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mw_user_password_reset`
--

INSERT INTO `mw_user_password_reset` (`request_id`, `user_id`, `reset_key`, `ip_address`, `status`, `date_added`, `last_updated`) VALUES
(1, 2, '41e60ad2ae5a13ae9a0f0d1cb8763f2a3a1c7fa9', '176.252.173.220', 'active', '2016-03-17 20:21:47', '2016-03-17 20:21:47');

-- --------------------------------------------------------

--
-- Table structure for table `mw_zone`
--

CREATE TABLE IF NOT EXISTS `mw_zone` (
`zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(50) NOT NULL,
  `status` char(10) NOT NULL DEFAULT 'active',
  `date_added` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3970 ;

--
-- Dumping data for table `mw_zone`
--

INSERT INTO `mw_zone` (`zone_id`, `country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(1, 1, 'Badakhshan', 'BDS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2, 1, 'Badghis', 'BDG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3, 1, 'Baghlan', 'BGL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(4, 1, 'Balkh', 'BAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(5, 1, 'Bamian', 'BAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(6, 1, 'Farah', 'FRA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(7, 1, 'Faryab', 'FYB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(8, 1, 'Ghazni', 'GHA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(9, 1, 'Ghowr', 'GHO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(10, 1, 'Helmand', 'HEL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(11, 1, 'Herat', 'HER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(12, 1, 'Jowzjan', 'JOW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(13, 1, 'Kabul', 'KAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(14, 1, 'Kandahar', 'KAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(15, 1, 'Kapisa', 'KAP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(16, 1, 'Khost', 'KHO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(17, 1, 'Konar', 'KNR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(18, 1, 'Kondoz', 'KDZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(19, 1, 'Laghman', 'LAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(20, 1, 'Lowgar', 'LOW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(21, 1, 'Nangrahar', 'NAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(22, 1, 'Nimruz', 'NIM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(23, 1, 'Nurestan', 'NUR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(24, 1, 'Oruzgan', 'ORU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(25, 1, 'Paktia', 'PIA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(26, 1, 'Paktika', 'PKA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(27, 1, 'Parwan', 'PAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(28, 1, 'Samangan', 'SAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(29, 1, 'Sar-e Pol', 'SAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(30, 1, 'Takhar', 'TAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(31, 1, 'Wardak', 'WAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(32, 1, 'Zabol', 'ZAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(33, 2, 'Berat', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(34, 2, 'Bulqize', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(35, 2, 'Delvine', 'DL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(36, 2, 'Devoll', 'DV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(37, 2, 'Diber', 'DI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(38, 2, 'Durres', 'DR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(39, 2, 'Elbasan', 'EL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(40, 2, 'Kolonje', 'ER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(41, 2, 'Fier', 'FR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(42, 2, 'Gjirokaster', 'GJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(43, 2, 'Gramsh', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(44, 2, 'Has', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(45, 2, 'Kavaje', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(46, 2, 'Kurbin', 'KB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(47, 2, 'Kucove', 'KC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(48, 2, 'Korce', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(49, 2, 'Kruje', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(50, 2, 'Kukes', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(51, 2, 'Librazhd', 'LB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(52, 2, 'Lezhe', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(53, 2, 'Lushnje', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(54, 2, 'Malesi e Madhe', 'MM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(55, 2, 'Mallakaster', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(56, 2, 'Mat', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(57, 2, 'Mirdite', 'MR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(58, 2, 'Peqin', 'PQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(59, 2, 'Permet', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(60, 2, 'Pogradec', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(61, 2, 'Puke', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(62, 2, 'Shkoder', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(63, 2, 'Skrapar', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(64, 2, 'Sarande', 'SR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(65, 2, 'Tepelene', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(66, 2, 'Tropoje', 'TP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(67, 2, 'Tirane', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(68, 2, 'Vlore', 'VL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(69, 3, 'Adrar', 'ADR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(70, 3, 'Ain Defla', 'ADE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(71, 3, 'Ain Temouchent', 'ATE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(72, 3, 'Alger', 'ALG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(73, 3, 'Annaba', 'ANN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(74, 3, 'Batna', 'BAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(75, 3, 'Bechar', 'BEC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(76, 3, 'Bejaia', 'BEJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(77, 3, 'Biskra', 'BIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(78, 3, 'Blida', 'BLI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(80, 3, 'Bouira', 'BOA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(81, 3, 'Boumerdes', 'BMD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(82, 3, 'Chlef', 'CHL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(83, 3, 'Constantine', 'CON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(84, 3, 'Djelfa', 'DJE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(85, 3, 'El Bayadh', 'EBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(86, 3, 'El Oued', 'EOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(87, 3, 'El Tarf', 'ETA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(88, 3, 'Ghardaia', 'GHA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(89, 3, 'Guelma', 'GUE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(90, 3, 'Illizi', 'ILL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(91, 3, 'Jijel', 'JIJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(92, 3, 'Khenchela', 'KHE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(93, 3, 'Laghouat', 'LAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(94, 3, 'Muaskar', 'MUA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(95, 3, 'Medea', 'MED', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(96, 3, 'Mila', 'MIL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(97, 3, 'Mostaganem', 'MOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(98, 3, 'M''Sila', 'MSI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(99, 3, 'Naama', 'NAA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(100, 3, 'Oran', 'ORA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(101, 3, 'Ouargla', 'OUA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(102, 3, 'Oum el-Bouaghi', 'OEB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(103, 3, 'Relizane', 'REL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(104, 3, 'Saida', 'SAI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(105, 3, 'Setif', 'SET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(106, 3, 'Sidi Bel Abbes', 'SBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(107, 3, 'Skikda', 'SKI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(108, 3, 'Souk Ahras', 'SAH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(109, 3, 'Tamanghasset', 'TAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(110, 3, 'Tebessa', 'TEB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(111, 3, 'Tiaret', 'TIA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(112, 3, 'Tindouf', 'TIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(113, 3, 'Tipaza', 'TIP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(114, 3, 'Tissemsilt', 'TIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(115, 3, 'Tizi Ouzou', 'TOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(116, 3, 'Tlemcen', 'TLE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(117, 4, 'Eastern', 'E', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(118, 4, 'Manu''a', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(119, 4, 'Rose Island', 'R', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(120, 4, 'Swains Island', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(121, 4, 'Western', 'W', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(122, 5, 'Andorra la Vella', 'ALV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(123, 5, 'Canillo', 'CAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(124, 5, 'Encamp', 'ENC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(125, 5, 'Escaldes-Engordany', 'ESE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(126, 5, 'La Massana', 'LMA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(127, 5, 'Ordino', 'ORD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(128, 5, 'Sant Julia de Loria', 'SJL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(129, 6, 'Bengo', 'BGO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(130, 6, 'Benguela', 'BGU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(131, 6, 'Bie', 'BIE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(132, 6, 'Cabinda', 'CAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(133, 6, 'Cuando-Cubango', 'CCU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(134, 6, 'Cuanza Norte', 'CNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(135, 6, 'Cuanza Sul', 'CUS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(136, 6, 'Cunene', 'CNN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(137, 6, 'Huambo', 'HUA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(138, 6, 'Huila', 'HUI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(139, 6, 'Luanda', 'LUA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(140, 6, 'Lunda Norte', 'LNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(141, 6, 'Lunda Sul', 'LSU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(142, 6, 'Malange', 'MAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(143, 6, 'Moxico', 'MOX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(144, 6, 'Namibe', 'NAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(145, 6, 'Uige', 'UIG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(146, 6, 'Zaire', 'ZAI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(147, 9, 'Saint George', 'ASG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(148, 9, 'Saint Miki', 'ASJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(149, 9, 'Saint Mary', 'ASM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(150, 9, 'Saint Paul', 'ASL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(151, 9, 'Saint Peter', 'ASR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(152, 9, 'Saint Philip', 'ASH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(153, 9, 'Barbuda', 'BAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(154, 9, 'Redonda', 'RED', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(156, 10, 'Buenos Aires', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(157, 10, 'Catamarca', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(158, 10, 'Chaco', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(159, 10, 'Chubut', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(160, 10, 'Cordoba', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(161, 10, 'Corrientes', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(162, 10, 'Distrito Federal', 'DF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(163, 10, 'Entre Rios', 'ER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(164, 10, 'Formosa', 'FO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(165, 10, 'Jujuy', 'JU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(166, 10, 'La Pampa', 'LP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(167, 10, 'La Rioja', 'LR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(168, 10, 'Mendoza', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(169, 10, 'Misiones', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(170, 10, 'Neuquen', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(171, 10, 'Rio Negro', 'RN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(172, 10, 'Salta', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(173, 10, 'San Juan', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(174, 10, 'San Luis', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(175, 10, 'Santa Cruz', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(176, 10, 'Santa Fe', 'SF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(177, 10, 'Santiago del Estero', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(178, 10, 'Tierra del Fuego', 'TF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(179, 10, 'Tucuman', 'TU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(180, 11, 'Aragatsotn', 'AGT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(181, 11, 'Ararat', 'ARR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(182, 11, 'Armavir', 'ARM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(183, 11, 'Geghark''unik''', 'GEG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(184, 11, 'Kotayk''', 'KOT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(185, 11, 'Lorri', 'LOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(186, 11, 'Shirak', 'SHI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(187, 11, 'Syunik''', 'SYU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(188, 11, 'Tavush', 'TAV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(189, 11, 'Vayots'' Dzor', 'VAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(190, 11, 'Yerevan', 'YER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(191, 13, 'Australian Capital Territory', 'ACT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(192, 13, 'New South Wales', 'NSW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(193, 13, 'Northern Territory', 'NT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(194, 13, 'Queensland', 'QLD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(195, 13, 'South Australia', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(196, 13, 'Tasmania', 'TAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(197, 13, 'Victoria', 'VIC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(198, 13, 'Western Australia', 'WA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(199, 14, 'Burgenland', 'BUR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(200, 14, 'KÃ¤rnten', 'KAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(203, 14, 'Salzburg', 'SAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(204, 14, 'Steiermark', 'STE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(205, 14, 'Tirol', 'TIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(206, 14, 'Vorarlberg', 'VOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(207, 14, 'Wien', 'WIE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(208, 15, 'Ali Bayramli', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(209, 15, 'Abseron', 'ABS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(210, 15, 'AgcabAdi', 'AGC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(211, 15, 'Agdam', 'AGM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(212, 15, 'Agdas', 'AGS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(213, 15, 'Agstafa', 'AGA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(214, 15, 'Agsu', 'AGU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(215, 15, 'Astara', 'AST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(216, 15, 'Baki', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(217, 15, 'BabAk', 'BAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(218, 15, 'BalakAn', 'BAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(219, 15, 'BArdA', 'BAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(220, 15, 'Beylaqan', 'BEY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(221, 15, 'Bilasuvar', 'BIL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(222, 15, 'Cabrayil', 'CAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(223, 15, 'Calilabab', 'CAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(224, 15, 'Culfa', 'CUL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(225, 15, 'Daskasan', 'DAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(226, 15, 'Davaci', 'DAV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(227, 15, 'Fuzuli', 'FUZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(228, 15, 'Ganca', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(229, 15, 'Gadabay', 'GAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(230, 15, 'Goranboy', 'GOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(231, 15, 'Goycay', 'GOY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(232, 15, 'Haciqabul', 'HAC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(233, 15, 'Imisli', 'IMI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(234, 15, 'Ismayilli', 'ISM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(235, 15, 'Kalbacar', 'KAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(236, 15, 'Kurdamir', 'KUR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(237, 15, 'Lankaran', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(238, 15, 'Lacin', 'LAC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(239, 15, 'Lankaran', 'LAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(240, 15, 'Lerik', 'LER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(241, 15, 'Masalli', 'MAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(242, 15, 'Mingacevir', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(243, 15, 'Naftalan', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(244, 15, 'Neftcala', 'NEF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(245, 15, 'Oguz', 'OGU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(246, 15, 'Ordubad', 'ORD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(247, 15, 'Qabala', 'QAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(248, 15, 'Qax', 'QAX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(249, 15, 'Qazax', 'QAZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(250, 15, 'Qobustan', 'QOB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(251, 15, 'Quba', 'QBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(252, 15, 'Qubadli', 'QBI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(253, 15, 'Qusar', 'QUS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(254, 15, 'Saki', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(255, 15, 'Saatli', 'SAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(256, 15, 'Sabirabad', 'SAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(257, 15, 'Sadarak', 'SAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(258, 15, 'Sahbuz', 'SAH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(259, 15, 'Saki', 'SAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(260, 15, 'Salyan', 'SAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(261, 15, 'Sumqayit', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(262, 15, 'Samaxi', 'SMI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(263, 15, 'Samkir', 'SKR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(264, 15, 'Samux', 'SMX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(265, 15, 'Sarur', 'SAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(266, 15, 'Siyazan', 'SIY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(267, 15, 'Susa', 'SS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(268, 15, 'Susa', 'SUS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(269, 15, 'Tartar', 'TAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(270, 15, 'Tovuz', 'TOV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(271, 15, 'Ucar', 'UCA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(272, 15, 'Xankandi', 'XA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(273, 15, 'Xacmaz', 'XAC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(274, 15, 'Xanlar', 'XAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(275, 15, 'Xizi', 'XIZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(276, 15, 'Xocali', 'XCI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(277, 15, 'Xocavand', 'XVD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(278, 15, 'Yardimli', 'YAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(279, 15, 'Yevlax', 'YEV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(280, 15, 'Zangilan', 'ZAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(281, 15, 'Zaqatala', 'ZAQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(282, 15, 'Zardab', 'ZAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(283, 15, 'Naxcivan', 'NX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(284, 16, 'Acklins', 'ACK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(285, 16, 'Berry Islands', 'BER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(286, 16, 'Bimini', 'BIM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(287, 16, 'Black Point', 'BLK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(288, 16, 'Cat Island', 'CAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(289, 16, 'Central Abaco', 'CAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(290, 16, 'Central Andros', 'CAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(291, 16, 'Central Eleuthera', 'CEL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(292, 16, 'City of Freeport', 'FRE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(293, 16, 'Crooked Island', 'CRO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(294, 16, 'East Grand Bahama', 'EGB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(295, 16, 'Exuma', 'EXU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(296, 16, 'Grand Cay', 'GRD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(297, 16, 'Harbour Island', 'HAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(298, 16, 'Hope Town', 'HOP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(299, 16, 'Inagua', 'INA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(300, 16, 'Long Island', 'LNG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(301, 16, 'Mangrove Cay', 'MAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(302, 16, 'Mayaguana', 'MAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(303, 16, 'Moore''s Island', 'MOO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(304, 16, 'North Abaco', 'NAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(305, 16, 'North Andros', 'NAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(306, 16, 'North Eleuthera', 'NEL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(307, 16, 'Ragged Island', 'RAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(308, 16, 'Rum Cay', 'RUM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(309, 16, 'San Salvador', 'SAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(310, 16, 'South Abaco', 'SAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(311, 16, 'South Andros', 'SAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(312, 16, 'South Eleuthera', 'SEL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(313, 16, 'Spanish Wells', 'SWE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(314, 16, 'West Grand Bahama', 'WGB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(315, 17, 'Capital', 'CAP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(316, 17, 'Central', 'CEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(317, 17, 'Muharraq', 'MUH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(318, 17, 'Northern', 'NOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(319, 17, 'Southern', 'SOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(320, 18, 'Barisal', 'BAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(321, 18, 'Chittagong', 'CHI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(322, 18, 'Dhaka', 'DHA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(323, 18, 'Khulna', 'KHU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(324, 18, 'Rajshahi', 'RAJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(325, 18, 'Sylhet', 'SYL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(326, 19, 'Christ Church', 'CC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(327, 19, 'Saint Andrew', 'AND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(328, 19, 'Saint George', 'GEO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(329, 19, 'Saint James', 'JAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(330, 19, 'Saint Miki', 'JOH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(331, 19, 'Saint Joseph', 'JOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(332, 19, 'Saint Lucy', 'LUC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(333, 19, 'Saint Michael', 'MIC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(334, 19, 'Saint Peter', 'PET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(335, 19, 'Saint Philip', 'PHI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(336, 19, 'Saint Thomas', 'THO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(337, 20, 'Brestskaya (Brest)', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(338, 20, 'Homyel''skaya (Homyel'')', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(339, 20, 'Horad Minsk', 'HM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(342, 20, 'Minskaya', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(344, 21, 'Antwerpen', 'VAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(345, 21, 'Brabant Wallon', 'WBR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(346, 21, 'Hainaut', 'WHT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(347, 21, 'Liege', 'WLG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(348, 21, 'Limburg', 'VLI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(349, 21, 'Luxembourg', 'WLX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(350, 21, 'Namur', 'WNA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(351, 21, 'Oost-Vlaanderen', 'VOV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(352, 21, 'Vlaams Brabant', 'VBR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(353, 21, 'West-Vlaanderen', 'VWV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(354, 22, 'Belize', 'BZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(355, 22, 'Cayo', 'CY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(356, 22, 'Corozal', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(357, 22, 'Orange Walk', 'OW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(358, 22, 'Stann Creek', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(359, 22, 'Toledo', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(360, 23, 'Alibori', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(361, 23, 'Atakora', 'AK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(362, 23, 'Atlantique', 'AQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(363, 23, 'Borgou', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(364, 23, 'Collines', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(365, 23, 'Donga', 'DO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(366, 23, 'Kouffo', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(367, 23, 'Littoral', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(368, 23, 'Mono', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(369, 23, 'Oueme', 'OU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(370, 23, 'Plateau', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(371, 23, 'Zou', 'ZO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(372, 24, 'Devonshire', 'DS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(373, 24, 'Hamilton City', 'HC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(374, 24, 'Hamilton', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(375, 24, 'Paget', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(376, 24, 'Pembroke', 'PB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(377, 24, 'Saint George City', 'GC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(378, 24, 'Saint George''s', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(379, 24, 'Sandys', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(380, 24, 'Smith''s', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(381, 24, 'Southampton', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(382, 24, 'Warwick', 'WA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(383, 25, 'Bumthang', 'BUM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(384, 25, 'Chukha', 'CHU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(385, 25, 'Dagana', 'DAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(386, 25, 'Gasa', 'GAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(387, 25, 'Haa', 'HAA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(388, 25, 'Lhuntse', 'LHU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(389, 25, 'Mongar', 'MON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(390, 25, 'Paro', 'PAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(391, 25, 'Pemagatshel', 'PEM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(392, 25, 'Punakha', 'PUN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(393, 25, 'Samdrup Jongkhar', 'SJO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(394, 25, 'Samtse', 'SAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(395, 25, 'Sarpang', 'SAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(396, 25, 'Thimphu', 'THI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(397, 25, 'Trashigang', 'TRG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(398, 25, 'Trashiyangste', 'TRY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(399, 25, 'Trongsa', 'TRO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(400, 25, 'Tsirang', 'TSI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(401, 25, 'Wangdue Phodrang', 'WPH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(402, 25, 'Zhemgang', 'ZHE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(403, 26, 'Beni', 'BEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(404, 26, 'Chuquisaca', 'CHU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(405, 26, 'Cochabamba', 'COC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(406, 26, 'La Paz', 'LPZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(407, 26, 'Oruro', 'ORU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(408, 26, 'Pando', 'PAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(409, 26, 'Potosi', 'POT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(410, 26, 'Santa Cruz', 'SCZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(411, 26, 'Tarija', 'TAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(412, 27, 'Brcko district', 'BRO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(414, 27, 'Posavski Kanton', 'FPO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(415, 27, 'Tuzlanski Kanton', 'FTU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(421, 27, 'Kanton Sarajevo', 'FSA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(422, 27, 'Zapadnobosanska', 'FZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(423, 27, 'Banja Luka', 'SBL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(424, 27, 'Doboj', 'SDO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(425, 27, 'Bijeljina', 'SBI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(426, 27, 'Vlasenica', 'SVL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(428, 27, 'Foca', 'SFO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(429, 27, 'Trebinje', 'STR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(430, 28, 'Central', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(431, 28, 'Ghanzi', 'GH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(432, 28, 'Kgalagadi', 'KD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(433, 28, 'Kgatleng', 'KT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(434, 28, 'Kweneng', 'KW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(435, 28, 'Ngamiland', 'NG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(436, 28, 'North East', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(437, 28, 'North West', 'NW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(438, 28, 'South East', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(439, 28, 'Southern', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(440, 30, 'Acre', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(441, 30, 'Alagoas', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(442, 30, 'Amapa', 'AP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(443, 30, 'Amazonas', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(444, 30, 'Bahia', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(445, 30, 'Ceara', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(446, 30, 'Distrito Federal', 'DF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(447, 30, 'Espirito Santo', 'ES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(448, 30, 'Goias', 'GO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(449, 30, 'Maranhao', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(450, 30, 'Mato Grosso', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(451, 30, 'Mato Grosso do Sul', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(452, 30, 'Minas Gerais', 'MG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(453, 30, 'Para', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(454, 30, 'Paraiba', 'PB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(455, 30, 'Parana', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(456, 30, 'Pernambuco', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(457, 30, 'Piaui', 'PI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(458, 30, 'Rio de Janeiro', 'RJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(459, 30, 'Rio Grande do Norte', 'RN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(460, 30, 'Rio Grande do Sul', 'RS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(461, 30, 'Rondonia', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(462, 30, 'Roraima', 'RR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(463, 30, 'Santa Catarina', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(464, 30, 'Sao Paulo', 'SP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(465, 30, 'Sergipe', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(466, 30, 'Tocantins', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(467, 31, 'Peros Banhos', 'PB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(468, 31, 'Salomon Islands', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(469, 31, 'Nelsons Island', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(470, 31, 'Three Brothers', 'TB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(471, 31, 'Eagle Islands', 'EA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(472, 31, 'Danger Island', 'DI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(473, 31, 'Egmont Islands', 'EG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(474, 31, 'Diego Garcia', 'DG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(475, 32, 'Belait', 'BEL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(476, 32, 'Brunei and Muara', 'BRM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(477, 32, 'Temburong', 'TEM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(478, 32, 'Tutong', 'TUT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(479, 33, 'Blagoevgrad', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(480, 33, 'Burgas', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(481, 33, 'Dobrich', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(482, 33, 'Gabrovo', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(483, 33, 'Haskovo', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(484, 33, 'Kardjali', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(485, 33, 'Kyustendil', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(486, 33, 'Lovech', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(487, 33, 'Montana', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(488, 33, 'Pazardjik', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(489, 33, 'Pernik', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(490, 33, 'Pleven', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(491, 33, 'Plovdiv', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(492, 33, 'Razgrad', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(493, 33, 'Shumen', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(494, 33, 'Silistra', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(495, 33, 'Sliven', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(496, 33, 'Smolyan', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(497, 33, 'Sofia', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(498, 33, 'Sofia - town', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(499, 33, 'Stara Zagora', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(500, 33, 'Targovishte', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(501, 33, 'Varna', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(502, 33, 'Veliko Tarnovo', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(503, 33, 'Vidin', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(504, 33, 'Vratza', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(505, 33, 'Yambol', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(506, 34, 'Bale', 'BAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(507, 34, 'Bam', 'BAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(508, 34, 'Banwa', 'BAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(509, 34, 'Bazega', 'BAZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(510, 34, 'Bougouriba', 'BOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(511, 34, 'Boulgou', 'BLG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(512, 34, 'Boulkiemde', 'BOK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(513, 34, 'Comoe', 'COM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(514, 34, 'Ganzourgou', 'GAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(515, 34, 'Gnagna', 'GNA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(516, 34, 'Gourma', 'GOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(517, 34, 'Houet', 'HOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(518, 34, 'Ioba', 'IOA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(519, 34, 'Kadiogo', 'KAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(520, 34, 'Kenedougou', 'KEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(521, 34, 'Komondjari', 'KOD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(522, 34, 'Kompienga', 'KOP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(523, 34, 'Kossi', 'KOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(524, 34, 'Koulpelogo', 'KOL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(525, 34, 'Kouritenga', 'KOT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(526, 34, 'Kourweogo', 'KOW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(527, 34, 'Leraba', 'LER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(528, 34, 'Loroum', 'LOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(529, 34, 'Mouhoun', 'MOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(530, 34, 'Nahouri', 'NAH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(531, 34, 'Namentenga', 'NAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(532, 34, 'Nayala', 'NAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(533, 34, 'Noumbiel', 'NOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(534, 34, 'Oubritenga', 'OUB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(535, 34, 'Oudalan', 'OUD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(536, 34, 'Passore', 'PAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(537, 34, 'Poni', 'PON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(538, 34, 'Sanguie', 'SAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(539, 34, 'Sanmatenga', 'SAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(540, 34, 'Seno', 'SEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(541, 34, 'Sissili', 'SIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(542, 34, 'Soum', 'SOM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(543, 34, 'Sourou', 'SOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(544, 34, 'Tapoa', 'TAP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(545, 34, 'Tuy', 'TUY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(546, 34, 'Yagha', 'YAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(547, 34, 'Yatenga', 'YAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(548, 34, 'Ziro', 'ZIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(549, 34, 'Zondoma', 'ZOD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(550, 34, 'Zoundweogo', 'ZOW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(551, 35, 'Bubanza', 'BB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(552, 35, 'Bujumbura', 'BJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(553, 35, 'Bururi', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(554, 35, 'Cankuzo', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(555, 35, 'Cibitoke', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(556, 35, 'Gitega', 'GI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(557, 35, 'Karuzi', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(558, 35, 'Kayanza', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(559, 35, 'Kirundo', 'KI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(560, 35, 'Makamba', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(561, 35, 'Muramvya', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(562, 35, 'Muyinga', 'MY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(563, 35, 'Mwaro', 'MW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(564, 35, 'Ngozi', 'NG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(565, 35, 'Rutana', 'RT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(566, 35, 'Ruyigi', 'RY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(567, 36, 'Phnom Penh', 'PP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(569, 36, 'Pailin', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(570, 36, 'Keb', 'KB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(571, 36, 'Banteay Meanchey', 'BM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(572, 36, 'Battambang', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(573, 36, 'Kampong Cham', 'KM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(574, 36, 'Kampong Chhnang', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(575, 36, 'Kampong Speu', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(576, 36, 'Kampong Som', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(577, 36, 'Kampong Thom', 'KT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(578, 36, 'Kampot', 'KP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(579, 36, 'Kandal', 'KL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(580, 36, 'Kaoh Kong', 'KK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(581, 36, 'Kratie', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(582, 36, 'Mondul Kiri', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(583, 36, 'Oddar Meancheay', 'OM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(584, 36, 'Pursat', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(585, 36, 'Preah Vihear', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(586, 36, 'Prey Veng', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(587, 36, 'Ratanak Kiri', 'RK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(588, 36, 'Siemreap', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(589, 36, 'Stung Treng', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(590, 36, 'Svay Rieng', 'SR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(591, 36, 'Takeo', 'TK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(593, 37, 'Centre', 'CEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(594, 37, 'East (Est)', 'EST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');
INSERT INTO `mw_zone` (`zone_id`, `country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(596, 37, 'Littoral', 'LIT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(597, 37, 'North (Nord)', 'NOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(599, 37, 'West (Ouest)', 'OUE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(600, 37, 'South (Sud)', 'SUD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(602, 38, 'Alberta', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(603, 38, 'British Columbia', 'BC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(604, 38, 'Manitoba', 'MB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(605, 38, 'New Brunswick', 'NB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(606, 38, 'Newfoundland and Labrador', 'NL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(607, 38, 'Northwest Territories', 'NT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(608, 38, 'Nova Scotia', 'NS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(609, 38, 'Nunavut', 'NU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(610, 38, 'Ontario', 'ON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(611, 38, 'Prince Edward Island', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(612, 38, 'Qu&eacute;bec', 'QC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(613, 38, 'Saskatchewan', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(614, 38, 'Yukon Territory', 'YT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(615, 39, 'Boa Vista', 'BV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(616, 39, 'Brava', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(617, 39, 'Calheta de Sao Miguel', 'CS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(618, 39, 'Maio', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(619, 39, 'Mosteiros', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(620, 39, 'Paul', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(621, 39, 'Porto Novo', 'PN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(622, 39, 'Praia', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(623, 39, 'Ribeira Grande', 'RG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(624, 39, 'Sal', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(625, 39, 'Santa Catarina', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(626, 39, 'Santa Cruz', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(627, 39, 'Sao Domingos', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(628, 39, 'Sao Filipe', 'SF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(629, 39, 'Sao Nicolau', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(630, 39, 'Sao Vicente', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(631, 39, 'Tarrafal', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(632, 40, 'Creek', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(633, 40, 'Eastern', 'EA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(634, 40, 'Midland', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(635, 40, 'South Town', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(636, 40, 'Spot Bay', 'SP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(637, 40, 'Stake Bay', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(638, 40, 'West End', 'WD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(639, 40, 'Western', 'WN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(640, 41, 'Bamingui-Bangoran', 'BBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(641, 41, 'Basse-Kotto', 'BKO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(642, 41, 'Haute-Kotto', 'HKO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(643, 41, 'Haut-Mbomou', 'HMB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(644, 41, 'Kemo', 'KEM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(645, 41, 'Lobaye', 'LOB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(646, 41, 'Mambere-KadeÃ”', 'MKD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(647, 41, 'Mbomou', 'MBO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(648, 41, 'Nana-Mambere', 'NMM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(649, 41, 'Ombella-M''Poko', 'OMP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(650, 41, 'Ouaka', 'OUK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(651, 41, 'Ouham', 'OUH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(652, 41, 'Ouham-Pende', 'OPE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(653, 41, 'Vakaga', 'VAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(654, 41, 'Nana-Grebizi', 'NGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(655, 41, 'Sangha-Mbaere', 'SMB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(656, 41, 'Bangui', 'BAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(657, 42, 'Batha', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(658, 42, 'Biltine', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(660, 42, 'Chari-Baguirmi', 'CB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(661, 42, 'Guera', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(662, 42, 'Kanem', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(663, 42, 'Lac', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(664, 42, 'Logone Occidental', 'LC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(665, 42, 'Logone Oriental', 'LR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(666, 42, 'Mayo-Kebbi', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(667, 42, 'Moyen-Chari', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(668, 42, 'Ouaddai', 'OU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(669, 42, 'Salamat', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(670, 42, 'Tandjile', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(672, 43, 'Antofagasta', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(673, 43, 'Araucania', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(674, 43, 'Atacama', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(675, 43, 'Bio-Bio', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(676, 43, 'Coquimbo', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(677, 43, 'Libertador General Bernardo O''Hi', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(678, 43, 'Los Lagos', 'LL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(679, 43, 'Magallanes y de la Antartica Chi', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(680, 43, 'Maule', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(681, 43, 'Region Metropolitana', 'RM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(682, 43, 'Tarapaca', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(683, 43, 'Valparaiso', 'VS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(684, 44, 'Anhui', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(685, 44, 'Beijing', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(686, 44, 'Chongqing', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(687, 44, 'Fujian', 'FU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(688, 44, 'Gansu', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(689, 44, 'Guangdong', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(690, 44, 'Guangxi', 'GX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(691, 44, 'Guizhou', 'GZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(692, 44, 'Hainan', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(693, 44, 'Hebei', 'HB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(694, 44, 'Heilongjiang', 'HL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(695, 44, 'Henan', 'HE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(696, 44, 'Hong Kong', 'HK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(697, 44, 'Hubei', 'HU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(698, 44, 'Hunan', 'HN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(699, 44, 'Inner Mongolia', 'IM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(700, 44, 'Jiangsu', 'JI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(701, 44, 'Jiangxi', 'JX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(702, 44, 'Jilin', 'JL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(703, 44, 'Liaoning', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(704, 44, 'Macau', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(705, 44, 'Ningxia', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(706, 44, 'Shaanxi', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(707, 44, 'Shandong', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(708, 44, 'Shanghai', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(709, 44, 'Shanxi', 'SX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(710, 44, 'Sichuan', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(711, 44, 'Tianjin', 'TI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(712, 44, 'Xinjiang', 'XI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(713, 44, 'Yunnan', 'YU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(714, 44, 'Zhejiang', 'ZH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(715, 46, 'Direction Island', 'D', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(716, 46, 'Home Island', 'H', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(717, 46, 'Horsburgh Island', 'O', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(718, 46, 'South Island', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(719, 46, 'West Island', 'W', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(720, 47, 'Amazonas', 'AMZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(721, 47, 'Antioquia', 'ANT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(722, 47, 'Arauca', 'ARA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(723, 47, 'Atlantico', 'ATL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(724, 47, 'Bogota D.C.', 'BDC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(725, 47, 'Bolivar', 'BOL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(726, 47, 'Boyaca', 'BOY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(727, 47, 'Caldas', 'CAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(728, 47, 'Caqueta', 'CAQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(729, 47, 'Casanare', 'CAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(730, 47, 'Cauca', 'CAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(731, 47, 'Cesar', 'CES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(732, 47, 'Choco', 'CHO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(733, 47, 'Cordoba', 'COR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(734, 47, 'Cundinamarca', 'CAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(735, 47, 'Guainia', 'GNA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(736, 47, 'Guajira', 'GJR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(737, 47, 'Guaviare', 'GVR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(738, 47, 'Huila', 'HUI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(739, 47, 'Magdalena', 'MAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(740, 47, 'Meta', 'MET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(741, 47, 'Narino', 'NAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(742, 47, 'Norte de Santander', 'NDS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(743, 47, 'Putumayo', 'PUT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(744, 47, 'Quindio', 'QUI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(745, 47, 'Risaralda', 'RIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(746, 47, 'San Andres y Providencia', 'SAP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(747, 47, 'Santander', 'SAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(748, 47, 'Sucre', 'SUC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(749, 47, 'Tolima', 'TOL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(750, 47, 'Valle del Cauca', 'VDC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(751, 47, 'Vaupes', 'VAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(752, 47, 'Vichada', 'VIC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(753, 48, 'Grande Comore', 'G', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(754, 48, 'Anjouan', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(755, 48, 'Moheli', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(756, 49, 'Bouenza', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(757, 49, 'Brazzaville', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(758, 49, 'Cuvette', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(759, 49, 'Cuvette-Ouest', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(760, 49, 'Kouilou', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(761, 49, 'Lekoumou', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(762, 49, 'Likouala', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(763, 49, 'Niari', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(764, 49, 'Plateaux', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(765, 49, 'Pool', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(766, 49, 'Sangha', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(767, 50, 'Pukapuka', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(768, 50, 'Rakahanga', 'RK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(769, 50, 'Manihiki', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(770, 50, 'Penrhyn', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(771, 50, 'Nassau Island', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(772, 50, 'Surwarrow', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(773, 50, 'Palmerston', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(774, 50, 'Aitutaki', 'AI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(775, 50, 'Manuae', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(776, 50, 'Takutea', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(777, 50, 'Mitiaro', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(778, 50, 'Atiu', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(779, 50, 'Mauke', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(780, 50, 'Rarotonga', 'RR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(781, 50, 'Mangaia', 'MG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(782, 51, 'Alajuela', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(783, 51, 'Cartago', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(784, 51, 'Guanacaste', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(785, 51, 'Heredia', 'HE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(786, 51, 'Limon', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(787, 51, 'Puntarenas', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(788, 51, 'San Jose', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(789, 52, 'Abengourou', 'ABE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(790, 52, 'Abidjan', 'ABI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(791, 52, 'Aboisso', 'ABO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(792, 52, 'Adiake', 'ADI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(793, 52, 'Adzope', 'ADZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(794, 52, 'Agboville', 'AGB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(795, 52, 'Agnibilekrou', 'AGN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(796, 52, 'Alepe', 'ALE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(797, 52, 'Bocanda', 'BOC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(798, 52, 'Bangolo', 'BAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(799, 52, 'Beoumi', 'BEO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(800, 52, 'Biankouma', 'BIA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(801, 52, 'Bondoukou', 'BDK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(802, 52, 'Bongouanou', 'BGN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(803, 52, 'Bouafle', 'BFL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(804, 52, 'Bouake', 'BKE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(805, 52, 'Bouna', 'BNA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(806, 52, 'Boundiali', 'BDL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(807, 52, 'Dabakala', 'DKL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(808, 52, 'Dabou', 'DBU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(809, 52, 'Daloa', 'DAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(810, 52, 'Danane', 'DAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(811, 52, 'Daoukro', 'DAO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(812, 52, 'Dimbokro', 'DIM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(813, 52, 'Divo', 'DIV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(814, 52, 'Duekoue', 'DUE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(815, 52, 'Ferkessedougou', 'FER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(816, 52, 'Gagnoa', 'GAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(817, 52, 'Grand-Bassam', 'GBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(818, 52, 'Grand-Lahou', 'GLA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(819, 52, 'Guiglo', 'GUI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(820, 52, 'Issia', 'ISS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(821, 52, 'Jacqueville', 'JAC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(822, 52, 'Katiola', 'KAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(823, 52, 'Korhogo', 'KOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(824, 52, 'Lakota', 'LAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(825, 52, 'Man', 'MAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(826, 52, 'Mankono', 'MKN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(827, 52, 'Mbahiakro', 'MBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(828, 52, 'Odienne', 'ODI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(829, 52, 'Oume', 'OUM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(830, 52, 'Sakassou', 'SAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(831, 52, 'San-Pedro', 'SPE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(832, 52, 'Sassandra', 'SAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(833, 52, 'Seguela', 'SEG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(834, 52, 'Sinfra', 'SIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(835, 52, 'Soubre', 'SOU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(836, 52, 'Tabou', 'TAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(837, 52, 'Tanda', 'TAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(838, 52, 'Tiebissou', 'TIE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(839, 52, 'Tingrela', 'TIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(840, 52, 'Tiassale', 'TIA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(841, 52, 'Touba', 'TBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(842, 52, 'Toulepleu', 'TLP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(843, 52, 'Toumodi', 'TMD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(844, 52, 'Vavoua', 'VAV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(845, 52, 'Yamoussoukro', 'YAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(846, 52, 'Zuenoula', 'ZUE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(847, 53, 'Bjelovar-Bilogora', 'BB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(848, 53, 'City of Zagreb', 'CZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(849, 53, 'Dubrovnik-Neretva', 'DN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(850, 53, 'Istra', 'IS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(851, 53, 'Karlovac', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(852, 53, 'Koprivnica-Krizevci', 'KK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(853, 53, 'Krapina-Zagorje', 'KZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(854, 53, 'Lika-Senj', 'LS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(855, 53, 'Medimurje', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(856, 53, 'Osijek-Baranja', 'OB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(857, 53, 'Pozega-Slavonia', 'PS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(858, 53, 'Primorje-Gorski Kotar', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(859, 53, 'Sibenik', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(860, 53, 'Sisak-Moslavina', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(861, 53, 'Slavonski Brod-Posavina', 'SB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(862, 53, 'Split-Dalmatia', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(863, 53, 'Varazdin', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(864, 53, 'Virovitica-Podravina', 'VP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(865, 53, 'Vukovar-Srijem', 'VS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(866, 53, 'Zadar-Knin', 'ZK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(867, 53, 'Zagreb', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(868, 54, 'Camaguey', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(869, 54, 'Ciego de Avila', 'CD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(870, 54, 'Cienfuegos', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(871, 54, 'Ciudad de La Habana', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(872, 54, 'Granma', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(873, 54, 'Guantanamo', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(874, 54, 'Holguin', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(875, 54, 'Isla de la Juventud', 'IJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(876, 54, 'La Habana', 'LH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(877, 54, 'Las Tunas', 'LT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(878, 54, 'Matanzas', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(879, 54, 'Pinar del Rio', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(880, 54, 'Sancti Spiritus', 'SS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(881, 54, 'Santiago de Cuba', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(882, 54, 'Villa Clara', 'VC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(883, 55, 'Famagusta', 'F', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(884, 55, 'Kyrenia', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(885, 55, 'Larnaca', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(886, 55, 'Limassol', 'I', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(887, 55, 'Nicosia', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(888, 55, 'Paphos', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(889, 56, 'ÃšsteckÃ½', 'U', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(890, 56, 'JihoÄeskÃ½', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(891, 56, 'JihomoravskÃ½', 'B', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(892, 56, 'KarlovarskÃ½', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(893, 56, 'KrÃ¡lovehradeckÃ½', 'H', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(894, 56, 'LibereckÃ½', 'L', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(895, 56, 'MoravskoslezskÃ½', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(896, 56, 'OlomouckÃ½', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(897, 56, 'PardubickÃ½', 'E', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(898, 56, 'PlzeÅˆskÃ½', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(899, 56, 'Praha', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(900, 56, 'StÅ™edoÄeskÃ½', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(901, 56, 'VysoÄina', 'J', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(902, 56, 'ZlÃ­nskÃ½', 'Z', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(903, 57, 'Arhus', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(904, 57, 'Bornholm', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(905, 57, 'Copenhagen', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(906, 57, 'Faroe Islands', 'FO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(907, 57, 'Frederiksborg', 'FR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(908, 57, 'Fyn', 'FY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(909, 57, 'Kobenhavn', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(910, 57, 'Nordjylland', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(911, 57, 'Ribe', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(912, 57, 'Ringkobing', 'RK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(913, 57, 'Roskilde', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(914, 57, 'Sonderjylland', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(915, 57, 'Storstrom', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(916, 57, 'Vejle', 'VK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(917, 57, 'Vestj&aelig;lland', 'VJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(918, 57, 'Viborg', 'VB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(919, 58, '''Ali Sabih', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(920, 58, 'Dikhil', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(921, 58, 'Djibouti', 'J', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(922, 58, 'Obock', 'O', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(923, 58, 'Tadjoura', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(924, 59, 'Saint Andrew Parish', 'AND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(925, 59, 'Saint David Parish', 'DAV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(926, 59, 'Saint George Parish', 'GEO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(927, 59, 'Saint Miki Parish', 'JOH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(928, 59, 'Saint Joseph Parish', 'JOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(929, 59, 'Saint Luke Parish', 'LUK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(930, 59, 'Saint Mark Parish', 'MAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(931, 59, 'Saint Miles Parish', 'PAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(932, 59, 'Saint Paul Parish', 'PAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(933, 59, 'Saint Peter Parish', 'PET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(934, 60, 'Distrito Nacional', 'DN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(935, 60, 'Azua', 'AZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(936, 60, 'Baoruco', 'BC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(937, 60, 'Barahona', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(938, 60, 'Dajabon', 'DJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(939, 60, 'Duarte', 'DU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(940, 60, 'Elias Pina', 'EL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(941, 60, 'El Seybo', 'SY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(942, 60, 'Espaillat', 'ET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(943, 60, 'Hato Mayor', 'HM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(944, 60, 'Independencia', 'IN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(945, 60, 'La Altagracia', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(946, 60, 'La Romana', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(947, 60, 'La Vega', 'VE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(949, 60, 'Monsenor Nouel', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(950, 60, 'Monte Cristi', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(951, 60, 'Monte Plata', 'MP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(952, 60, 'Pedernales', 'PD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(953, 60, 'Peravia (Bani)', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(954, 60, 'Puerto Plata', 'PP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(955, 60, 'Salcedo', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(956, 60, 'Samana', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(957, 60, 'Sanchez Ramirez', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(958, 60, 'San Cristobal', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(959, 60, 'San Jose de Ocoa', 'JO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(960, 60, 'San Juan', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(961, 60, 'San Pedro de Macoris', 'PM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(962, 60, 'Santiago', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(963, 60, 'Santiago Rodriguez', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(964, 60, 'Santo Domingo', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(965, 60, 'Valverde', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(966, 61, 'Aileu', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(967, 61, 'Ainaro', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(968, 61, 'Baucau', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(969, 61, 'Bobonaro', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(970, 61, 'Cova Lima', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(971, 61, 'Dili', 'DI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(972, 61, 'Ermera', 'ER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(973, 61, 'Lautem', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(974, 61, 'Liquica', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(975, 61, 'Manatuto', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(976, 61, 'Manufahi', 'MF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(977, 61, 'Oecussi', 'OE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(978, 61, 'Viqueque', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(979, 62, 'Azuay', 'AZU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(980, 62, 'Bolivar', 'BOL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(981, 62, 'Ca&ntilde;ar', 'CAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(982, 62, 'Carchi', 'CAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(983, 62, 'Chimborazo', 'CHI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(984, 62, 'Cotopaxi', 'COT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(985, 62, 'El Oro', 'EOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(986, 62, 'Esmeraldas', 'ESM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(987, 62, 'Gal&aacute;pagos', 'GPS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(988, 62, 'Guayas', 'GUA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(989, 62, 'Imbabura', 'IMB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(990, 62, 'Loja', 'LOJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(991, 62, 'Los Rios', 'LRO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(992, 62, 'Manab&iacute;', 'MAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(993, 62, 'Morona Santiago', 'MSA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(994, 62, 'Napo', 'NAP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(995, 62, 'Orellana', 'ORE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(996, 62, 'Pastaza', 'PAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(997, 62, 'Pichincha', 'PIC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(998, 62, 'Sucumb&iacute;os', 'SUC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(999, 62, 'Tungurahua', 'TUN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1001, 63, 'Ad Daqahliyah', 'DHY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1003, 63, 'Al Buhayrah', 'BHY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1004, 63, 'Al Fayyum', 'FYM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1005, 63, 'Al Gharbiyah', 'GBY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1006, 63, 'Al Iskandariyah', 'IDR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1007, 63, 'Al Isma''iliyah', 'IML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1008, 63, 'Al Jizah', 'JZH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1009, 63, 'Al Minufiyah', 'MFY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1010, 63, 'Al Minya', 'MNY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1011, 63, 'Al Qahirah', 'QHR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1012, 63, 'Al Qalyubiyah', 'QLY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1015, 63, 'As Suways', 'SWY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1016, 63, 'Aswan', 'ASW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1017, 63, 'Asyut', 'ASY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1018, 63, 'Bani Suwayf', 'BSW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1019, 63, 'Bur Sa''id', 'BSD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1020, 63, 'Dumyat', 'DMY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1021, 63, 'Janub Sina''', 'JNS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1023, 63, 'Matruh', 'MAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1024, 63, 'Qina', 'QIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1025, 63, 'Shamal Sina''', 'SHS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1026, 63, 'Suhaj', 'SUH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1027, 64, 'Ahuachapan', 'AH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1028, 64, 'Cabanas', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1029, 64, 'Chalatenango', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1030, 64, 'Cuscatlan', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1031, 64, 'La Libertad', 'LB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1032, 64, 'La Paz', 'PZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1033, 64, 'La Union', 'UN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1034, 64, 'Morazan', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1035, 64, 'San Miguel', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1036, 64, 'San Salvador', 'SS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1037, 64, 'San Vicente', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1038, 64, 'Santa Ana', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1039, 64, 'Sonsonate', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1040, 64, 'Usulutan', 'US', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1041, 65, 'Provincia Annobon', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1042, 65, 'Provincia Bioko Norte', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1043, 65, 'Provincia Bioko Sur', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1044, 65, 'Provincia Centro Sur', 'CS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1046, 65, 'Provincia Litoral', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1048, 66, 'Central (Maekel)', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1049, 66, 'Anseba (Keren)', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1052, 66, 'Southern (Debub)', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1062, 67, 'Polvamaa (Polva)', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1066, 67, 'Valgamaa (Valga)', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1068, 67, 'Vorumaa (Voru)', 'VO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1069, 68, 'Afar', 'AF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1070, 68, 'Amhara', 'AH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1071, 68, 'Benishangul-Gumaz', 'BG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1072, 68, 'Gambela', 'GB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1073, 68, 'Hariai', 'HR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1074, 68, 'Oromia', 'OR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1075, 68, 'Somali', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1077, 68, 'Tigray', 'TG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1078, 68, 'Addis Ababa', 'AA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1079, 68, 'Dire Dawa', 'DD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1080, 71, 'Central Division', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1081, 71, 'Northern Division', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1082, 71, 'Eastern Division', 'E', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1083, 71, 'Western Division', 'W', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1084, 71, 'Rotuma', 'R', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1085, 72, 'Ahvenanmaan Laani', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1086, 72, 'Etela-Suomen Laani', 'ES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1087, 72, 'Ita-Suomen Laani', 'IS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1088, 72, 'Lansi-Suomen Laani', 'LS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1089, 72, 'Lapin Lanani', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1090, 72, 'Oulun Laani', 'OU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1091, 73, 'Alsace', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1092, 73, 'Aquitaine', 'AQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1093, 73, 'Auvergne', 'AU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1094, 73, 'Brittany', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1095, 73, 'Burgundy', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1096, 73, 'Center Loire Valley', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1097, 73, 'Champagne', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1098, 73, 'Corse', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1099, 73, 'France Comte', 'FR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1100, 73, 'Languedoc Roussillon', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1101, 73, 'Limousin', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1102, 73, 'Lorraine', 'LO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1103, 73, 'Midi Pyrenees', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1104, 73, 'Nord Pas de Calais', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1105, 73, 'Normandy', 'NR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1106, 73, 'Paris / Ill de France', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1107, 73, 'Picardie', 'PI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1108, 73, 'Poitou Charente', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1109, 73, 'Provence', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1110, 73, 'Rhone Alps', 'RH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1111, 73, 'Riviera', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1112, 73, 'Western Loire Valley', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1113, 74, 'Etranger', 'Et', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1114, 74, 'Ain', '01', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1115, 74, 'Aisne', '02', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1116, 74, 'Allier', '03', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1117, 74, 'Alpes de Haute Provence', '04', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1118, 74, 'Hautes-Alpes', '05', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1119, 74, 'Alpes Maritimes', '06', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1120, 74, 'Ard&egrave;che', '07', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1121, 74, 'Ardennes', '08', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1122, 74, 'Ari&egrave;ge', '09', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1123, 74, 'Aube', '10', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1124, 74, 'Aude', '11', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1125, 74, 'Aveyron', '12', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1127, 74, 'Calvados', '14', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1128, 74, 'Cantal', '15', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1129, 74, 'Charente', '16', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1130, 74, 'Charente Maritime', '17', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1131, 74, 'Cher', '18', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1132, 74, 'Corr&egrave;ze', '19', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1133, 74, 'Corse du Sud', '2A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1134, 74, 'Haute Corse', '2B', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1137, 74, 'Creuse', '23', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1138, 74, 'Dordogne', '24', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1139, 74, 'Doubs', '25', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1140, 74, 'Dr&ocirc;me', '26', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1141, 74, 'Eure', '27', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1142, 74, 'Eure et Loir', '28', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1143, 74, 'Finist&egrave;re', '29', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1144, 74, 'Gard', '30', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1145, 74, 'Haute Garonne', '31', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1146, 74, 'Gers', '32', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1147, 74, 'Gironde', '33', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1148, 74, 'H&eacute;rault', '34', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1149, 74, 'Ille et Vilaine', '35', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1150, 74, 'Indre', '36', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1151, 74, 'Indre et Loire', '37', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1152, 74, 'Is&eacute;re', '38', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1153, 74, 'Jura', '39', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1154, 74, 'Landes', '40', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1155, 74, 'Loir et Cher', '41', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1156, 74, 'Loire', '42', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1157, 74, 'Haute Loire', '43', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1158, 74, 'Loire Atlantique', '44', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1159, 74, 'Loiret', '45', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1160, 74, 'Lot', '46', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1161, 74, 'Lot et Garonne', '47', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1162, 74, 'Loz&egrave;re', '48', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1163, 74, 'Maine et Loire', '49', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1164, 74, 'Manche', '50', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1165, 74, 'Marne', '51', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1166, 74, 'Haute Marne', '52', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1167, 74, 'Mayenne', '53', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1168, 74, 'Meurthe et Moselle', '54', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1169, 74, 'Meuse', '55', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1170, 74, 'Morbihan', '56', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1171, 74, 'Moselle', '57', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1172, 74, 'Ni&egrave;vre', '58', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1173, 74, 'Nord', '59', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1174, 74, 'Oise', '60', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1175, 74, 'Orne', '61', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');
INSERT INTO `mw_zone` (`zone_id`, `country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(1176, 74, 'Pas de Calais', '62', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1177, 74, 'Puy de D&ocirc;me', '63', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1181, 74, 'Bas Rhin', '67', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1182, 74, 'Haut Rhin', '68', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1183, 74, 'Rh&ocirc;ne', '69', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1186, 74, 'Sarthe', '72', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1187, 74, 'Savoie', '73', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1188, 74, 'Haute Savoie', '74', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1189, 74, 'Paris', '75', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1190, 74, 'Seine Maritime', '76', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1191, 74, 'Seine et Marne', '77', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1192, 74, 'Yvelines', '78', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1193, 74, 'Deux S&egrave;vres', '79', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1194, 74, 'Somme', '80', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1195, 74, 'Tarn', '81', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1196, 74, 'Tarn et Garonne', '82', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1197, 74, 'Var', '83', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1198, 74, 'Vaucluse', '84', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1199, 74, 'Vend&eacute;e', '85', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1200, 74, 'Vienne', '86', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1201, 74, 'Haute Vienne', '87', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1202, 74, 'Vosges', '88', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1203, 74, 'Yonne', '89', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1204, 74, 'Territoire de Belfort', '90', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1205, 74, 'Essonne', '91', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1206, 74, 'Hauts de Seine', '92', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1207, 74, 'Seine St-Denis', '93', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1208, 74, 'Val de Marne', '94', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1209, 74, 'Val d''Oise', '95', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1210, 76, 'Archipel des Marquises', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1211, 76, 'Archipel des Tuamotu', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1212, 76, 'Archipel des Tubuai', 'I', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1213, 76, 'Iles du Vent', 'V', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1214, 76, 'Iles Sous-le-Vent', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1215, 77, 'Iles Crozet', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1216, 77, 'Iles Kerguelen', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1217, 77, 'Ile Amsterdam', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1218, 77, 'Ile Saint-Paul', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1219, 77, 'Adelie Land', 'D', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1220, 78, 'Estuaire', 'ES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1221, 78, 'Haut-Ogooue', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1222, 78, 'Moyen-Ogooue', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1223, 78, 'Ngounie', 'NG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1224, 78, 'Nyanga', 'NY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1225, 78, 'Ogooue-Ivindo', 'OI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1226, 78, 'Ogooue-Lolo', 'OL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1227, 78, 'Ogooue-Maritime', 'OM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1228, 78, 'Woleu-Ntem', 'WN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1229, 79, 'Banjul', 'BJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1230, 79, 'Basse', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1231, 79, 'Brikama', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1232, 79, 'Janjangbure', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1233, 79, 'Kanifeng', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1234, 79, 'Kerewan', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1235, 79, 'Kuntaur', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1236, 79, 'Mansakonko', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1237, 79, 'Lower River', 'LR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1238, 79, 'Central River', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1239, 79, 'North Bank', 'NB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1240, 79, 'Upper River', 'UR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1241, 79, 'Western', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1242, 80, 'Abkhazia', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1243, 80, 'Ajaria', 'AJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1244, 80, 'Tbilisi', 'TB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1245, 80, 'Guria', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1246, 80, 'Imereti', 'IM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1247, 80, 'Kakheti', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1248, 80, 'Kvemo Kartli', 'KK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1253, 80, 'Shida Kartli', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1255, 81, 'Bayern', 'BAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1256, 81, 'Berlin', 'BER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1257, 81, 'Brandenburg', 'BRG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1258, 81, 'Bremen', 'BRE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1259, 81, 'Hamburg', 'HAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1260, 81, 'Hessen', 'HES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1262, 81, 'Niedersachsen', 'NDS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1265, 81, 'Saarland', 'SAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1266, 81, 'Sachsen', 'SAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1268, 81, 'Schleswig-Holstein', 'SCN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1269, 81, 'Th&uuml;ringen', 'THE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1270, 82, 'Ashanti Region', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1271, 82, 'Brong-Ahafo Region', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1272, 82, 'Central Region', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1273, 82, 'Eastern Region', 'EA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1274, 82, 'Greater Accra Region', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1275, 82, 'Northern Region', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1276, 82, 'Upper East Region', 'UE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1277, 82, 'Upper West Region', 'UW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1278, 82, 'Volta Region', 'VO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1279, 82, 'Western Region', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1280, 84, 'Attica', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1281, 84, 'Central Greece', 'CN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1282, 84, 'Central Macedonia', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1283, 84, 'Crete', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1284, 84, 'East Macedonia and Thrace', 'EM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1285, 84, 'Epirus', 'EP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1286, 84, 'Ionian Islands', 'II', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1287, 84, 'North Aegean', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1288, 84, 'Peloponnesos', 'PP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1289, 84, 'South Aegean', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1290, 84, 'Thessaly', 'TH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1291, 84, 'West Greece', 'WG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1292, 84, 'West Macedonia', 'WM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1293, 85, 'Avannaa', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1294, 85, 'Tunu', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1295, 85, 'Kitaa', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1296, 86, 'Saint Andrew', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1297, 86, 'Saint David', 'D', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1298, 86, 'Saint George', 'G', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1299, 86, 'Saint Miki', 'J', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1300, 86, 'Saint Mark', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1301, 86, 'Saint Miles', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1302, 86, 'Carriacou', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1303, 86, 'Petit Martinique', 'Q', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1304, 89, 'Alta Verapaz', 'AV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1305, 89, 'Baja Verapaz', 'BV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1306, 89, 'Chimaltenango', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1307, 89, 'Chiquimula', 'CQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1308, 89, 'El Peten', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1309, 89, 'El Progreso', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1310, 89, 'El Quiche', 'QC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1311, 89, 'Escuintla', 'ES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1312, 89, 'Guatemala', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1313, 89, 'Huehuetenango', 'HU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1314, 89, 'Izabal', 'IZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1315, 89, 'Jalapa', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1316, 89, 'Jutiapa', 'JU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1317, 89, 'Quetzaltenango', 'QZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1318, 89, 'Retalhuleu', 'RE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1319, 89, 'Sacatepequez', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1320, 89, 'San Marcos', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1321, 89, 'Santa Rosa', 'SR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1322, 89, 'Solola', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1323, 89, 'Suchitepequez', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1324, 89, 'Totonicapan', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1325, 89, 'Zacapa', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1326, 90, 'Conakry', 'CNK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1327, 90, 'Beyla', 'BYL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1328, 90, 'Boffa', 'BFA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1329, 90, 'Boke', 'BOK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1330, 90, 'Coyah', 'COY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1331, 90, 'Dabola', 'DBL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1332, 90, 'Dalaba', 'DLB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1333, 90, 'Dinguiraye', 'DGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1334, 90, 'Dubreka', 'DBR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1335, 90, 'Faranah', 'FRN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1336, 90, 'Forecariah', 'FRC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1337, 90, 'Fria', 'FRI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1338, 90, 'Gaoual', 'GAO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1339, 90, 'Gueckedou', 'GCD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1340, 90, 'Kankan', 'KNK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1341, 90, 'Kerouane', 'KRN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1342, 90, 'Kindia', 'KND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1343, 90, 'Kissidougou', 'KSD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1344, 90, 'Koubia', 'KBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1345, 90, 'Koundara', 'KDA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1346, 90, 'Kouroussa', 'KRA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1347, 90, 'Labe', 'LAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1348, 90, 'Lelouma', 'LLM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1349, 90, 'Lola', 'LOL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1350, 90, 'Macenta', 'MCT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1351, 90, 'Mali', 'MAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1352, 90, 'Mamou', 'MAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1353, 90, 'Mandiana', 'MAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1354, 90, 'Nzerekore', 'NZR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1355, 90, 'Pita', 'PIT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1356, 90, 'Siguiri', 'SIG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1357, 90, 'Telimele', 'TLM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1358, 90, 'Tougue', 'TOG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1359, 90, 'Yomou', 'YOM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1360, 91, 'Bafata Region', 'BF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1361, 91, 'Biombo Region', 'BB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1362, 91, 'Bissau Region', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1363, 91, 'Bolama Region', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1364, 91, 'Cacheu Region', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1365, 91, 'Gabu Region', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1366, 91, 'Oio Region', 'OI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1367, 91, 'Quinara Region', 'QU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1368, 91, 'Tombali Region', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1369, 92, 'Barima-Waini', 'BW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1371, 92, 'Demerara-Mahaica', 'DM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1372, 92, 'East Berbice-Corentyne', 'EC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1374, 92, 'Mahaica-Berbice', 'MB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1376, 92, 'Potaro-Siparuni', 'PI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1379, 93, 'Artibonite', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1380, 93, 'Centre', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1381, 93, 'Grand''Anse', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1382, 93, 'Nord', 'ND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1383, 93, 'Nord-Est', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1384, 93, 'Nord-Ouest', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1385, 93, 'Ouest', 'OU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1386, 93, 'Sud', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1387, 93, 'Sud-Est', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1388, 94, 'Flat Island', 'F', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1389, 94, 'McDonald Island', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1390, 94, 'Shag Island', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1391, 94, 'Heard Island', 'H', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1392, 95, 'Atlantida', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1393, 95, 'Choluteca', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1394, 95, 'Colon', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1395, 95, 'Comayagua', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1396, 95, 'Copan', 'CP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1397, 95, 'Cortes', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1398, 95, 'El Paraiso', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1399, 95, 'Francisco Morazan', 'FM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1400, 95, 'Gracias a Dios', 'GD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1401, 95, 'Intibuca', 'IN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1403, 95, 'La Paz', 'PZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1404, 95, 'Lempira', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1405, 95, 'Ocotepeque', 'OC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1406, 95, 'Olancho', 'OL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1407, 95, 'Santa Barbara', 'SB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1408, 95, 'Valle', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1409, 95, 'Yoro', 'YO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1419, 96, 'Islands New Territories', 'NIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1421, 96, 'North New Territories', 'NNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1422, 96, 'Sai Kung New Territories', 'NSK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1423, 96, 'Sha Tin New Territories', 'NST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1424, 96, 'Tai Po New Territories', 'NTP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1427, 96, 'Yuen Long New Territories', 'NYL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1428, 97, 'Bacs-Kiskun', 'BK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1429, 97, 'Baranya', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1430, 97, 'Bekes', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1431, 97, 'Bekescsaba', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1432, 97, 'Borsod-Abauj-Zemplen', 'BZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1433, 97, 'Budapest', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1434, 97, 'Csongrad', 'CS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1435, 97, 'Debrecen', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1436, 97, 'Dunaujvaros', 'DU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1437, 97, 'Eger', 'EG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1438, 97, 'Fejer', 'FE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1439, 97, 'Gyor', 'GY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1440, 97, 'Gyor-Moson-Sopron', 'GM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1441, 97, 'Hajdu-Bihar', 'HB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1442, 97, 'Heves', 'HE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1443, 97, 'Hodmezovasarhely', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1444, 97, 'Jasz-Nagykun-Szolnok', 'JN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1445, 97, 'Kaposvar', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1446, 97, 'Kecskemet', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1447, 97, 'Komarom-Esztergom', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1448, 97, 'Miskolc', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1449, 97, 'Nagykanizsa', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1450, 97, 'Nograd', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1451, 97, 'Nyiregyhaza', 'NY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1452, 97, 'Pecs', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1453, 97, 'Pest', 'PS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1454, 97, 'Somogy', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1455, 97, 'Sopron', 'SP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1456, 97, 'Szabolcs-Szatmar-Bereg', 'SS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1457, 97, 'Szeged', 'SZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1458, 97, 'Szekesfehervar', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1459, 97, 'Szolnok', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1460, 97, 'Szombathely', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1461, 97, 'Tatabanya', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1462, 97, 'Tolna', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1463, 97, 'Vas', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1464, 97, 'Veszprem', 'VE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1465, 97, 'Zala', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1466, 97, 'Zalaegerszeg', 'ZZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1467, 98, 'Austurland', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1469, 98, 'Norourland eystra', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1470, 98, 'Norourland vestra', 'NV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1471, 98, 'Suourland', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1472, 98, 'Suournes', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1473, 98, 'Vestfiroir', 'VF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1474, 98, 'Vesturland', 'VL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1476, 99, 'Andhra Pradesh', 'AP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1477, 99, 'Arunachal Pradesh', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1478, 99, 'Assam', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1479, 99, 'Bihar', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1480, 99, 'Chandigarh', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1482, 99, 'Daman and Diu', 'DM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1483, 99, 'Delhi', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1484, 99, 'Goa', 'GO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1485, 99, 'Gujarat', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1486, 99, 'Haryana', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1487, 99, 'Himachal Pradesh', 'HP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1488, 99, 'Jammu and Kashmir', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1489, 99, 'Karnataka', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1490, 99, 'Kerala', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1491, 99, 'Lakshadweep Islands', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1492, 99, 'Madhya Pradesh', 'MP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1493, 99, 'Maharashtra', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1494, 99, 'Manipur', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1495, 99, 'Meghalaya', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1496, 99, 'Mizoram', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1497, 99, 'Nagaland', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1498, 99, 'Orissa', 'OR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1499, 99, 'Pondicherry', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1500, 99, 'Punjab', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1501, 99, 'Rajasthan', 'RA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1502, 99, 'Sikkim', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1503, 99, 'Tamil Nadu', 'TN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1504, 99, 'Tripura', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1505, 99, 'Uttar Pradesh', 'UP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1506, 99, 'West Bengal', 'WB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1507, 100, 'Aceh', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1508, 100, 'Bali', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1509, 100, 'Banten', 'BT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1510, 100, 'Bengkulu', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1511, 100, 'BoDeTaBek', 'BD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1512, 100, 'Gorontalo', 'GO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1513, 100, 'Jakarta Raya', 'JK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1514, 100, 'Jambi', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1515, 100, 'Jawa Barat', 'JB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1516, 100, 'Jawa Tengah', 'JT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1517, 100, 'Jawa Timur', 'JI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1518, 100, 'Kalimantan Barat', 'KB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1519, 100, 'Kalimantan Selatan', 'KS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1520, 100, 'Kalimantan Tengah', 'KT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1521, 100, 'Kalimantan Timur', 'KI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1523, 100, 'Lampung', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1524, 100, 'Maluku', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1525, 100, 'Maluku Utara', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1528, 100, 'Papua', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1529, 100, 'Riau', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1530, 100, 'Sulawesi Selatan', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1531, 100, 'Sulawesi Tengah', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1532, 100, 'Sulawesi Tenggara', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1533, 100, 'Sulawesi Utara', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1534, 100, 'Sumatera Barat', 'SB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1535, 100, 'Sumatera Selatan', 'SS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1536, 100, 'Sumatera Utara', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1537, 100, 'Yogyakarta', 'YO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1538, 101, 'Tehran', 'TEH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1539, 101, 'Qom', 'QOM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1540, 101, 'Markazi', 'MKZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1541, 101, 'Qazvin', 'QAZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1542, 101, 'Gilan', 'GIL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1543, 101, 'Ardabil', 'ARD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1544, 101, 'Zanjan', 'ZAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1545, 101, 'East Azarbaijan', 'EAZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1546, 101, 'West Azarbaijan', 'WEZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1547, 101, 'Kurdistan', 'KRD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1548, 101, 'Hamadan', 'HMD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1549, 101, 'Kermanshah', 'KRM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1550, 101, 'Ilam', 'ILM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1551, 101, 'Lorestan', 'LRS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1552, 101, 'Khuzestan', 'KZT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1555, 101, 'Bushehr', 'BSH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1556, 101, 'Fars', 'FAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1557, 101, 'Hormozgan', 'HRM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1559, 101, 'Kerman', 'KRB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1560, 101, 'Yazd', 'YZD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1561, 101, 'Esfahan', 'EFH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1562, 101, 'Semnan', 'SMN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1563, 101, 'Mazandaran', 'MZD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1564, 101, 'Golestan', 'GLS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1565, 101, 'North Khorasan', 'NKH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1566, 101, 'Razavi Khorasan', 'RKH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1567, 101, 'South Khorasan', 'SKH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1568, 102, 'Baghdad', 'BD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1569, 102, 'Salah ad Din', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1570, 102, 'Diyala', 'DY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1571, 102, 'Wasit', 'WS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1572, 102, 'Maysan', 'MY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1573, 102, 'Al Basrah', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1574, 102, 'Dhi Qar', 'DQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1575, 102, 'Al Muthanna', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1576, 102, 'Al Qadisyah', 'QA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1577, 102, 'Babil', 'BB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1578, 102, 'Al Karbala', 'KB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1579, 102, 'An Najaf', 'NJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1580, 102, 'Al Anbar', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1581, 102, 'Ninawa', 'NN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1582, 102, 'Dahuk', 'DH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1583, 102, 'Arbil', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1584, 102, 'At Ta''mim', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1585, 102, 'As Sulaymaniyah', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1586, 103, 'Carlow', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1587, 103, 'Cavan', 'CV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1588, 103, 'Clare', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1589, 103, 'Cork', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1590, 103, 'Donegal', 'DO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1591, 103, 'Dublin', 'DU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1592, 103, 'Galway', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1593, 103, 'Kerry', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1594, 103, 'Kildare', 'KI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1595, 103, 'Kilkenny', 'KL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1596, 103, 'Laois', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1597, 103, 'Leitrim', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1598, 103, 'Limerick', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1599, 103, 'Longford', 'LO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1600, 103, 'Louth', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1601, 103, 'Mayo', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1602, 103, 'Meath', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1603, 103, 'Monaghan', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1604, 103, 'Offaly', 'OF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1605, 103, 'Roscommon', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1606, 103, 'Sligo', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1607, 103, 'Tipperary', 'TI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1608, 103, 'Waterford', 'WA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1609, 103, 'Westmeath', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1610, 103, 'Wexford', 'WX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1611, 103, 'Wicklow', 'WI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1612, 104, 'Be''er Sheva', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1613, 104, 'Bika''at Hayarden', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1614, 104, 'Eilat and Arava', 'EA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1615, 104, 'Galil', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1616, 104, 'Haifa', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1617, 104, 'Jehuda Mountains', 'JM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1618, 104, 'Jerusalem', 'JE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1619, 104, 'Negev', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1620, 104, 'Semaria', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1621, 104, 'Sharon', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1643, 106, 'Clarendon Parish', 'CLA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1644, 106, 'Hanover Parish', 'HAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1645, 106, 'Kingston Parish', 'KIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1646, 106, 'Manchester Parish', 'MAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1647, 106, 'Portland Parish', 'POR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1648, 106, 'Saint Andrew Parish', 'AND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1649, 106, 'Saint Ann Parish', 'ANN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1650, 106, 'Saint Catherine Parish', 'CAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1652, 106, 'Saint James Parish', 'JAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1653, 106, 'Saint Mary Parish', 'MAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1654, 106, 'Saint Thomas Parish', 'THO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1655, 106, 'Trelawny Parish', 'TRL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1656, 106, 'Westmoreland Parish', 'WML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1657, 107, 'Aichi', 'AI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1658, 107, 'Akita', 'AK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1659, 107, 'Aomori', 'AO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1660, 107, 'Chiba', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1661, 107, 'Ehime', 'EH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1662, 107, 'Fukui', 'FK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1663, 107, 'Fukuoka', 'FU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1664, 107, 'Fukushima', 'FS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1665, 107, 'Gifu', 'GI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1666, 107, 'Gumma', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1667, 107, 'Hiroshima', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1668, 107, 'Hokkaido', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1669, 107, 'Hyogo', 'HY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1670, 107, 'Ibaraki', 'IB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1671, 107, 'Ishikawa', 'IS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1672, 107, 'Iwate', 'IW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1673, 107, 'Kagawa', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1674, 107, 'Kagoshima', 'KG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1675, 107, 'Kanagawa', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1676, 107, 'Kochi', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1677, 107, 'Kumamoto', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1678, 107, 'Kyoto', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1679, 107, 'Mie', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1680, 107, 'Miyagi', 'MY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1681, 107, 'Miyazaki', 'MZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1682, 107, 'Nagano', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1683, 107, 'Nagasaki', 'NG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1684, 107, 'Nara', 'NR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1685, 107, 'Niigata', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1686, 107, 'Oita', 'OI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1687, 107, 'Okayama', 'OK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1688, 107, 'Okinawa', 'ON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1689, 107, 'Osaka', 'OS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1690, 107, 'Saga', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1691, 107, 'Saitama', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1692, 107, 'Shiga', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1693, 107, 'Shimane', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1694, 107, 'Shizuoka', 'SZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1695, 107, 'Tochigi', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1696, 107, 'Tokushima', 'TS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1697, 107, 'Tokyo', 'TK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1698, 107, 'Tottori', 'TT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1699, 107, 'Toyama', 'TY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1700, 107, 'Wakayama', 'WA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1701, 107, 'Yamagata', 'YA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1702, 107, 'Yamaguchi', 'YM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1703, 107, 'Yamanashi', 'YN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1704, 108, '''Amman', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1705, 108, 'Ajlun', 'AJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1706, 108, 'Al ''Aqabah', 'AA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1707, 108, 'Al Balqa''', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1708, 108, 'Al Karak', 'AK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1709, 108, 'Al Mafraq', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1710, 108, 'At Tafilah', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1711, 108, 'Az Zarqa''', 'AZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1712, 108, 'Irbid', 'IR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1713, 108, 'Jarash', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1714, 108, 'Ma''an', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1715, 108, 'Madaba', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1716, 109, 'Almaty', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1717, 109, 'Almaty City', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1718, 109, 'Aqmola', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1719, 109, 'Aqtobe', 'AQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1720, 109, 'Astana City', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1721, 109, 'Atyrau', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1722, 109, 'Batys Qazaqstan', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1723, 109, 'Bayqongyr City', 'BY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1724, 109, 'Mangghystau', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1726, 109, 'Pavlodar', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1727, 109, 'Qaraghandy', 'QA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1728, 109, 'Qostanay', 'QO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1729, 109, 'Qyzylorda', 'QY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1732, 109, 'Zhambyl', 'ZH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1733, 110, 'Central', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1734, 110, 'Coast', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1735, 110, 'Eastern', 'EA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1736, 110, 'Nairobi Area', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1737, 110, 'North Eastern', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1738, 110, 'Nyanza', 'NY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1739, 110, 'Rift Valley', 'RV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1740, 110, 'Western', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1741, 111, 'Abaiang', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1742, 111, 'Abemama', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1743, 111, 'Aranuka', 'AK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1744, 111, 'Arorae', 'AO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1745, 111, 'Banaba', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1746, 111, 'Beru', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1747, 111, 'Butaritari', 'bT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1748, 111, 'Kanton', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1749, 111, 'Kiritimati', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1750, 111, 'Kuria', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1751, 111, 'Maiana', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1752, 111, 'Makin', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1753, 111, 'Marakei', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1754, 111, 'Nikunau', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1755, 111, 'Nonouti', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1756, 111, 'Onotoa', 'ON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1757, 111, 'Tabiteuea', 'TT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1758, 111, 'Tabuaeran', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1759, 111, 'Tamana', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1760, 111, 'Tarawa', 'TW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1761, 111, 'Teraina', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1762, 112, 'Chagang-do', 'CHA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1763, 112, 'Hamgyong-bukto', 'HAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1764, 112, 'Hamgyong-namdo', 'HAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1765, 112, 'Hwanghae-bukto', 'HWB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1766, 112, 'Hwanghae-namdo', 'HWN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1767, 112, 'Kangwon-do', 'KAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');
INSERT INTO `mw_zone` (`zone_id`, `country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(1768, 112, 'P''yongan-bukto', 'PYB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1769, 112, 'P''yongan-namdo', 'PYN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1772, 112, 'P''yongyang Special City', 'PYO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1773, 113, 'Ch''ungch''ong-bukto', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1774, 113, 'Ch''ungch''ong-namdo', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1775, 113, 'Cheju-do', 'CD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1776, 113, 'Cholla-bukto', 'CB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1777, 113, 'Cholla-namdo', 'CN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1778, 113, 'Inch''on-gwangyoksi', 'IG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1779, 113, 'Kangwon-do', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1781, 113, 'Kyonggi-do', 'KD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1782, 113, 'Kyongsang-bukto', 'KB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1783, 113, 'Kyongsang-namdo', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1785, 113, 'Soul-t''ukpyolsi', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1788, 114, 'Al ''Asimah', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1789, 114, 'Al Ahmadi', 'AA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1790, 114, 'Al Farwaniyah', 'AF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1791, 114, 'Al Jahra''', 'AJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1792, 114, 'Hawalli', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1793, 115, 'Bishkek', 'GB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1794, 115, 'Batken', 'B', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1795, 115, 'Chu', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1796, 115, 'Jalal-Abad', 'J', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1797, 115, 'Naryn', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1798, 115, 'Osh', 'O', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1799, 115, 'Talas', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1800, 115, 'Ysyk-Kol', 'Y', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1801, 116, 'Vientiane', 'VT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1802, 116, 'Attapu', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1803, 116, 'Bokeo', 'BK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1804, 116, 'Bolikhamxai', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1805, 116, 'Champasak', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1806, 116, 'Houaphan', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1807, 116, 'Khammouan', 'KH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1808, 116, 'Louang Namtha', 'LM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1809, 116, 'Louangphabang', 'LP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1810, 116, 'Oudomxai', 'OU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1811, 116, 'Phongsali', 'PH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1812, 116, 'Salavan', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1813, 116, 'Savannakhet', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1814, 116, 'Vientiane', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1815, 116, 'Xaignabouli', 'XA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1816, 116, 'Xekong', 'XE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1817, 116, 'Xiangkhoang', 'XI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1818, 116, 'Xaisomboun', 'XN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1819, 117, 'Aizkraukles Rajons', 'AIZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1820, 117, 'Aluksnes Rajons', 'ALU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1821, 117, 'Balvu Rajons', 'BAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1822, 117, 'Bauskas Rajons', 'BAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1823, 117, 'Cesu Rajons', 'CES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1824, 117, 'Daugavpils Rajons', 'DGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1825, 117, 'Dobeles Rajons', 'DOB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1826, 117, 'Gulbenes Rajons', 'GUL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1827, 117, 'Jekabpils Rajons', 'JEK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1828, 117, 'Jelgavas Rajons', 'JGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1829, 117, 'Kraslavas Rajons', 'KRA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1830, 117, 'Kuldigas Rajons', 'KUL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1831, 117, 'Liepajas Rajons', 'LPR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1832, 117, 'Limbazu Rajons', 'LIM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1833, 117, 'Ludzas Rajons', 'LUD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1834, 117, 'Madonas Rajons', 'MAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1835, 117, 'Ogres Rajons', 'OGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1836, 117, 'Preilu Rajons', 'PRE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1837, 117, 'Rezeknes Rajons', 'RZR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1838, 117, 'Rigas Rajons', 'RGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1839, 117, 'Saldus Rajons', 'SAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1840, 117, 'Talsu Rajons', 'TAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1841, 117, 'Tukuma Rajons', 'TUK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1842, 117, 'Valkas Rajons', 'VLK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1843, 117, 'Valmieras Rajons', 'VLM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1844, 117, 'Ventspils Rajons', 'VSR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1845, 117, 'Daugavpils', 'DGV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1846, 117, 'Jelgava', 'JGV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1847, 117, 'Jurmala', 'JUR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1848, 117, 'Liepaja', 'LPK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1849, 117, 'Rezekne', 'RZK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1850, 117, 'Riga', 'RGA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1851, 117, 'Ventspils', 'VSL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1852, 119, 'Berea', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1853, 119, 'Butha-Buthe', 'BB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1854, 119, 'Leribe', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1855, 119, 'Mafeteng', 'MF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1856, 119, 'Maseru', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1857, 119, 'Mohale''s Hoek', 'MH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1858, 119, 'Mokhotlong', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1859, 119, 'Qacha''s Nek', 'QN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1860, 119, 'Quthing', 'QT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1861, 119, 'Thaba-Tseka', 'TT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1862, 120, 'Bomi', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1863, 120, 'Bong', 'BG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1864, 120, 'Grand Bassa', 'GB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1865, 120, 'Grand Cape Mount', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1866, 120, 'Grand Gedeh', 'GG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1867, 120, 'Grand Kru', 'GK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1868, 120, 'Lofa', 'LO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1869, 120, 'Margibi', 'MG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1870, 120, 'Maryland', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1871, 120, 'Montserrado', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1872, 120, 'Nimba', 'NB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1873, 120, 'River Cess', 'RC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1874, 120, 'Sinoe', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1875, 121, 'Ajdabiya', 'AJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1876, 121, 'Al ''Aziziyah', 'AZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1877, 121, 'Al Fatih', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1879, 121, 'Al Jufrah', 'JU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1880, 121, 'Al Khums', 'KH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1881, 121, 'Al Kufrah', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1882, 121, 'An Nuqat al Khams', 'NK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1883, 121, 'Ash Shati''', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1884, 121, 'Awbari', 'AW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1885, 121, 'Az Zawiyah', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1886, 121, 'Banghazi', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1887, 121, 'Darnah', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1888, 121, 'Ghadamis', 'GD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1889, 121, 'Gharyan', 'GY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1890, 121, 'Misratah', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1891, 121, 'Murzuq', 'MZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1892, 121, 'Sabha', 'SB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1893, 121, 'Sawfajjin', 'SW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1894, 121, 'Surt', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1896, 121, 'Tarhunah', 'TH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1897, 121, 'Tubruq', 'TU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1898, 121, 'Yafran', 'YA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1899, 121, 'Zlitan', 'ZL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1900, 122, 'Vaduz', 'V', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1901, 122, 'Schaan', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1902, 122, 'Balzers', 'B', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1903, 122, 'Triesen', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1904, 122, 'Eschen', 'E', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1905, 122, 'Mauren', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1906, 122, 'Triesenberg', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1907, 122, 'Ruggell', 'R', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1908, 122, 'Gamprin', 'G', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1909, 122, 'Schellenberg', 'L', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1910, 122, 'Planken', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1911, 123, 'Alytus', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1912, 123, 'Kaunas', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1913, 123, 'Klaipeda', 'KL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1914, 123, 'Marijampole', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1915, 123, 'Panevezys', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1916, 123, 'Siauliai', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1917, 123, 'Taurage', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1918, 123, 'Telsiai', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1919, 123, 'Utena', 'UT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1920, 123, 'Vilnius', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1921, 124, 'Diekirch', 'DD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1922, 124, 'Clervaux', 'DC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1923, 124, 'Redange', 'DR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1924, 124, 'Vianden', 'DV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1925, 124, 'Wiltz', 'DW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1926, 124, 'Grevenmacher', 'GG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1927, 124, 'Echternach', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1928, 124, 'Remich', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1929, 124, 'Luxembourg', 'LL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1930, 124, 'Capellen', 'LC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1931, 124, 'Esch-sur-Alzette', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1932, 124, 'Mersch', 'LM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1934, 125, 'St. Anthony Parish', 'ANT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1936, 125, 'Cathedral Parish', 'CAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1937, 125, 'St. Lawrence Parish', 'LAW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1938, 127, 'Antananarivo', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1939, 127, 'Antsiranana', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1940, 127, 'Fianarantsoa', 'FN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1941, 127, 'Mahajanga', 'MJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1942, 127, 'Toamasina', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1943, 127, 'Toliara', 'TL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1944, 128, 'Balaka', 'BLK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1945, 128, 'Blantyre', 'BLT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1946, 128, 'Chikwawa', 'CKW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1947, 128, 'Chiradzulu', 'CRD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1948, 128, 'Chitipa', 'CTP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1949, 128, 'Dedza', 'DDZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1950, 128, 'Dowa', 'DWA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1951, 128, 'Karonga', 'KRG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1952, 128, 'Kasungu', 'KSG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1953, 128, 'Likoma', 'LKM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1954, 128, 'Lilongwe', 'LLG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1955, 128, 'Machinga', 'MCG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1956, 128, 'Mangochi', 'MGC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1957, 128, 'Mchinji', 'MCH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1958, 128, 'Mulanje', 'MLJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1959, 128, 'Mwanza', 'MWZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1960, 128, 'Mzimba', 'MZM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1961, 128, 'Ntcheu', 'NTU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1962, 128, 'Nkhata Bay', 'NKB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1963, 128, 'Nkhotakota', 'NKH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1964, 128, 'Nsanje', 'NSJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1965, 128, 'Ntchisi', 'NTI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1966, 128, 'Phalombe', 'PHL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1967, 128, 'Rumphi', 'RMP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1968, 128, 'Salima', 'SLM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1969, 128, 'Thyolo', 'THY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1970, 128, 'Zomba', 'ZBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1971, 129, 'Johor', 'JO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1972, 129, 'Kedah', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1973, 129, 'Kelantan', 'KL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1974, 129, 'Labuan', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1975, 129, 'Melaka', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1976, 129, 'Negeri Sembilan', 'NS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1977, 129, 'Pahang', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1978, 129, 'Perak', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1979, 129, 'Perlis', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1980, 129, 'Pulau Pinang', 'PP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1981, 129, 'Sabah', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1982, 129, 'Sarawak', 'SR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1983, 129, 'Selangor', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1984, 129, 'Terengganu', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1985, 129, 'Wilayah Persekutuan', 'WP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1992, 130, 'Faadhippolhu', 'FAA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1993, 130, 'Male Atoll', 'MAA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1996, 130, 'Felidhe Atoll', 'FEA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1997, 130, 'Mulaku Atoll', 'MUA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2000, 130, 'Kolhumadulu', 'KLH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2001, 130, 'Hadhdhunmathi', 'HDH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2004, 130, 'Fua Mulaku', 'FMU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2005, 130, 'Addu', 'ADD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2006, 131, 'Gao', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2007, 131, 'Kayes', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2008, 131, 'Kidal', 'KD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2009, 131, 'Koulikoro', 'KL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2010, 131, 'Mopti', 'MP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2011, 131, 'Segou', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2012, 131, 'Sikasso', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2013, 131, 'Tombouctou', 'TB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2014, 131, 'Bamako Capital District', 'CD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2015, 132, 'Attard', 'ATT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2016, 132, 'Balzan', 'BAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2017, 132, 'Birgu', 'BGU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2018, 132, 'Birkirkara', 'BKK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2019, 132, 'Birzebbuga', 'BRZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2020, 132, 'Bormla', 'BOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2021, 132, 'Dingli', 'DIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2022, 132, 'Fgura', 'FGU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2023, 132, 'Floriana', 'FLO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2024, 132, 'Gudja', 'GDJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2025, 132, 'Gzira', 'GZR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2026, 132, 'Gargur', 'GRG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2027, 132, 'Gaxaq', 'GXQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2028, 132, 'Hamrun', 'HMR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2029, 132, 'Iklin', 'IKL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2030, 132, 'Isla', 'ISL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2031, 132, 'Kalkara', 'KLK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2032, 132, 'Kirkop', 'KRK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2033, 132, 'Lija', 'LIJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2034, 132, 'Luqa', 'LUQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2035, 132, 'Marsa', 'MRS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2036, 132, 'Marsaskala', 'MKL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2037, 132, 'Marsaxlokk', 'MXL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2038, 132, 'Mdina', 'MDN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2039, 132, 'Melliea', 'MEL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2040, 132, 'Mgarr', 'MGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2041, 132, 'Mosta', 'MST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2042, 132, 'Mqabba', 'MQA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2043, 132, 'Msida', 'MSI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2044, 132, 'Mtarfa', 'MTF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2045, 132, 'Naxxar', 'NAX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2046, 132, 'Paola', 'PAO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2047, 132, 'Pembroke', 'PEM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2048, 132, 'Pieta', 'PIE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2049, 132, 'Qormi', 'QOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2050, 132, 'Qrendi', 'QRE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2051, 132, 'Rabat', 'RAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2052, 132, 'Safi', 'SAF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2053, 132, 'San Giljan', 'SGI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2054, 132, 'Santa Lucija', 'SLU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2056, 132, 'San Gwann', 'SGW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2057, 132, 'Santa Venera', 'SVE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2058, 132, 'Siggiewi', 'SIG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2059, 132, 'Sliema', 'SLM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2060, 132, 'Swieqi', 'SWQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2061, 132, 'Ta Xbiex', 'TXB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2062, 132, 'Tarxien', 'TRX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2063, 132, 'Valletta', 'VLT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2064, 132, 'Xgajra', 'XGJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2065, 132, 'Zabbar', 'ZBR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2066, 132, 'Zebbug', 'ZBG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2067, 132, 'Zejtun', 'ZJT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2068, 132, 'Zurrieq', 'ZRQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2069, 132, 'Fontana', 'FNT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2070, 132, 'Ghajnsielem', 'GHJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2071, 132, 'Gharb', 'GHR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2072, 132, 'Ghasri', 'GHS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2073, 132, 'Kercem', 'KRC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2074, 132, 'Munxar', 'MUN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2075, 132, 'Nadur', 'NAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2076, 132, 'Qala', 'QAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2077, 132, 'Victoria', 'VIC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2078, 132, 'San Lawrenz', 'SLA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2079, 132, 'Sannat', 'SNT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2080, 132, 'Xagra', 'ZAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2081, 132, 'Xewkija', 'XEW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2082, 132, 'Zebbug', 'ZEB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2083, 133, 'Ailinginae', 'ALG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2084, 133, 'Ailinglaplap', 'ALL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2085, 133, 'Ailuk', 'ALK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2086, 133, 'Arno', 'ARN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2087, 133, 'Aur', 'AUR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2088, 133, 'Bikar', 'BKR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2089, 133, 'Bikini', 'BKN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2090, 133, 'Bokak', 'BKK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2091, 133, 'Ebon', 'EBN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2092, 133, 'Enewetak', 'ENT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2093, 133, 'Erikub', 'EKB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2094, 133, 'Jabat', 'JBT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2095, 133, 'Jaluit', 'JLT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2096, 133, 'Jemo', 'JEM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2097, 133, 'Kili', 'KIL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2098, 133, 'Kwajalein', 'KWJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2099, 133, 'Lae', 'LAE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2100, 133, 'Lib', 'LIB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2101, 133, 'Likiep', 'LKP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2102, 133, 'Majuro', 'MJR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2103, 133, 'Maloelap', 'MLP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2104, 133, 'Mejit', 'MJT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2105, 133, 'Mili', 'MIL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2106, 133, 'Namorik', 'NMK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2107, 133, 'Namu', 'NAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2108, 133, 'Rongelap', 'RGL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2109, 133, 'Rongrik', 'RGK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2110, 133, 'Toke', 'TOK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2111, 133, 'Ujae', 'UJA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2112, 133, 'Ujelang', 'UJL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2113, 133, 'Utirik', 'UTK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2114, 133, 'Wotho', 'WTH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2115, 133, 'Wotje', 'WTJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2116, 135, 'Adrar', 'AD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2117, 135, 'Assaba', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2118, 135, 'Brakna', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2120, 135, 'Gorgol', 'GO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2121, 135, 'Guidimaka', 'GM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2122, 135, 'Hodh Ech Chargui', 'HC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2123, 135, 'Hodh El Gharbi', 'HG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2124, 135, 'Inchiri', 'IN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2125, 135, 'Tagant', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2126, 135, 'Tiris Zemmour', 'TZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2127, 135, 'Trarza', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2128, 135, 'Nouakchott', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2130, 136, 'Curepipe', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2131, 136, 'Port Louis', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2132, 136, 'Quatre Bornes', 'QB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2133, 136, 'Vacoas-Phoenix', 'VP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2134, 136, 'Agalega Islands', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2136, 136, 'Rodrigues', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2137, 136, 'Black River', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2138, 136, 'Flacq', 'FL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2139, 136, 'Grand Port', 'GP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2140, 136, 'Moka', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2141, 136, 'Pamplemousses', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2142, 136, 'Plaines Wilhems', 'PW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2143, 136, 'Port Louis', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2144, 136, 'Riviere du Rempart', 'RR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2145, 136, 'Savanne', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2146, 138, 'Baja California Norte', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2147, 138, 'Baja California Sur', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2148, 138, 'Campeche', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2149, 138, 'Chiapas', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2150, 138, 'Chihuahua', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2152, 138, 'Colima', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2153, 138, 'Distrito Federal', 'DF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2154, 138, 'Durango', 'DU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2155, 138, 'Guanajuato', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2156, 138, 'Guerrero', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2157, 138, 'Hidalgo', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2158, 138, 'Jalisco', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2159, 138, 'Mexico', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2160, 138, 'Michoacan de Ocampo', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2161, 138, 'Morelos', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2162, 138, 'Nayarit', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2163, 138, 'Nuevo Leon', 'NL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2164, 138, 'Oaxaca', 'OA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2165, 138, 'Puebla', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2166, 138, 'Queretaro de Arteaga', 'QA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2167, 138, 'Quintana Roo', 'QR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2168, 138, 'San Luis Potosi', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2169, 138, 'Sinaloa', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2170, 138, 'Sonora', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2171, 138, 'Tabasco', 'TB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2172, 138, 'Tamaulipas', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2173, 138, 'Tlaxcala', 'TL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2174, 138, 'Veracruz-Llave', 'VE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2175, 138, 'Yucatan', 'YU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2176, 138, 'Zacatecas', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2177, 139, 'Chuuk', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2178, 139, 'Kosrae', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2179, 139, 'Pohnpei', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2180, 139, 'Yap', 'Y', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2181, 140, 'Gagauzia', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2182, 140, 'Chisinau', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2183, 140, 'Balti', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2184, 140, 'Cahul', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2185, 140, 'Edinet', 'ED', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2186, 140, 'Lapusna', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2187, 140, 'Orhei', 'OR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2188, 140, 'Soroca', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2189, 140, 'Tighina', 'TI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2190, 140, 'Ungheni', 'UN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2191, 140, 'Stâ€šnga Nistrului', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2192, 141, 'Fontvieille', 'FV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2193, 141, 'La Condamine', 'LC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2194, 141, 'Monaco-Ville', 'MV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2195, 141, 'Monte-Carlo', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2196, 142, 'Ulanbaatar', '1', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2197, 142, 'Orhon', '035', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2198, 142, 'Darhan uul', '037', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2199, 142, 'Hentiy', '039', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2200, 142, 'Hovsgol', '041', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2201, 142, 'Hovd', '043', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2202, 142, 'Uvs', '046', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2203, 142, 'Tov', '047', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2204, 142, 'Selenge', '049', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2205, 142, 'Suhbaatar', '051', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2206, 142, 'Omnogovi', '053', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2207, 142, 'Ovorhangay', '055', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2208, 142, 'Dzavhan', '057', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2209, 142, 'DundgovL', '059', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2210, 142, 'Dornod', '061', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2211, 142, 'Dornogov', '063', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2212, 142, 'Govi-Sumber', '064', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2213, 142, 'Govi-Altay', '065', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2214, 142, 'Bulgan', '067', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2215, 142, 'Bayanhongor', '069', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2216, 142, 'Bayan-Olgiy', '071', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2217, 142, 'Arhangay', '073', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2218, 143, 'Saint Anthony', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2219, 143, 'Saint Georges', 'G', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2220, 143, 'Saint Peter', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2221, 144, 'Agadir', 'AGD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2222, 144, 'Al Hoceima', 'HOC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2223, 144, 'Azilal', 'AZI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2224, 144, 'Beni Mellal', 'BME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2225, 144, 'Ben Slimane', 'BSL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2226, 144, 'Boulemane', 'BLM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2227, 144, 'Casablanca', 'CBL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2228, 144, 'Chaouen', 'CHA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2229, 144, 'El Jadida', 'EJA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2231, 144, 'Er Rachidia', 'ERA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2232, 144, 'Essaouira', 'ESS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2233, 144, 'Fes', 'FES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2234, 144, 'Figuig', 'FIG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2235, 144, 'Guelmim', 'GLM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2236, 144, 'Ifrane', 'IFR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2237, 144, 'Kenitra', 'KEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2238, 144, 'Khemisset', 'KHM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2239, 144, 'Khenifra', 'KHN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2240, 144, 'Khouribga', 'KHO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2241, 144, 'Laayoune', 'LYN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2242, 144, 'Larache', 'LAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2243, 144, 'Marrakech', 'MRK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2244, 144, 'Meknes', 'MKN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2245, 144, 'Nador', 'NAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2246, 144, 'Ouarzazate', 'ORZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2247, 144, 'Oujda', 'OUJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2248, 144, 'Rabat-Sale', 'RSA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2249, 144, 'Safi', 'SAF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2250, 144, 'Settat', 'SET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2251, 144, 'Sidi Kacem', 'SKA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2252, 144, 'Tangier', 'TGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2253, 144, 'Tan-Tan', 'TAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2254, 144, 'Taounate', 'TAO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2255, 144, 'Taroudannt', 'TRD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2256, 144, 'Tata', 'TAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2257, 144, 'Taza', 'TAZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2258, 144, 'Tetouan', 'TET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2259, 144, 'Tiznit', 'TIZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2260, 144, 'Ad Dakhla', 'ADK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2261, 144, 'Boujdour', 'BJD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2262, 144, 'Es Smara', 'ESM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2263, 145, 'Cabo Delgado', 'CD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2264, 145, 'Gaza', 'GZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2265, 145, 'Inhambane', 'IN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2266, 145, 'Manica', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2267, 145, 'Maputo (city)', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2268, 145, 'Maputo', 'MP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2269, 145, 'Nampula', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2270, 145, 'Niassa', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2271, 145, 'Sofala', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2272, 145, 'Tete', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2273, 145, 'Zambezia', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2274, 146, 'Ayeyarwady', 'AY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2275, 146, 'Bago', 'BG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2276, 146, 'Magway', 'MG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2277, 146, 'Mandalay', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2278, 146, 'Sagaing', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2279, 146, 'Tanintharyi', 'TN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2280, 146, 'Yangon', 'YG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2281, 146, 'Chin State', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2282, 146, 'Kachin State', 'KC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2283, 146, 'Kayah State', 'KH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2284, 146, 'Kayin State', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2285, 146, 'Mon State', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2286, 146, 'Rakhine State', 'RK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2287, 146, 'Shan State', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2288, 147, 'Caprivi', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2289, 147, 'Erongo', 'ER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2290, 147, 'Hardap', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2291, 147, 'Karas', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2292, 147, 'Kavango', 'KV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2293, 147, 'Khomas', 'KH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2294, 147, 'Kunene', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2295, 147, 'Ohangwena', 'OW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2296, 147, 'Omaheke', 'OK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2297, 147, 'Omusati', 'OT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2298, 147, 'Oshana', 'ON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2299, 147, 'Oshikoto', 'OO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2300, 147, 'Otjozondjupa', 'OJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2301, 148, 'Aiwo', 'AO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2302, 148, 'Anabar', 'AA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2303, 148, 'Anetan', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2304, 148, 'Anibare', 'AI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2305, 148, 'Baiti', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2306, 148, 'Boe', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2307, 148, 'Buada', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2308, 148, 'Denigomodu', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2309, 148, 'Ewa', 'EW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2310, 148, 'Ijuw', 'IJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2311, 148, 'Meneng', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2312, 148, 'Nibok', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2313, 148, 'Uaboe', 'UA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2314, 148, 'Yaren', 'YA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2315, 149, 'Bagmati', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2316, 149, 'Bheri', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2317, 149, 'Dhawalagiri', 'DH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2318, 149, 'Gandaki', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2319, 149, 'Janakpur', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2320, 149, 'Karnali', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2321, 149, 'Kosi', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2322, 149, 'Lumbini', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2323, 149, 'Mahakali', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2324, 149, 'Mechi', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2325, 149, 'Narayani', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2326, 149, 'Rapti', 'RA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2327, 149, 'Sagarmatha', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2328, 149, 'Seti', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2329, 150, 'Drenthe', 'DR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2330, 150, 'Flevoland', 'FL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2331, 150, 'Friesland', 'FR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2332, 150, 'Gelderland', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2333, 150, 'Groningen', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2334, 150, 'Limburg', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2335, 150, 'Noord Brabant', 'NB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2336, 150, 'Noord Holland', 'NH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2337, 150, 'Overijssel', 'OV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2338, 150, 'Utrecht', 'UT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2339, 150, 'Zeeland', 'ZE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2340, 150, 'Zuid Holland', 'ZH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2341, 152, 'Iles Loyaute', 'L', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2342, 152, 'Nord', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2343, 152, 'Sud', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2344, 153, 'Auckland', 'AUK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');
INSERT INTO `mw_zone` (`zone_id`, `country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(2345, 153, 'Bay of Plenty', 'BOP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2346, 153, 'Canterbury', 'CAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2347, 153, 'Coromandel', 'COR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2348, 153, 'Gisborne', 'GIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2349, 153, 'Fiordland', 'FIO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2350, 153, 'Hawke''s Bay', 'HKB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2351, 153, 'Marlborough', 'MBH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2354, 153, 'Nelson', 'NSN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2355, 153, 'Northland', 'NTL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2356, 153, 'Otago', 'OTA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2357, 153, 'Southland', 'STL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2358, 153, 'Taranaki', 'TKI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2359, 153, 'Wellington', 'WGN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2360, 153, 'Waikato', 'WKO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2361, 153, 'Wairprarapa', 'WAI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2362, 153, 'West Coast', 'WTC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2363, 154, 'Atlantico Norte', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2364, 154, 'Atlantico Sur', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2365, 154, 'Boaco', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2366, 154, 'Carazo', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2367, 154, 'Chinandega', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2368, 154, 'Chontales', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2369, 154, 'Esteli', 'ES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2370, 154, 'Granada', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2371, 154, 'Jinotega', 'JI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2372, 154, 'Leon', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2373, 154, 'Madriz', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2374, 154, 'Managua', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2375, 154, 'Masaya', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2376, 154, 'Matagalpa', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2377, 154, 'Nuevo Segovia', 'NS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2378, 154, 'Rio San Juan', 'RS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2379, 154, 'Rivas', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2380, 155, 'Agadez', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2381, 155, 'Diffa', 'DF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2382, 155, 'Dosso', 'DS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2383, 155, 'Maradi', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2384, 155, 'Niamey', 'NM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2385, 155, 'Tahoua', 'TH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2386, 155, 'Tillaberi', 'TL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2387, 155, 'Zinder', 'ZD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2388, 156, 'Abia', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2390, 156, 'Adamawa', 'AD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2391, 156, 'Akwa Ibom', 'AK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2392, 156, 'Anambra', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2393, 156, 'Bauchi', 'BC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2394, 156, 'Bayelsa', 'BY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2395, 156, 'Benue', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2396, 156, 'Borno', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2397, 156, 'Cross River', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2398, 156, 'Delta', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2399, 156, 'Ebonyi', 'EB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2400, 156, 'Edo', 'ED', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2401, 156, 'Ekiti', 'EK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2402, 156, 'Enugu', 'EN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2403, 156, 'Gombe', 'GO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2404, 156, 'Imo', 'IM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2405, 156, 'Jigawa', 'JI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2406, 156, 'Kaduna', 'KD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2407, 156, 'Kano', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2408, 156, 'Katsina', 'KT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2409, 156, 'Kebbi', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2410, 156, 'Kogi', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2411, 156, 'Kwara', 'KW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2412, 156, 'Lagos', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2413, 156, 'Nassarawa', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2414, 156, 'Niger', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2415, 156, 'Ogun', 'OG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2416, 156, 'Ondo', 'ONG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2417, 156, 'Osun', 'OS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2418, 156, 'Oyo', 'OY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2419, 156, 'Plateau', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2420, 156, 'Rivers', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2421, 156, 'Sokoto', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2422, 156, 'Taraba', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2423, 156, 'Yobe', 'YO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2424, 156, 'Zamfara', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2425, 159, 'Northern Islands', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2426, 159, 'Rota', 'R', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2427, 159, 'Saipan', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2428, 159, 'Tinian', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2429, 160, 'Akershus', 'AK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2430, 160, 'Aust-Agder', 'AA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2431, 160, 'Buskerud', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2432, 160, 'Finnmark', 'FM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2433, 160, 'Hedmark', 'HM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2434, 160, 'Hordaland', 'HL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2435, 160, 'More og Romdal', 'MR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2436, 160, 'Nord-Trondelag', 'NT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2437, 160, 'Nordland', 'NL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2438, 160, 'Ostfold', 'OF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2439, 160, 'Oppland', 'OP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2440, 160, 'Oslo', 'OL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2441, 160, 'Rogaland', 'RL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2442, 160, 'Sor-Trondelag', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2443, 160, 'Sogn og Fjordane', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2444, 160, 'Svalbard', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2445, 160, 'Telemark', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2446, 160, 'Troms', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2447, 160, 'Vest-Agder', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2448, 160, 'Vestfold', 'VF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2449, 161, 'Ad Dakhiliyah', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2450, 161, 'Al Batinah', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2451, 161, 'Al Wusta', 'WU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2452, 161, 'Ash Sharqiyah', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2453, 161, 'Az Zahirah', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2454, 161, 'Masqat', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2455, 161, 'Musandam', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2456, 161, 'Zufar', 'ZU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2457, 162, 'Balochistan', 'B', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2459, 162, 'Islamabad Capital Territory', 'I', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2460, 162, 'North-West Frontier', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2461, 162, 'Punjab', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2462, 162, 'Sindh', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2463, 163, 'Aimeliik', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2464, 163, 'Airai', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2465, 163, 'Angaur', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2466, 163, 'Hatohobei', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2467, 163, 'Kayangel', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2468, 163, 'Koror', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2469, 163, 'Melekeok', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2470, 163, 'Ngaraard', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2471, 163, 'Ngarchelong', 'NG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2472, 163, 'Ngardmau', 'ND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2473, 163, 'Ngatpang', 'NT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2474, 163, 'Ngchesar', 'NC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2475, 163, 'Ngeremlengui', 'NR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2476, 163, 'Ngiwal', 'NW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2477, 163, 'Peleliu', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2478, 163, 'Sonsorol', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2479, 164, 'Bocas del Toro', 'BT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2480, 164, 'Chiriqui', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2481, 164, 'Cocle', 'CC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2482, 164, 'Colon', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2483, 164, 'Darien', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2484, 164, 'Herrera', 'HE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2485, 164, 'Los Santos', 'LS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2486, 164, 'Panama', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2487, 164, 'San Blas', 'SB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2488, 164, 'Veraguas', 'VG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2489, 165, 'Bougainville', 'BV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2490, 165, 'Central', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2491, 165, 'Chimbu', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2492, 165, 'Eastern Highlands', 'EH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2493, 165, 'East New Britain', 'EB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2494, 165, 'East Sepik', 'ES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2495, 165, 'Enga', 'EN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2496, 165, 'Gulf', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2497, 165, 'Madang', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2498, 165, 'Manus', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2499, 165, 'Milne Bay', 'MB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2500, 165, 'Morobe', 'MR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2501, 165, 'National Capital', 'NC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2502, 165, 'New Ireland', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2503, 165, 'Northern', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2504, 165, 'Sandaun', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2505, 165, 'Southern Highlands', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2506, 165, 'Western', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2507, 165, 'Western Highlands', 'WH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2508, 165, 'West New Britain', 'WB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2509, 166, 'Alto Paraguay', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2510, 166, 'Alto Parana', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2511, 166, 'Amambay', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2512, 166, 'Asuncion', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2513, 166, 'Boqueron', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2514, 166, 'Caaguazu', 'CG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2515, 166, 'Caazapa', 'CZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2516, 166, 'Canindeyu', 'CN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2517, 166, 'Central', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2518, 166, 'Concepcion', 'CC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2519, 166, 'Cordillera', 'CD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2520, 166, 'Guaira', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2521, 166, 'Itapua', 'IT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2522, 166, 'Misiones', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2523, 166, 'Neembucu', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2524, 166, 'Paraguari', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2525, 166, 'Presidente Hayes', 'PH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2526, 166, 'San Pedro', 'SP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2527, 167, 'Amazonas', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2528, 167, 'Ancash', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2529, 167, 'Apurimac', 'AP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2530, 167, 'Arequipa', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2531, 167, 'Ayacucho', 'AY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2532, 167, 'Cajamarca', 'CJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2533, 167, 'Callao', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2534, 167, 'Cusco', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2535, 167, 'Huancavelica', 'HV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2536, 167, 'Huanuco', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2537, 167, 'Ica', 'IC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2538, 167, 'Junin', 'JU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2539, 167, 'La Libertad', 'LD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2540, 167, 'Lambayeque', 'LY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2541, 167, 'Lima', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2542, 167, 'Loreto', 'LO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2543, 167, 'Madre de Dios', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2544, 167, 'Moquegua', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2545, 167, 'Pasco', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2546, 167, 'Piura', 'PI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2547, 167, 'Puno', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2548, 167, 'San Martin', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2549, 167, 'Tacna', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2550, 167, 'Tumbes', 'TU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2551, 167, 'Ucayali', 'UC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2552, 168, 'Abra', 'ABR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2553, 168, 'Agusan del Norte', 'ANO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2554, 168, 'Agusan del Sur', 'ASU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2555, 168, 'Aklan', 'AKL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2556, 168, 'Albay', 'ALB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2557, 168, 'Antique', 'ANT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2558, 168, 'Apayao', 'APY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2559, 168, 'Aurora', 'AUR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2560, 168, 'Basilan', 'BAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2561, 168, 'Bataan', 'BTA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2562, 168, 'Batanes', 'BTE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2563, 168, 'Batangas', 'BTG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2564, 168, 'Biliran', 'BLR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2565, 168, 'Benguet', 'BEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2566, 168, 'Bohol', 'BOL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2567, 168, 'Bukidnon', 'BUK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2568, 168, 'Bulacan', 'BUL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2569, 168, 'Cagayan', 'CAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2570, 168, 'Camarines Norte', 'CNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2571, 168, 'Camarines Sur', 'CSU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2572, 168, 'Camiguin', 'CAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2573, 168, 'Capiz', 'CAP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2574, 168, 'Catanduanes', 'CAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2575, 168, 'Cavite', 'CAV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2576, 168, 'Cebu', 'CEB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2577, 168, 'Compostela', 'CMP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2578, 168, 'Davao del Norte', 'DNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2579, 168, 'Davao del Sur', 'DSU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2580, 168, 'Davao Oriental', 'DOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2581, 168, 'Eastern Samar', 'ESA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2582, 168, 'Guimaras', 'GUI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2583, 168, 'Ifugao', 'IFU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2584, 168, 'Ilocos Norte', 'INO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2585, 168, 'Ilocos Sur', 'ISU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2586, 168, 'Iloilo', 'ILO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2587, 168, 'Isabela', 'ISA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2588, 168, 'Kalinga', 'KAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2589, 168, 'Laguna', 'LAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2590, 168, 'Lanao del Norte', 'LNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2591, 168, 'Lanao del Sur', 'LSU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2592, 168, 'La Union', 'UNI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2593, 168, 'Leyte', 'LEY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2594, 168, 'Maguindanao', 'MAG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2595, 168, 'Marinduque', 'MRN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2596, 168, 'Masbate', 'MSB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2597, 168, 'Mindoro Occidental', 'MIC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2598, 168, 'Mindoro Oriental', 'MIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2599, 168, 'Misamis Occidental', 'MSC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2600, 168, 'Misamis Oriental', 'MOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2601, 168, 'Mountain', 'MOP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2602, 168, 'Negros Occidental', 'NOC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2603, 168, 'Negros Oriental', 'NOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2604, 168, 'North Cotabato', 'NCT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2605, 168, 'Northern Samar', 'NSM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2606, 168, 'Nueva Ecija', 'NEC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2608, 168, 'Palawan', 'PLW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2609, 168, 'Pampanga', 'PMP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2610, 168, 'Pangasinan', 'PNG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2611, 168, 'Quezon', 'QZN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2612, 168, 'Quirino', 'QRN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2613, 168, 'Rizal', 'RIZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2614, 168, 'Romblon', 'ROM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2615, 168, 'Samar', 'SMR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2616, 168, 'Sarangani', 'SRG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2617, 168, 'Siquijor', 'SQJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2618, 168, 'Sorsogon', 'SRS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2619, 168, 'South Cotabato', 'SCO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2620, 168, 'Southern Leyte', 'SLE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2621, 168, 'Sultan Kudarat', 'SKU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2622, 168, 'Sulu', 'SLU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2623, 168, 'Surigao del Norte', 'SNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2624, 168, 'Surigao del Sur', 'SSU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2625, 168, 'Tarlac', 'TAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2626, 168, 'Tawi-Tawi', 'TAW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2627, 168, 'Zambales', 'ZBL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2631, 170, 'Dolnoslaskie', 'DO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2633, 170, 'Lodzkie', 'LO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2634, 170, 'Lubelskie', 'LL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2635, 170, 'Lubuskie', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2636, 170, 'Malopolskie', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2637, 170, 'Mazowieckie', 'MZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2638, 170, 'Opolskie', 'OP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2639, 170, 'Podkarpackie', 'PP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2640, 170, 'Podlaskie', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2641, 170, 'Pomorskie', 'PM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2642, 170, 'Slaskie', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2643, 170, 'Swietokrzyskie', 'SW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2645, 170, 'Wielkopolskie', 'WP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2646, 170, 'Zachodniopomorskie', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2647, 198, 'Saint Pierre', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2648, 198, 'Miquelon', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2649, 171, 'A&ccedil;ores', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2650, 171, 'Aveiro', 'AV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2651, 171, 'Beja', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2652, 171, 'Braga', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2653, 171, 'Bragan&ccedil;a', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2654, 171, 'Castelo Branco', 'CB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2655, 171, 'Coimbra', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2656, 171, '&Eacute;vora', 'EV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2657, 171, 'Faro', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2658, 171, 'Guarda', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2659, 171, 'Leiria', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2660, 171, 'Lisboa', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2661, 171, 'Madeira', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2662, 171, 'Portalegre', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2663, 171, 'Porto', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2664, 171, 'Santar&eacute;m', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2665, 171, 'Set&uacute;bal', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2666, 171, 'Viana do Castelo', 'VC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2667, 171, 'Vila Real', 'VR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2668, 171, 'Viseu', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2669, 173, 'Ad Dawhah', 'DW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2670, 173, 'Al Ghuwayriyah', 'GW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2671, 173, 'Al Jumayliyah', 'JM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2672, 173, 'Al Khawr', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2673, 173, 'Al Wakrah', 'WK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2674, 173, 'Ar Rayyan', 'RN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2675, 173, 'Jarayan al Batinah', 'JB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2676, 173, 'Madinat ash Shamal', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2677, 173, 'Umm Sa''id', 'UD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2678, 173, 'Umm Salal', 'UL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2679, 175, 'Alba', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2680, 175, 'Arad', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2681, 175, 'Arges', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2682, 175, 'Bacau', 'BC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2683, 175, 'Bihor', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2684, 175, 'Bistrita-Nasaud', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2685, 175, 'Botosani', 'BT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2686, 175, 'Brasov', 'BV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2687, 175, 'Braila', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2688, 175, 'Bucuresti', 'B', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2689, 175, 'Buzau', 'BZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2690, 175, 'Caras-Severin', 'CS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2691, 175, 'Calarasi', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2692, 175, 'Cluj', 'CJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2693, 175, 'Constanta', 'CT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2694, 175, 'Covasna', 'CV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2695, 175, 'Dimbovita', 'DB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2696, 175, 'Dolj', 'DJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2697, 175, 'Galati', 'GL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2698, 175, 'Giurgiu', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2699, 175, 'Gorj', 'GJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2700, 175, 'Harghita', 'HR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2701, 175, 'Hunedoara', 'HD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2702, 175, 'Ialomita', 'IL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2703, 175, 'Iasi', 'IS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2704, 175, 'Ilfov', 'IF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2705, 175, 'Maramures', 'MM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2706, 175, 'Mehedinti', 'MH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2707, 175, 'Mures', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2708, 175, 'Neamt', 'NT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2709, 175, 'Olt', 'OT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2710, 175, 'Prahova', 'PH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2711, 175, 'Satu-Mare', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2712, 175, 'Salaj', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2713, 175, 'Sibiu', 'SB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2714, 175, 'Suceava', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2715, 175, 'Teleorman', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2716, 175, 'Timis', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2717, 175, 'Tulcea', 'TL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2718, 175, 'Vaslui', 'VS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2719, 175, 'Valcea', 'VL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2720, 175, 'Vrancea', 'VN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2721, 176, 'Abakan', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2722, 176, 'Aginskoye', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2723, 176, 'Anadyr', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2724, 176, 'Arkahangelsk', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2725, 176, 'Astrakhan', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2726, 176, 'Barnaul', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2727, 176, 'Belgorod', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2728, 176, 'Birobidzhan', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2729, 176, 'Blagoveshchensk', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2730, 176, 'Bryansk', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2731, 176, 'Cheboksary', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2732, 176, 'Chelyabinsk', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2733, 176, 'Cherkessk', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2734, 176, 'Chita', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2735, 176, 'Dudinka', 'DU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2736, 176, 'Elista', 'EL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2737, 176, 'Gomo-Altaysk', 'GO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2738, 176, 'Gorno-Altaysk', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2739, 176, 'Groznyy', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2740, 176, 'Irkutsk', 'IR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2741, 176, 'Ivanovo', 'IV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2742, 176, 'Izhevsk', 'IZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2743, 176, 'Kalinigrad', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2744, 176, 'Kaluga', 'KL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2745, 176, 'Kasnodar', 'KS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2746, 176, 'Kazan', 'KZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2747, 176, 'Kemerovo', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2748, 176, 'Khabarovsk', 'KH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2749, 176, 'Khanty-Mansiysk', 'KM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2750, 176, 'Kostroma', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2751, 176, 'Krasnodar', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2752, 176, 'Krasnoyarsk', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2753, 176, 'Kudymkar', 'KU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2754, 176, 'Kurgan', 'KG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2755, 176, 'Kursk', 'KK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2756, 176, 'Kyzyl', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2757, 176, 'Lipetsk', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2758, 176, 'Magadan', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2759, 176, 'Makhachkala', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2760, 176, 'Maykop', 'MY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2761, 176, 'Moscow', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2762, 176, 'Murmansk', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2763, 176, 'Nalchik', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2764, 176, 'Naryan Mar', 'NR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2765, 176, 'Nazran', 'NZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2766, 176, 'Nizhniy Novgorod', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2767, 176, 'Novgorod', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2768, 176, 'Novosibirsk', 'NV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2769, 176, 'Omsk', 'OM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2770, 176, 'Orel', 'OR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2771, 176, 'Orenburg', 'OE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2772, 176, 'Palana', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2773, 176, 'Penza', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2774, 176, 'Perm', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2776, 176, 'Petrozavodsk', 'PT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2777, 176, 'Pskov', 'PS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2778, 176, 'Rostov-na-Donu', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2779, 176, 'Ryazan', 'RY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2780, 176, 'Salekhard', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2781, 176, 'Samara', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2782, 176, 'Saransk', 'SR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2783, 176, 'Saratov', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2784, 176, 'Smolensk', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2785, 176, 'St. Petersburg', 'SP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2786, 176, 'Stavropol', 'ST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2787, 176, 'Syktyvkar', 'SY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2788, 176, 'Tambov', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2789, 176, 'Tomsk', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2790, 176, 'Tula', 'TU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2791, 176, 'Tura', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2792, 176, 'Tver', 'TV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2793, 176, 'Tyumen', 'TY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2794, 176, 'Ufa', 'UF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2795, 176, 'Ul''yanovsk', 'UL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2796, 176, 'Ulan-Ude', 'UU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2797, 176, 'Ust''-Ordynskiy', 'US', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2798, 176, 'Vladikavkaz', 'VL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2799, 176, 'Vladimir', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2800, 176, 'Vladivostok', 'VV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2801, 176, 'Volgograd', 'VG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2802, 176, 'Vologda', 'VD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2803, 176, 'Voronezh', 'VO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2804, 176, 'Vyatka', 'VY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2805, 176, 'Yakutsk', 'YA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2806, 176, 'Yaroslavl', 'YR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2807, 176, 'Yekaterinburg', 'YE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2808, 176, 'Yoshkar-Ola', 'YO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2809, 177, 'Butare', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2810, 177, 'Byumba', 'BY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2811, 177, 'Cyangugu', 'CY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2812, 177, 'Gikongoro', 'GK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2813, 177, 'Gisenyi', 'GS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2814, 177, 'Gitarama', 'GT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2815, 177, 'Kibungo', 'KG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2816, 177, 'Kibuye', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2817, 177, 'Kigali Rurale', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2818, 177, 'Kigali-ville', 'KV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2819, 177, 'Ruhengeri', 'RU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2820, 177, 'Umutara', 'UM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2823, 178, 'Saint George Basseterre', 'SGB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2824, 178, 'Saint George Gingerland', 'SGG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2825, 178, 'Saint James Windward', 'SJW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2826, 178, 'Saint Miki Capesterre', 'SJC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2827, 178, 'Saint Miki Figtree', 'SJF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2828, 178, 'Saint Mary Cayon', 'SMC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2832, 178, 'Saint Thomas Lowland', 'STL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2835, 179, 'Anse-la-Raye', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2836, 179, 'Castries', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2837, 179, 'Choiseul', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2838, 179, 'Dauphin', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2839, 179, 'Dennery', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2840, 179, 'Gros-Islet', 'GI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2841, 179, 'Laborie', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2842, 179, 'Micoud', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2843, 179, 'Praslin', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2844, 179, 'Soufriere', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2845, 179, 'Vieux-Fort', 'VF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2846, 180, 'Charlotte', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2847, 180, 'Grenadines', 'R', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2848, 180, 'Saint Andrew', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2849, 180, 'Saint David', 'D', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2850, 180, 'Saint George', 'G', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2851, 180, 'Saint Miles', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2852, 181, 'A''ana', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2854, 181, 'Atua', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2855, 181, 'Fa''asaleleaga', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2856, 181, 'Gaga''emauga', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2857, 181, 'Gagaifomauga', 'GF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2858, 181, 'Palauli', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2859, 181, 'Satupa''itea', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2860, 181, 'Tuamasaga', 'TU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2861, 181, 'Va''a-o-Fonoti', 'VF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2862, 181, 'Vaisigano', 'VS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2863, 182, 'Acquaviva', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2864, 182, 'Borgo Maggiore', 'BM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2865, 182, 'Chiesanuova', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2866, 182, 'Domagnano', 'DO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2867, 182, 'Faetano', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2868, 182, 'Fiorentino', 'FI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2869, 182, 'Montegiardino', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2870, 182, 'Citta di San Marino', 'SM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2871, 182, 'Serravalle', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2872, 183, 'Sao Tome', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2873, 183, 'Principe', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2874, 184, 'Al Bahah', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2876, 184, 'Al Jawf', 'JF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2877, 184, 'Al Madinah', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2878, 184, 'Al Qasim', 'QS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2879, 184, 'Ar Riyad', 'RD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2881, 184, '''Asir', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2882, 184, 'Ha''il', 'HL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2883, 184, 'Jizan', 'JZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2884, 184, 'Makkah', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2885, 184, 'Najran', 'NR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2886, 184, 'Tabuk', 'TB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2887, 185, 'Dakar', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2888, 185, 'Diourbel', 'DI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2889, 185, 'Fatick', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2890, 185, 'Kaolack', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2891, 185, 'Kolda', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2892, 185, 'Louga', 'LO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2893, 185, 'Matam', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2894, 185, 'Saint-Louis', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2895, 185, 'Tambacounda', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2896, 185, 'Thies', 'TH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2897, 185, 'Ziguinchor', 'ZI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2898, 186, 'Anse aux Pins', 'AP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2899, 186, 'Anse Boileau', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2900, 186, 'Anse Etoile', 'AE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2901, 186, 'Anse Louis', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2902, 186, 'Anse Royale', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2903, 186, 'Baie Lazare', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2904, 186, 'Baie Sainte Anne', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2905, 186, 'Beau Vallon', 'BV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2906, 186, 'Bel Air', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2907, 186, 'Bel Ombre', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2908, 186, 'Cascade', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2909, 186, 'Glacis', 'GL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2910, 186, 'Grand'' Anse (on Mahe)', 'GM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2911, 186, 'Grand'' Anse (on Praslin)', 'GP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2912, 186, 'La Digue', 'DG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2913, 186, 'La Riviere Anglaise', 'RA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2914, 186, 'Mont Buxton', 'MB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2915, 186, 'Mont Fleuri', 'MF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2916, 186, 'Plaisance', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2917, 186, 'Pointe La Rue', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2918, 186, 'Port Glaud', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2919, 186, 'Saint Louis', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2920, 186, 'Takamaka', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2921, 187, 'Eastern', 'E', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');
INSERT INTO `mw_zone` (`zone_id`, `country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(2922, 187, 'Northern', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2923, 187, 'Southern', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2924, 187, 'Western', 'W', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2925, 189, 'BanskobystrickÃ½', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2926, 189, 'BratislavskÃ½', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2927, 189, 'KoÅ¡ickÃ½', 'KO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2928, 189, 'Nitriansky', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2929, 189, 'PreÅ¡ovskÃ½', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2930, 189, 'TrenÄiansky', 'TC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2931, 189, 'TrnavskÃ½', 'TV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2932, 189, 'Å½ilinskÃ½', 'ZI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2933, 191, 'Central', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2934, 191, 'Choiseul', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2935, 191, 'Guadalcanal', 'GC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2936, 191, 'Honiara', 'HO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2937, 191, 'Isabel', 'IS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2938, 191, 'Makira', 'MK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2939, 191, 'Malaita', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2940, 191, 'Rennell and Bellona', 'RB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2941, 191, 'Temotu', 'TM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2942, 191, 'Western', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2943, 192, 'Awdal', 'AW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2944, 192, 'Bakool', 'BK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2945, 192, 'Banaadir', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2946, 192, 'Bari', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2947, 192, 'Bay', 'BY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2948, 192, 'Galguduud', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2949, 192, 'Gedo', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2950, 192, 'Hiiraan', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2951, 192, 'Jubbada Dhexe', 'JD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2952, 192, 'Jubbada Hoose', 'JH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2953, 192, 'Mudug', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2954, 192, 'Nugaal', 'NU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2955, 192, 'Sanaag', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2957, 192, 'Shabeellaha Hoose', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2958, 192, 'Sool', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2959, 192, 'Togdheer', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2960, 192, 'Woqooyi Galbeed', 'WG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2961, 193, 'Eastern Cape', 'EC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2962, 193, 'Free State', 'FS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2963, 193, 'Gauteng', 'GT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2964, 193, 'KwaZulu-Natal', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2965, 193, 'Limpopo', 'LP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2966, 193, 'Mpumalanga', 'MP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2967, 193, 'North West', 'NW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2968, 193, 'Northern Cape', 'NC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2969, 193, 'Western Cape', 'WC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2970, 195, 'La Coru&ntilde;a', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2971, 195, '&Aacute;lava', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2972, 195, 'Albacete', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2973, 195, 'Alicante', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2974, 195, 'Almeria', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2975, 195, 'Asturias', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2976, 195, '&Aacute;vila', 'AV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2977, 195, 'Badajoz', 'BJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2978, 195, 'Baleares', 'IB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2979, 195, 'Barcelona', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2980, 195, 'Burgos', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2981, 195, 'C&aacute;ceres', 'CC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2982, 195, 'C&aacute;diz', 'CZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2983, 195, 'Cantabria', 'CT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2984, 195, 'Castell&oacute;n', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2985, 195, 'Ceuta', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2986, 195, 'Ciudad Real', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2987, 195, 'C&oacute;rdoba', 'CD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2988, 195, 'Cuenca', 'CU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2989, 195, 'Girona', 'GI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2990, 195, 'Granada', 'GD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2991, 195, 'Guadalajara', 'GJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2993, 195, 'Huelva', 'HL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2994, 195, 'Huesca', 'HS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2995, 195, 'Ja&eacute;n', 'JN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2996, 195, 'La Rioja', 'RJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2997, 195, 'Las Palmas', 'PM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2998, 195, 'Leon', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(2999, 195, 'Lleida', 'LL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3000, 195, 'Lugo', 'LG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3001, 195, 'Madrid', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3002, 195, 'Malaga', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3003, 195, 'Melilla', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3004, 195, 'Murcia', 'MU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3005, 195, 'Navarra', 'NV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3006, 195, 'Ourense', 'OU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3007, 195, 'Palencia', 'PL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3008, 195, 'Pontevedra', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3009, 195, 'Salamanca', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3011, 195, 'Segovia', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3012, 195, 'Sevilla', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3013, 195, 'Soria', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3014, 195, 'Tarragona', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3015, 195, 'Teruel', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3016, 195, 'Toledo', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3017, 195, 'Valencia', 'VC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3018, 195, 'Valladolid', 'VD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3019, 195, 'Vizcaya', 'VZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3020, 195, 'Zamora', 'ZM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3021, 195, 'Zaragoza', 'ZR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3022, 196, 'Central', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3023, 196, 'Eastern', 'EA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3024, 196, 'North Central', 'NC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3025, 196, 'Northern', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3026, 196, 'North Western', 'NW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3027, 196, 'Sabaragamuwa', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3028, 196, 'Southern', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3029, 196, 'Uva', 'UV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3030, 196, 'Western', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3031, 197, 'Ascension', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3032, 197, 'Saint Helena', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3033, 197, 'Tristan da Cunha', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3034, 199, 'A''ali an Nil', 'ANL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3036, 199, 'Al Buhayrat', 'BRT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3037, 199, 'Al Jazirah', 'JZR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3038, 199, 'Al Khartum', 'KRT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3039, 199, 'Al Qadarif', 'QDR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3040, 199, 'Al Wahdah', 'WDH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3041, 199, 'An Nil al Abyad', 'ANB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3042, 199, 'An Nil al Azraq', 'ANZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3043, 199, 'Ash Shamaliyah', 'ASH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3044, 199, 'Bahr al Jabal', 'BJA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3045, 199, 'Gharb al Istiwa''iyah', 'GIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3047, 199, 'Gharb Darfur', 'GDA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3048, 199, 'Gharb Kurdufan', 'GKU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3049, 199, 'Janub Darfur', 'JDA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3050, 199, 'Janub Kurdufan', 'JKU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3051, 199, 'Junqali', 'JQL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3052, 199, 'Kassala', 'KSL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3053, 199, 'Nahr an Nil', 'NNL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3055, 199, 'Shamal Darfur', 'SDA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3056, 199, 'Shamal Kurdufan', 'SKU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3057, 199, 'Sharq al Istiwa''iyah', 'SIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3058, 199, 'Sinnar', 'SNR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3059, 199, 'Warab', 'WRB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3060, 200, 'Brokopondo', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3061, 200, 'Commewijne', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3062, 200, 'Coronie', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3063, 200, 'Marowijne', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3064, 200, 'Nickerie', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3065, 200, 'Para', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3066, 200, 'Paramaribo', 'PM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3067, 200, 'Saramacca', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3068, 200, 'Sipaliwini', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3069, 200, 'Wanica', 'WA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3070, 202, 'Hhohho', 'H', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3071, 202, 'Lubombo', 'L', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3072, 202, 'Manzini', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3073, 202, 'Shishelweni', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3074, 203, 'Blekinge', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3075, 203, 'Dalarna', 'W', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3076, 203, 'G&auml;vleborg', 'X', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3077, 203, 'Gotland', 'I', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3078, 203, 'Halland', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3079, 203, 'J&auml;mtland', 'Z', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3081, 203, 'Kalmar', 'H', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3082, 203, 'Kronoberg', 'G', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3083, 203, 'Norrbotten', 'BD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3084, 203, '&Ouml;rebro', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3086, 203, 'Sk&aring;ne', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3087, 203, 'S&ouml;dermanland', 'D', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3088, 203, 'Stockholm', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3089, 203, 'Uppsala', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3090, 203, 'V&auml;rmland', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3091, 203, 'V&auml;sterbotten', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3092, 203, 'V&auml;sternorrland', 'Y', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3093, 203, 'V&auml;stmanland', 'U', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3095, 204, 'Aargau', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3098, 204, 'Basel-Stadt', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3099, 204, 'Basel-Landschaft', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3100, 204, 'Bern', 'BE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3101, 204, 'Fribourg', 'FR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3102, 204, 'Gen&egrave;ve', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3103, 204, 'Glarus', 'GL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3104, 204, 'Graub&uuml;nden', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3105, 204, 'Jura', 'JU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3106, 204, 'Luzern', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3107, 204, 'Neuch&acirc;tel', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3108, 204, 'Nidwald', 'NW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3109, 204, 'Obwald', 'OW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3110, 204, 'St. Gallen', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3111, 204, 'Schaffhausen', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3112, 204, 'Schwyz', 'SZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3113, 204, 'Solothurn', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3114, 204, 'Thurgau', 'TG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3115, 204, 'Ticino', 'TI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3116, 204, 'Uri', 'UR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3117, 204, 'Valais', 'VS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3118, 204, 'Vaud', 'VD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3119, 204, 'Zug', 'ZG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3120, 204, 'Z&uuml;rich', 'ZH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3121, 205, 'Al Hasakah', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3122, 205, 'Al Ladhiqiyah', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3123, 205, 'Al Qunaytirah', 'QU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3124, 205, 'Ar Raqqah', 'RQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3125, 205, 'As Suwayda', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3126, 205, 'Dara', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3127, 205, 'Dayr az Zawr', 'DZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3128, 205, 'Dimashq', 'DI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3129, 205, 'Halab', 'HL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3130, 205, 'Hamah', 'HM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3131, 205, 'Hims', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3132, 205, 'Idlib', 'ID', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3133, 205, 'Rif Dimashq', 'RD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3134, 205, 'Tartus', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3135, 206, 'Chang-hua', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3136, 206, 'Chia-i', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3137, 206, 'Hsin-chu', 'HS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3138, 206, 'Hua-lien', 'HL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3139, 206, 'I-lan', 'IL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3140, 206, 'Kao-hsiung county', 'KH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3141, 206, 'Kin-men', 'KM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3142, 206, 'Lien-chiang', 'LC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3143, 206, 'Miao-li', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3144, 206, 'Nan-t''ou', 'NT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3145, 206, 'P''eng-hu', 'PH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3146, 206, 'P''ing-tung', 'PT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3147, 206, 'T''ai-chung', 'TG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3148, 206, 'T''ai-nan', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3149, 206, 'T''ai-pei county', 'TP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3150, 206, 'T''ai-tung', 'TT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3151, 206, 'T''ao-yuan', 'TY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3152, 206, 'Yun-lin', 'YL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3153, 206, 'Chia-i city', 'CC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3154, 206, 'Chi-lung', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3155, 206, 'Hsin-chu', 'HC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3156, 206, 'T''ai-chung', 'TH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3157, 206, 'T''ai-nan', 'TN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3158, 206, 'Kao-hsiung city', 'KC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3159, 206, 'T''ai-pei city', 'TC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3160, 207, 'Gorno-Badakhstan', 'GB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3161, 207, 'Khatlon', 'KT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3162, 207, 'Sughd', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3163, 208, 'Arusha', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3164, 208, 'Dar es Salaam', 'DS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3165, 208, 'Dodoma', 'DO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3166, 208, 'Iringa', 'IR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3167, 208, 'Kagera', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3168, 208, 'Kigoma', 'KI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3169, 208, 'Kilimanjaro', 'KJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3170, 208, 'Lindi', 'LN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3171, 208, 'Manyara', 'MY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3172, 208, 'Mara', 'MR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3173, 208, 'Mbeya', 'MB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3174, 208, 'Morogoro', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3175, 208, 'Mtwara', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3176, 208, 'Mwanza', 'MW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3177, 208, 'Pemba North', 'PN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3178, 208, 'Pemba South', 'PS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3179, 208, 'Pwani', 'PW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3180, 208, 'Rukwa', 'RK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3181, 208, 'Ruvuma', 'RV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3182, 208, 'Shinyanga', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3183, 208, 'Singida', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3184, 208, 'Tabora', 'TB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3185, 208, 'Tanga', 'TN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3186, 208, 'Zanzibar Central/South', 'ZC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3187, 208, 'Zanzibar North', 'ZN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3190, 209, 'Ang Thong', 'Ang Thong', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3192, 209, 'Bangkok', 'Bangkok', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3193, 209, 'Buriram', 'Buriram', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3195, 209, 'Chai Nat', 'Chai Nat', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3200, 209, 'Chon Buri', 'Chon Buri', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3201, 209, 'Chumphon', 'Chumphon', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3202, 209, 'Kalasin', 'Kalasin', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3206, 209, 'Krabi', 'Krabi', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3207, 209, 'Lampang', 'Lampang', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3208, 209, 'Lamphun', 'Lamphun', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3209, 209, 'Loei', 'Loei', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3210, 209, 'Lop Buri', 'Lop Buri', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3213, 209, 'Mukdahan', 'Mukdahan', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3220, 209, 'Nan', 'Nan', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3221, 209, 'Narathiwat', 'Narathiwat', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3223, 209, 'Nong Khai', 'Nong Khai', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3226, 209, 'Pattani', 'Pattani', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3227, 209, 'Phangnga', 'Phangnga', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3228, 209, 'Phatthalung', 'Phatthalung', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3229, 209, 'Phayao', 'Phayao', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3230, 209, 'Phetchabun', 'Phetchabun', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3232, 209, 'Phichit', 'Phichit', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3234, 209, 'Phrae', 'Phrae', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3235, 209, 'Phuket', 'Phuket', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3238, 209, 'Ranong', 'Ranong', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3240, 209, 'Rayong', 'Rayong', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3241, 209, 'Roi Et', 'Roi Et', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3247, 209, 'Sara Buri', 'Sara Buri', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3248, 209, 'Satun', 'Satun', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3249, 209, 'Sing Buri', 'Sing Buri', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3250, 209, 'Sisaket', 'Sisaket', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3251, 209, 'Songkhla', 'Songkhla', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3252, 209, 'Sukhothai', 'Sukhothai', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3254, 209, 'Surat Thani', 'Surat Thani', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3255, 209, 'Surin', 'Surin', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3256, 209, 'Tak', 'Tak', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3257, 209, 'Trang', 'Trang', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3258, 209, 'Trat', 'Trat', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3260, 209, 'Udon Thani', 'Udon Thani', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3262, 209, 'Uttaradit', 'Uttaradit', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3263, 209, 'Yala', 'Yala', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3264, 209, 'Yasothon', 'Yasothon', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3265, 210, 'Kara', 'K', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3266, 210, 'Plateaux', 'P', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3267, 210, 'Savanes', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3268, 210, 'Centrale', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3269, 210, 'Maritime', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3270, 211, 'Atafu', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3271, 211, 'Fakaofo', 'F', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3272, 211, 'Nukunonu', 'N', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3273, 212, 'Ha''apai', 'H', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3274, 212, 'Tongatapu', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3275, 212, 'Vava''u', 'V', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3277, 213, 'Diego Martin', 'DM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3279, 213, 'Penal/Debe', 'PD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3280, 213, 'Princes Town', 'PT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3281, 213, 'Sangre Grande', 'SG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3282, 213, 'San Juan/Laventille', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3283, 213, 'Siparia', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3284, 213, 'Tunapuna/Piarco', 'TP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3285, 213, 'Port of Spain', 'PS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3286, 213, 'San Fernando', 'SF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3287, 213, 'Arima', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3288, 213, 'Point Fortin', 'PF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3289, 213, 'Chaguanas', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3290, 213, 'Tobago', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3291, 214, 'Ariana', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3292, 214, 'Beja', 'BJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3293, 214, 'Ben Arous', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3294, 214, 'Bizerte', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3295, 214, 'Gabes', 'GB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3296, 214, 'Gafsa', 'GF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3297, 214, 'Jendouba', 'JE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3298, 214, 'Kairouan', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3299, 214, 'Kasserine', 'KS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3300, 214, 'Kebili', 'KB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3301, 214, 'Kef', 'KF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3302, 214, 'Mahdia', 'MH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3303, 214, 'Manouba', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3304, 214, 'Medenine', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3305, 214, 'Monastir', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3306, 214, 'Nabeul', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3307, 214, 'Sfax', 'SF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3308, 214, 'Sidi', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3309, 214, 'Siliana', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3310, 214, 'Sousse', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3311, 214, 'Tataouine', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3312, 214, 'Tozeur', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3313, 214, 'Tunis', 'TU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3314, 214, 'Zaghouan', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3315, 215, 'Adana', 'ADA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3316, 215, 'AdÄ±yaman', 'ADI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3317, 215, 'Afyonkarahisar', 'AFY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3318, 215, 'AÄŸrÄ±', 'AGR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3319, 215, 'Aksaray', 'AKS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3320, 215, 'Amasya', 'AMA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3321, 215, 'Ankara', 'ANK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3322, 215, 'Antalya', 'ANT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3323, 215, 'Ardahan', 'ARD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3324, 215, 'Artvin', 'ART', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3325, 215, 'AydÄ±n', 'AYI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3326, 215, 'BalÄ±kesir', 'BAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3327, 215, 'BartÄ±n', 'BAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3328, 215, 'Batman', 'BAT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3329, 215, 'Bayburt', 'BAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3330, 215, 'Bilecik', 'BIL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3331, 215, 'BingÃ¶l', 'BIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3332, 215, 'Bitlis', 'BIT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3333, 215, 'Bolu', 'BOL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3334, 215, 'Burdur', 'BRD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3335, 215, 'Bursa', 'BRS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3336, 215, 'Ã‡anakkale', 'CKL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3337, 215, 'Ã‡ankÄ±rÄ±', 'CKR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3338, 215, 'Ã‡orum', 'COR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3339, 215, 'Denizli', 'DEN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3340, 215, 'Diyarbakir', 'DIY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3341, 215, 'DÃ¼zce', 'DUZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3342, 215, 'Edirne', 'EDI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3343, 215, 'Elazig', 'ELA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3344, 215, 'Erzincan', 'EZC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3345, 215, 'Erzurum', 'EZR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3346, 215, 'EskiÅŸehir', 'ESK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3347, 215, 'Gaziantep', 'GAZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3348, 215, 'Giresun', 'GIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3349, 215, 'GÃ¼mÃ¼ÅŸhane', 'GMS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3350, 215, 'Hakkari', 'HKR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3351, 215, 'Hatay', 'HTY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3352, 215, 'IÄŸdÄ±r', 'IGD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3353, 215, 'Isparta', 'ISP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3354, 215, 'Ä°stanbul', 'IST', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3355, 215, 'Ä°zmir', 'IZM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3356, 215, 'KahramanmaraÅŸ', 'KAH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3357, 215, 'KarabÃ¼k', 'KRB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3358, 215, 'Karaman', 'KRM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3359, 215, 'Kars', 'KRS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3360, 215, 'Kastamonu', 'KAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3361, 215, 'Kayseri', 'KAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3362, 215, 'Kilis', 'KLS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3363, 215, 'Kirikkale', 'KRK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3364, 215, 'Kirklareli', 'KLR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3365, 215, 'Kirsehir', 'KRH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3366, 215, 'Kocaeli', 'KOC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3367, 215, 'Konya', 'KON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3368, 215, 'KÃ¼tahya', 'KUT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3369, 215, 'Malatya', 'MAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3370, 215, 'Manisa', 'MAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3371, 215, 'Mardin', 'MAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3372, 215, 'Mersin', 'MER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3373, 215, 'MuÄŸla', 'MUG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3374, 215, 'MuÅŸ', 'MUS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3375, 215, 'NevÅŸehir', 'NEV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3376, 215, 'NiÄŸde', 'NIG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3377, 215, 'Ordu', 'ORD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3378, 215, 'Osmaniye', 'OSM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3379, 215, 'Rize', 'RIZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3380, 215, 'Sakarya', 'SAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3381, 215, 'Samsun', 'SAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3382, 215, 'ÅžanlÄ±urfa', 'SAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3383, 215, 'Siirt', 'SII', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3384, 215, 'Sinop', 'SIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3385, 215, 'ÅžÄ±rnak', 'SIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3386, 215, 'Sivas', 'SIV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3387, 215, 'TekirdaÄŸ', 'TEL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3388, 215, 'Tokat', 'TOK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3389, 215, 'Trabzon', 'TRA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3390, 215, 'Tunceli', 'TUN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3391, 215, 'UÅŸak', 'USK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3392, 215, 'Van', 'VAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3393, 215, 'Yalova', 'YAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3394, 215, 'Yozgat', 'YOZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3395, 215, 'Zonguldak', 'ZON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3396, 216, 'Ahal Welayaty', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3397, 216, 'Balkan Welayaty', 'B', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3398, 216, 'Dashhowuz Welayaty', 'D', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3399, 216, 'Lebap Welayaty', 'L', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3400, 216, 'Mary Welayaty', 'M', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3401, 217, 'Ambergris Cays', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3402, 217, 'Dellis Cay', 'DC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3403, 217, 'French Cay', 'FC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3404, 217, 'Little Water Cay', 'LW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3405, 217, 'Parrot Cay', 'RC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3406, 217, 'Pine Cay', 'PN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3407, 217, 'Salt Cay', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3408, 217, 'Grand Turk', 'GT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3409, 217, 'South Caicos', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3410, 217, 'East Caicos', 'EC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3411, 217, 'Middle Caicos', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3412, 217, 'North Caicos', 'NC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3413, 217, 'Providenciales', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3414, 217, 'West Caicos', 'WC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3415, 218, 'Nanumanga', 'NMG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3416, 218, 'Niulakita', 'NLK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3417, 218, 'Niutao', 'NTO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3418, 218, 'Funafuti', 'FUN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3419, 218, 'Nanumea', 'NME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3420, 218, 'Nui', 'NUI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3421, 218, 'Nukufetau', 'NFT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3422, 218, 'Nukulaelae', 'NLL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3423, 218, 'Vaitupu', 'VAI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3424, 219, 'Kalangala', 'KAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3425, 219, 'Kampala', 'KMP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3426, 219, 'Kayunga', 'KAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3427, 219, 'Kiboga', 'KIB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3428, 219, 'Luwero', 'LUW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3429, 219, 'Masaka', 'MAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3430, 219, 'Mpigi', 'MPI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3431, 219, 'Mubende', 'MUB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3432, 219, 'Mukono', 'MUK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3433, 219, 'Nakasongola', 'NKS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3434, 219, 'Rakai', 'RAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3435, 219, 'Sembabule', 'SEM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3436, 219, 'Wakiso', 'WAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3437, 219, 'Bugiri', 'BUG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3438, 219, 'Busia', 'BUS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3439, 219, 'Iganga', 'IGA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3440, 219, 'Jinja', 'JIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3441, 219, 'Kaberamaido', 'KAB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3442, 219, 'Kamuli', 'KML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3443, 219, 'Kapchorwa', 'KPC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3444, 219, 'Katakwi', 'KTK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3445, 219, 'Kumi', 'KUM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3446, 219, 'Mayuge', 'MAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3447, 219, 'Mbale', 'MBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3448, 219, 'Pallisa', 'PAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3449, 219, 'Sironko', 'SIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3450, 219, 'Soroti', 'SOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3451, 219, 'Tororo', 'TOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3452, 219, 'Adjumani', 'ADJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3453, 219, 'Apac', 'APC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3454, 219, 'Arua', 'ARU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3455, 219, 'Gulu', 'GUL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3456, 219, 'Kitgum', 'KIT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3457, 219, 'Kotido', 'KOT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3458, 219, 'Lira', 'LIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3459, 219, 'Moroto', 'MRT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3460, 219, 'Moyo', 'MOY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3461, 219, 'Nakapiripirit', 'NAK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3462, 219, 'Nebbi', 'NEB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3463, 219, 'Pader', 'PAD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3464, 219, 'Yumbe', 'YUM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3465, 219, 'Bundibugyo', 'BUN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3466, 219, 'Bushenyi', 'BSH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3467, 219, 'Hoima', 'HOI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3468, 219, 'Kabale', 'KBL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3469, 219, 'Kabarole', 'KAR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3470, 219, 'Kamwenge', 'KAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3471, 219, 'Kanungu', 'KAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3472, 219, 'Kasese', 'KAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3473, 219, 'Kibaale', 'KBA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3474, 219, 'Kisoro', 'KIS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3475, 219, 'Kyenjojo', 'KYE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3476, 219, 'Masindi', 'MSN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3477, 219, 'Mbarara', 'MBR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3478, 219, 'Ntungamo', 'NTU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3479, 219, 'Rukungiri', 'RUK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3480, 220, 'Cherkasy', 'CK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3481, 220, 'Chernihiv', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3482, 220, 'Chernivtsi', 'CV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3483, 220, 'Crimea', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3484, 220, 'Dnipropetrovs''k', 'DN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3485, 220, 'Donets''k', 'DO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3486, 220, 'Ivano-Frankivs''k', 'IV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3487, 220, 'Kharkiv Kherson', 'KL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3488, 220, 'Khmel''nyts''kyy', 'KM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3489, 220, 'Kirovohrad', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3490, 220, 'Kiev', 'KV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3491, 220, 'Kyyiv', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3492, 220, 'Luhans''k', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');
INSERT INTO `mw_zone` (`zone_id`, `country_id`, `name`, `code`, `status`, `date_added`, `last_updated`) VALUES
(3493, 220, 'L''viv', 'LV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3494, 220, 'Mykolayiv', 'MY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3495, 220, 'Odesa', 'OD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3496, 220, 'Poltava', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3497, 220, 'Rivne', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3498, 220, 'Sevastopol', 'SE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3499, 220, 'Sumy', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3500, 220, 'Ternopil''', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3501, 220, 'Vinnytsya', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3502, 220, 'Volyn''', 'VO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3503, 220, 'Zakarpattya', 'ZK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3504, 220, 'Zaporizhzhya', 'ZA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3505, 220, 'Zhytomyr', 'ZH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3506, 221, 'Abu Zaby', 'AZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3507, 221, '''Ajman', 'AJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3508, 221, 'Al Fujayrah', 'FU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3509, 221, 'Ash Shariqah', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3510, 221, 'Dubayy', 'DU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3511, 221, 'R''as al Khaymah', 'RK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3512, 221, 'Umm al Qaywayn', 'UQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3513, 222, 'Aberdeen', 'ABN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3514, 222, 'Aberdeenshire', 'ABNS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3515, 222, 'Anglesey', 'ANG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3516, 222, 'Angus', 'AGS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3517, 222, 'Argyll and Bute', 'ARY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3518, 222, 'Bedfordshire', 'BEDS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3519, 222, 'Berkshire', 'BERKS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3520, 222, 'Blaenau Gwent', 'BLA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3521, 222, 'Bridgend', 'BRI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3522, 222, 'Bristol', 'BSTL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3523, 222, 'Buckinghamshire', 'BUCKS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3524, 222, 'Caerphilly', 'CAE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3525, 222, 'Cambridgeshire', 'CAMBS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3526, 222, 'Cardiff', 'CDF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3527, 222, 'Carmarthenshire', 'CARM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3528, 222, 'Ceredigion', 'CDGN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3529, 222, 'Cheshire', 'CHES', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3530, 222, 'Clackmannanshire', 'CLACK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3531, 222, 'Conwy', 'CON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3532, 222, 'Cornwall', 'CORN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3533, 222, 'Denbighshire', 'DNBG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3534, 222, 'Derbyshire', 'DERBY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3535, 222, 'Devon', 'DVN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3536, 222, 'Dorset', 'DOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3537, 222, 'Dumfries and Galloway', 'DGL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3538, 222, 'Dundee', 'DUND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3539, 222, 'Durham', 'DHM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3540, 222, 'East Ayrshire', 'ARYE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3542, 222, 'East Lothian', 'LOTE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3543, 222, 'East Renfrewshire', 'RENE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3545, 222, 'East Sussex', 'SXE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3546, 222, 'Edinburgh', 'EDIN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3547, 222, 'Essex', 'ESX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3548, 222, 'Falkirk', 'FALK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3549, 222, 'Fife', 'FFE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3550, 222, 'Flintshire', 'FLINT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3551, 222, 'Glasgow', 'GLAS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3552, 222, 'Gloucestershire', 'GLOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3553, 222, 'Greater London', 'LDN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3554, 222, 'Greater Manchester', 'MCH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3555, 222, 'Gwynedd', 'GDD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3556, 222, 'Hampshire', 'HANTS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3557, 222, 'Herefordshire', 'HWR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3558, 222, 'Hertfordshire', 'HERTS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3559, 222, 'Highlands', 'HLD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3560, 222, 'Inverclyde', 'IVER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3561, 222, 'Isle of Wight', 'IOW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3562, 222, 'Kent', 'KNT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3563, 222, 'Lancashire', 'LANCS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3564, 222, 'Leicestershire', 'LEICS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3565, 222, 'Lincolnshire', 'LINCS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3566, 222, 'Merseyside', 'MSY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3567, 222, 'Merthyr Tydfil', 'MERT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3568, 222, 'Midlothian', 'MLOT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3569, 222, 'Monmouthshire', 'MMOUTH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3570, 222, 'Moray', 'MORAY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3572, 222, 'Newport', 'NEWPT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3573, 222, 'Norfolk', 'NOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3574, 222, 'North Ayrshire', 'ARYN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3575, 222, 'North Lanarkshire', 'LANN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3576, 222, 'North Yorkshire', 'YSN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3577, 222, 'Northamptonshire', 'NHM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3578, 222, 'Northumberland', 'NLD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3579, 222, 'Nottinghamshire', 'NOT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3580, 222, 'Orkney Islands', 'ORK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3581, 222, 'Oxfordshire', 'OFE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3582, 222, 'Pembrokeshire', 'PEM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3583, 222, 'Perth and Kinross', 'PERTH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3584, 222, 'Powys', 'PWS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3585, 222, 'Renfrewshire', 'REN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3587, 222, 'Rutland', 'RUT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3588, 222, 'Scottish Borders', 'BOR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3589, 222, 'Shetland Islands', 'SHET', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3590, 222, 'Shropshire', 'SPE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3591, 222, 'Somerset', 'SOM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3592, 222, 'South Ayrshire', 'ARYS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3593, 222, 'South Lanarkshire', 'LANS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3594, 222, 'South Yorkshire', 'YSS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3595, 222, 'Staffordshire', 'SFD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3596, 222, 'Stirling', 'STIR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3597, 222, 'Suffolk', 'SFK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3598, 222, 'Surrey', 'SRY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3599, 222, 'Swansea', 'SWAN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3600, 222, 'Torfaen', 'TORF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3601, 222, 'Tyne and Wear', 'TWR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3603, 222, 'Warwickshire', 'WARKS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3604, 222, 'West Dunbartonshire', 'WDUN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3605, 222, 'West Lothian', 'WLOT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3606, 222, 'West Midlands', 'WMD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3607, 222, 'West Sussex', 'SXW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3608, 222, 'West Yorkshire', 'YSW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3609, 222, 'Western Isles', 'WIL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3610, 222, 'Wiltshire', 'WLT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3611, 222, 'Worcestershire', 'WORCS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3612, 222, 'Wrexham', 'WRX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3613, 223, 'Alabama', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3614, 223, 'Alaska', 'AK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3615, 223, 'American Samoa', 'AS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3616, 223, 'Arizona', 'AZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3617, 223, 'Arkansas', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3618, 223, 'Armed Forces Africa', 'AF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3619, 223, 'Armed Forces Americas', 'AA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3620, 223, 'Armed Forces Canada', 'AC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3621, 223, 'Armed Forces Europe', 'AE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3622, 223, 'Armed Forces Middle East', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3623, 223, 'Armed Forces Pacific', 'AP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3624, 223, 'California', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3625, 223, 'Colorado', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3626, 223, 'Connecticut', 'CT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3627, 223, 'Delaware', 'DE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3628, 223, 'District of Columbia', 'DC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3629, 223, 'Federated States Of Micronesia', 'FM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3630, 223, 'Florida', 'FL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3631, 223, 'Georgia', 'GA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3632, 223, 'Guam', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3633, 223, 'Hawaii', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3634, 223, 'Idaho', 'ID', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3635, 223, 'Illinois', 'IL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3636, 223, 'Indiana', 'IN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3637, 223, 'Iowa', 'IA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3638, 223, 'Kansas', 'KS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3639, 223, 'Kentucky', 'KY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3640, 223, 'Louisiana', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3641, 223, 'Maine', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3642, 223, 'Marshall Islands', 'MH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3643, 223, 'Maryland', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3644, 223, 'Massachusetts', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3645, 223, 'Michigan', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3646, 223, 'Minnesota', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3647, 223, 'Mississippi', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3648, 223, 'Missouri', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3649, 223, 'Montana', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3650, 223, 'Nebraska', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3651, 223, 'Nevada', 'NV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3652, 223, 'New Hampshire', 'NH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3653, 223, 'New Jersey', 'NJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3654, 223, 'New Mexico', 'NM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3655, 223, 'New York', 'NY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3656, 223, 'North Carolina', 'NC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3657, 223, 'North Dakota', 'ND', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3658, 223, 'Northern Mariana Islands', 'MP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3659, 223, 'Ohio', 'OH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3660, 223, 'Oklahoma', 'OK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3661, 223, 'Oregon', 'OR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3662, 223, 'Palau', 'PW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3663, 223, 'Pennsylvania', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3664, 223, 'Puerto Rico', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3665, 223, 'Rhode Island', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3666, 223, 'South Carolina', 'SC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3667, 223, 'South Dakota', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3668, 223, 'Tennessee', 'TN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3669, 223, 'Texas', 'TX', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3670, 223, 'Utah', 'UT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3671, 223, 'Vermont', 'VT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3672, 223, 'Virgin Islands', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3673, 223, 'Virginia', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3674, 223, 'Washington', 'WA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3675, 223, 'West Virginia', 'WV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3676, 223, 'Wisconsin', 'WI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3677, 223, 'Wyoming', 'WY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3678, 224, 'Baker Island', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3679, 224, 'Howland Island', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3680, 224, 'Jarvis Island', 'JI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3681, 224, 'Johnston Atoll', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3682, 224, 'Kingman Reef', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3683, 224, 'Midway Atoll', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3684, 224, 'Navassa Island', 'NI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3685, 224, 'Palmyra Atoll', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3686, 224, 'Wake Island', 'WI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3687, 225, 'Artigas', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3688, 225, 'Canelones', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3689, 225, 'Cerro Largo', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3690, 225, 'Colonia', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3691, 225, 'Durazno', 'DU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3692, 225, 'Flores', 'FS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3693, 225, 'Florida', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3694, 225, 'Lavalleja', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3695, 225, 'Maldonado', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3696, 225, 'Montevideo', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3697, 225, 'Paysandu', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3698, 225, 'Rio Negro', 'RN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3699, 225, 'Rivera', 'RV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3700, 225, 'Rocha', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3701, 225, 'Salto', 'SL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3702, 225, 'San Jose', 'SJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3703, 225, 'Soriano', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3704, 225, 'Tacuarembo', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3705, 225, 'Treinta y Tres', 'TT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3706, 226, 'Andijon', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3707, 226, 'Buxoro', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3708, 226, 'Farg''ona', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3709, 226, 'Jizzax', 'JI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3710, 226, 'Namangan', 'NG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3711, 226, 'Navoiy', 'NW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3712, 226, 'Qashqadaryo', 'QA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3713, 226, 'Qoraqalpog''iston Republikasi', 'QR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3714, 226, 'Samarqand', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3715, 226, 'Sirdaryo', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3716, 226, 'Surxondaryo', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3717, 226, 'Toshkent City', 'TK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3718, 226, 'Toshkent Region', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3719, 226, 'Xorazm', 'XO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3720, 227, 'Malampa', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3721, 227, 'Penama', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3722, 227, 'Sanma', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3723, 227, 'Shefa', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3724, 227, 'Tafea', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3725, 227, 'Torba', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3726, 229, 'Amazonas', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3727, 229, 'Anzoategui', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3728, 229, 'Apure', 'AP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3729, 229, 'Aragua', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3730, 229, 'Barinas', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3731, 229, 'Bolivar', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3732, 229, 'Carabobo', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3733, 229, 'Cojedes', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3734, 229, 'Delta Amacuro', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3735, 229, 'Dependencias Federales', 'DF', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3736, 229, 'Distrito Federal', 'DI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3737, 229, 'Falcon', 'FA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3738, 229, 'Guarico', 'GU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3739, 229, 'Lara', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3740, 229, 'Merida', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3741, 229, 'Miranda', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3742, 229, 'Monagas', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3743, 229, 'Nueva Esparta', 'NE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3744, 229, 'Portuguesa', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3745, 229, 'Sucre', 'SU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3746, 229, 'Tachira', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3747, 229, 'Trujillo', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3748, 229, 'Vargas', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3749, 229, 'Yaracuy', 'YA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3750, 229, 'Zulia', 'ZU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3751, 230, 'An Giang', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3752, 230, 'Bac Giang', 'BG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3753, 230, 'Bac Kan', 'BK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3754, 230, 'Bac Lieu', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3755, 230, 'Bac Ninh', 'BC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3757, 230, 'Ben Tre', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3758, 230, 'Binh Dinh', 'BH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3759, 230, 'Binh Duong', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3760, 230, 'Binh Phuoc', 'BP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3761, 230, 'Binh Thuan', 'BT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3762, 230, 'Ca Mau', 'CM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3763, 230, 'Can Tho', 'CT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3764, 230, 'Cao Bang', 'CB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3765, 230, 'Dak Lak', 'DL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3766, 230, 'Dak Nong', 'DG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3767, 230, 'Da Nang', 'DN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3768, 230, 'Dien Bien', 'DB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3769, 230, 'Dong Nai', 'DI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3770, 230, 'Dong Thap', 'DT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3771, 230, 'Gia Lai', 'GL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3772, 230, 'Ha Giang', 'HG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3773, 230, 'Hai Duong', 'HD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3774, 230, 'Hai Phong', 'HP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3775, 230, 'Ha Nam', 'HM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3776, 230, 'Ha Noi', 'HI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3777, 230, 'Ha Tay', 'HT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3778, 230, 'Ha Tinh', 'HH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3779, 230, 'Hoa Binh', 'HB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3780, 230, 'Ho Chi Minh City', 'HC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3781, 230, 'Hau Giang', 'HU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3782, 230, 'Hung Yen', 'HY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3783, 232, 'Saint Croix', 'C', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3784, 232, 'Saint Miki', 'J', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3785, 232, 'Saint Thomas', 'T', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3786, 233, 'Alo', 'A', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3787, 233, 'Sigave', 'S', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3788, 233, 'Wallis', 'W', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3789, 235, 'Abyan', 'AB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3790, 235, 'Adan', 'AD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3791, 235, 'Amran', 'AM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3792, 235, 'Al Bayda', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3793, 235, 'Ad Dali', 'DA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3794, 235, 'Dhamar', 'DH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3795, 235, 'Hadramawt', 'HD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3796, 235, 'Hajjah', 'HJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3797, 235, 'Al Hudaydah', 'HU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3798, 235, 'Ibb', 'IB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3799, 235, 'Al Jawf', 'JA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3800, 235, 'Lahij', 'LA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3801, 235, 'Ma''rib', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3802, 235, 'Al Mahrah', 'MR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3803, 235, 'Al Mahwit', 'MW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3804, 235, 'Sa''dah', 'SD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3805, 235, 'San''a', 'SN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3806, 235, 'Shabwah', 'SH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3807, 235, 'Ta''izz', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3808, 236, 'Kosovo', 'KOS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3809, 236, 'Montenegro', 'MON', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3810, 236, 'Serbia', 'SER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3811, 236, 'Vojvodina', 'VOJ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3812, 237, 'Bas-Congo', 'BC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3813, 237, 'Bandundu', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3814, 237, 'Equateur', 'EQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3815, 237, 'Katanga', 'KA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3816, 237, 'Kasai-Oriental', 'KE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3817, 237, 'Kinshasa', 'KN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3818, 237, 'Kasai-Occidental', 'KW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3819, 237, 'Maniema', 'MA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3820, 237, 'Nord-Kivu', 'NK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3821, 237, 'Orientale', 'OR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3822, 237, 'Sud-Kivu', 'SK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3823, 238, 'Central', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3824, 238, 'Copperbelt', 'CB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3825, 238, 'Eastern', 'EA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3826, 238, 'Luapula', 'LP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3827, 238, 'Lusaka', 'LK', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3828, 238, 'Northern', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3829, 238, 'North-Western', 'NW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3830, 238, 'Southern', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3831, 238, 'Western', 'WE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3832, 239, 'Bulawayo', 'BU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3833, 239, 'Harare', 'HA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3834, 239, 'Manicaland', 'ML', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3835, 239, 'Mashonaland Central', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3836, 239, 'Mashonaland East', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3837, 239, 'Mashonaland West', 'MW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3838, 239, 'Masvingo', 'MV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3839, 239, 'Matabeleland North', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3840, 239, 'Matabeleland South', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3841, 239, 'Midlands', 'MD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3842, 105, 'Agrigento', 'AG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3843, 105, 'Alessandria', 'AL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3844, 105, 'Ancona', 'AN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3845, 105, 'Aosta', 'AO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3846, 105, 'Arezzo', 'AR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3847, 105, 'Ascoli Piceno', 'AP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3848, 105, 'Asti', 'AT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3849, 105, 'Avellino', 'AV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3850, 105, 'Bari', 'BA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3851, 105, 'Belluno', 'BL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3852, 105, 'Benevento', 'BN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3853, 105, 'Bergamo', 'BG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3854, 105, 'Biella', 'BI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3855, 105, 'Bologna', 'BO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3856, 105, 'Bolzano', 'BZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3857, 105, 'Brescia', 'BS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3858, 105, 'Brindisi', 'BR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3859, 105, 'Cagliari', 'CA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3860, 105, 'Caltanissetta', 'CL', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3861, 105, 'Campobasso', 'CB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3862, 105, 'Carbonia-Iglesias', 'CI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3863, 105, 'Caserta', 'CE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3864, 105, 'Catania', 'CT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3865, 105, 'Catanzaro', 'CZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3866, 105, 'Chieti', 'CH', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3867, 105, 'Como', 'CO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3868, 105, 'Cosenza', 'CS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3869, 105, 'Cremona', 'CR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3870, 105, 'Crotone', 'KR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3871, 105, 'Cuneo', 'CN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3872, 105, 'Enna', 'EN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3873, 105, 'Ferrara', 'FE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3874, 105, 'Firenze', 'FI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3875, 105, 'Foggia', 'FG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3876, 105, 'Forli-Cesena', 'FC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3877, 105, 'Frosinone', 'FR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3878, 105, 'Genova', 'GE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3879, 105, 'Gorizia', 'GO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3880, 105, 'Grosseto', 'GR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3881, 105, 'Imperia', 'IM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3882, 105, 'Isernia', 'IS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3883, 105, 'L&#39;Aquila', 'AQ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3884, 105, 'La Spezia', 'SP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3885, 105, 'Latina', 'LT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3886, 105, 'Lecce', 'LE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3887, 105, 'Lecco', 'LC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3888, 105, 'Livorno', 'LI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3889, 105, 'Lodi', 'LO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3890, 105, 'Lucca', 'LU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3891, 105, 'Macerata', 'MC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3892, 105, 'Mantova', 'MN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3893, 105, 'Massa-Carrara', 'MS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3894, 105, 'Matera', 'MT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3895, 105, 'Medio Campidano', 'VS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3896, 105, 'Messina', 'ME', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3897, 105, 'Milano', 'MI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3898, 105, 'Modena', 'MO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3899, 105, 'Napoli', 'NA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3900, 105, 'Novara', 'NO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3901, 105, 'Nuoro', 'NU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3902, 105, 'Ogliastra', 'OG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3903, 105, 'Olbia-Tempio', 'OT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3904, 105, 'Oristano', 'OR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3905, 105, 'Padova', 'PD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3906, 105, 'Palermo', 'PA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3907, 105, 'Parma', 'PR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3908, 105, 'Pavia', 'PV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3909, 105, 'Perugia', 'PG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3910, 105, 'Pesaro e Urbino', 'PU', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3911, 105, 'Pescara', 'PE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3912, 105, 'Piacenza', 'PC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3913, 105, 'Pisa', 'PI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3914, 105, 'Pistoia', 'PT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3915, 105, 'Pordenone', 'PN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3916, 105, 'Potenza', 'PZ', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3917, 105, 'Prato', 'PO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3918, 105, 'Ragusa', 'RG', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3919, 105, 'Ravenna', 'RA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3920, 105, 'Reggio Calabria', 'RC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3921, 105, 'Reggio Emilia', 'RE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3922, 105, 'Rieti', 'RI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3923, 105, 'Rimini', 'RN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3924, 105, 'Roma', 'RM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3925, 105, 'Rovigo', 'RO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3926, 105, 'Salerno', 'SA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3927, 105, 'Sassari', 'SS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3928, 105, 'Savona', 'SV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3929, 105, 'Siena', 'SI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3930, 105, 'Siracusa', 'SR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3931, 105, 'Sondrio', 'SO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3932, 105, 'Taranto', 'TA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3933, 105, 'Teramo', 'TE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3934, 105, 'Terni', 'TR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3935, 105, 'Torino', 'TO', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3936, 105, 'Trapani', 'TP', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3937, 105, 'Trento', 'TN', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3938, 105, 'Treviso', 'TV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3939, 105, 'Trieste', 'TS', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3940, 105, 'Udine', 'UD', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3941, 105, 'Varese', 'VA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3942, 105, 'Venezia', 'VE', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3944, 105, 'Vercelli', 'VC', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3945, 105, 'Verona', 'VR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3946, 105, 'Vibo Valentia', 'VV', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3947, 105, 'Vicenza', 'VI', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3948, 105, 'Viterbo', 'VT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3949, 222, 'County Antrim', 'ANT', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3950, 222, 'County Armagh', 'ARM', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3951, 222, 'County Down', 'DOW', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3952, 222, 'County Fermanagh', 'FER', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3953, 222, 'County Londonderry', 'LDY', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3954, 222, 'County Tyrone', 'TYR', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3955, 222, 'Cumbria', 'CMA', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3956, 190, 'Pomurska', '1', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3957, 190, 'Podravska', '2', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3958, 190, 'KoroÅ¡ka', '3', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3959, 190, 'Savinjska', '4', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3960, 190, 'Zasavska', '5', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3961, 190, 'Spodnjeposavska', '6', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3962, 190, 'Jugovzhodna Slovenija', '7', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3963, 190, 'Osrednjeslovenska', '8', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3964, 190, 'Gorenjska', '9', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3965, 190, 'Notranjsko-kraÅ¡ka', '10', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3966, 190, 'GoriÅ¡ka', '11', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3967, 190, 'Obalno-kraÅ¡ka', '12', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3968, 33, 'Ruse', '', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24'),
(3969, 101, 'Alborz', 'ALB', 'active', '2013-06-07 21:18:24', '2013-06-07 21:18:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mw_article`
--
ALTER TABLE `mw_article`
 ADD PRIMARY KEY (`article_id`), ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `mw_article_category`
--
ALTER TABLE `mw_article_category`
 ADD PRIMARY KEY (`category_id`), ADD UNIQUE KEY `slug` (`slug`), ADD KEY `fk_article_category_article_category1_idx` (`parent_id`);

--
-- Indexes for table `mw_article_to_category`
--
ALTER TABLE `mw_article_to_category`
 ADD PRIMARY KEY (`article_id`,`category_id`), ADD KEY `fk_article_to_category_article_category1_idx` (`category_id`), ADD KEY `fk_article_to_category_article1_idx` (`article_id`);

--
-- Indexes for table `mw_bounce_server`
--
ALTER TABLE `mw_bounce_server`
 ADD PRIMARY KEY (`server_id`), ADD KEY `fk_bounce_server_customer1_idx` (`customer_id`), ADD KEY `status` (`status`);

--
-- Indexes for table `mw_campaign`
--
ALTER TABLE `mw_campaign`
 ADD PRIMARY KEY (`campaign_id`), ADD UNIQUE KEY `campaign_uid_UNIQUE` (`campaign_uid`), ADD KEY `fk_campaign_list1_idx` (`list_id`), ADD KEY `fk_campaign_list_segment1_idx` (`segment_id`), ADD KEY `fk_campaign_customer1_idx` (`customer_id`), ADD KEY `fk_campaign_campaign_group1_idx` (`group_id`), ADD KEY `type` (`type`), ADD KEY `status_delivery_logs_archived_campaign_id` (`status`,`delivery_logs_archived`,`campaign_id`);

--
-- Indexes for table `mw_campaign_abuse_report`
--
ALTER TABLE `mw_campaign_abuse_report`
 ADD PRIMARY KEY (`report_id`), ADD KEY `fk_campaign_abuse_report_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_abuse_report_customer1_idx` (`customer_id`), ADD KEY `fk_campaign_abuse_report_list1_idx` (`list_id`), ADD KEY `fk_campaign_abuse_report_list_subscriber1_idx` (`subscriber_id`);

--
-- Indexes for table `mw_campaign_attachment`
--
ALTER TABLE `mw_campaign_attachment`
 ADD PRIMARY KEY (`attachment_id`), ADD KEY `fk_campaign_attachment_campaign1_idx` (`campaign_id`);

--
-- Indexes for table `mw_campaign_bounce_log`
--
ALTER TABLE `mw_campaign_bounce_log`
 ADD PRIMARY KEY (`log_id`), ADD UNIQUE KEY `cid_sid` (`campaign_id`,`subscriber_id`), ADD KEY `fk_campaign_bounce_log_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_bounce_log_list_subscriber1_idx` (`subscriber_id`), ADD KEY `sub_proc_bt` (`subscriber_id`,`processed`,`bounce_type`), ADD KEY `proc_bt` (`processed`,`bounce_type`);

--
-- Indexes for table `mw_campaign_delivery_log`
--
ALTER TABLE `mw_campaign_delivery_log`
 ADD PRIMARY KEY (`log_id`), ADD KEY `fk_campaign_delivery_log_list_subscriber1_idx` (`subscriber_id`), ADD KEY `fk_campaign_delivery_log_campaign1_idx` (`campaign_id`), ADD KEY `sub_proc_status` (`subscriber_id`,`processed`,`status`), ADD KEY `proc_status` (`processed`,`status`), ADD KEY `email_message_id` (`email_message_id`), ADD KEY `cid_status` (`campaign_id`,`status`), ADD KEY `cid_date_added` (`campaign_id`,`date_added`), ADD KEY `fk_campaign_delivery_log_delivery_server1_idx` (`server_id`);

--
-- Indexes for table `mw_campaign_delivery_log_archive`
--
ALTER TABLE `mw_campaign_delivery_log_archive`
 ADD PRIMARY KEY (`log_id`), ADD KEY `fk_campaign_delivery_log_archive_list_subscriber1_idx` (`subscriber_id`), ADD KEY `fk_campaign_delivery_log_archive_campaign1_idx` (`campaign_id`), ADD KEY `sub_proc_status` (`subscriber_id`,`processed`,`status`), ADD KEY `proc_status` (`processed`,`status`), ADD KEY `email_message_id` (`email_message_id`), ADD KEY `fk_campaign_delivery_log_archive_delivery_server1_idx` (`server_id`);

--
-- Indexes for table `mw_campaign_forward_friend`
--
ALTER TABLE `mw_campaign_forward_friend`
 ADD PRIMARY KEY (`forward_id`), ADD KEY `fk_campaign_forward_friend_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_forward_friend_list_subscriber1_idx` (`subscriber_id`);

--
-- Indexes for table `mw_campaign_group`
--
ALTER TABLE `mw_campaign_group`
 ADD PRIMARY KEY (`group_id`), ADD UNIQUE KEY `group_uid` (`group_uid`), ADD KEY `fk_campaign_group_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_campaign_open_action_list_field`
--
ALTER TABLE `mw_campaign_open_action_list_field`
 ADD PRIMARY KEY (`action_id`), ADD KEY `fk_campaign_open_action_list_field_list1_idx` (`list_id`), ADD KEY `fk_campaign_open_action_list_field_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_open_action_list_field_list_field1_idx` (`field_id`);

--
-- Indexes for table `mw_campaign_open_action_subscriber`
--
ALTER TABLE `mw_campaign_open_action_subscriber`
 ADD PRIMARY KEY (`action_id`), ADD KEY `fk_campaign_open_action_subscriber_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_open_action_subscriber_list1_idx` (`list_id`);

--
-- Indexes for table `mw_campaign_option`
--
ALTER TABLE `mw_campaign_option`
 ADD PRIMARY KEY (`campaign_id`), ADD KEY `fk_campaign_option_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_option_campaign2_idx` (`autoresponder_open_campaign_id`), ADD KEY `fk_campaign_option_campaign3_idx` (`regular_open_unopen_campaign_id`), ADD KEY `fk_campaign_option_campaign4_idx` (`tracking_domain_id`);

--
-- Indexes for table `mw_campaign_template`
--
ALTER TABLE `mw_campaign_template`
 ADD PRIMARY KEY (`template_id`), ADD KEY `fk_customer_email_template1_idx` (`customer_template_id`), ADD KEY `fk_campaign_template_campaign1_idx` (`campaign_id`);

--
-- Indexes for table `mw_campaign_template_url_action_list_field`
--
ALTER TABLE `mw_campaign_template_url_action_list_field`
 ADD PRIMARY KEY (`url_id`), ADD KEY `fk_campaign_template_url_action_list_field_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_template_url_action_list_field_list1_idx` (`list_id`), ADD KEY `fk_campaign_template_url_action_list_field_campaign_temp_idx` (`template_id`), ADD KEY `fk_campaign_template_url_action_list_field_list_field1_idx` (`field_id`);

--
-- Indexes for table `mw_campaign_template_url_action_subscriber`
--
ALTER TABLE `mw_campaign_template_url_action_subscriber`
 ADD PRIMARY KEY (`url_id`), ADD KEY `fk_campaign_template_url_action_subscriber_campaign_t_idx` (`template_id`), ADD KEY `fk_campaign_template_url_action_subscriber_list1_idx` (`list_id`), ADD KEY `fk_campaign_template_url_action_subscriber_campaign1_idx` (`campaign_id`);

--
-- Indexes for table `mw_campaign_temporary_source`
--
ALTER TABLE `mw_campaign_temporary_source`
 ADD PRIMARY KEY (`source_id`), ADD KEY `fk_campaign_temporary_source_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_temporary_source_list1_idx` (`list_id`), ADD KEY `fk_campaign_temporary_source_list_segment1_idx` (`segment_id`);

--
-- Indexes for table `mw_campaign_to_delivery_server`
--
ALTER TABLE `mw_campaign_to_delivery_server`
 ADD PRIMARY KEY (`campaign_id`,`server_id`), ADD KEY `fk_campaign_to_delivery_server_delivery_server1_idx` (`server_id`), ADD KEY `fk_campaign_to_delivery_server_campaign1_idx` (`campaign_id`);

--
-- Indexes for table `mw_campaign_track_open`
--
ALTER TABLE `mw_campaign_track_open`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_campaign_track_open_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_track_open_list_subscriber1_idx` (`subscriber_id`), ADD KEY `fk_campaign_track_open_ip_location1_idx` (`location_id`);

--
-- Indexes for table `mw_campaign_track_unsubscribe`
--
ALTER TABLE `mw_campaign_track_unsubscribe`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_campaign_track_unsubscribe_campaign1_idx` (`campaign_id`), ADD KEY `fk_campaign_track_unsubscribe_list_subscriber1_idx` (`subscriber_id`), ADD KEY `fk_campaign_track_unsubscribe_ip_location1_idx` (`location_id`), ADD KEY `date_added` (`date_added`);

--
-- Indexes for table `mw_campaign_track_url`
--
ALTER TABLE `mw_campaign_track_url`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_campaign_track_url_list_subscriber1_idx` (`subscriber_id`), ADD KEY `fk_campaign_track_url_ip_location1_idx` (`location_id`), ADD KEY `fk_campaign_track_url_campaign_url1_idx` (`url_id`);

--
-- Indexes for table `mw_campaign_url`
--
ALTER TABLE `mw_campaign_url`
 ADD PRIMARY KEY (`url_id`), ADD KEY `campaign_hash` (`campaign_id`,`hash`), ADD KEY `fk_campaign_url_campaign1_idx` (`campaign_id`);

--
-- Indexes for table `mw_company_type`
--
ALTER TABLE `mw_company_type`
 ADD PRIMARY KEY (`type_id`), ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `mw_country`
--
ALTER TABLE `mw_country`
 ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `mw_currency`
--
ALTER TABLE `mw_currency`
 ADD PRIMARY KEY (`currency_id`), ADD UNIQUE KEY `code_UNIQUE` (`code`);

--
-- Indexes for table `mw_customer`
--
ALTER TABLE `mw_customer`
 ADD PRIMARY KEY (`customer_id`), ADD UNIQUE KEY `customer_uid_UNIQUE` (`customer_uid`), ADD UNIQUE KEY `email_UNIQUE` (`email`), ADD KEY `fk_customer_language1_idx` (`language_id`), ADD KEY `fk_customer_customer_group1_idx` (`group_id`), ADD KEY `oauth` (`oauth_uid`,`oauth_provider`);

--
-- Indexes for table `mw_customer_action_log`
--
ALTER TABLE `mw_customer_action_log`
 ADD PRIMARY KEY (`log_id`), ADD KEY `fk_customer_notification_log_customer1_idx` (`customer_id`), ADD KEY `customer_category_reference` (`customer_id`,`category`,`reference_id`);

--
-- Indexes for table `mw_customer_api_key`
--
ALTER TABLE `mw_customer_api_key`
 ADD PRIMARY KEY (`key_id`), ADD UNIQUE KEY `public_UNIQUE` (`public`), ADD UNIQUE KEY `private_UNIQUE` (`private`), ADD KEY `fk_customer_api_key_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_customer_auto_login_token`
--
ALTER TABLE `mw_customer_auto_login_token`
 ADD PRIMARY KEY (`token_id`), ADD KEY `fk_customer_auto_login_token_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_customer_campaign_tag`
--
ALTER TABLE `mw_customer_campaign_tag`
 ADD PRIMARY KEY (`tag_id`), ADD UNIQUE KEY `customer_campaign_tag_uid` (`tag_uid`), ADD KEY `fk_customer_campaign_tag_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_customer_company`
--
ALTER TABLE `mw_customer_company`
 ADD PRIMARY KEY (`company_id`), ADD KEY `fk_customer_company_country1_idx` (`country_id`), ADD KEY `fk_customer_company_zone1_idx` (`zone_id`), ADD KEY `fk_customer_company_customer1_idx` (`customer_id`), ADD KEY `fk_customer_company_company_type1_idx` (`type_id`);

--
-- Indexes for table `mw_customer_email_blacklist`
--
ALTER TABLE `mw_customer_email_blacklist`
 ADD PRIMARY KEY (`email_id`), ADD UNIQUE KEY `unique_id_UNIQUE` (`email_uid`), ADD UNIQUE KEY `customer_id_email_UNIQUE` (`customer_id`,`email`), ADD KEY `fk_customer_email_blacklist_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_customer_email_template`
--
ALTER TABLE `mw_customer_email_template`
 ADD PRIMARY KEY (`template_id`), ADD KEY `fk_customer_email_template_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_customer_group`
--
ALTER TABLE `mw_customer_group`
 ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `mw_customer_group_option`
--
ALTER TABLE `mw_customer_group_option`
 ADD PRIMARY KEY (`option_id`), ADD KEY `fk_customer_group_option_customer_group1_idx` (`group_id`), ADD KEY `group_code` (`group_id`,`code`);

--
-- Indexes for table `mw_customer_login_log`
--
ALTER TABLE `mw_customer_login_log`
 ADD PRIMARY KEY (`log_id`), ADD KEY `fk_customer_login_log_customer1_idx` (`customer_id`), ADD KEY `fk_customer_login_log_ip_location1_idx` (`location_id`);

--
-- Indexes for table `mw_customer_message`
--
ALTER TABLE `mw_customer_message`
 ADD PRIMARY KEY (`message_id`), ADD KEY `fk_customer_message_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_customer_password_reset`
--
ALTER TABLE `mw_customer_password_reset`
 ADD PRIMARY KEY (`request_id`), ADD KEY `fk_customer_password_reset_customer1` (`customer_id`), ADD KEY `key_status` (`reset_key`,`status`);

--
-- Indexes for table `mw_customer_quota_mark`
--
ALTER TABLE `mw_customer_quota_mark`
 ADD PRIMARY KEY (`mark_id`), ADD KEY `fk_customer_quota_mark_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_delivery_server`
--
ALTER TABLE `mw_delivery_server`
 ADD PRIMARY KEY (`server_id`), ADD KEY `fk_delivery_server_bounce_server1_idx` (`bounce_server_id`), ADD KEY `idx_gen0` (`status`,`hourly_quota`,`probability`), ADD KEY `fk_delivery_server_customer1_idx` (`customer_id`), ADD KEY `fk_delivery_server_tracking_domain1_idx` (`tracking_domain_id`);

--
-- Indexes for table `mw_delivery_server_domain_policy`
--
ALTER TABLE `mw_delivery_server_domain_policy`
 ADD PRIMARY KEY (`domain_id`), ADD KEY `fk_delivery_server_domain_policy_delivery_server1_idx` (`server_id`), ADD KEY `server_domain_policy` (`server_id`,`domain`,`policy`);

--
-- Indexes for table `mw_delivery_server_to_customer_group`
--
ALTER TABLE `mw_delivery_server_to_customer_group`
 ADD PRIMARY KEY (`server_id`,`group_id`), ADD KEY `fk_delivery_server_to_customer_group_customer_group1_idx` (`group_id`), ADD KEY `fk_delivery_server_to_customer_group_delivery_server1_idx` (`server_id`);

--
-- Indexes for table `mw_delivery_server_usage_log`
--
ALTER TABLE `mw_delivery_server_usage_log`
 ADD PRIMARY KEY (`log_id`), ADD KEY `fk_delivery_server_usage_log_delivery_server1_idx` (`server_id`), ADD KEY `fk_delivery_server_usage_log_customer1_idx` (`customer_id`), ADD KEY `server_date` (`server_id`,`date_added`), ADD KEY `customer_countable_date` (`customer_id`,`customer_countable`,`date_added`);

--
-- Indexes for table `mw_email_blacklist`
--
ALTER TABLE `mw_email_blacklist`
 ADD PRIMARY KEY (`email_id`), ADD UNIQUE KEY `email` (`email`), ADD KEY `fk_email_blacklist_list_subscriber1_idx` (`subscriber_id`);

--
-- Indexes for table `mw_email_blacklist_monitor`
--
ALTER TABLE `mw_email_blacklist_monitor`
 ADD PRIMARY KEY (`monitor_id`);

--
-- Indexes for table `mw_email_blacklist_suggest`
--
ALTER TABLE `mw_email_blacklist_suggest`
 ADD PRIMARY KEY (`email_id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `mw_feedback_loop_server`
--
ALTER TABLE `mw_feedback_loop_server`
 ADD PRIMARY KEY (`server_id`), ADD KEY `fk_feedback_loop_server_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_guest_fail_attempt`
--
ALTER TABLE `mw_guest_fail_attempt`
 ADD PRIMARY KEY (`attempt_id`), ADD KEY `ip_hash_date` (`ip_address_hash`,`date_added`);

--
-- Indexes for table `mw_ip_location`
--
ALTER TABLE `mw_ip_location`
 ADD PRIMARY KEY (`location_id`), ADD UNIQUE KEY `ip_address_UNIQUE` (`ip_address`);

--
-- Indexes for table `mw_language`
--
ALTER TABLE `mw_language`
 ADD PRIMARY KEY (`language_id`), ADD KEY `is_default` (`is_default`);

--
-- Indexes for table `mw_list`
--
ALTER TABLE `mw_list`
 ADD PRIMARY KEY (`list_id`), ADD UNIQUE KEY `unique_id_UNIQUE` (`list_uid`), ADD KEY `fk_list_customer1_idx` (`customer_id`), ADD KEY `status_visibility` (`status`,`visibility`);

--
-- Indexes for table `mw_list_company`
--
ALTER TABLE `mw_list_company`
 ADD PRIMARY KEY (`list_id`), ADD KEY `fk_customer_company_country1_idx` (`country_id`), ADD KEY `fk_customer_company_zone1_idx` (`zone_id`), ADD KEY `fk_list_company_company_type1_idx` (`type_id`);

--
-- Indexes for table `mw_list_customer_notification`
--
ALTER TABLE `mw_list_customer_notification`
 ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `mw_list_default`
--
ALTER TABLE `mw_list_default`
 ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `mw_list_field`
--
ALTER TABLE `mw_list_field`
 ADD PRIMARY KEY (`field_id`), ADD KEY `fk_list_field_list1_idx` (`list_id`), ADD KEY `fk_list_field_list_field_type1_idx` (`type_id`), ADD KEY `list_tag` (`list_id`,`tag`);

--
-- Indexes for table `mw_list_field_option`
--
ALTER TABLE `mw_list_field_option`
 ADD PRIMARY KEY (`option_id`), ADD KEY `fk_list_field_option_list_field1_idx` (`field_id`);

--
-- Indexes for table `mw_list_field_type`
--
ALTER TABLE `mw_list_field_type`
 ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `mw_list_field_value`
--
ALTER TABLE `mw_list_field_value`
 ADD PRIMARY KEY (`value_id`), ADD KEY `fk_list_field_value_list_field1_idx` (`field_id`), ADD KEY `fk_list_field_value_list_subscriber1_idx` (`subscriber_id`), ADD KEY `field_subscriber` (`field_id`,`subscriber_id`), ADD KEY `field_id_value` (`field_id`,`value`);

--
-- Indexes for table `mw_list_page`
--
ALTER TABLE `mw_list_page`
 ADD PRIMARY KEY (`list_id`,`type_id`), ADD KEY `fk_list_page_list_page_type1_idx` (`type_id`);

--
-- Indexes for table `mw_list_page_type`
--
ALTER TABLE `mw_list_page_type`
 ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `mw_list_segment`
--
ALTER TABLE `mw_list_segment`
 ADD PRIMARY KEY (`segment_id`), ADD UNIQUE KEY `segment_uid` (`segment_uid`), ADD KEY `fk_list_segment_list1_idx` (`list_id`);

--
-- Indexes for table `mw_list_segment_condition`
--
ALTER TABLE `mw_list_segment_condition`
 ADD PRIMARY KEY (`condition_id`), ADD KEY `fk_list_segment_condition_list_segment_operator1_idx` (`operator_id`), ADD KEY `fk_list_segment_condition_list_segment1_idx` (`segment_id`), ADD KEY `fk_list_segment_condition_list_field1_idx` (`field_id`);

--
-- Indexes for table `mw_list_segment_operator`
--
ALTER TABLE `mw_list_segment_operator`
 ADD PRIMARY KEY (`operator_id`);

--
-- Indexes for table `mw_list_subscriber`
--
ALTER TABLE `mw_list_subscriber`
 ADD PRIMARY KEY (`subscriber_id`), ADD UNIQUE KEY `unique_id_UNIQUE` (`subscriber_uid`), ADD KEY `fk_list_subscriber_list1_idx` (`list_id`), ADD KEY `list_email` (`list_id`,`email`), ADD KEY `status_last_updated` (`status`,`last_updated`), ADD KEY `list_id_status` (`list_id`,`status`), ADD KEY `email` (`email`);

--
-- Indexes for table `mw_list_subscriber_action`
--
ALTER TABLE `mw_list_subscriber_action`
 ADD PRIMARY KEY (`action_id`), ADD KEY `fk_list_subscriber_action_list1_idx` (`source_list_id`), ADD KEY `fk_list_subscriber_action_list2_idx` (`target_list_id`);

--
-- Indexes for table `mw_list_subscriber_field_cache`
--
ALTER TABLE `mw_list_subscriber_field_cache`
 ADD PRIMARY KEY (`subscriber_id`), ADD KEY `fk_list_subscriber_field_cache_list_subscriber1_idx` (`subscriber_id`);

--
-- Indexes for table `mw_list_subscriber_list_move`
--
ALTER TABLE `mw_list_subscriber_list_move`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_list_subscriber_list_move_list_subscriber1_idx` (`source_subscriber_id`), ADD KEY `fk_list_subscriber_list_move_list1_idx` (`source_list_id`), ADD KEY `fk_list_subscriber_list_move_list_subscriber2_idx` (`destination_subscriber_id`), ADD KEY `fk_list_subscriber_list_move_list2_idx` (`destination_list_id`);

--
-- Indexes for table `mw_option`
--
ALTER TABLE `mw_option`
 ADD PRIMARY KEY (`category`,`key`);

--
-- Indexes for table `mw_price_plan`
--
ALTER TABLE `mw_price_plan`
 ADD PRIMARY KEY (`plan_id`), ADD UNIQUE KEY `plan_uid_UNIQUE` (`plan_uid`), ADD KEY `fk_price_plan_customer_group1_idx` (`group_id`);

--
-- Indexes for table `mw_price_plan_order`
--
ALTER TABLE `mw_price_plan_order`
 ADD PRIMARY KEY (`order_id`), ADD UNIQUE KEY `order_uid_UNIQUE` (`order_uid`), ADD KEY `fk_price_plan_order_price_plan1_idx` (`plan_id`), ADD KEY `fk_price_plan_order_customer1_idx` (`customer_id`), ADD KEY `fk_price_plan_order_price_plan_promo_code1_idx` (`promo_code_id`), ADD KEY `fk_price_plan_order_currency1_idx` (`currency_id`), ADD KEY `fk_price_plan_order_price_plan_tax1_idx` (`tax_id`);

--
-- Indexes for table `mw_price_plan_order_note`
--
ALTER TABLE `mw_price_plan_order_note`
 ADD PRIMARY KEY (`note_id`), ADD KEY `fk_price_plan_order_note_price_plan_order1_idx` (`order_id`), ADD KEY `fk_price_plan_order_note_customer1_idx` (`customer_id`), ADD KEY `fk_price_plan_order_note_user1_idx` (`user_id`);

--
-- Indexes for table `mw_price_plan_order_transaction`
--
ALTER TABLE `mw_price_plan_order_transaction`
 ADD PRIMARY KEY (`transaction_id`), ADD UNIQUE KEY `transaction_uid_UNIQUE` (`transaction_uid`), ADD KEY `fk_price_plan_order_transaction_price_plan_order1_idx` (`order_id`);

--
-- Indexes for table `mw_price_plan_promo_code`
--
ALTER TABLE `mw_price_plan_promo_code`
 ADD PRIMARY KEY (`promo_code_id`);

--
-- Indexes for table `mw_sending_domain`
--
ALTER TABLE `mw_sending_domain`
 ADD PRIMARY KEY (`domain_id`), ADD KEY `fk_sending_domain_customer1_idx` (`customer_id`), ADD KEY `name_verified_customer` (`name`,`verified`,`customer_id`);

--
-- Indexes for table `mw_session`
--
ALTER TABLE `mw_session`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mw_tag_registry`
--
ALTER TABLE `mw_tag_registry`
 ADD PRIMARY KEY (`tag_id`), ADD UNIQUE KEY `tag_UNIQUE` (`tag`);

--
-- Indexes for table `mw_tax`
--
ALTER TABLE `mw_tax`
 ADD PRIMARY KEY (`tax_id`), ADD KEY `fk_tax_zone1_idx` (`zone_id`), ADD KEY `fk_tax_country1_idx` (`country_id`);

--
-- Indexes for table `mw_tour_slideshow`
--
ALTER TABLE `mw_tour_slideshow`
 ADD PRIMARY KEY (`slideshow_id`);

--
-- Indexes for table `mw_tour_slideshow_slide`
--
ALTER TABLE `mw_tour_slideshow_slide`
 ADD PRIMARY KEY (`slide_id`), ADD KEY `fk_tour_slideshow_slide_tour_slideshow1_idx` (`slideshow_id`);

--
-- Indexes for table `mw_tracking_domain`
--
ALTER TABLE `mw_tracking_domain`
 ADD PRIMARY KEY (`domain_id`), ADD KEY `fk_tracking_domain_customer1_idx` (`customer_id`);

--
-- Indexes for table `mw_transactional_email`
--
ALTER TABLE `mw_transactional_email`
 ADD PRIMARY KEY (`email_id`), ADD UNIQUE KEY `email_uid_UNIQUE` (`email_uid`), ADD KEY `fk_transactional_email_customer1_idx` (`customer_id`), ADD KEY `status_send_at_retries_max_retries` (`status`,`send_at`,`retries`,`max_retries`);

--
-- Indexes for table `mw_transactional_email_log`
--
ALTER TABLE `mw_transactional_email_log`
 ADD PRIMARY KEY (`log_id`), ADD KEY `fk_transactional_email_log_transactional_email1_idx` (`email_id`);

--
-- Indexes for table `mw_user`
--
ALTER TABLE `mw_user`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `user_uid_UNIQUE` (`user_uid`), ADD UNIQUE KEY `email_UNIQUE` (`email`), ADD KEY `fk_user_language1_idx` (`language_id`), ADD KEY `fk_user_user_group1_idx` (`group_id`);

--
-- Indexes for table `mw_user_auto_login_token`
--
ALTER TABLE `mw_user_auto_login_token`
 ADD PRIMARY KEY (`token_id`), ADD KEY `fk_user_auto_login_token_user1_idx` (`user_id`);

--
-- Indexes for table `mw_user_group`
--
ALTER TABLE `mw_user_group`
 ADD PRIMARY KEY (`group_id`), ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `mw_user_group_route_access`
--
ALTER TABLE `mw_user_group_route_access`
 ADD PRIMARY KEY (`route_id`), ADD KEY `fk_user_group_route_access_user_group1_idx` (`group_id`), ADD KEY `group_route_access` (`group_id`,`route`,`access`);

--
-- Indexes for table `mw_user_message`
--
ALTER TABLE `mw_user_message`
 ADD PRIMARY KEY (`message_id`), ADD KEY `fk_user_message_user1_idx` (`user_id`);

--
-- Indexes for table `mw_user_password_reset`
--
ALTER TABLE `mw_user_password_reset`
 ADD PRIMARY KEY (`request_id`), ADD KEY `fk_user_password_reset_user1_idx` (`user_id`), ADD KEY `key_status` (`reset_key`,`status`);

--
-- Indexes for table `mw_zone`
--
ALTER TABLE `mw_zone`
 ADD PRIMARY KEY (`zone_id`), ADD KEY `fk_zone_country1_idx` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mw_article`
--
ALTER TABLE `mw_article`
MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_article_category`
--
ALTER TABLE `mw_article_category`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_bounce_server`
--
ALTER TABLE `mw_bounce_server`
MODIFY `server_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_campaign`
--
ALTER TABLE `mw_campaign`
MODIFY `campaign_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `mw_campaign_abuse_report`
--
ALTER TABLE `mw_campaign_abuse_report`
MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_attachment`
--
ALTER TABLE `mw_campaign_attachment`
MODIFY `attachment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_bounce_log`
--
ALTER TABLE `mw_campaign_bounce_log`
MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_delivery_log`
--
ALTER TABLE `mw_campaign_delivery_log`
MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mw_campaign_delivery_log_archive`
--
ALTER TABLE `mw_campaign_delivery_log_archive`
MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_forward_friend`
--
ALTER TABLE `mw_campaign_forward_friend`
MODIFY `forward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_group`
--
ALTER TABLE `mw_campaign_group`
MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_open_action_list_field`
--
ALTER TABLE `mw_campaign_open_action_list_field`
MODIFY `action_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_open_action_subscriber`
--
ALTER TABLE `mw_campaign_open_action_subscriber`
MODIFY `action_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `mw_campaign_template`
--
ALTER TABLE `mw_campaign_template`
MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `mw_campaign_template_url_action_list_field`
--
ALTER TABLE `mw_campaign_template_url_action_list_field`
MODIFY `url_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_template_url_action_subscriber`
--
ALTER TABLE `mw_campaign_template_url_action_subscriber`
MODIFY `url_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_temporary_source`
--
ALTER TABLE `mw_campaign_temporary_source`
MODIFY `source_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_track_open`
--
ALTER TABLE `mw_campaign_track_open`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mw_campaign_track_unsubscribe`
--
ALTER TABLE `mw_campaign_track_unsubscribe`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_campaign_track_url`
--
ALTER TABLE `mw_campaign_track_url`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_campaign_url`
--
ALTER TABLE `mw_campaign_url`
MODIFY `url_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `mw_company_type`
--
ALTER TABLE `mw_company_type`
MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `mw_country`
--
ALTER TABLE `mw_country`
MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `mw_currency`
--
ALTER TABLE `mw_currency`
MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_customer`
--
ALTER TABLE `mw_customer`
MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_customer_action_log`
--
ALTER TABLE `mw_customer_action_log`
MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=264;
--
-- AUTO_INCREMENT for table `mw_customer_api_key`
--
ALTER TABLE `mw_customer_api_key`
MODIFY `key_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_customer_auto_login_token`
--
ALTER TABLE `mw_customer_auto_login_token`
MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `mw_customer_campaign_tag`
--
ALTER TABLE `mw_customer_campaign_tag`
MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_company`
--
ALTER TABLE `mw_customer_company`
MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_email_blacklist`
--
ALTER TABLE `mw_customer_email_blacklist`
MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_email_template`
--
ALTER TABLE `mw_customer_email_template`
MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_group`
--
ALTER TABLE `mw_customer_group`
MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_group_option`
--
ALTER TABLE `mw_customer_group_option`
MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_login_log`
--
ALTER TABLE `mw_customer_login_log`
MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `mw_customer_message`
--
ALTER TABLE `mw_customer_message`
MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_password_reset`
--
ALTER TABLE `mw_customer_password_reset`
MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_customer_quota_mark`
--
ALTER TABLE `mw_customer_quota_mark`
MODIFY `mark_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_delivery_server`
--
ALTER TABLE `mw_delivery_server`
MODIFY `server_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_delivery_server_domain_policy`
--
ALTER TABLE `mw_delivery_server_domain_policy`
MODIFY `domain_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_delivery_server_usage_log`
--
ALTER TABLE `mw_delivery_server_usage_log`
MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2246;
--
-- AUTO_INCREMENT for table `mw_email_blacklist`
--
ALTER TABLE `mw_email_blacklist`
MODIFY `email_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_email_blacklist_monitor`
--
ALTER TABLE `mw_email_blacklist_monitor`
MODIFY `monitor_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_email_blacklist_suggest`
--
ALTER TABLE `mw_email_blacklist_suggest`
MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_feedback_loop_server`
--
ALTER TABLE `mw_feedback_loop_server`
MODIFY `server_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_guest_fail_attempt`
--
ALTER TABLE `mw_guest_fail_attempt`
MODIFY `attempt_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_ip_location`
--
ALTER TABLE `mw_ip_location`
MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_language`
--
ALTER TABLE `mw_language`
MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_list`
--
ALTER TABLE `mw_list`
MODIFY `list_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mw_list_field`
--
ALTER TABLE `mw_list_field`
MODIFY `field_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mw_list_field_option`
--
ALTER TABLE `mw_list_field_option`
MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_list_field_type`
--
ALTER TABLE `mw_list_field_type`
MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `mw_list_field_value`
--
ALTER TABLE `mw_list_field_value`
MODIFY `value_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `mw_list_page_type`
--
ALTER TABLE `mw_list_page_type`
MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mw_list_segment`
--
ALTER TABLE `mw_list_segment`
MODIFY `segment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_list_segment_condition`
--
ALTER TABLE `mw_list_segment_condition`
MODIFY `condition_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_list_segment_operator`
--
ALTER TABLE `mw_list_segment_operator`
MODIFY `operator_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mw_list_subscriber`
--
ALTER TABLE `mw_list_subscriber`
MODIFY `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mw_list_subscriber_action`
--
ALTER TABLE `mw_list_subscriber_action`
MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_list_subscriber_list_move`
--
ALTER TABLE `mw_list_subscriber_list_move`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mw_price_plan`
--
ALTER TABLE `mw_price_plan`
MODIFY `plan_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_price_plan_order`
--
ALTER TABLE `mw_price_plan_order`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_price_plan_order_note`
--
ALTER TABLE `mw_price_plan_order_note`
MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_price_plan_order_transaction`
--
ALTER TABLE `mw_price_plan_order_transaction`
MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_price_plan_promo_code`
--
ALTER TABLE `mw_price_plan_promo_code`
MODIFY `promo_code_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_sending_domain`
--
ALTER TABLE `mw_sending_domain`
MODIFY `domain_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_tag_registry`
--
ALTER TABLE `mw_tag_registry`
MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `mw_tax`
--
ALTER TABLE `mw_tax`
MODIFY `tax_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_tour_slideshow`
--
ALTER TABLE `mw_tour_slideshow`
MODIFY `slideshow_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mw_tour_slideshow_slide`
--
ALTER TABLE `mw_tour_slideshow_slide`
MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `mw_tracking_domain`
--
ALTER TABLE `mw_tracking_domain`
MODIFY `domain_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_transactional_email`
--
ALTER TABLE `mw_transactional_email`
MODIFY `email_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_transactional_email_log`
--
ALTER TABLE `mw_transactional_email_log`
MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_user`
--
ALTER TABLE `mw_user`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mw_user_auto_login_token`
--
ALTER TABLE `mw_user_auto_login_token`
MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `mw_user_group`
--
ALTER TABLE `mw_user_group`
MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_user_group_route_access`
--
ALTER TABLE `mw_user_group_route_access`
MODIFY `route_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_user_message`
--
ALTER TABLE `mw_user_message`
MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mw_user_password_reset`
--
ALTER TABLE `mw_user_password_reset`
MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mw_zone`
--
ALTER TABLE `mw_zone`
MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3970;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `mw_article_category`
--
ALTER TABLE `mw_article_category`
ADD CONSTRAINT `fk_article_category_article_category1` FOREIGN KEY (`parent_id`) REFERENCES `mw_article_category` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_article_to_category`
--
ALTER TABLE `mw_article_to_category`
ADD CONSTRAINT `fk_article_to_category_article1` FOREIGN KEY (`article_id`) REFERENCES `mw_article` (`article_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_article_to_category_article_category1` FOREIGN KEY (`category_id`) REFERENCES `mw_article_category` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_bounce_server`
--
ALTER TABLE `mw_bounce_server`
ADD CONSTRAINT `fk_bounce_server_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign`
--
ALTER TABLE `mw_campaign`
ADD CONSTRAINT `fk_campaign_campaign_group1` FOREIGN KEY (`group_id`) REFERENCES `mw_campaign_group` (`group_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_list_segment1` FOREIGN KEY (`segment_id`) REFERENCES `mw_list_segment` (`segment_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_abuse_report`
--
ALTER TABLE `mw_campaign_abuse_report`
ADD CONSTRAINT `fk_campaign_abuse_report_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_abuse_report_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_abuse_report_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_abuse_report_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_attachment`
--
ALTER TABLE `mw_campaign_attachment`
ADD CONSTRAINT `fk_campaign_attachment_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_bounce_log`
--
ALTER TABLE `mw_campaign_bounce_log`
ADD CONSTRAINT `fk_campaign_bounce_log_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_bounce_log_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_delivery_log`
--
ALTER TABLE `mw_campaign_delivery_log`
ADD CONSTRAINT `fk_campaign_delivery_log_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_delivery_log_delivery_server1` FOREIGN KEY (`server_id`) REFERENCES `mw_delivery_server` (`server_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_delivery_log_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_delivery_log_archive`
--
ALTER TABLE `mw_campaign_delivery_log_archive`
ADD CONSTRAINT `fk_campaign_delivery_log_archive_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_delivery_log_archive_delivery_server1` FOREIGN KEY (`server_id`) REFERENCES `mw_delivery_server` (`server_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_delivery_log_archive_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_forward_friend`
--
ALTER TABLE `mw_campaign_forward_friend`
ADD CONSTRAINT `fk_campaign_forward_friend_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_forward_friend_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_group`
--
ALTER TABLE `mw_campaign_group`
ADD CONSTRAINT `fk_campaign_group_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_open_action_list_field`
--
ALTER TABLE `mw_campaign_open_action_list_field`
ADD CONSTRAINT `fk_campaign_open_action_list_field_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_open_action_list_field_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_open_action_list_field_list_field1` FOREIGN KEY (`field_id`) REFERENCES `mw_list_field` (`field_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_open_action_subscriber`
--
ALTER TABLE `mw_campaign_open_action_subscriber`
ADD CONSTRAINT `fk_campaign_open_action_subscriber_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_open_action_subscriber_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_option`
--
ALTER TABLE `mw_campaign_option`
ADD CONSTRAINT `fk_campaign_option_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_option_campaign2` FOREIGN KEY (`autoresponder_open_campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_option_campaign3` FOREIGN KEY (`regular_open_unopen_campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_option_campaign4` FOREIGN KEY (`tracking_domain_id`) REFERENCES `mw_tracking_domain` (`domain_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_template`
--
ALTER TABLE `mw_campaign_template`
ADD CONSTRAINT `fk_campaign_template_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_customer_email_template1` FOREIGN KEY (`customer_template_id`) REFERENCES `mw_customer_email_template` (`template_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_template_url_action_list_field`
--
ALTER TABLE `mw_campaign_template_url_action_list_field`
ADD CONSTRAINT `fk_campaign_template_url_action_list_field_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_template_url_action_list_field_campaign_templa1` FOREIGN KEY (`template_id`) REFERENCES `mw_campaign_template` (`template_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_template_url_action_list_field_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_template_url_action_list_field_list_field1` FOREIGN KEY (`field_id`) REFERENCES `mw_list_field` (`field_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_template_url_action_subscriber`
--
ALTER TABLE `mw_campaign_template_url_action_subscriber`
ADD CONSTRAINT `fk_campaign_template_url_action_subscriber_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_template_url_action_subscriber_campaign_tem1` FOREIGN KEY (`template_id`) REFERENCES `mw_campaign_template` (`template_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_template_url_action_subscriber_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_temporary_source`
--
ALTER TABLE `mw_campaign_temporary_source`
ADD CONSTRAINT `fk_campaign_temporary_source_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_temporary_source_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_temporary_source_list_segment1` FOREIGN KEY (`segment_id`) REFERENCES `mw_list_segment` (`segment_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_to_delivery_server`
--
ALTER TABLE `mw_campaign_to_delivery_server`
ADD CONSTRAINT `fk_campaign_to_delivery_server_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_to_delivery_server_delivery_server1` FOREIGN KEY (`server_id`) REFERENCES `mw_delivery_server` (`server_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_track_open`
--
ALTER TABLE `mw_campaign_track_open`
ADD CONSTRAINT `fk_campaign_track_open_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_track_open_ip_location1` FOREIGN KEY (`location_id`) REFERENCES `mw_ip_location` (`location_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_track_open_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_track_unsubscribe`
--
ALTER TABLE `mw_campaign_track_unsubscribe`
ADD CONSTRAINT `fk_campaign_track_unsubscribe_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_track_unsubscribe_ip_location1` FOREIGN KEY (`location_id`) REFERENCES `mw_ip_location` (`location_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_track_unsubscribe_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_track_url`
--
ALTER TABLE `mw_campaign_track_url`
ADD CONSTRAINT `fk_campaign_track_url_campaign_url1` FOREIGN KEY (`url_id`) REFERENCES `mw_campaign_url` (`url_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_track_url_ip_location1` FOREIGN KEY (`location_id`) REFERENCES `mw_ip_location` (`location_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_campaign_track_url_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_campaign_url`
--
ALTER TABLE `mw_campaign_url`
ADD CONSTRAINT `fk_campaign_url_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `mw_campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer`
--
ALTER TABLE `mw_customer`
ADD CONSTRAINT `fk_customer_customer_group1` FOREIGN KEY (`group_id`) REFERENCES `mw_customer_group` (`group_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_customer_language1` FOREIGN KEY (`language_id`) REFERENCES `mw_language` (`language_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_action_log`
--
ALTER TABLE `mw_customer_action_log`
ADD CONSTRAINT `fk_customer_notification_log_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_api_key`
--
ALTER TABLE `mw_customer_api_key`
ADD CONSTRAINT `fk_customer_api_key_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_auto_login_token`
--
ALTER TABLE `mw_customer_auto_login_token`
ADD CONSTRAINT `fk_customer_auto_login_token_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_campaign_tag`
--
ALTER TABLE `mw_customer_campaign_tag`
ADD CONSTRAINT `fk_customer_campaign_tag_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_company`
--
ALTER TABLE `mw_customer_company`
ADD CONSTRAINT `fk_customer_company_company_type1` FOREIGN KEY (`type_id`) REFERENCES `mw_company_type` (`type_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_customer_company_country10` FOREIGN KEY (`country_id`) REFERENCES `mw_country` (`country_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_customer_company_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_customer_company_zone10` FOREIGN KEY (`zone_id`) REFERENCES `mw_zone` (`zone_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_email_blacklist`
--
ALTER TABLE `mw_customer_email_blacklist`
ADD CONSTRAINT `fk_customer_email_blacklist_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_email_template`
--
ALTER TABLE `mw_customer_email_template`
ADD CONSTRAINT `fk_customer_email_template_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_group_option`
--
ALTER TABLE `mw_customer_group_option`
ADD CONSTRAINT `fk_customer_group_option_customer_group1` FOREIGN KEY (`group_id`) REFERENCES `mw_customer_group` (`group_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_login_log`
--
ALTER TABLE `mw_customer_login_log`
ADD CONSTRAINT `fk_customer_login_log_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_customer_login_log_ip_location1` FOREIGN KEY (`location_id`) REFERENCES `mw_ip_location` (`location_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_message`
--
ALTER TABLE `mw_customer_message`
ADD CONSTRAINT `fk_customer_message_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_password_reset`
--
ALTER TABLE `mw_customer_password_reset`
ADD CONSTRAINT `fk_customer_password_reset_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_customer_quota_mark`
--
ALTER TABLE `mw_customer_quota_mark`
ADD CONSTRAINT `fk_customer_quota_mark_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_delivery_server`
--
ALTER TABLE `mw_delivery_server`
ADD CONSTRAINT `fk_delivery_server1` FOREIGN KEY (`bounce_server_id`) REFERENCES `mw_bounce_server` (`server_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_delivery_server_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_delivery_server_tracking_domain1` FOREIGN KEY (`tracking_domain_id`) REFERENCES `mw_tracking_domain` (`domain_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_delivery_server_domain_policy`
--
ALTER TABLE `mw_delivery_server_domain_policy`
ADD CONSTRAINT `fk_delivery_server_domain_policy_delivery_server1` FOREIGN KEY (`server_id`) REFERENCES `mw_delivery_server` (`server_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_delivery_server_to_customer_group`
--
ALTER TABLE `mw_delivery_server_to_customer_group`
ADD CONSTRAINT `fk_delivery_server_to_customer_group_customer_group1` FOREIGN KEY (`group_id`) REFERENCES `mw_customer_group` (`group_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_delivery_server_to_customer_group_delivery_server1` FOREIGN KEY (`server_id`) REFERENCES `mw_delivery_server` (`server_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_delivery_server_usage_log`
--
ALTER TABLE `mw_delivery_server_usage_log`
ADD CONSTRAINT `fk_delivery_server_usage_log_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_delivery_server_usage_log_delivery_server1` FOREIGN KEY (`server_id`) REFERENCES `mw_delivery_server` (`server_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_email_blacklist`
--
ALTER TABLE `mw_email_blacklist`
ADD CONSTRAINT `fk_email_blacklist1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_feedback_loop_server`
--
ALTER TABLE `mw_feedback_loop_server`
ADD CONSTRAINT `fk_feedback_loop_server_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list`
--
ALTER TABLE `mw_list`
ADD CONSTRAINT `fk_list_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_company`
--
ALTER TABLE `mw_list_company`
ADD CONSTRAINT `fk_customer_company_country100` FOREIGN KEY (`country_id`) REFERENCES `mw_country` (`country_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_customer_company_zone100` FOREIGN KEY (`zone_id`) REFERENCES `mw_zone` (`zone_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_company_company_type1` FOREIGN KEY (`type_id`) REFERENCES `mw_company_type` (`type_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_company_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_customer_notification`
--
ALTER TABLE `mw_list_customer_notification`
ADD CONSTRAINT `fk_list_notification_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_default`
--
ALTER TABLE `mw_list_default`
ADD CONSTRAINT `fk_list_default_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_field`
--
ALTER TABLE `mw_list_field`
ADD CONSTRAINT `fk_list_field_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_field_list_field_type1` FOREIGN KEY (`type_id`) REFERENCES `mw_list_field_type` (`type_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_field_option`
--
ALTER TABLE `mw_list_field_option`
ADD CONSTRAINT `fk_list_field_option_list_field1` FOREIGN KEY (`field_id`) REFERENCES `mw_list_field` (`field_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_field_value`
--
ALTER TABLE `mw_list_field_value`
ADD CONSTRAINT `fk_list_field_value_list_field1` FOREIGN KEY (`field_id`) REFERENCES `mw_list_field` (`field_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_field_value_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_page`
--
ALTER TABLE `mw_list_page`
ADD CONSTRAINT `fk_list_page_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_page_list_page_type1` FOREIGN KEY (`type_id`) REFERENCES `mw_list_page_type` (`type_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_segment`
--
ALTER TABLE `mw_list_segment`
ADD CONSTRAINT `fk_list_segment_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_segment_condition`
--
ALTER TABLE `mw_list_segment_condition`
ADD CONSTRAINT `fk_list_segment_condition_list_field1` FOREIGN KEY (`field_id`) REFERENCES `mw_list_field` (`field_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_segment_condition_list_segment1` FOREIGN KEY (`segment_id`) REFERENCES `mw_list_segment` (`segment_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_segment_condition_list_segment_operator1` FOREIGN KEY (`operator_id`) REFERENCES `mw_list_segment_operator` (`operator_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_subscriber`
--
ALTER TABLE `mw_list_subscriber`
ADD CONSTRAINT `fk_subscriber_list1` FOREIGN KEY (`list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_subscriber_action`
--
ALTER TABLE `mw_list_subscriber_action`
ADD CONSTRAINT `fk_list_subscriber_action_list1` FOREIGN KEY (`source_list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_subscriber_action_list2` FOREIGN KEY (`target_list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_subscriber_field_cache`
--
ALTER TABLE `mw_list_subscriber_field_cache`
ADD CONSTRAINT `fk_list_subscriber_field_cache_list_subscriber1` FOREIGN KEY (`subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_list_subscriber_list_move`
--
ALTER TABLE `mw_list_subscriber_list_move`
ADD CONSTRAINT `fk_list_subscriber_list_move_list1` FOREIGN KEY (`source_list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_subscriber_list_move_list2` FOREIGN KEY (`destination_list_id`) REFERENCES `mw_list` (`list_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_subscriber_list_move_list_subscriber1` FOREIGN KEY (`source_subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_list_subscriber_list_move_list_subscriber2` FOREIGN KEY (`destination_subscriber_id`) REFERENCES `mw_list_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_price_plan`
--
ALTER TABLE `mw_price_plan`
ADD CONSTRAINT `fk_price_plan_customer_group1` FOREIGN KEY (`group_id`) REFERENCES `mw_customer_group` (`group_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_price_plan_order`
--
ALTER TABLE `mw_price_plan_order`
ADD CONSTRAINT `fk_price_plan_order_currency1` FOREIGN KEY (`currency_id`) REFERENCES `mw_currency` (`currency_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_price_plan_order_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_price_plan_order_price_plan1` FOREIGN KEY (`plan_id`) REFERENCES `mw_price_plan` (`plan_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_price_plan_order_price_plan_promo_code1` FOREIGN KEY (`promo_code_id`) REFERENCES `mw_price_plan_promo_code` (`promo_code_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_price_plan_order_tax1` FOREIGN KEY (`tax_id`) REFERENCES `mw_tax` (`tax_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_price_plan_order_note`
--
ALTER TABLE `mw_price_plan_order_note`
ADD CONSTRAINT `fk_price_plan_order_note_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_price_plan_order_note_price_plan_order1` FOREIGN KEY (`order_id`) REFERENCES `mw_price_plan_order` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_price_plan_order_note_user1` FOREIGN KEY (`user_id`) REFERENCES `mw_user` (`user_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_price_plan_order_transaction`
--
ALTER TABLE `mw_price_plan_order_transaction`
ADD CONSTRAINT `fk_price_plan_order_transaction_price_plan_order1` FOREIGN KEY (`order_id`) REFERENCES `mw_price_plan_order` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_sending_domain`
--
ALTER TABLE `mw_sending_domain`
ADD CONSTRAINT `fk_sending_domain_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_tax`
--
ALTER TABLE `mw_tax`
ADD CONSTRAINT `fk_tax_country1` FOREIGN KEY (`country_id`) REFERENCES `mw_country` (`country_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_tax_zone1` FOREIGN KEY (`zone_id`) REFERENCES `mw_zone` (`zone_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_tour_slideshow_slide`
--
ALTER TABLE `mw_tour_slideshow_slide`
ADD CONSTRAINT `fk_tour_slideshow_slide_tour_slideshow1` FOREIGN KEY (`slideshow_id`) REFERENCES `mw_tour_slideshow` (`slideshow_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_tracking_domain`
--
ALTER TABLE `mw_tracking_domain`
ADD CONSTRAINT `fk_tracking_domain_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_transactional_email`
--
ALTER TABLE `mw_transactional_email`
ADD CONSTRAINT `fk_transactional_email_customer1` FOREIGN KEY (`customer_id`) REFERENCES `mw_customer` (`customer_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_transactional_email_log`
--
ALTER TABLE `mw_transactional_email_log`
ADD CONSTRAINT `fk_transactional_email_log_transactional_email1` FOREIGN KEY (`email_id`) REFERENCES `mw_transactional_email` (`email_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_user`
--
ALTER TABLE `mw_user`
ADD CONSTRAINT `fk_user_language1` FOREIGN KEY (`language_id`) REFERENCES `mw_language` (`language_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_user_group1` FOREIGN KEY (`group_id`) REFERENCES `mw_user_group` (`group_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `mw_user_auto_login_token`
--
ALTER TABLE `mw_user_auto_login_token`
ADD CONSTRAINT `fk_user_auto_login_token_user1` FOREIGN KEY (`user_id`) REFERENCES `mw_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_user_group_route_access`
--
ALTER TABLE `mw_user_group_route_access`
ADD CONSTRAINT `fk_user_group_route_access_user_group1` FOREIGN KEY (`group_id`) REFERENCES `mw_user_group` (`group_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_user_message`
--
ALTER TABLE `mw_user_message`
ADD CONSTRAINT `fk_user_message_user1` FOREIGN KEY (`user_id`) REFERENCES `mw_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_user_password_reset`
--
ALTER TABLE `mw_user_password_reset`
ADD CONSTRAINT `fk_user_password_reset_user1` FOREIGN KEY (`user_id`) REFERENCES `mw_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `mw_zone`
--
ALTER TABLE `mw_zone`
ADD CONSTRAINT `fk_zone_country1` FOREIGN KEY (`country_id`) REFERENCES `mw_country` (`country_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

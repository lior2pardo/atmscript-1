DROP TABLE IF EXISTS `esps`;
DROP TABLE IF EXISTS `leads`;
DROP TABLE IF EXISTS `drips`;

CREATE TABLE `esps` (
  `esp_name` VARCHAR(60) NOT NULL,
  `processed_leads` INT NOT NULL,
  `remaining_leads` INT NOT NULL,
  `drip_hourly_rate` INT NOT NULL,
  `drip_offset_ms` INT NOT NULL,
  `last_update` TIMESTAMP(6) NULL,
  `last_batch` INT NOT NULL,
  `last_seq` BIGINT NOT NULL,
  PRIMARY KEY (`esp_name`));

CREATE TABLE `leads` (
  `esp_name` VARCHAR(30) NOT NULL,
  `batch` INT NOT NULL,
  `seq` BIGINT NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `first_name` VARCHAR(255) NULL,
  `last_name` VARCHAR(255) NULL,
  PRIMARY KEY (`esp_name`, `batch`, `seq`));

CREATE TABLE `drips` (
  `timeframe` TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed` INT NOT NULL,
  PRIMARY KEY (`timeframe`));

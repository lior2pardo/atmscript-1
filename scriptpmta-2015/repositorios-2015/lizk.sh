#!/bin/bash
#
# /etc/init.d/lizk
# Subsystem file for "Lizk" server
#
# chkconfig: 2345 95 05
# description: Starts and stops the Lizk server
# processname: java
# pidfile: /var/run/lizk.pid
# logfile: /home/admin/log/lizk.log
#

NAME=`basename $0`
PIDFILE=/var/run/$NAME.pid
JARFILE=/home/admin/lizk.jar
USER=admin
GRACETIME=10
JAVAOPTS="-Djava.net.preferIPv4Stack=true -Dlog4j.configuration=file:/home/admin/log4j.properties"
JAVAARGS=

do_start() {

	if [ -f $PIDFILE ] 
	then
		pid=`cat $PIDFILE`
        res=`ps --pid $pid 2> /dev/null | grep -c $pid 2> /dev/null`
        if [ $res -eq '0' ]
        then
            rm -f $PIDFILE > /dev/null
        else
			echo "$NAME already running with PID $pid"
			exit 1;
        fi
	fi
	
	touch $PIDFILE
	chown $USER $PIDFILE
	
	# encoding might be broken otherwise
	export LANG=en_US.UTF-8
	
	pid=$(su -m -c "java -XX:OnOutOfMemoryError='kill -9 %p' $JAVAOPTS -jar $JARFILE $JAVAARGS > /dev/null & echo \$! " "$USER")
		
	echo "$pid" > $PIDFILE

	# wait for process to start
	sleep 4

	if [ `ps --pid $pid 2> /dev/null | grep -c $pid 2> /dev/null` -eq '0' ]; then
		echo "Process did not start!"
		rm -f $PIDFILE
		exit 1;
	fi 
	
	echo "Started with PID: $pid"
}

do_stop() {

	if [ -f $PIDFILE ] 
	then
		pid=`cat $PIDFILE`
		echo "Stopping $pid"
		
		kill -s TERM $pid > /dev/null
		rm -f $PIDFILE
		
		count=0;
		until [ `ps --pid $pid 2> /dev/null | grep -c $pid 2> /dev/null` -eq '0' ] || [ $count -gt $GRACETIME ]
    	do
      		sleep 1
      		let count=$count+1;
    	done

    	if [ $count -gt $GRACETIME ]; then
    		echo "Force stop of $NAME"
      		kill -9 $pid
    	fi
    	
    	echo "Stopped"
	fi
}

do_status() {
	
	if [ -f $PIDFILE ] 
    then
        pid=`cat $PIDFILE`
        res=`ps --pid $pid 2> /dev/null | grep -c $pid 2> /dev/null`
        if [ $res -eq '0' ]
        then
            rm -f $PIDFILE > /dev/null
            echo "$NAME is not running" 
            exit 1;
        else
            echo "$NAME is running with PID $pid" 
            exit 0;
        fi
    else
        echo "$NAME is not running" 
        exit 3;
    fi

}

do_signal() {

	if [ -f $PIDFILE ] 
	then
		pid=`cat $PIDFILE`
		echo "Signalling $pid"
		kill -s USR2 $pid > /dev/null
	fi
}


case "$1" in
start)  echo "Starting $NAME"
        do_start
        ;;
stop)  echo "Stopping $NAME"
        do_stop
        ;;
status)  
        do_status
        ;;
signal)  
        do_signal
        ;;
restart)
		do_stop
		do_start
		;;
*)      echo "Usage: service $NAME start|stop|restart|status|signal"
        exit 1
        ;;
esac
exit 0
echo "Updating Code ..."
DbServer=localhost
DbUser=root
DbPass=$7
DbName=mailwizz
mv email_hustler.zip /var/www
zip -r "mw-$(date +"%Y-%m-%d").zip" /var/www/mw
#zip -r mw.zip ./mw
chmod -R 777 /var/www/mw
unzip -o /var/www/email_hustler.zip -d /var/www/mw/
#unzip -o email_hustler.zip -d ./mw

mysql -uroot -p$DbPass $DbName < /var/www/mw/mw_payment.sql

if grep "php -q /var/www/mw/apps/console/console.php blacklist-email" /var/spool/cron/root; then echo "Blacklist Cron Already Exists"; else echo "00 01 * * * php -q /var/www/mw/apps/console/console.php blacklist-email" >> /var/spool/cron/root; fi

echo "Successfully updated."
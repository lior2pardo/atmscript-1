#!/bin/sh

DbServer=localhost
DbUser=root
DbPass=$7
DbName=mailwizz
accDbName=mailwizzcc
accDbUser=root
accDbPwd=$7

echo "Extracting campaigncreator..."
unzip -q campaignblaster.zip
chmod +x campaignblaster
#sh installcc.sh db.server db.uname db.pwd db.name

echo "Copying campaignblaster to www..."
yes | cp -R campaignblaster /var/www/


echo "Extracting ATMBulkServerImport..."
unzip -q ATMBulkServerImport.zip
chmod +x atmbulkserverimport

echo "Copying ATMBulkServerImport to www..."
yes | cp -R atmbulkserverimport /var/www/

echo "Extracting ATMCampaignCreatorSR..."
unzip -q ATMCampaignCreatorSR.zip
chmod +x campaigncreator

echo "Copying ATMCampaignCreatorSR to www..."
yes | cp -R campaigncreator /var/www/

#echo $DbServer
#echo $DbUser
#echo $DbPass
#echo $DbName

#if [ $# -lt 4 ]
#then
#    echo 'Incorrect number of parameters.'    
#    exit 1
#fi

#DbServer=$1
#DbUser=$2
#DbPass=$3
#DbName=$4

#campaignblaster
echo "Configuring campaignblaster..." 
sed -i -e "s#localhost#$DbServer#g" "/var/www/campaignblaster/database.php"
sed -i -e "s#root#$DbUser#g" "/var/www/campaignblaster/database.php"
sed -i -e "s#dbpass#$DbPass#g" "/var/www/campaignblaster/database.php"
sed -i -e "s#db_name#$DbName#g" "/var/www/campaignblaster/database.php"

echo "Configuring ATMBulkServerImport..."
sed -i -e "s#localhost#$DbServer#g" "/var/www/atmbulkserverimport/index.php"
sed -i -e "s#root#$DbUser#g" "/var/www/atmbulkserverimport/index.php"
sed -i -e "s#dbpass#$DbPass#g" "/var/www/atmbulkserverimport/index.php"
sed -i -e "s#DbName#$DbName#g" "/var/www/atmbulkserverimport/index.php"

#campaigncreator
echo "Configuring ATMCampaignCreatorSR..."

sed -i -e "s#acc_db_user#$accDbUser#g" "/var/www/campaigncreator/config/app.php"
sed -i -e "s#acc_db_pwd#$accDbPwd#g" "/var/www/campaigncreator/config/app.php"
sed -i -e "s#acc_db_name#$accDbName#g" "/var/www/campaigncreator/config/app.php"

mysql -u $DbUser --password=$DbPass -e "create database $accDbName;"
mysql -u $DbUser --password=$DbPass $accDbName < /var/www/campaigncreator/db/optifzia_mailer.sql

service httpd restart

echo "Setup is finished!!"
echo "URLs are (Replace domain-name with actual name):"
echo "domain-name/campaignblaster"
echo "domain-name/atmbulkserverimport"
echo "domain-name/campaigncreator"